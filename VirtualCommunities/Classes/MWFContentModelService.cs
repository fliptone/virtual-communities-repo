﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VirtualCommunities.Classes
{
    public class MWFContentModelService
    {
        private IMemoryCache _cache;
        private string _originalLocale;
        private string _locale;
        private static readonly string _cacheKey = "site-content-";
        private string _pathToContentFiles = Directory.GetCurrentDirectory() + @"/App_Data/";
        private TranslationModel[] _cachedTranslations;
        private PageContents _pageContents;

        const string translationsFilename = "translations.json";

        public void Init(IMemoryCache cache, string locale)
        {
            _cache = cache;
            _originalLocale = locale;
            switch (locale)
            {
                case "en-us":
                case "en-au":
                case "en-ca":
                case "en-gb":
                    _locale = "en-us";
                    break;
                default:
                    _locale = locale;
                    break;
            }

            if (!_cache.TryGetValue(_cacheKey + _locale, out _pageContents))
            {
                try
                {
                    // read the file into a string 
                    string path = _pathToContentFiles + "page-content-" + _locale + ".json";
                    if (!File.Exists(path)) { path = _pathToContentFiles + "page-content-en-us.json"; }
                    string file_text = File.ReadAllText(path);
                    _pageContents = JsonConvert.DeserializeObject<PageContents>(file_text);

                    // cache the results
                    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();
                    options.SetAbsoluteExpiration(DateTime.Now.AddHours(2));
                    _cache.Set(_cacheKey + _locale, _pageContents, options);
                }
                catch
                {
                    _pageContents = new PageContents();
                }
            }

            if (!_cache.TryGetValue("translations", out _cachedTranslations))
            {
                try
                {
                    string json = File.ReadAllText(_pathToContentFiles + translationsFilename);
                    _cachedTranslations = JsonConvert.DeserializeObject<TranslationModel[]>(json);

                    MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();
                    options.SetAbsoluteExpiration(DateTime.Now.AddHours(2));
                    _cache.Set("translations", _cachedTranslations, options);
                }
                catch
                {
                    _cachedTranslations = null;
                }
            }
        }

        public MWFContentModel GetContentByKey(string key)
        {
            MWFContentModel copiedContent = new MWFContentModel();
            if (_pageContents.Blades.ContainsKey(key))
            {
                MWFContentModel cachedContent = _pageContents.Blades[key];
                copiedContent.AltTag = cachedContent.AltTag;
                copiedContent.Badge = cachedContent.Badge;
                copiedContent.FriendlyName = cachedContent.FriendlyName;
                copiedContent.Heading = cachedContent.Heading;
                copiedContent.HeadingTag = cachedContent.HeadingTag;
                copiedContent.ImageUrl = cachedContent.ImageUrl;
                copiedContent.IsLoaded = true;
                copiedContent.Key = key;
                copiedContent.Link1Text = cachedContent.Link1Text;
                copiedContent.Link1Url = string.Format(cachedContent.Link1Url, _originalLocale);
                copiedContent.Link2Text = cachedContent.Link2Text;
                copiedContent.Link2Url = string.Format(cachedContent.Link2Url, _originalLocale);
                copiedContent.Paragraph = cachedContent.Paragraph;
                copiedContent.Subheading = cachedContent.Subheading;
            }
            return copiedContent;
        }

        internal string GetLocalizedString(string value)
        {
            if (_locale == "fr-ca")
            {
                if (_cachedTranslations != null)
                {
                    TranslationModel tm = _cachedTranslations.Where(t => t.en_us == value).FirstOrDefault();
                    return tm == null ? value : tm.fr_ca;
                }
                else
                {
                    return value;
                }
            }
            else
            {
                return value;
            }
        }


        //public string GeghtStaticContent(string key)
        //{
        //    string localizedKey;
        //    switch (_locale)
        //    {
        //        case "en-us":
        //        case "en-au":
        //        case "en-ca":
        //        case "en-gb":
        //            localizedKey = key + "-en-us";
        //            break;
        //        default:
        //            localizedKey = key + "-" + _locale;
        //            break;
        //    }

        //    //https://www.lifewire.com/html-codes-french-characters-4062211
        //    //é	&eacute; right
        //    //è	&egrave; left
        //    //ê	&ecirc; up
        //    Dictionary<string, string> staticContent = new Dictionary<string, string>
        //    {
        //        // BREADCRUMB
        //        { "breadcrumb-aria-label-en-us", "Your location in the site" },
        //        { "breadcrumb-aria-label-fr-ca", "Votre emplacement sur le site" },

        //        { "breadcrumb-home-label-en-us", "Home" },
        //        { "breadcrumb-home-label-fr-ca", "Accueil" },

        //        { "breadcrumb-site-label-en-us", "Virtual Communities" },
        //        { "breadcrumb-site-label-fr-ca", "Communaut&eacute;s virtuelles" },

        //        { "breadcrumb-events-label-en-us", "Events" },
        //        { "breadcrumb-events-label-fr-ca", "&Eacute;v&eacute;nements" },


        //        // TIMEZONE
        //        { "time-zone-label-en-us", "Your time zone" },
        //        { "time-zone-label-fr-ca", "Votre fuseau horaire" },


        //        // FILTER
        //        { "filter-header-label-en-us", "Filter your search" },
        //        { "filter-header-label-fr-ca", "Filtrer votre recherche" },

        //        { "filter-programs-label-en-us", "Workshop type" },
        //        { "filter-programs-label-fr-ca", "Type d'atelier" },

        //        { "filter-audiences-label-en-us", "Audience" },
        //        { "filter-audiences-label-fr-ca", "Public cible " },

        //        { "filter-topics-label-en-us", "Topic" },
        //        { "filter-topics-label-fr-ca", "Sujet" },

        //        { "filter-dates-label-en-us", "Date" },
        //        { "filter-dates-label-fr-ca", "Dates" },

        //        { "filter-apply-label-en-us", "Apply filters" },
        //        { "filter-apply-label-fr-ca", "Appliquer les filtres" },

        //        { "filter-results-label-en-us", "Results" },
        //        { "filter-results-label-fr-ca", "R&eacute;sultats" },

        //        { "filter-results-through-label-en-us", "-" },
        //        { "filter-results-through-label-fr-ca", "à" },

        //        { "filter-results-of-label-en-us", "of" },
        //        { "filter-results-of-label-fr-ca", "sur" },

        //        { "filter-applied-label-en-us", "Applied filters" },
        //        { "filter-applied-label-fr-ca", "Filtres appliqu&eacute;s" },

        //        { "filter-to-remove-label-en-us", "" },
        //        { "filter-to-remove-label-fr-ca", "Pour supprimer un filtre, s&eacute;lectionnez-le puis appuyez sur Entr&eacute;e ou barre d'espace." },

        //        { "filter-when-this-week-en-us", "This week" },
        //        { "filter-when-this-week-fr-ca", "Filtres appliqu&eacute;s" },



        //        // PAGINATION
        //        { "pagination-header-label-en-us", "Pagination for search results: {0} pages" },
        //        { "pagination-header-label-fr-ca", "Pagination pour les r&eacute;sultats de recherche: {0} pages" },

        //        { "pagination-items-per-page-label-en-us", "Items per page" },
        //        { "pagination-items-per-page-label-fr-ca", "&Eacute;l&eacute;ments par page" },



        //        // EVENT CARD
        //        { "event-card-workshops-label-en-us", "Workshop type" },
        //        { "event-card-workshops-label-fr-ca", "Type d'atelier" },

        //        { "event-card-audiences-label-en-us", "Audiences" },
        //        { "event-card-audiences-label-fr-ca", "Publics cibles" },

        //        { "event-card-topics-label-en-us", "Topics" },
        //        { "event-card-topics-label-fr-ca", "Sujets" },

        //        { "event-card-dates-label-en-us", "Dates" },
        //        { "event-card-dates-label-fr-ca", "Dates" },

        //        { "event-card-hosted-label-en-us", "Hosted by" },
        //        { "event-card-hosted-label-fr-ca", "Organis&eacute; par" },

        //        { "event-card-store-at-label-en-us", "The Microsoft Store at" },
        //        { "event-card-store-at-label-fr-ca", "Le Microsoft Store de" },

        //        { "event-card-read-label-en-us", "Read more" },
        //        { "event-card-read-label-fr-ca", "En savoir plus" },

        //        { "event-card-learn-label-en-us", "Learn more" },
        //        { "event-card-learn-label-fr-ca", "Pour en savoir plus" },

        //        { "event-card-register-label-en-us", "Register now" },
        //        { "event-card-register-label-fr-ca", "Inscrivez-vous dès maintenant" },

        //        { "event-card-remove-filter-label-en-us", "Remove filter" },
        //        { "event-card-remove-filter-label-fr-ca", "Supprimer le filtre" },


        //        // INDEX PAGE
        //        { "index-time-zone-label-en-us", "Upcoming events" },
        //        { "index-time-zone-label-fr-ca", "&Eacute;v&eacute;nements à venir" },


        //        // EVENTS PAGE
        //        { "events-find-label-en-us", "Find events" },
        //        { "events-find-label-fr-ca", "Trouver des &eacute;v&eacute;nements" },


        //        // SPOTLIGHT PAGE
        //        { "spotlight-find-label-en-us", "Find {0} events" },
        //        { "spotlight-find-label-fr-ca", "Trouver des &eacute;v&eacute;nements {0}" },


        //        //EVENT DETAIL PAGE
        //        { "event-detail-date-time-label-en-us", "Date/Time" },
        //        { "event-detail-date-time-label-fr-ca", "Date/heure" },

        //        { "event-detail-details-header-label-en-us", "Event details" },
        //        { "event-detail-details-header-label-fr-ca", "D&eacute;tails de l'&eacute;v&eacute;nement" },

        //        { "event-detail-related-label-en-us", "Related events" },
        //        { "event-detail-related-label-fr-ca", "&Eacute;v&eacute;nements connexes" },

        //    };

        //    if (!staticContent.TryGetValue(localizedKey, out string value))
        //    {
        //        value = localizedKey;
        //    }
        //    return value;
        //}
    }
}