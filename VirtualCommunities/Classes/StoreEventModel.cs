﻿using System;
using System.Globalization;
using TimeZoneConverter;

namespace VirtualCommunities.Classes
{
    public class StoreEventModel
    {
        public string eventGuid { get; set; }
        public string eventId { get; set; }
        public string FormattedEventId { get { return eventId.ToLower().Replace("_", "-"); } }
        public string name { get; set; }
        public string description { get; set; }
        public string timeZoneName { get; set; }
        public string Programs { get; set; } = "";
        public string[] audience { get; set; }
        public string[] topics { get; set; }
        public string retailLocation { get; set; }
        public string registrationUrl { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string CityStateCountryString
        {
            get
            {
                string result = "";
                if (!string.IsNullOrEmpty(city))
                {
                    result = city.Trim() + ", ";
                }
                if (!string.IsNullOrEmpty(state))
                {
                    result += state.Trim();
                }
                if (!string.IsNullOrEmpty(country))
                {
                    result = result + " " + country;
                }
                result = result.Trim();
                if (result.Length == 0) { result = "Microsoft Store"; }
                return result;
            }
        }

        public string startDate { get; set; }
        public DateTime OriginStartDate
        {
            get
            {
                return DateTime.Parse(startDate).ToUniversalTime();
            }
        }
        //public string OriginStartDateString { get { return OriginStartDate.ToString(this.DateFormatString); } }
        //public string OriginStartTimeString { get { return OriginStartDate.ToString("h:mm tt"); } }

        public string endDate { get; set; }
        public double Duration
        {
            get
            {
                DateTime originEndDate = DateTime.Parse(endDate).ToUniversalTime();
                return originEndDate.Subtract(OriginStartDate).TotalMinutes;
            }
        }
        public string OriginEndTimeString { get { return OriginStartDate.AddMinutes(Duration).ToString("h:mm tt"); } }

        public string LocalTimeZone { get; set; }
        public DateTime LocalStartDate { get; set; }
        public string LocalStartDateString { get { return LocalStartDate.ToString(this.DateFormatString); } }
        public string LocalStartTimeString { get { return LocalStartDate.ToString("h:mm tt"); } }
        public string LocalEndTimeString { get { return LocalStartDate.AddMinutes(Duration).ToString("h:mm tt"); } }


        public string TopicsList { get; set; }
        public string EventImage { get; set; }
        public string SearchString { get; set; }

        public string DateFormatString { get; set; } = "dddd, MMMM d, yyyy";


        public int? maximumCapacity { get; set; }
        public int? registrationCount { get; set; }
        public bool HasAvailability
        {
            get
            {
                bool result = true;
                if (maximumCapacity.HasValue)
                {
                    if (registrationCount.HasValue)
                    {
                        result = maximumCapacity.Value > registrationCount.Value;
                    }
                }
                return result;
            }
        }



        /* public string detailsUrl { get; set; }
        public int publishStatus { get; set; }
        public int segmentReporting { get; set; }
        public bool isPromotedEvent { get; set; }
        public bool isVirtualEvent { get; set; }
        public bool isWaitlisted { get; set; }
        public bool autoRegister { get; set; }
        public bool allSessionsMandatory { get; set; }
        public string postalCode { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string termsAndConditions { get; set; }
        public string[] assignedTo { get; set; }
        public int sessionCount { get; set; }
        public string employeeNotes { get; set; }
        public string eventSpace { get; set; }
        public string businessUnitId { get; set; } */

        public string ToHtmlString(EventCardLabelModel eclm, string locale)
        {
            return @"<div class=""d-flex col-12 col-md-4"" style=""margin-bottom: 40px;"">
                    <div class=""card material-surface mb-g mb-md-0 p-4 white-drop"">
                        <div class=""card-body mb-4"">
                            <div id=""" + eventGuid + @""" class=""card-body__inner"">
                                <img style=""float:right; padding: 0 0 0 10px;"" height=""50"" src=""" + EventImage + @""" />
                                <h3 class="" "">" + name + @"</h3>
                                <p class=""mb-0"">" + LocalStartDateString + @"<br />" +
                                LocalStartTimeString + "&nbsp;-&nbsp;" + LocalEndTimeString + " (" + LocalTimeZone + @")</p>
                            </div>
                            <div class=""mt-3"">
                                <p class=""mb-0""><strong>" + eclm.topicsLabel + @":</strong> " + TopicsList + @"</p>
                            </div>
                            <div class=""card-footer"">
                                <div class=""link-group"">
                                    <a href=""/" + locale + "/store/workshops-training-and-events/" + FormattedEventId + @""" class=""cta"" aria-label=""" + eclm.learnLabel + @""">" + eclm.learnLabel + @"</a>
                                    <a href=""" + registrationUrl + @""" class=""cta"" aria-label=""" + eclm.registerLabel + @""">" + eclm.registerLabel + @"</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
        }
    }
}
