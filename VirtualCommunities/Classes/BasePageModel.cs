﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;

namespace VirtualCommunities.Classes
{
    public class BasePageModel : PageModel
    {
        internal IMemoryCache _cache;
        internal MWFContentModelService _contentModelService;
        internal MWFContentModel _metadata;

        public EventCardLabelModel eclm { get; set; }
        public FilterContentModel fcm { get; set; }
        public PaginationContentModel pcm { get; set; }
        public TimeZoneModel tzm { get; set; }
        public string locale { get; set; }
        public string dateFormatString { get; set; }

        internal BasePageModel(IMemoryCache cache)
        {
            _cache = cache;
        }

        internal RedirectResult BaseOnGet(string locale, string metadataKey)
        {
            // ensure we are only loading valid locales
            string validLocales = "en-us,en-gb,en-au,en-ca,fr-ca";
            if (string.IsNullOrEmpty(locale))
            {
                return Redirect("/en-us/store/workshops-training-and-events/");
            }
            else if (!validLocales.Contains(locale))
            {
                return Redirect("/en-us/store/workshops-training-and-events/");
            }
            else
            {
                // load the UHF for the requested locale
                ShellModel shell = ShellModelService.Load(_cache, locale);
                ViewData["ShellCssIncludes"] = shell.CssIncludes;
                ViewData["ShellHeaderHtml"] = shell.HeaderHtml;
                ViewData["ShellFooterHtml"] = shell.FooterHtml;
                ViewData["ShellJavaScriptIncludes"] = shell.JavaScriptIncludes;

                // store the locale for use by the view
                this.locale = locale;
                ViewData["locale"] = locale;
                switch (locale)
                {
                    case "en-gb":
                    case "en-au":
                    case "fr-ca":
                        dateFormatString = "dddd, d MMMM yyyy";
                        break;
                    default:
                        dateFormatString = "dddd, MMMM d, yyyy";
                        break;
                }

                // get a reference to the content model service
                _contentModelService = new MWFContentModelService();
                _contentModelService.Init(_cache, locale);
                ViewData["content-model-service"] = _contentModelService;

                // get the page metadata content for the locale
                _metadata = _contentModelService.GetContentByKey(metadataKey);
                ViewData["meta-title"] = _metadata.Heading;
                ViewData["meta-description"] = _metadata.Paragraph;
                ViewData["meta-canonical"] = _metadata.Link1Url;
                ViewData["meta-keywords"] = _metadata.Subheading; // overloading this for keywords
                ViewData["meta-image-alt"] = _metadata.AltTag;

                // load event card content based on locale
                eclm = new EventCardLabelModel();
                eclm.Load(_contentModelService);

                // load the filter and pagination
                fcm = new FilterContentModel();
                fcm.LoadLabelsForLocale(_contentModelService);
                pcm = new PaginationContentModel();
                pcm.LoadLabelsForLocale(_contentModelService);
                tzm = new TimeZoneModel();
                tzm.LoadLabelsForLocale(_contentModelService);

                ViewData["LocalizedStrings"] = @"<!-- localized strings -->
<script>
    var topicsLabel = """ + eclm.topicsLabel + @""";
    var datesLabel = """ + eclm.datesLabel + @"""
    var hostedLabel = """ + eclm.hostedLabel + @""";
    var storeAtLabel = """ + eclm.storeAtLabel + @""";
    var learnLabel = """ + eclm.learnLabel + @""";
    var registerLabel = """ + eclm.registerLabel + @""";
    var removeFilterLabel = """ + eclm.removeFilterLabel + @""";
    var locale = """ + locale + @""";
</script>";
            }
            return null;
        }
    }
}
