﻿using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace VirtualCommunities.Classes
{
    public class ShellModelService
    {
        private static readonly string _serviceEndpoint = "http://uhf.microsoft.com";
        private static readonly string _partnerId = "MSConsumer"; //"RetailStore2";
        private static readonly string _headerId = "MSConsumerSkipToMainHeader"; //"RetailStore2Header";
        private static readonly string _footerId = "MSConsumerFooter"; //"RetailStore2Footer";
        private static readonly string _userAgent = "Microsoft";

        private static readonly string _cacheKey = "Microsoft-UHF-";

        public static ShellModel Load(IMemoryCache cache, string locale)
        {
            if (!cache.TryGetValue(_cacheKey + locale, out ShellModel shell))
            {
                shell = Load(locale);
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();
                options.SetAbsoluteExpiration(DateTime.Now.AddHours(24));
                cache.Set(_cacheKey + locale, shell, options);
            }
            return shell;
        }

        private static ShellModel Load(string locale)
        {
            string pathToContentFiles = Directory.GetCurrentDirectory() + @"/App_Data/";
            string shelFilename = "msft-shell-" + locale + ".xml";
            try
            {
                string xml = string.Empty;
                using (var client = new WebClient())
                {
                    var serviceUrl = _serviceEndpoint +
                        "/" + locale + "/shell/xml/" + _partnerId +
                        "?headerId=" + _headerId +
                        "&footerId=" + _footerId; // + "&CookieComplianceEnabled=true";
                    client.Headers.Add("user-agent", _userAgent);
                    client.Encoding = Encoding.UTF8;
                    xml = client.DownloadString(serviceUrl);
                    File.WriteAllText(pathToContentFiles + shelFilename, xml);
                }
                return ConvertXmlToModel(xml, locale);
            }
            catch (Exception)
            {
                string xml = File.ReadAllText(pathToContentFiles + shelFilename);
                return ConvertXmlToModel(xml, locale);
            }
        }

        private static ShellModel ConvertXmlToModel(string xml, string locale)
        {
            var document = XDocument.Parse(xml);
            var root = document.Element("shell");

            // show or hide the language selector
            string headerHtml = EnsureStringValue(root, "headerHtml").Value;
            if (!ShowLanguageSelector(locale))
            {
                headerHtml = headerHtml.Replace("id=\"uhf-l-nav\"", "id=\"uhf-l-nav\" style=\"display: none;\"");
            }

            return new ShellModel
            {
                CssIncludes = EnsureStringValue(root, "cssIncludes"),
                JavaScriptIncludes = EnsureStringValue(root, "javascriptIncludes"),
                FooterHtml = EnsureStringValue(root, "footerHtml"),
                HeaderHtml = new HtmlString(headerHtml),
            };
        }

        private static bool ShowLanguageSelector(string locale)
        {
            switch (locale)
            {
                case "en-ca":
                case "fr-ca":
                    return true;
                default:
                    return false;
            }
        }

        private static HtmlString EnsureStringValue(XElement root, string elementName)
        {
            var element = root.Element(elementName);
            return new HtmlString(element != null ? (string)element : string.Empty);
        }
    }
}
