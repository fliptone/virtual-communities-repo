﻿using Microsoft.AspNetCore.Html;

namespace VirtualCommunities.Classes
{
    public class ShellModel
    {
        public HtmlString CssIncludes { get; set; }
        public HtmlString JavaScriptIncludes { get; set; }
        public HtmlString HeaderHtml { get; set; }
        public HtmlString FooterHtml { get; set; }
    }
}
