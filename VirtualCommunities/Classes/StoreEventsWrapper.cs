﻿namespace VirtualCommunities.Classes
{
    public class StoreEventsWrapper
    {
        public StoreEventModel[] Events { get; set; }
        //public  DropdownFilterModel[] Audiences { get; set; }
        public DropdownFilterModel[] Topics { get; set; }
        public DropdownFilterModel[] Whens { get; set; }
        public int TotalRecords { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int CurrentFirst { get; set; }
        public int CurrentLast { get; set; }
        //public DropdownFilterModel[] Programs { get; internal set; }
    }
}
