﻿
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace VirtualCommunities.Classes
{
    public class EventsCache
    {
        // Shared auth URL
        const string authUrl = "https://login.microsoftonline.com/72f988bf-86f1-41af-91ab-2d7cd011db47";

        ////PPE
        //const string apiEnvironment = "api://d504f5a5-3265-45f7-9a98-49a426494f23"; // (ppe)
        //const string eventsUrl = "https://storeevents-ppe.azurewebsites.net/events?PageSize=5000&StartDate={0}&PublishStatus=100000003";
        //const string topicsUrl = "https://storeevents-ppe.azurewebsites.net/events/topics";

        //PROD
        const string apiEnvironment = "api://558b6d52-8889-4b14-b52d-74c2a40525b5"; // (prod)
        const string eventsUrl = "https://storeevents.azurewebsites.net/v1.0/events?PageSize=50&StartDate={0}&PublishStatus=100000003&isPromotedEvent=true&isVirtualEvent=true";
        const string topicsUrl = "https://storeevents.azurewebsites.net/v1.0/events/topics";

        const string _topicsCacheKey = "topics-cache"; 
        const string _eventsCacheKey = "events-cache";
        const string _cacheIsValidKey = "cache-is-valid";
        const int _cacheTimeoutMinutes = 60;

        private string _pathToContentFiles = Directory.GetCurrentDirectory() + @"/App_Data/";
        private IMemoryCache _cache;
        private ILogger<PageModel> _logger;

        internal StoreEventModel GetEventById(string id)
        {
            throw new NotImplementedException();
        }

        // topics endpoint
        private StoreMetadataModel[] _cachedTopics;
        public StoreMetadataModel[] Topics
        {
            get
            {
                return _cachedTopics;
            }
        }

        // events endpoint
        private StoreEventModel[] _cachedEvents;
        public StoreEventModel[] Events
        {
            get
            {
                return _cachedEvents;
            }
        }

        // time zone offset data endpoint
        //private Dictionary<string, DateTime> _cachedTimeZoneOffsets;
        //public Dictionary<string, DateTime> TimeZoneOffsetData
        //{
        //    get
        //    {
        //        return _cachedTimeZoneOffsets;
        //    }
        //}

        public void Init(IMemoryCache cache, ILogger<PageModel> logger)
        {
            _cache = cache;
            _logger = logger;

            _cache.TryGetValue(_cacheIsValidKey, out bool cacheIsValid);
            Debug.WriteLine("----------");
            Debug.WriteLine("cache is valid: " + cacheIsValid);

            LoadTopics(cacheIsValid);
            LoadEvents(cacheIsValid);

            //LoadTimeZoneOffsets();
            if (!cacheIsValid)
            {
                // when the cache times out, store the time zone offsets (including any new ones)
                //WriteTimeZoneOffsets();

                // reset the cache timeout
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();
                options.SetAbsoluteExpiration(DateTime.Now.AddMinutes(_cacheTimeoutMinutes));
                _cache.Set(_cacheIsValidKey, true, options);
            }

            if (_cachedTopics == null)
            {
                _logger.LogError("EventsCache.Init() _cachedTopics was empty");
                _cachedTopics = new StoreMetadataModel[0];
            }
            if (_cachedEvents == null)
            {
                _logger.LogError("EventsCache.Init() _cachedEvents was empty");
                _cachedEvents = new StoreEventModel[0];
            }
            //if (_cachedTimeZoneOffsets == null)
            //{
            //    _logger.LogError("EventsCache.Init() _cachedTimeZoneOffsets was empty");
            //    _cachedTimeZoneOffsets = new Dictionary<string, DateTime>();
            //}

            //Debug.WriteLine("API request complete");
        }

        public DateTime GetLocalTimeTZI(DateTime utcTime, string localTimeZone)
        {
            TimeZoneInfo tzInfo;
            try
            {
                tzInfo = TimeZoneInfo.FindSystemTimeZoneById(localTimeZone);
            }
            catch (Exception ex)
            {
                _logger.Log(Microsoft.Extensions.Logging.LogLevel.Error, ex, localTimeZone);
                tzInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            }
            var local = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzInfo);
            return new DateTime(local.Year, local.Month, local.Day, local.Hour, local.Minute, local.Second, DateTimeKind.Unspecified);
        }

        public string GetTopicLabel(string key)
        {
            string value = key.ToString();
            StoreMetadataModel smm = _cachedTopics.Where(t => t.value == int.Parse(key)).FirstOrDefault();
            if (smm != null)
            {
                value = smm.label;
            }
            return value;
        }

        private void LoadTopics(bool cacheIsValid)
        {
            if (!_cache.TryGetValue(_topicsCacheKey, out _cachedTopics) || !cacheIsValid)
            {
                try
                {
                    // get an authenticated client
                    HttpClient client = GetAuthorizedClient();

                    // make the topics API call
                    var response = client.GetAsync(topicsUrl).Result;
                    string json = response.Content.ReadAsStringAsync().Result;
                    _cachedTopics = JsonConvert.DeserializeObject<StoreMetadataModel[]>(json);

                    // store latest set to disk as a backup
                    File.WriteAllText(_pathToContentFiles + _topicsCacheKey + ".json", json);
                }
                catch (Exception ex)
                {
                    // if there was an error, load the last backup from disk
                    _logger.LogError(ex, "EventsCache.LoadTopics");
                    string json = File.ReadAllText(_pathToContentFiles + _topicsCacheKey + ".json");
                    _cachedTopics = JsonConvert.DeserializeObject<StoreMetadataModel[]>(json);
                }

                // write the topics to the in-memory cache
                MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();
                options.SetAbsoluteExpiration(DateTime.Now.AddMinutes(_cacheTimeoutMinutes * 2));
                _cache.Set(_topicsCacheKey, _cachedTopics, options);
                Debug.WriteLine("topics loaded");
            }
        }

        private async void LoadEvents(bool cacheIsValid)
        {
            MemoryCacheEntryOptions options = new MemoryCacheEntryOptions();

            if (!_cache.TryGetValue(_eventsCacheKey, out _cachedEvents) || !cacheIsValid)
            {
                if (_cache.TryGetValue("events-are-loading", out bool eventsAreLoading))
                {
                    Debug.WriteLine("WAIT: events are loading...");
                }
                else
                {
                    Debug.WriteLine("loading events ...");
                    try
                    {
                        options.SetAbsoluteExpiration(DateTime.Now.AddSeconds(30));
                        _cache.Set("events-are-loading", true, options);

                        // get an authenticated client
                        HttpClient client = GetAuthorizedClient();

                        // make the events API call
                        string eventsUrlWithDate = string.Format(eventsUrl, DateTime.Today.ToString("M-d-yyyy"));

                        List<StoreEventModel> eventsList = new List<StoreEventModel>();
                        while(eventsUrlWithDate != null)
                        {
                            var response = await client.GetAsync(eventsUrlWithDate);
                            string paginationJson = response.Headers.GetValues("X-Pagination").FirstOrDefault();
                            PaginationData pd = JsonConvert.DeserializeObject<PaginationData>(paginationJson);
                            string pageJson = response.Content.ReadAsStringAsync().Result;
                            StoreEventModel[] thisPageOfEvents = JsonConvert.DeserializeObject<StoreEventModel[]>(pageJson);
                            foreach (StoreEventModel sem in thisPageOfEvents)
                            {
                                eventsList.Add(sem);
                            }
                            eventsUrlWithDate = pd.NextPage;
                        }

                        // store latest set to disk as a backup
                        _cachedEvents = eventsList.ToArray();
                        string json = JsonConvert.SerializeObject(_cachedEvents);
                        File.WriteAllText(_pathToContentFiles + _eventsCacheKey + ".json", json);
                    }
                    catch (Exception ex)
                    {
                        // if there was an error, load the last backup from disk
                        _logger.LogError(ex, "EventsCache.LoadEvents");
                        string json = File.ReadAllText(_pathToContentFiles + _eventsCacheKey + ".json");
                        _cachedEvents = JsonConvert.DeserializeObject<StoreEventModel[]>(json);
                    }

                    // trim the set to those events that still have openings
                    _cachedEvents = _cachedEvents.Where(t => t.OriginStartDate >= DateTime.Now && t.HasAvailability).OrderBy(t => t.OriginStartDate).ToArray();

                    // loop through the events, and set some static data
                    for (int i = 0; i < _cachedEvents.Length; i++)
                    {
                        StoreEventModel e = _cachedEvents[i];

                        // set the "program" values based on audience
                        e.Programs = "";
                        for (int j = 0; j < e.audience.Length; j++)
                        {
                            string programKey = GetProgramByAudience(e.audience[j]);
                            if (!e.Programs.Contains(programKey)) { e.Programs = e.Programs + programKey + ","; } // intentional no space after comma
                        }

                        // set the "program" values based on topic
                        for (int j = 0; j < e.topics.Length; j++)
                        {
                            string programKey = GetProgramByTopic(e.topics[j]);
                            if (!e.Programs.Contains(programKey)) { e.Programs = e.Programs + programKey + ","; } // intentional no space after comma
                        }

                        // clean the description, set the image, and build the search string
                        e.description = CleanDescription(e.description);
                        e.EventImage = GetEventImage(e);
                        e.SearchString = " " + e.name.ToLower() + " " + StripHTML(e.description).ToLower() + " ";
                    }

                    // write the events to the in-memory cache
                    options.SetAbsoluteExpiration(DateTime.Now.AddMinutes(_cacheTimeoutMinutes * 2));
                    _cache.Set(_eventsCacheKey, _cachedEvents, options);
                    Debug.WriteLine("events loaded");
                }
            }
        }

        private HttpClient GetAuthorizedClient()
        {
            // retrieve access token
            string client_id = KeyVault.GetSecret("events_client_id");
            string client_secret = KeyVault.GetSecret("events_client_secret");
            AuthenticationContext context = new AuthenticationContext(authUrl);
            string accessToken = context.AcquireTokenAsync(apiEnvironment, new ClientCredential(client_id, client_secret)).Result?.AccessToken;

            HttpClient client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(10);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            return client;
        }

        private string GetProgramByTopic(string topic)
        {
            string programKey;
            switch (topic)
            {
                case "10000073":
                case "10000003":
                    programKey = "204"; // Gamers
                    break;
                case "10000014":
                    programKey = "301"; // LinkedIn
                    break;
                case "10000053":
                    programKey = "302"; // Summer Camp
                    break;
                case "10000079":
                    programKey = "304"; // Surface Duo
                    break;
                default:
                    programKey = "0";
                    break;
            }
            return programKey;
        }

        private string GetProgramByAudience(string audience)
        {
            string programKey;
            switch (audience)
            {
                case "10000004":
                case "10000005":
                    programKey = "201"; // Students
                    break;
                case "10000016":
                case "10000014":
                case "10000015":
                case "10000013":
                case "10000017":
                case "10000002":
                case "10000003":
                    programKey = "202"; // Teachers
                    break;
                case "10000010":
                case "10000012":
                case "10000011":
                case "10000006":
                    programKey = "203"; // Professionals
                    break;
                case "10000007":
                case "10000018":
                    programKey = "204"; // Gamers
                    break;
                default:
                    programKey = "0";
                    break;
            }
            return programKey;
        }

        private string GetEventImage(StoreEventModel e)
        {
            string imageName = "topics/microsoft-logo.png";
            for (int i = 0; i < e.topics.Length; i++)
            {
                string thisTopic = e.topics[i];

                if (thisTopic.Contains("10000049") || thisTopic.Contains("10000042")) // STEM & Coding
                {
                    imageName = "digital-passport-badge-binary.png";
                    break;
                }
                else if (thisTopic.Contains("10000073") || thisTopic.Contains("10000003")) // Gaming & Gaming League
                {
                    imageName = "digital-passport-badge-control.png";
                    break;
                }
                else if (thisTopic.Contains("10000041") || thisTopic.Contains("10000059")) // Creative & Creativity
                {
                    imageName = "digital-passport-badge-lightbulb.png";
                    break;
                }
                else if (thisTopic.Contains("10000076")) // Community
                {
                    imageName = "digital-passport-badge-red.png";
                    break;
                }
                else if (thisTopic.Contains("10000074")) // Museum
                {
                    imageName = "digital-passport-badge-museum.png";
                    break;
                }
                else if (thisTopic.Contains("10000075")) // Around the world
                {
                    imageName = "digital-passport-badge-rocket.png";
                    break;
                }
                else if (thisTopic.Contains("10000014")) // LinkedIn
                {
                    imageName = "topics/linkedin-logo.png";
                    break;
                }
                else if (thisTopic.Contains("10000037")) // Office 365
                {
                    imageName = "topics/office365-logo.jpg";
                    break;
                }
                else if (thisTopic.Contains("10000004")) // Minecraft
                {
                    imageName = "topics/minecraft-logo.png";
                    break;
                }
            }
            return "/en-us/store/workshops-training-and-events/images/" + imageName;
        }

        private string CleanDescription(string description)
        {
            string result = "";
            if (!string.IsNullOrEmpty(description))
            {
                result = description;
                result = result.Replace("font-size:11px", "");
                result = result.Replace("font-size: 11px", "");
                result = result.Replace("font-size:11pt", "");
                result = result.Replace("font-size: 11pt", "");
                result = result.Replace("font-size:12px", "");
                result = result.Replace("font-size: 12px", "");
                result = result.Replace("font-size:14px", "");
                result = result.Replace("font-size: 14px", "");
                result = result.Replace("Calibri", "");
                result = result.Replace("font-family:Arial,Helvetica,sans-serif", "");
                result = result.Replace("font-family:Tahoma,Geneva,sans-serif", "");
                result = result.Replace("font-family:Segoe UI,Frutiger,Helvetica Neue,Arial,sans-serif", "");
                result = result.Replace("SCXW107707257", "");
                result = result.Replace("color:#1a1a1a", "");
                result = result.Replace("background-color: rgb(255, 255, 255)", "");
                result = result.Replace("background:white", "");
                result = result.Replace("color:#595959", "");
                result = result.Replace("color:#0563c1", "");
                result = result.Replace("line-height: 105%", "");
                result = result.Replace("line-height:105%", "");
            }
            return result;
        }

        private string StripHTML(string str)
        {
            string result = str.Replace("&nbsp;", " ");
            result = result.Replace("&amp;", " and ");
            result = result.Replace("-", " ");
            result = result.Replace("\n", " ");
            result = Regex.Replace(result, @"<(.|\n)*?>", string.Empty);
            result = Regex.Replace(result, @"[^\w\s]", "");
            while (result.Contains("  "))
            {
                result = result.Replace("  ", " ");
            }
            return result;
        }

    }
}
