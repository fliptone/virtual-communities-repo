﻿using System.Collections.Generic;

namespace VirtualCommunities.Classes
{
    public class PageContents
    {
        public Dictionary<string, MWFContentModel> Blades { get; set; }
    }

    public class MWFContentModel
    {
        public string Key { get; set; }
        public bool IsLoaded { get; set; } = false;
        public string Badge { get; set; } = "badge";
        public string ImageUrl { get; set; } = "image url";
        public string AltTag { get; set; } = "alt tag";
        public string HeadingTag { get; set; } = "h1";
        public string Heading { get; set; } = "header";
        public string Subheading { get; set; } = "subheading";
        public string FriendlyName { get; set; } = "friendly name";
        public string Paragraph { get; set; } = "paragraph";
        public string Link1Text { get; set; } = "link1 text";
        public string Link1Url { get; set; } = "link1 url";
        public string Link2Text { get; set; } = "link2 text";
        public string Link2Url { get; set; } = "link2 url";
    }


}