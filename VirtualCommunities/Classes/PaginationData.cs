﻿namespace VirtualCommunities.Classes
{
    public class PaginationData
    {
        public int Count { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public object PreviousPage { get; set; }
        public string NextPage { get; set; }
    }
}
