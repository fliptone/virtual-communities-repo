﻿namespace VirtualCommunities.Classes
{
    public class DropdownFilterModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public string DisplayValue { get { return Value + " (" + Count + "<span class=\"sr-only\"> items</span>)"; } }

        public DropdownFilterModel(string key, string value, int count)
        {
            this.Key = key;
            this.Value = value;
            this.Count = count;
        }
    }
}
