﻿namespace VirtualCommunities.Classes
{
    public class EventCardLabelModel
    {
        public string topicsLabel { get; set; }
        public string datesLabel { get; set; }
        public string hostedLabel { get; set; }
        public string storeAtLabel { get; set; }
        public string learnLabel { get; set; }
        public string registerLabel { get; set; }
        public string removeFilterLabel { get; set; }
        public string relatedEventsLabel { get; set; }

        public void Load(MWFContentModelService contentModelService)
        {
            topicsLabel = contentModelService.GetLocalizedString("Topics");
            datesLabel = contentModelService.GetLocalizedString("Dates");
            hostedLabel = contentModelService.GetLocalizedString("Hosted by");
            storeAtLabel = contentModelService.GetLocalizedString("The Microsoft Store at");
            learnLabel = contentModelService.GetLocalizedString("Learn more");
            registerLabel = contentModelService.GetLocalizedString("Register now");
            removeFilterLabel = contentModelService.GetLocalizedString("Remove filter");
            relatedEventsLabel = contentModelService.GetLocalizedString("Related events");
        }
    }
}
