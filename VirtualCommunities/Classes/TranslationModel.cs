﻿namespace VirtualCommunities.Classes
{
    public class TranslationModel
    {
        public string en_us { get; set; }
        public string fr_ca { get; set; }
    }
}