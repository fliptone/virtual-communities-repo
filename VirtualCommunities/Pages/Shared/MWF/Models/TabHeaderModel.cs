﻿namespace VirtualCommunities.Classes
{
    public class TabHeaderModel
    {
        public string[] TabNames { get; set; } = new string[5];
        public string Previous { get; set; }
        public string Next { get; set; }
    }
}
