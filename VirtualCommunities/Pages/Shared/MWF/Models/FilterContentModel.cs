﻿namespace VirtualCommunities.Classes
{
    public class FilterContentModel
    {
        public string TopicsLabel { get; set; }
        public string DatesLabel { get; set; }
        public string ApplyFiltersLabel { get; set; }
        public string ResultsLabel { get; set; }
        public string ResultsThroughLabel { get; set; }
        public string ResultsOfLabel { get; set; }
        public string AppliedFiltersLabel { get; set; }
        public string ToRemoveFilterLabel { get; set; }

        public string BackToTabsLabel { get; set; }
        public void LoadLabelsForLocale(MWFContentModelService cp)
        {
            TopicsLabel = cp.GetLocalizedString("Topic");
            DatesLabel = cp.GetLocalizedString("Date");
            ApplyFiltersLabel = cp.GetLocalizedString("Apply filters");
            ResultsLabel = cp.GetLocalizedString("Results");
            ResultsThroughLabel = cp.GetLocalizedString("-");
            ResultsOfLabel = cp.GetLocalizedString("of");
            AppliedFiltersLabel = cp.GetLocalizedString("Applied filters");
            ToRemoveFilterLabel = cp.GetLocalizedString("To remove a filter, select it then press enter or spacebar.");
            BackToTabsLabel = cp.GetLocalizedString("Back to tabs");
        }
    }
}
