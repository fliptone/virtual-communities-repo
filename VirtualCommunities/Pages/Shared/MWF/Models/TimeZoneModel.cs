﻿using System;

namespace VirtualCommunities.Classes
{
    public class TimeZoneModel
    {
        public string HeaderLabel { get; set; }
        public string DropdownLabel { get; set; }
        public void LoadLabelsForLocale(MWFContentModelService cp)
        {
            HeaderLabel = cp.GetLocalizedString("Find the event for you");
            DropdownLabel = cp.GetLocalizedString("Your time zone");
        }
    }
}
