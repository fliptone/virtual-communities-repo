﻿namespace VirtualCommunities.Classes
{
    public class PaginationContentModel
    {
        public string ItemsPerPage { get; set; }

        public void LoadLabelsForLocale(MWFContentModelService cp)
        {
            ItemsPerPage = cp.GetLocalizedString("Items per page");
        }
    }
}
