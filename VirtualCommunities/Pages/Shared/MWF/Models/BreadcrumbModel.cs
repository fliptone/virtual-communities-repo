﻿using System.Collections.Generic;

namespace VirtualCommunities.Classes
{
    public class BreadcrumbModel
    {
        public string AriaIntroLabel { get; set; } = "Your location in the site";
        public KeyValuePair<string, string>[] Levels { get; set; }

        public BreadcrumbModel()
        {
            this.Levels = new KeyValuePair<string, string>[4];
        }
    }
}
