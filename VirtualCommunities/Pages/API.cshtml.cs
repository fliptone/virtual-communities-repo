using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using TimeZoneConverter;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class APIModel : PageModel
    {
        public string Json { get; set; } = "{}";

        private readonly ILogger<PageModel> _logger;
        private IMemoryCache _cache;
        private long milliseconds = 0;

        public APIModel(ILogger<PageModel> logger, IMemoryCache cache)
        {
            _logger = logger;
            _cache = cache;
        }

        public void OnGet(string locale)
        {
            //Microsoft.ApplicationInsights.Extensibility.Implementation.TelemetryDebugWriter.IsTracingDisabled = true;
            //EventsCache ec = new EventsCache();
            //ec.Init(_cache, _logger);

            //DateTime today = DateTime.Today;
            //today = new DateTime(today.Year, today.Month, today.Day, 13, 30, 0, DateTimeKind.Utc);
            //ReadOnlyCollection<TimeZoneInfo> systemTimeZones = TimeZoneInfo.GetSystemTimeZones();

            //List<TimeZoneInfo> timeZones = new List<TimeZoneInfo>();
            //foreach (var timezone in systemTimeZones)
            //{
            //    try
            //    {
            //        TimeZoneInfo localTZI = TimeZoneInfo.FindSystemTimeZoneById(timezone.StandardName);
            //        string iana = TZConvert.WindowsToIana(timezone.StandardName);
            //        DateTime api = ec.GetLocalTimeTZI(today, iana);
            //        if (api != DateTime.MinValue)
            //        {
            //            if(timezone.StandardName != "Kamchatka Standard Time")
            //            {
            //                timeZones.Add(localTZI);
            //            }
            //        }
            //        else
            //        {
            //            //Debug.WriteLine("failed IANA: " + iana);
            //        }
            //    }
            //    catch
            //    {
            //        //Debug.WriteLine("failed TimeZoneInfo: " + timezone.StandardName);
            //    }
            //}

            //for (int i = 92; i < 180; i++)
            //{
            //    DateTime utc = today.AddDays(i);
            //    Debug.WriteLine("----" + utc.ToString("MM/dd/yyyy"));
            //    foreach (var timezone in timeZones)
            //    {
            //        string iana = TZConvert.WindowsToIana(timezone.StandardName);
            //        DateTime api = ec.GetLocalTimeTZI(utc, iana);
            //        if (api != DateTime.MinValue)
            //        {
            //            DateTime tzi = ec.GetLocalTimeTZI(utc, timezone.StandardName);
            //            if (api != tzi)
            //            {
            //                Debug.WriteLine("MISMATCH: {0} - UTC={1} / API={2} / TZI={3}", timezone.StandardName, utc.ToString("MM/dd/yyyy hh:mm tt"), api.ToString("MM/dd/yyyy hh:mm tt"), tzi.ToString("MM/dd/yyyy hh:mm tt"));
            //            }
            //        }
            //        else
            //        {
            //            Debug.WriteLine("FAILED: {0} - UTC={1} / API={2}", timezone.StandardName, utc.ToString("MM/dd/yyyy hh:mm tt"), api.ToString("MM/dd/yyyy hh:mm tt"));
            //        }
            //    }
            //}

            //WriteTimestamp("API start");
            StoreEventsWrapper eventWrapper = new StoreEventsWrapper();
            eventWrapper.Events = new StoreEventModel[0];

            // get query params
            string program = HttpContext.Request.Query["program"];
            string topic = HttpContext.Request.Query["topic"];
            string when = HttpContext.Request.Query["when"];
            string keyword = HttpContext.Request.Query["keyword"];

            // get page size for results
            int.TryParse(HttpContext.Request.Query["size"], out int pageSize);
            if (pageSize == 0) { pageSize = 24; }

            // get the current page
            int.TryParse(HttpContext.Request.Query["page"], out int currentPage);
            if (currentPage == 0) { currentPage = 1; }
            eventWrapper.CurrentPage = currentPage;

            // calculate the time windows for the search
            DateTime today = DateTime.Today;
            DateTime week1Start = today;
            DateTime week2Start = today.AddDays(7 - (int)today.DayOfWeek);
            DateTime week3Start = week2Start.AddDays(7);
            DateTime week4Start = week3Start.AddDays(7);
            DateTime week5Start = week4Start.AddDays(7);
            DateTime week6Start = week5Start.AddDays(7);
            DateTime week7Start = week6Start.AddDays(7);
            DateTime week8Start = week7Start.AddDays(7);
            DateTime week9Start = week8Start.AddDays(7);
            DateTime week10Start = week9Start.AddDays(7);

            // get the appropriate date format
            string dateFormatString = "dddd, MMMM d, yyyy";
            if (locale == "en-gb" || locale == "en-au" || locale == "fr-ca") { dateFormatString = "dddd, d MMMM yyyy"; }

            EventsCache ec = new EventsCache();
            ec.Init(_cache, _logger);
            StoreEventModel[] results = ec.Events;

            if (results.Length > 0)
            {
                // if we received a program param, filter by it
                if (!string.IsNullOrEmpty(program))
                {
                    results = results.Where(t => t.Programs.Contains(program)).ToArray();
                }

                // if we received a topic param, filter by it
                if (!string.IsNullOrEmpty(topic))
                {
                    results = results.Where(t => t.topics.Contains(topic)).ToArray();
                }

                // if we received a date window param, filter by it
                if (!string.IsNullOrEmpty(when))
                {
                    DateTime whenStart = DateTime.Parse(when);
                    DateTime whenEnd = whenStart.AddDays(7 - (int)whenStart.DayOfWeek);
                    results = results.Where(t => t.OriginStartDate >= whenStart).Where(t => t.OriginStartDate < whenEnd).ToArray();
                }

                // if we received a keyword param, filter by it
                if (!string.IsNullOrEmpty(keyword))
                {
                    string term = " " + StripHTML(WebUtility.UrlDecode(keyword).ToLower()) + " ";
                    results = results.Where(t => t.SearchString.Contains(term)).ToArray();
                }

                if (results.Length > 0)
                {
                    // record the total number of matched records
                    eventWrapper.TotalRecords = results.Length;
                    double pageCount = (double)eventWrapper.TotalRecords / (double)pageSize;
                    eventWrapper.TotalPages = (int)Math.Ceiling(pageCount);

                    // calculate the pagination info
                    eventWrapper.CurrentFirst = (eventWrapper.CurrentPage - 1) * pageSize + 1;
                    eventWrapper.CurrentLast = eventWrapper.CurrentFirst + pageSize - 1;
                    if (eventWrapper.CurrentLast > eventWrapper.TotalRecords) { eventWrapper.CurrentLast = eventWrapper.TotalRecords; }
                }

                // create the filter counts
                int week1Count = 0;
                int week2Count = 0;
                int week3Count = 0;
                int week4Count = 0;
                int week5Count = 0;
                int week6Count = 0;
                int week7Count = 0;
                int week8Count = 0;
                int week9Count = 0;
                int week10Count = 0;
                Dictionary<string, DropdownFilterModel> topicsDictionary = new Dictionary<string, DropdownFilterModel>();
                for (int i = 0; i < results.Length; i++)
                {
                    StoreEventModel e = results[i];

                    // create topic dropdown
                    if (string.IsNullOrEmpty(topic))
                    {
                        for (int j = 0; j < e.topics.Length; j++)
                        {
                            string key = e.topics[j];
                            if (!topicsDictionary.ContainsKey(key))
                            {
                                topicsDictionary.Add(key, new DropdownFilterModel(key, GetTopicLabel(ec.Topics, key), 1));
                            }
                            else
                            {
                                topicsDictionary[key].Count++;
                            }
                        }
                    }

                    // 'when' counts
                    week1Count += e.OriginStartDate >= week1Start && e.OriginStartDate < week2Start ? 1 : 0;
                    week2Count += e.OriginStartDate >= week2Start && e.OriginStartDate < week3Start ? 1 : 0;
                    week3Count += e.OriginStartDate >= week3Start && e.OriginStartDate < week4Start ? 1 : 0;
                    week4Count += e.OriginStartDate >= week4Start && e.OriginStartDate < week5Start ? 1 : 0;
                    week5Count += e.OriginStartDate >= week5Start && e.OriginStartDate < week6Start ? 1 : 0;
                    week6Count += e.OriginStartDate >= week6Start && e.OriginStartDate < week7Start ? 1 : 0;
                    week7Count += e.OriginStartDate >= week7Start && e.OriginStartDate < week8Start ? 1 : 0;
                    week8Count += e.OriginStartDate >= week8Start && e.OriginStartDate < week9Start ? 1 : 0;
                    week9Count += e.OriginStartDate >= week9Start && e.OriginStartDate < week10Start ? 1 : 0;
                    week10Count += e.OriginStartDate >= week10Start ? 1 : 0;
                }

                // if we received a topic param, show only that one item
                if (!string.IsNullOrEmpty(topic))
                {
                    topicsDictionary.Add(topic, new DropdownFilterModel(topic, GetTopicLabel(ec.Topics, topic), results.Length));
                }
                eventWrapper.Topics = topicsDictionary.Values.OrderBy(t => t.DisplayValue).ToArray();

                // add the date windows 
                eventWrapper.Whens = new DropdownFilterModel[10];
                eventWrapper.Whens[0] = new DropdownFilterModel(week1Start.ToString("MM-dd-yyyy"), "This week", week1Count);
                eventWrapper.Whens[1] = new DropdownFilterModel(week2Start.ToString("MM-dd-yyyy"), "Next week", week2Count);
                eventWrapper.Whens[2] = new DropdownFilterModel(week3Start.ToString("MM-dd-yyyy"), "Week of " + week3Start.ToString("MMMM d"), week3Count);
                eventWrapper.Whens[3] = new DropdownFilterModel(week4Start.ToString("MM-dd-yyyy"), "Week of " + week4Start.ToString("MMMM d"), week4Count);
                eventWrapper.Whens[4] = new DropdownFilterModel(week5Start.ToString("MM-dd-yyyy"), "Week of " + week5Start.ToString("MMMM d"), week5Count);
                eventWrapper.Whens[5] = new DropdownFilterModel(week6Start.ToString("MM-dd-yyyy"), "Week of " + week6Start.ToString("MMMM d"), week6Count);
                eventWrapper.Whens[6] = new DropdownFilterModel(week7Start.ToString("MM-dd-yyyy"), "Week of " + week7Start.ToString("MMMM d"), week7Count);
                eventWrapper.Whens[7] = new DropdownFilterModel(week8Start.ToString("MM-dd-yyyy"), "Week of " + week8Start.ToString("MMMM d"), week8Count);
                eventWrapper.Whens[8] = new DropdownFilterModel(week9Start.ToString("MM-dd-yyyy"), "Week of " + week9Start.ToString("MMMM d"), week9Count);
                eventWrapper.Whens[9] = new DropdownFilterModel(week10Start.ToString("MM-dd-yyyy"), "Later", week10Count);

                // trim the set to the current page and size
                results = results.Skip(eventWrapper.CurrentFirst - 1).Take(pageSize).ToArray();

                // make a copy of the items for display in the local time
                StoreEventModel[] localizedEvents = new StoreEventModel[results.Length];
                //string localTimeZone = Request.Cookies["store-communities-local-timezone"] ?? "America/Los_Angeles";
                string localTimeZoneAlt = Request.Cookies["store-communities-local-timezone-alt"] ?? "Pacific Standard Time";
                for (int i = 0; i < results.Length; i++)
                {
                    StoreEventModel sem = CopyEvent(results[i]);
                    sem.DateFormatString = dateFormatString;
                    sem.LocalTimeZone = localTimeZoneAlt;
                    sem.LocalStartDate = ec.GetLocalTimeTZI(sem.OriginStartDate, localTimeZoneAlt);
                    localizedEvents[i] = sem;
                    //Debug.WriteLine("{0}\t{1}\t{2}", sem.FormattedEventId, sem.LocalStartDate, localTimeZoneAlt);

                    sem.TopicsList = "";
                    for (int j = 0; j < sem.topics.Length; j++)
                    {
                        sem.TopicsList = sem.TopicsList + ec.GetTopicLabel(sem.topics[j]) + ", "; // intentional space after comma
                    }
                    if (sem.TopicsList.Length > 2) { sem.TopicsList = sem.TopicsList[0..^2]; }
                }

                // write the events to the output object
                eventWrapper.Events = localizedEvents;
            }

            // put the results into the wrapper and serialize
            this.Json = JsonConvert.SerializeObject(eventWrapper);
        }

        private string GetTopicLabel(StoreMetadataModel[] topics, string topic)
        {
            string value = topic.ToString();
            StoreMetadataModel smm = topics.Where(t => t.value == int.Parse(topic)).FirstOrDefault();
            if (smm != null)
            {
                value = smm.label;
            }
            return value;
        }

        private StoreEventModel CopyEvent(StoreEventModel cachedEvent)
        {
            StoreEventModel copiedEvent = new StoreEventModel();
            copiedEvent.audience = cachedEvent.audience;
            copiedEvent.city = cachedEvent.city;
            copiedEvent.country = cachedEvent.country;
            copiedEvent.description = cachedEvent.description;
            copiedEvent.endDate = cachedEvent.endDate;
            copiedEvent.eventId = cachedEvent.eventId;
            copiedEvent.eventGuid = cachedEvent.eventGuid;
            copiedEvent.name = cachedEvent.name;
            copiedEvent.Programs = cachedEvent.Programs;
            copiedEvent.registrationUrl = cachedEvent.registrationUrl;
            copiedEvent.retailLocation = cachedEvent.retailLocation;
            copiedEvent.startDate = cachedEvent.startDate;
            copiedEvent.state = cachedEvent.state;
            copiedEvent.timeZoneName = cachedEvent.timeZoneName;
            copiedEvent.topics = cachedEvent.topics;
            copiedEvent.TopicsList = cachedEvent.TopicsList;
            copiedEvent.EventImage = cachedEvent.EventImage;
            copiedEvent.SearchString = cachedEvent.SearchString;
            return copiedEvent;
        }

        private string StripHTML(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
        }

        private void WriteTimestamp(string message)
        {
            long m = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            Debug.WriteLine("{0} = {1}", message, m - milliseconds);
            milliseconds = m;
        }

    }
}