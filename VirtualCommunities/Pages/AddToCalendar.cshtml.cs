﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Text;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class AddToCalendarModel : PageModel
    {
        private readonly ILogger<PageModel> _logger;
        private IMemoryCache _cache;

        public AddToCalendarModel(ILogger<PageModel> logger, IMemoryCache cache)
        {
            _logger = logger;
            _cache = cache;
        }

        public ActionResult OnGet(string locale, string id)
        {
            //20200528T210000Z
            //May 28, 2020 from 2:00 PM to 3:00 PM
            string styleString = "font-size:12px; font-family:Segoe UI,Frutiger,Helvetica Neue,Arial,sans-serif; background:white; color:black;";

            //TODO fix this hard-coded en-us
            //Microsoft Store Event	= &Eacute;v&eacute;nement du Microsoft Store
            // Register today = Inscrivez-vous d&egrave;s aujourd'hui

            // ************ do not indent this text!
            string contents = @"BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT
DTSTART:{2:yyyyMMddThhmm}
DTEND:{3:yyyyMMddThhmm}
SUMMARY:{4}
LOCATION:{5}
X-ALT-DESC;FMTTYPE=text/html:Microsoft Store Event<br/><br/>{1}<br/>{4}<br/><p style=""{0}"">{6}</p><p style=""{0}"">Register today.</p>
PRIORITY:3
END:VEVENT
END:VCALENDAR
";
            // ************ do not indent this text!

            EventsCache ec = new EventsCache();
            ec.Init(_cache, _logger);
            StoreEventModel e = ec.GetEventById(id);
            if (e != null)
            {
                //string localTimeZone = Request.Cookies["store-communities-local-timezone"] ?? "America/Los_Angeles";
                string localTimeZoneAlt = Request.Cookies["store-communities-local-timezone-alt"] ?? "Pacific Standard Time";
                if (!string.IsNullOrEmpty(localTimeZoneAlt))
                {
                    e.LocalStartDate = ec.GetLocalTimeTZI(e.OriginStartDate, localTimeZoneAlt);
                    contents = string.Format(contents, styleString, "", e.LocalStartDate, e.LocalStartDate.AddMinutes(e.Duration),
                            e.name, e.retailLocation, e.description);
                    var byteArray = Encoding.ASCII.GetBytes(contents);
                    var stream = new MemoryStream(byteArray);
                    return File(stream, "text/plain", String.Format("{0}.ics", e.eventId));
                }
            }
            return RedirectToAction("Error");
        }
    }
}