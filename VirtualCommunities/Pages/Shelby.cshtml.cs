﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class ShelbyModel : BasePageModel
    {
        public BreadcrumbModel breadcrumb { get; set; }

        private readonly ILogger<PageModel> _logger;
        public ShelbyModel(ILogger<PageModel> logger, IMemoryCache cache) : base(cache)
        {
            _logger = logger;
        }
        public ActionResult OnGet(string locale)
        {
            string pageName = "shelbycounty-schools";
            RedirectResult baseOnGetResult = base.BaseOnGet(locale, pageName + "-metadata");
            if (baseOnGetResult != null) { return baseOnGetResult; }

            // load breadcrumb content based on locale
            breadcrumb = new BreadcrumbModel();
            string homeLabel = _contentModelService.GetLocalizedString("Home");
            breadcrumb.Levels[0] = new KeyValuePair<string, string>("https://www.microsoft.com/" + locale, homeLabel);
            string siteLabel = _contentModelService.GetLocalizedString("Virtual Communities");
            breadcrumb.Levels[1] = new KeyValuePair<string, string>("/" + locale + "/store/workshops-training-and-events/", siteLabel);
            breadcrumb.Levels[2] = new KeyValuePair<string, string>("#", _metadata.FriendlyName);

            tzm.HeaderLabel = "Find workshops and training for you";
            ViewData["noindex"] = "<meta name=\"robots\" content=\"noindex, nofollow\">";

            return null;
        }
    }
}