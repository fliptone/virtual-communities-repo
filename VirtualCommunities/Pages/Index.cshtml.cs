﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class IndexModel : BasePageModel
    {
        public BreadcrumbModel breadcrumb { get; set; }
        public TabHeaderModel tab_headers { get; set; }
        public MWFContentModel hero { get; set; }

        private readonly ILogger<PageModel> _logger;

        public IndexModel(ILogger<PageModel> logger, IMemoryCache cache) : base(cache)
        {
            _logger = logger;
        }

        public ActionResult OnGet(string locale)
        {
            RedirectResult baseOnGetResult = base.BaseOnGet(locale, "index-metadata");
            if (baseOnGetResult != null) { return baseOnGetResult; }

            // load breadcrumb content based on locale
            breadcrumb = new BreadcrumbModel();
            string homeLabel = _contentModelService.GetLocalizedString("Home");
            breadcrumb.Levels[0] = new KeyValuePair<string, string>("https://www.microsoft.com/" + locale, homeLabel);
            string siteLabel = _contentModelService.GetLocalizedString("Virtual Communities");
            breadcrumb.Levels[1] = new KeyValuePair<string, string>("#", siteLabel);

            // load tab header content based on locale
            tab_headers = new TabHeaderModel();
            tab_headers.TabNames[0] = _contentModelService.GetLocalizedString("All");
            tab_headers.TabNames[1] = _contentModelService.GetLocalizedString("Students");
            tab_headers.TabNames[2] = _contentModelService.GetLocalizedString("Teachers");
            tab_headers.TabNames[3] = _contentModelService.GetLocalizedString("Professionals");
            tab_headers.TabNames[4] = _contentModelService.GetLocalizedString("Gamers");
            tab_headers.Previous = _contentModelService.GetLocalizedString("Previous");
            tab_headers.Next = _contentModelService.GetLocalizedString("Next");

            // get the page content
            hero = _contentModelService.GetContentByKey("index-multi");

            return null;
        }
    }
}