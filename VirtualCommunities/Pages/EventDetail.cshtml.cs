﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class EventDetailModel : BasePageModel
    {
        public BreadcrumbModel breadcrumb { get; set; }
        public StoreEventModel Event { get; set; }
        public StoreEventModel[] RelatedEvents { get; set; }
        public string dateTimeLabel { get; set; }

        private readonly ILogger<PageModel> _logger;

        public EventDetailModel(ILogger<PageModel> logger, IMemoryCache cache) : base(cache)
        {
            _logger = logger;
        }

        public ActionResult OnGet(string locale, string id)
        {
            RedirectResult baseOnGetResult = base.BaseOnGet(locale, "event-detail-metadata");
            if (baseOnGetResult != null) { return baseOnGetResult; }

            //string localTimeZone = Request.Cookies["store-communities-local-timezone"] ?? "America/Los_Angeles";
            string localTimeZoneAlt = Request.Cookies["store-communities-local-timezone-alt"] ?? "Pacific Standard Time";

            EventsCache ec = new EventsCache();
            ec.Init(_cache, _logger);
            StoreEventModel sem = ec.Events.Where(t => t.FormattedEventId == id).FirstOrDefault();
            if (sem == null)
            {
                return Redirect("/Error");
            }
            this.Event = PrepareEventData(ec, sem, localTimeZoneAlt);

            // load breadcrumb content based on locale
            breadcrumb = new BreadcrumbModel();
            string homeLabel = _contentModelService.GetLocalizedString("Home");
            breadcrumb.Levels[0] = new KeyValuePair<string, string>("https://www.microsoft.com/" + locale, homeLabel);
            string siteLabel = _contentModelService.GetLocalizedString("Virtual Communities");
            breadcrumb.Levels[1] = new KeyValuePair<string, string>("/" + locale + "/store/workshops-training-and-events/", siteLabel);
            breadcrumb.Levels[2] = new KeyValuePair<string, string>("#", this.Event.name);

            // page content
            ViewData["meta-title"] = string.Format(ViewData["meta-title"].ToString(), this.Event.name);

            // load any page-specific static page content based on locale
            dateTimeLabel = _contentModelService.GetLocalizedString("Date/Time");
            tzm.HeaderLabel = _contentModelService.GetLocalizedString("Event details");

            // load three related events
            RelatedEvents = new StoreEventModel[3];
            string[] topicIds = new string[] { "10000037", "10000052", "10000042" };
            StoreEventModel[] threeEvents = ec.Events.Take(3).ToArray();
            for (int i = 0; i < threeEvents.Length; i++)
            {
                if (threeEvents[i] != null)
                {
                    this.RelatedEvents[i] = PrepareEventData(ec, threeEvents[i], localTimeZoneAlt);
                }
            }

            return null;
        }

        private StoreEventModel PrepareEventData(EventsCache ec, StoreEventModel thisEvent, string localTimeZoneAlt)
        {
            StoreEventModel copiedEvent = new StoreEventModel
            {
                city = thisEvent.city,
                country = thisEvent.country,
                description = thisEvent.description,
                endDate = thisEvent.endDate,
                eventId = thisEvent.eventId,
                eventGuid = thisEvent.eventGuid,
                name = thisEvent.name,
                Programs = thisEvent.Programs,
                registrationUrl = thisEvent.registrationUrl,
                retailLocation = thisEvent.retailLocation,
                startDate = thisEvent.startDate,
                state = thisEvent.state,
                timeZoneName = thisEvent.timeZoneName,
                topics = thisEvent.topics,
                TopicsList = thisEvent.TopicsList,
                EventImage = thisEvent.EventImage,
                DateFormatString = dateFormatString,
                LocalTimeZone = localTimeZoneAlt
            };
            copiedEvent.LocalStartDate = ec.GetLocalTimeTZI(copiedEvent.OriginStartDate, localTimeZoneAlt);

            copiedEvent.TopicsList = "";
            for (int j = 0; j < copiedEvent.topics.Length; j++)
            {
                copiedEvent.TopicsList = copiedEvent.TopicsList + ec.GetTopicLabel(copiedEvent.topics[j]) + ", "; // intentional space after comma
            }
            if (copiedEvent.TopicsList.Length > 2) { copiedEvent.TopicsList = copiedEvent.TopicsList[0..^2]; }

            return copiedEvent;
        }
    }
}