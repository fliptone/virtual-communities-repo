using Microsoft.AspNetCore.Mvc.RazorPages;

namespace VirtualCommunities.Pages
{
    public class CodeCheckModel : PageModel
    {
        public string Json { get; set; } = "{}";

        public void OnGet()
        {
            string code = HttpContext.Request.Query["code"];
            if (code == "lasso")
            {
                this.Json = "{ \"url\": \"https://forms.office.com/Pages/ResponsePage.aspx?id=v4j5cvGGr0GRqy180BHbRxKuuH8O5eJLgsWdRZybuFZUN0ZaUVFLRDNSSlFDS0tVUlNBQjZHT0NLTC4u\" }";
            }
            else
            {
                this.Json = "{ \"url\": \"\" }";
            }
        }
    }
}