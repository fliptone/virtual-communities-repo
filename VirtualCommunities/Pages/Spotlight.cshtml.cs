﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using VirtualCommunities.Classes;

namespace VirtualCommunities.Pages
{
    public class SpotlightModel : BasePageModel
    {
        public BreadcrumbModel breadcrumb { get; set; }
        public MWFContentModel hero { get; set; }
        public MWFContentModel quick_links { get; set; }
        public string spotlightId { get; set; }
        public string pageName { get; set; }

        public bool ShowWonderWomanBlade
        {
            get
            {
                bool result = Request.Query.ContainsKey("ww");
                if (locale == "en-us")
                {
                    if (pageName == "digital-passport")
                    {
                        if (DateTime.Now >= new DateTime(2020, 8, 10, 7, 0, 0)) // 7AM is midnight PST
                        {
                            if (DateTime.Now < new DateTime(2020, 8, 24, 7, 0, 0)) // 7AM is midnight PST
                            {
                                result = true;
                            }
                        }
                    }
                }
                return result;
            }
        }

        private readonly ILogger<PageModel> _logger;

        public SpotlightModel(ILogger<PageModel> logger, IMemoryCache cache) : base(cache)
        {
            _logger = logger;
        }
        public ActionResult OnGet(string locale)
        {
            // grab the page name off the end of the url
            pageName = HttpContext.Request.Path.Value;
            pageName = pageName.Substring(pageName.LastIndexOf("/") + 1);

            RedirectResult baseOnGetResult = base.BaseOnGet(locale, pageName + "-metadata");
            if (baseOnGetResult != null) { return baseOnGetResult; }

            // load breadcrumb content based on locale
            breadcrumb = new BreadcrumbModel();
            string homeLabel = _contentModelService.GetLocalizedString("Home");
            breadcrumb.Levels[0] = new KeyValuePair<string, string>("https://www.microsoft.com/" + locale, homeLabel);
            string siteLabel = _contentModelService.GetLocalizedString("Virtual Communities");
            breadcrumb.Levels[1] = new KeyValuePair<string, string>("/" + locale + "/store/workshops-training-and-events/", siteLabel);
            breadcrumb.Levels[2] = new KeyValuePair<string, string>("#", _metadata.FriendlyName);

            // get the page content
            hero = _contentModelService.GetContentByKey(pageName + "-hero");
            quick_links = _contentModelService.GetContentByKey(pageName + "-quick-links");

            // load any page-specific static page content based on locale
            spotlightId = _metadata.Badge; // overloading this param to hold the spotlight id
            if(spotlightId == "304")
            {
                tzm.HeaderLabel = string.Format(_contentModelService.GetLocalizedString("Find a live demo session"), _metadata.FriendlyName);
            }
            else
            {
                tzm.HeaderLabel = string.Format(_contentModelService.GetLocalizedString("Find {0} events"), _metadata.FriendlyName);
            }

            return null;
        }
    }
}