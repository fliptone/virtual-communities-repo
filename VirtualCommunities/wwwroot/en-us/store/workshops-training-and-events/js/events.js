﻿var currentPage = 1;
var spotlight = "";
var program = "";
var topic = "";
var when = "";
var keyword = "";

$(document).ready(function () {
    $(".apply-filter-button").on("click", function (e) {
        $(".dropdown-toggle").attr("aria-expanded", "false");
        $(".dropdown-toggle").removeClass("active");
        $(".dropdown-menu").removeClass("show");
        currentPage = 1;
        updateHash();
        refreshEvents();
        e.preventDefault();
    });

    $("#keyword-search-button").on("click", function (e) {
        addKeyword();
    });

    $("#keyword-search").keypress(function (evt) {
        if (evt.which == 13) {
            addKeyword();
        }
    });
});

function refreshEvents() {

    currentPage = getHashParam("page");
    program = getHashParam("program");
    topic = getHashParam("topic");
    when = getHashParam("when");
    keyword = getHashParam("keyword");
    //var spotlight = getHashParam("spotlight");
    if (spotlight.length > 0) { program = spotlight; }
    console.log("refreshEvents: " + currentPage + "/" + program + "/" + topic + "/" + when + "/" + keyword);

    // clear out the UI from the last results
    $("#events-grid").empty();
    $('<div style="width: 100%;"><img style="margin-left: auto; margin-right: auto; display: block;" src="/en-us/store/workshops-training-and-events/images/loading.gif" /></div>').appendTo("#events-grid");
    $("#results-data").css("display", "none");
    $("#results-empty").css("display", "none");
    $("#pagination").empty();
    $(".applied-filter").remove();
    $("#applied-filters-row").css("display", "none");

    // select the right tab
    $(".tab-group-item").removeClass("active");
    $(".tab-group-item[data-value='" + program + "']").addClass("active");

    // call the API service
    var pageSize = $("#page-size").val();
    var apiStart = Date.now();
    var jqxhr = $.getJSON("/" + locale + "/store/workshops-training-and-events/api?program=" + encodeURI(program) + "&topic=" + encodeURI(topic) + "&when=" + encodeURI(when) + "&keyword=" + encodeURI(keyword) + "&size=" + pageSize + "&page=" + currentPage, function () {

        $("#events-grid").empty();
        console.log("success");
        var data = jqxhr.responseJSON;

        for (var i = 0; i < data.Events.length; i++) {

            // render each card
            var storeEvent = data.Events[i];
            var eventHtml = '<div class="d-flex col-12 col-md-4" style="margin-bottom: 40px;">' +
                '<div class="card material-surface mb-g mb-md-0 p-4 white-drop">' +
                '<div class="card-body mb-4">' +
                '<div id="' + storeEvent.eventGuid + '" class="card-body__inner">' +
                '<img style="float:right; padding: 0 0 0 10px;" height="50" src="' + storeEvent.EventImage + '" />' +
                '<h3 class=" ">' + storeEvent.name + '</h3>' +
                '<p class="mb-0">' + storeEvent.LocalStartDateString + '<br />' +
                '' + storeEvent.LocalStartTimeString + '&nbsp;-&nbsp;' + storeEvent.LocalEndTimeString + ' (' + storeEvent.LocalTimeZone + ')</p>' +
                '</div>' +
                '<div class="mt-3">' +
                '<p class="mb-0"><strong>' + topicsLabel + ':</strong> ' + storeEvent.TopicsList + '</p>' +
                '</div>' +
                '<div class="card-footer">' +
                '<div class="link-group">' +
                '<a href="/' + locale + '/store/workshops-training-and-events/' + storeEvent.FormattedEventId + '" class="cta" aria-label="' + learnLabel + '">' + learnLabel + '</a>' +
                '<a href="' + storeEvent.registrationUrl + '" class="cta" aria-label="' + registerLabel + '">' + registerLabel + '</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            $(eventHtml).appendTo("#events-grid");
        }
        //'<p class="mb-0">' + storeEvent.OriginStartDateString + '<br />' +
        //'' + storeEvent.OriginStartTimeString + '&nbsp;-&nbsp;' + storeEvent.OriginEndTimeString + ' (' + storeEvent.OriginTimeZone + ')</p>' +

        // set the pagination parameters
        $("#current-first").html(data.CurrentFirst);
        $("#current-last").html(data.CurrentLast);
        $("#total-results").html(data.TotalRecords);

        if (data.TotalRecords > 0) {

            // show the results counts in the UI
            $("#results-data").css("display", "block");

            // set up the paging
            if (data.TotalPages > 1) {
                var minPage = data.CurrentPage - 4;
                if (minPage < 2) { minPage = 2; }
                var maxPage = data.CurrentPage + 3;
                if (maxPage >= data.TotalPages) { maxPage = data.TotalPages - 1; }

                if (data.CurrentPage > 1) { renderPaginationEntry(data.CurrentPage, data.CurrentPage - 1, ""); }
                renderPaginationEntry(data.CurrentPage, 1, 1);
                if (minPage > 2) { $('<li class="page-item" style="padding-top: 8px;"><span style="padding-left: 5px; padding-right: 5px;">...</span></li>').appendTo("#pagination"); }
                for (var i = minPage; i <= maxPage; i++) {
                    renderPaginationEntry(data.CurrentPage, i, i);
                }
                if (maxPage < data.TotalPages - 1) { $('<li class="page-item" style="padding-top: 8px;"><span style="padding-left: 5px; padding-right: 5px;">...</span></li>').appendTo("#pagination"); }
                renderPaginationEntry(data.CurrentPage, data.TotalPages, data.TotalPages);
                if (data.CurrentPage < data.TotalPages) { renderPaginationEntry(data.CurrentPage, data.CurrentPage + 1, ""); }
            }
            $("#pagination-nav").attr("aria-label", "Pagination for search results: " + data.TotalPages + " pages");
            $("#pagination-nav2").html($("#pagination-nav").html());

        }
        else {
            // show the no result message
            console.log("no results");
            $("#results-empty").css("display", "block");
        }

        // populate the 'topic' dropdown list
        $("#dropdown-topic-items").empty();
        if (data.Topics != null) {
            $("<legend class='d-none'>" + topicsLabel + "</legend>").appendTo("#dropdown-topic-items");
            for (var i = 0; i < data.Topics.length; i++) {
                var item = data.Topics[i];
                var html = '<div class="form-check">' +
                    '<input type="radio" class="form-check-input" id="topic-' + item.Key + '" name="topic" value="' + item.Key + '" data-display-value="' + item.Value + '" />' +
                    '<label class="form-check-label" for="topic-' + item.Key + '">' + item.DisplayValue + '</label>' +
                    '</div>';
                $(html).appendTo("#dropdown-topic-items");
            }
            $('input[id="topic-' + topic + '"]').prop("checked", true);
            applyFilter("topic", topic);
        }

        // populate the 'when' dropdown list
        $("#dropdown-when-items").empty();
       if (data.Whens != null) {
            $("<legend class='d-none'>" + datesLabel + "</legend>").appendTo("#dropdown-when-items");
            for (var i = 0; i < data.Whens.length; i++) {
                var item = data.Whens[i];
                if (item.Count > 0) {
                    var html = '<div class="form-check">' +
                        '<input type="radio" class="form-check-input" id="when-' + item.Key + '" name="when" value="' + item.Key + '" data-display-value="' + item.Value + '" />' +
                        '<label class="form-check-label" for="when-' + item.Key + '">' + item.DisplayValue + '</label>' +
                        '</div>';
                    $(html).appendTo("#dropdown-when-items");
                }
            }
            $('input[id="when-' + when + '"]').prop("checked", true);
            applyFilter("when", when);
        }

        if (keyword != "") {
            var term = decodeURI(keyword);
            addAppliedFilter("keyword", "search", term);
        }
        var apiEnd = Date.now();
        console.log(apiEnd - apiStart);

    })
        .fail(function (xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            console.log('Error - ' + errorMessage);
        });
}

function renderPaginationEntry(currentPage, pageNum, label) {
    if (currentPage == pageNum) {
        var el = $('<li class="page-item active" aria-current="page"><button class="page-link btn-outline-dark" onclick="setPageNumberAndRefresh(' + pageNum + ');">' + label + '</button></li>').appendTo("#pagination");
    }
    else {
        $('<li class="page-item"><button class="page-link btn-outline-dark" onclick="setPageNumberAndRefresh(' + pageNum + ');">' + label + '</button></li>').appendTo("#pagination");
    }
}

function setPageNumberAndRefresh(num) {
    currentPage = num;
    var divid = document.getElementById("filter-heading");
    if (divid) {
        divid.style.display = 'block';
        divid.scrollIntoView(true);
    }
    updateHash();
    refreshEvents();
}

function updateHash() {
    topic = "";
    when = "";

    $('input[name="topic"]:checked').each(function () {
        topic = $(this).val();
    });

    $('input[name="when"]:checked').each(function () {
        when = $(this).val();
    });

    location.hash = "#page=" + currentPage;
    if (program.length > 0) location.hash += "&program=" + program; // used for audience
    if (topic.length > 0) location.hash += "&topic=" + topic;
    if (when.length > 0) location.hash += "&when=" + when;
    if (keyword.length > 0) location.hash += "&keyword=" + keyword;
}

function getHashParam(key) {
    var value = "";
    if (location.hash) {
        var hash = location.hash.substring(1);
        var params = hash.split("&");
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split("=");
            if (key == param[0]) {
                value = param[1];
            }
        }
    }
    return value;
}

function applyFilter(group, value) {
    var displayText = $("#" + group + "-" + value).attr("data-display-value");
    if (displayText != null) {
        console.log(displayText);
        addAppliedFilter(group, value, displayText);
    }
}

function addKeyword() {
    keyword = $("#keyword-search").val();
    if (keyword != "") {
        $("#keyword-search").val("");
        currentPage = 1;
        updateHash();
        refreshEvents();
    }
}

function addAppliedFilter(group, value, displayText) {
    var html = '<li class="applied-filter list-inline-item m-0" data-id="' + group + '-' + value + '">' +
        '<button class="btn-outline-dark mx-3 applied-filter-button btn btn-link text-decoration-none glyph-append glyph-append-cancel ml-n2" aria-label="' + displayText + ', ' + removeFilterLabel + '">' + displayText + '</button>' +
        '</li>';
    var elem = $(html).appendTo("#applied-filters");

    $(elem).keypress(function (evt) {
        if (evt.which == 13 || evt.which == 32) {
            $('input[id="' + $(this).attr("data-id") + '"]').prop("checked", false);
            if ($(this).attr("data-id") == "keyword-search") { keyword = ""; }
            $(this).remove();
            currentPage = 1;
            updateHash();
            refreshEvents();
        }
    });

    $(elem).click(function () {
        $('input[id="' + $(this).attr("data-id") + '"]').prop("checked", false);
        if ($(this).attr("data-id") == "keyword-search") { keyword = ""; }
        $(this).remove();
        currentPage = 1;
        updateHash();
        refreshEvents();
    });

    $("#applied-filters-row").css("display", "block");
}
