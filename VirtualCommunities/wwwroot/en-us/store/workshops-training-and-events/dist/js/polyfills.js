(function (factory) {
	typeof define === 'function' && define.amd ? define(factory) :
	factory();
}((function () { 'use strict';

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var picturefill = createCommonjsModule(function (module) {
	/*! picturefill - v3.0.2 - 2016-02-12
	 * https://scottjehl.github.io/picturefill/
	 * Copyright (c) 2016 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT
	 */
	/*! Gecko-Picture - v1.0
	 * https://github.com/scottjehl/picturefill/tree/3.0/src/plugins/gecko-picture
	 * Firefox's early picture implementation (prior to FF41) is static and does
	 * not react to viewport changes. This tiny module fixes this.
	 */
	(function(window) {
		/*jshint eqnull:true */
		var ua = navigator.userAgent;

		if ( window.HTMLPictureElement && ((/ecko/).test(ua) && ua.match(/rv\:(\d+)/) && RegExp.$1 < 45) ) {
			addEventListener("resize", (function() {
				var timer;

				var dummySrc = document.createElement("source");

				var fixRespimg = function(img) {
					var source, sizes;
					var picture = img.parentNode;

					if (picture.nodeName.toUpperCase() === "PICTURE") {
						source = dummySrc.cloneNode();

						picture.insertBefore(source, picture.firstElementChild);
						setTimeout(function() {
							picture.removeChild(source);
						});
					} else if (!img._pfLastSize || img.offsetWidth > img._pfLastSize) {
						img._pfLastSize = img.offsetWidth;
						sizes = img.sizes;
						img.sizes += ",100vw";
						setTimeout(function() {
							img.sizes = sizes;
						});
					}
				};

				var findPictureImgs = function() {
					var i;
					var imgs = document.querySelectorAll("picture > img, img[srcset][sizes]");
					for (i = 0; i < imgs.length; i++) {
						fixRespimg(imgs[i]);
					}
				};
				var onResize = function() {
					clearTimeout(timer);
					timer = setTimeout(findPictureImgs, 99);
				};
				var mq = window.matchMedia && matchMedia("(orientation: landscape)");
				var init = function() {
					onResize();

					if (mq && mq.addListener) {
						mq.addListener(onResize);
					}
				};

				dummySrc.srcset = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";

				if (/^[c|i]|d$/.test(document.readyState || "")) {
					init();
				} else {
					document.addEventListener("DOMContentLoaded", init);
				}

				return onResize;
			})());
		}
	})(window);

	/*! Picturefill - v3.0.2
	 * http://scottjehl.github.io/picturefill
	 * Copyright (c) 2015 https://github.com/scottjehl/picturefill/blob/master/Authors.txt;
	 *  License: MIT
	 */

	(function( window, document, undefined$1 ) {

		// HTML shim|v it for old IE (IE9 will still need the HTML video tag workaround)
		document.createElement( "picture" );

		var warn, eminpx, alwaysCheckWDescriptor, evalId;
		// local object for method references and testing exposure
		var pf = {};
		var isSupportTestReady = false;
		var noop = function() {};
		var image = document.createElement( "img" );
		var getImgAttr = image.getAttribute;
		var setImgAttr = image.setAttribute;
		var removeImgAttr = image.removeAttribute;
		var docElem = document.documentElement;
		var types = {};
		var cfg = {
			//resource selection:
			algorithm: ""
		};
		var srcAttr = "data-pfsrc";
		var srcsetAttr = srcAttr + "set";
		// ua sniffing is done for undetectable img loading features,
		// to do some non crucial perf optimizations
		var ua = navigator.userAgent;
		var supportAbort = (/rident/).test(ua) || ((/ecko/).test(ua) && ua.match(/rv\:(\d+)/) && RegExp.$1 > 35 );
		var curSrcProp = "currentSrc";
		var regWDesc = /\s+\+?\d+(e\d+)?w/;
		var regSize = /(\([^)]+\))?\s*(.+)/;
		var setOptions = window.picturefillCFG;
		/**
		 * Shortcut property for https://w3c.github.io/webappsec/specs/mixedcontent/#restricts-mixed-content ( for easy overriding in tests )
		 */
		// baseStyle also used by getEmValue (i.e.: width: 1em is important)
		var baseStyle = "position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)";
		var fsCss = "font-size:100%!important;";
		var isVwDirty = true;

		var cssCache = {};
		var sizeLengthCache = {};
		var DPR = window.devicePixelRatio;
		var units = {
			px: 1,
			"in": 96
		};
		var anchor = document.createElement( "a" );
		/**
		 * alreadyRun flag used for setOptions. is it true setOptions will reevaluate
		 * @type {boolean}
		 */
		var alreadyRun = false;

		// Reusable, non-"g" Regexes

		// (Don't use \s, to avoid matching non-breaking space.)
		var regexLeadingSpaces = /^[ \t\n\r\u000c]+/,
		    regexLeadingCommasOrSpaces = /^[, \t\n\r\u000c]+/,
		    regexLeadingNotSpaces = /^[^ \t\n\r\u000c]+/,
		    regexTrailingCommas = /[,]+$/,
		    regexNonNegativeInteger = /^\d+$/,

		    // ( Positive or negative or unsigned integers or decimals, without or without exponents.
		    // Must include at least one digit.
		    // According to spec tests any decimal point must be followed by a digit.
		    // No leading plus sign is allowed.)
		    // https://html.spec.whatwg.org/multipage/infrastructure.html#valid-floating-point-number
		    regexFloatingPoint = /^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/;

		var on = function(obj, evt, fn, capture) {
			if ( obj.addEventListener ) {
				obj.addEventListener(evt, fn, capture || false);
			} else if ( obj.attachEvent ) {
				obj.attachEvent( "on" + evt, fn);
			}
		};

		/**
		 * simple memoize function:
		 */

		var memoize = function(fn) {
			var cache = {};
			return function(input) {
				if ( !(input in cache) ) {
					cache[ input ] = fn(input);
				}
				return cache[ input ];
			};
		};

		// UTILITY FUNCTIONS

		// Manual is faster than RegEx
		// http://jsperf.com/whitespace-character/5
		function isSpace(c) {
			return (c === "\u0020" || // space
			        c === "\u0009" || // horizontal tab
			        c === "\u000A" || // new line
			        c === "\u000C" || // form feed
			        c === "\u000D");  // carriage return
		}

		/**
		 * gets a mediaquery and returns a boolean or gets a css length and returns a number
		 * @param css mediaqueries or css length
		 * @returns {boolean|number}
		 *
		 * based on: https://gist.github.com/jonathantneal/db4f77009b155f083738
		 */
		var evalCSS = (function() {

			var regLength = /^([\d\.]+)(em|vw|px)$/;
			var replace = function() {
				var args = arguments, index = 0, string = args[0];
				while (++index in args) {
					string = string.replace(args[index], args[++index]);
				}
				return string;
			};

			var buildStr = memoize(function(css) {

				return "return " + replace((css || "").toLowerCase(),
					// interpret `and`
					/\band\b/g, "&&",

					// interpret `,`
					/,/g, "||",

					// interpret `min-` as >=
					/min-([a-z-\s]+):/g, "e.$1>=",

					// interpret `max-` as <=
					/max-([a-z-\s]+):/g, "e.$1<=",

					//calc value
					/calc([^)]+)/g, "($1)",

					// interpret css values
					/(\d+[\.]*[\d]*)([a-z]+)/g, "($1 * e.$2)",
					//make eval less evil
					/^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/ig, ""
				) + ";";
			});

			return function(css, length) {
				var parsedLength;
				if (!(css in cssCache)) {
					cssCache[css] = false;
					if (length && (parsedLength = css.match( regLength ))) {
						cssCache[css] = parsedLength[ 1 ] * units[parsedLength[ 2 ]];
					} else {
						/*jshint evil:true */
						try{
							cssCache[css] = new Function("e", buildStr(css))(units);
						} catch(e) {}
						/*jshint evil:false */
					}
				}
				return cssCache[css];
			};
		})();

		var setResolution = function( candidate, sizesattr ) {
			if ( candidate.w ) { // h = means height: || descriptor.type === 'h' do not handle yet...
				candidate.cWidth = pf.calcListLength( sizesattr || "100vw" );
				candidate.res = candidate.w / candidate.cWidth ;
			} else {
				candidate.res = candidate.d;
			}
			return candidate;
		};

		/**
		 *
		 * @param opt
		 */
		var picturefill = function( opt ) {

			if (!isSupportTestReady) {return;}

			var elements, i, plen;

			var options = opt || {};

			if ( options.elements && options.elements.nodeType === 1 ) {
				if ( options.elements.nodeName.toUpperCase() === "IMG" ) {
					options.elements =  [ options.elements ];
				} else {
					options.context = options.elements;
					options.elements =  null;
				}
			}

			elements = options.elements || pf.qsa( (options.context || document), ( options.reevaluate || options.reselect ) ? pf.sel : pf.selShort );

			if ( (plen = elements.length) ) {

				pf.setupRun( options );
				alreadyRun = true;

				// Loop through all elements
				for ( i = 0; i < plen; i++ ) {
					pf.fillImg(elements[ i ], options);
				}

				pf.teardownRun( options );
			}
		};

		/**
		 * outputs a warning for the developer
		 * @param {message}
		 * @type {Function}
		 */
		warn = ( window.console && console.warn ) ?
			function( message ) {
				console.warn( message );
			} :
			noop
		;

		if ( !(curSrcProp in image) ) {
			curSrcProp = "src";
		}

		// Add support for standard mime types.
		types[ "image/jpeg" ] = true;
		types[ "image/gif" ] = true;
		types[ "image/png" ] = true;

		function detectTypeSupport( type, typeUri ) {
			// based on Modernizr's lossless img-webp test
			// note: asynchronous
			var image = new window.Image();
			image.onerror = function() {
				types[ type ] = false;
				picturefill();
			};
			image.onload = function() {
				types[ type ] = image.width === 1;
				picturefill();
			};
			image.src = typeUri;
			return "pending";
		}

		// test svg support
		types[ "image/svg+xml" ] = document.implementation.hasFeature( "http://www.w3.org/TR/SVG11/feature#Image", "1.1" );

		/**
		 * updates the internal vW property with the current viewport width in px
		 */
		function updateMetrics() {

			isVwDirty = false;
			DPR = window.devicePixelRatio;
			cssCache = {};
			sizeLengthCache = {};

			pf.DPR = DPR || 1;

			units.width = Math.max(window.innerWidth || 0, docElem.clientWidth);
			units.height = Math.max(window.innerHeight || 0, docElem.clientHeight);

			units.vw = units.width / 100;
			units.vh = units.height / 100;

			evalId = [ units.height, units.width, DPR ].join("-");

			units.em = pf.getEmValue();
			units.rem = units.em;
		}

		function chooseLowRes( lowerValue, higherValue, dprValue, isCached ) {
			var bonusFactor, tooMuch, bonus, meanDensity;

			//experimental
			if (cfg.algorithm === "saveData" ){
				if ( lowerValue > 2.7 ) {
					meanDensity = dprValue + 1;
				} else {
					tooMuch = higherValue - dprValue;
					bonusFactor = Math.pow(lowerValue - 0.6, 1.5);

					bonus = tooMuch * bonusFactor;

					if (isCached) {
						bonus += 0.1 * bonusFactor;
					}

					meanDensity = lowerValue + bonus;
				}
			} else {
				meanDensity = (dprValue > 1) ?
					Math.sqrt(lowerValue * higherValue) :
					lowerValue;
			}

			return meanDensity > dprValue;
		}

		function applyBestCandidate( img ) {
			var srcSetCandidates;
			var matchingSet = pf.getSet( img );
			var evaluated = false;
			if ( matchingSet !== "pending" ) {
				evaluated = evalId;
				if ( matchingSet ) {
					srcSetCandidates = pf.setRes( matchingSet );
					pf.applySetCandidate( srcSetCandidates, img );
				}
			}
			img[ pf.ns ].evaled = evaluated;
		}

		function ascendingSort( a, b ) {
			return a.res - b.res;
		}

		function setSrcToCur( img, src, set ) {
			var candidate;
			if ( !set && src ) {
				set = img[ pf.ns ].sets;
				set = set && set[set.length - 1];
			}

			candidate = getCandidateForSrc(src, set);

			if ( candidate ) {
				src = pf.makeUrl(src);
				img[ pf.ns ].curSrc = src;
				img[ pf.ns ].curCan = candidate;

				if ( !candidate.res ) {
					setResolution( candidate, candidate.set.sizes );
				}
			}
			return candidate;
		}

		function getCandidateForSrc( src, set ) {
			var i, candidate, candidates;
			if ( src && set ) {
				candidates = pf.parseSet( set );
				src = pf.makeUrl(src);
				for ( i = 0; i < candidates.length; i++ ) {
					if ( src === pf.makeUrl(candidates[ i ].url) ) {
						candidate = candidates[ i ];
						break;
					}
				}
			}
			return candidate;
		}

		function getAllSourceElements( picture, candidates ) {
			var i, len, source, srcset;

			// SPEC mismatch intended for size and perf:
			// actually only source elements preceding the img should be used
			// also note: don't use qsa here, because IE8 sometimes doesn't like source as the key part in a selector
			var sources = picture.getElementsByTagName( "source" );

			for ( i = 0, len = sources.length; i < len; i++ ) {
				source = sources[ i ];
				source[ pf.ns ] = true;
				srcset = source.getAttribute( "srcset" );

				// if source does not have a srcset attribute, skip
				if ( srcset ) {
					candidates.push( {
						srcset: srcset,
						media: source.getAttribute( "media" ),
						type: source.getAttribute( "type" ),
						sizes: source.getAttribute( "sizes" )
					} );
				}
			}
		}

		/**
		 * Srcset Parser
		 * By Alex Bell |  MIT License
		 *
		 * @returns Array [{url: _, d: _, w: _, h:_, set:_(????)}, ...]
		 *
		 * Based super duper closely on the reference algorithm at:
		 * https://html.spec.whatwg.org/multipage/embedded-content.html#parse-a-srcset-attribute
		 */

		// 1. Let input be the value passed to this algorithm.
		// (TO-DO : Explain what "set" argument is here. Maybe choose a more
		// descriptive & more searchable name.  Since passing the "set" in really has
		// nothing to do with parsing proper, I would prefer this assignment eventually
		// go in an external fn.)
		function parseSrcset(input, set) {

			function collectCharacters(regEx) {
				var chars,
				    match = regEx.exec(input.substring(pos));
				if (match) {
					chars = match[ 0 ];
					pos += chars.length;
					return chars;
				}
			}

			var inputLength = input.length,
			    url,
			    descriptors,
			    currentDescriptor,
			    state,
			    c,

			    // 2. Let position be a pointer into input, initially pointing at the start
			    //    of the string.
			    pos = 0,

			    // 3. Let candidates be an initially empty source set.
			    candidates = [];

			/**
			* Adds descriptor properties to a candidate, pushes to the candidates array
			* @return undefined
			*/
			// (Declared outside of the while loop so that it's only created once.
			// (This fn is defined before it is used, in order to pass JSHINT.
			// Unfortunately this breaks the sequencing of the spec comments. :/ )
			function parseDescriptors() {

				// 9. Descriptor parser: Let error be no.
				var pError = false,

				// 10. Let width be absent.
				// 11. Let density be absent.
				// 12. Let future-compat-h be absent. (We're implementing it now as h)
				    w, d, h, i,
				    candidate = {},
				    desc, lastChar, value, intVal, floatVal;

				// 13. For each descriptor in descriptors, run the appropriate set of steps
				// from the following list:
				for (i = 0 ; i < descriptors.length; i++) {
					desc = descriptors[ i ];

					lastChar = desc[ desc.length - 1 ];
					value = desc.substring(0, desc.length - 1);
					intVal = parseInt(value, 10);
					floatVal = parseFloat(value);

					// If the descriptor consists of a valid non-negative integer followed by
					// a U+0077 LATIN SMALL LETTER W character
					if (regexNonNegativeInteger.test(value) && (lastChar === "w")) {

						// If width and density are not both absent, then let error be yes.
						if (w || d) {pError = true;}

						// Apply the rules for parsing non-negative integers to the descriptor.
						// If the result is zero, let error be yes.
						// Otherwise, let width be the result.
						if (intVal === 0) {pError = true;} else {w = intVal;}

					// If the descriptor consists of a valid floating-point number followed by
					// a U+0078 LATIN SMALL LETTER X character
					} else if (regexFloatingPoint.test(value) && (lastChar === "x")) {

						// If width, density and future-compat-h are not all absent, then let error
						// be yes.
						if (w || d || h) {pError = true;}

						// Apply the rules for parsing floating-point number values to the descriptor.
						// If the result is less than zero, let error be yes. Otherwise, let density
						// be the result.
						if (floatVal < 0) {pError = true;} else {d = floatVal;}

					// If the descriptor consists of a valid non-negative integer followed by
					// a U+0068 LATIN SMALL LETTER H character
					} else if (regexNonNegativeInteger.test(value) && (lastChar === "h")) {

						// If height and density are not both absent, then let error be yes.
						if (h || d) {pError = true;}

						// Apply the rules for parsing non-negative integers to the descriptor.
						// If the result is zero, let error be yes. Otherwise, let future-compat-h
						// be the result.
						if (intVal === 0) {pError = true;} else {h = intVal;}

					// Anything else, Let error be yes.
					} else {pError = true;}
				} // (close step 13 for loop)

				// 15. If error is still no, then append a new image source to candidates whose
				// URL is url, associated with a width width if not absent and a pixel
				// density density if not absent. Otherwise, there is a parse error.
				if (!pError) {
					candidate.url = url;

					if (w) { candidate.w = w;}
					if (d) { candidate.d = d;}
					if (h) { candidate.h = h;}
					if (!h && !d && !w) {candidate.d = 1;}
					if (candidate.d === 1) {set.has1x = true;}
					candidate.set = set;

					candidates.push(candidate);
				}
			} // (close parseDescriptors fn)

			/**
			* Tokenizes descriptor properties prior to parsing
			* Returns undefined.
			* (Again, this fn is defined before it is used, in order to pass JSHINT.
			* Unfortunately this breaks the logical sequencing of the spec comments. :/ )
			*/
			function tokenize() {

				// 8.1. Descriptor tokeniser: Skip whitespace
				collectCharacters(regexLeadingSpaces);

				// 8.2. Let current descriptor be the empty string.
				currentDescriptor = "";

				// 8.3. Let state be in descriptor.
				state = "in descriptor";

				while (true) {

					// 8.4. Let c be the character at position.
					c = input.charAt(pos);

					//  Do the following depending on the value of state.
					//  For the purpose of this step, "EOF" is a special character representing
					//  that position is past the end of input.

					// In descriptor
					if (state === "in descriptor") {
						// Do the following, depending on the value of c:

					  // Space character
					  // If current descriptor is not empty, append current descriptor to
					  // descriptors and let current descriptor be the empty string.
					  // Set state to after descriptor.
						if (isSpace(c)) {
							if (currentDescriptor) {
								descriptors.push(currentDescriptor);
								currentDescriptor = "";
								state = "after descriptor";
							}

						// U+002C COMMA (,)
						// Advance position to the next character in input. If current descriptor
						// is not empty, append current descriptor to descriptors. Jump to the step
						// labeled descriptor parser.
						} else if (c === ",") {
							pos += 1;
							if (currentDescriptor) {
								descriptors.push(currentDescriptor);
							}
							parseDescriptors();
							return;

						// U+0028 LEFT PARENTHESIS (()
						// Append c to current descriptor. Set state to in parens.
						} else if (c === "\u0028") {
							currentDescriptor = currentDescriptor + c;
							state = "in parens";

						// EOF
						// If current descriptor is not empty, append current descriptor to
						// descriptors. Jump to the step labeled descriptor parser.
						} else if (c === "") {
							if (currentDescriptor) {
								descriptors.push(currentDescriptor);
							}
							parseDescriptors();
							return;

						// Anything else
						// Append c to current descriptor.
						} else {
							currentDescriptor = currentDescriptor + c;
						}
					// (end "in descriptor"

					// In parens
					} else if (state === "in parens") {

						// U+0029 RIGHT PARENTHESIS ())
						// Append c to current descriptor. Set state to in descriptor.
						if (c === ")") {
							currentDescriptor = currentDescriptor + c;
							state = "in descriptor";

						// EOF
						// Append current descriptor to descriptors. Jump to the step labeled
						// descriptor parser.
						} else if (c === "") {
							descriptors.push(currentDescriptor);
							parseDescriptors();
							return;

						// Anything else
						// Append c to current descriptor.
						} else {
							currentDescriptor = currentDescriptor + c;
						}

					// After descriptor
					} else if (state === "after descriptor") {

						// Do the following, depending on the value of c:
						// Space character: Stay in this state.
						if (isSpace(c)) ; else if (c === "") {
							parseDescriptors();
							return;

						// Anything else
						// Set state to in descriptor. Set position to the previous character in input.
						} else {
							state = "in descriptor";
							pos -= 1;

						}
					}

					// Advance position to the next character in input.
					pos += 1;

				// Repeat this step.
				} // (close while true loop)
			}

			// 4. Splitting loop: Collect a sequence of characters that are space
			//    characters or U+002C COMMA characters. If any U+002C COMMA characters
			//    were collected, that is a parse error.
			while (true) {
				collectCharacters(regexLeadingCommasOrSpaces);

				// 5. If position is past the end of input, return candidates and abort these steps.
				if (pos >= inputLength) {
					return candidates; // (we're done, this is the sole return path)
				}

				// 6. Collect a sequence of characters that are not space characters,
				//    and let that be url.
				url = collectCharacters(regexLeadingNotSpaces);

				// 7. Let descriptors be a new empty list.
				descriptors = [];

				// 8. If url ends with a U+002C COMMA character (,), follow these substeps:
				//		(1). Remove all trailing U+002C COMMA characters from url. If this removed
				//         more than one character, that is a parse error.
				if (url.slice(-1) === ",") {
					url = url.replace(regexTrailingCommas, "");
					// (Jump ahead to step 9 to skip tokenization and just push the candidate).
					parseDescriptors();

				//	Otherwise, follow these substeps:
				} else {
					tokenize();
				} // (close else of step 8)

			// 16. Return to the step labeled splitting loop.
			} // (Close of big while loop.)
		}

		/*
		 * Sizes Parser
		 *
		 * By Alex Bell |  MIT License
		 *
		 * Non-strict but accurate and lightweight JS Parser for the string value <img sizes="here">
		 *
		 * Reference algorithm at:
		 * https://html.spec.whatwg.org/multipage/embedded-content.html#parse-a-sizes-attribute
		 *
		 * Most comments are copied in directly from the spec
		 * (except for comments in parens).
		 *
		 * Grammar is:
		 * <source-size-list> = <source-size># [ , <source-size-value> ]? | <source-size-value>
		 * <source-size> = <media-condition> <source-size-value>
		 * <source-size-value> = <length>
		 * http://www.w3.org/html/wg/drafts/html/master/embedded-content.html#attr-img-sizes
		 *
		 * E.g. "(max-width: 30em) 100vw, (max-width: 50em) 70vw, 100vw"
		 * or "(min-width: 30em), calc(30vw - 15px)" or just "30vw"
		 *
		 * Returns the first valid <css-length> with a media condition that evaluates to true,
		 * or "100vw" if all valid media conditions evaluate to false.
		 *
		 */

		function parseSizes(strValue) {

			// (Percentage CSS lengths are not allowed in this case, to avoid confusion:
			// https://html.spec.whatwg.org/multipage/embedded-content.html#valid-source-size-list
			// CSS allows a single optional plus or minus sign:
			// http://www.w3.org/TR/CSS2/syndata.html#numbers
			// CSS is ASCII case-insensitive:
			// http://www.w3.org/TR/CSS2/syndata.html#characters )
			// Spec allows exponential notation for <number> type:
			// http://dev.w3.org/csswg/css-values/#numbers
			var regexCssLengthWithUnits = /^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i;

			// (This is a quick and lenient test. Because of optional unlimited-depth internal
			// grouping parens and strict spacing rules, this could get very complicated.)
			var regexCssCalc = /^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;

			var i;
			var unparsedSizesList;
			var unparsedSizesListLength;
			var unparsedSize;
			var lastComponentValue;
			var size;

			// UTILITY FUNCTIONS

			//  (Toy CSS parser. The goals here are:
			//  1) expansive test coverage without the weight of a full CSS parser.
			//  2) Avoiding regex wherever convenient.
			//  Quick tests: http://jsfiddle.net/gtntL4gr/3/
			//  Returns an array of arrays.)
			function parseComponentValues(str) {
				var chrctr;
				var component = "";
				var componentArray = [];
				var listArray = [];
				var parenDepth = 0;
				var pos = 0;
				var inComment = false;

				function pushComponent() {
					if (component) {
						componentArray.push(component);
						component = "";
					}
				}

				function pushComponentArray() {
					if (componentArray[0]) {
						listArray.push(componentArray);
						componentArray = [];
					}
				}

				// (Loop forwards from the beginning of the string.)
				while (true) {
					chrctr = str.charAt(pos);

					if (chrctr === "") { // ( End of string reached.)
						pushComponent();
						pushComponentArray();
						return listArray;
					} else if (inComment) {
						if ((chrctr === "*") && (str[pos + 1] === "/")) { // (At end of a comment.)
							inComment = false;
							pos += 2;
							pushComponent();
							continue;
						} else {
							pos += 1; // (Skip all characters inside comments.)
							continue;
						}
					} else if (isSpace(chrctr)) {
						// (If previous character in loop was also a space, or if
						// at the beginning of the string, do not add space char to
						// component.)
						if ( (str.charAt(pos - 1) && isSpace( str.charAt(pos - 1) ) ) || !component ) {
							pos += 1;
							continue;
						} else if (parenDepth === 0) {
							pushComponent();
							pos +=1;
							continue;
						} else {
							// (Replace any space character with a plain space for legibility.)
							chrctr = " ";
						}
					} else if (chrctr === "(") {
						parenDepth += 1;
					} else if (chrctr === ")") {
						parenDepth -= 1;
					} else if (chrctr === ",") {
						pushComponent();
						pushComponentArray();
						pos += 1;
						continue;
					} else if ( (chrctr === "/") && (str.charAt(pos + 1) === "*") ) {
						inComment = true;
						pos += 2;
						continue;
					}

					component = component + chrctr;
					pos += 1;
				}
			}

			function isValidNonNegativeSourceSizeValue(s) {
				if (regexCssLengthWithUnits.test(s) && (parseFloat(s) >= 0)) {return true;}
				if (regexCssCalc.test(s)) {return true;}
				// ( http://www.w3.org/TR/CSS2/syndata.html#numbers says:
				// "-0 is equivalent to 0 and is not a negative number." which means that
				// unitless zero and unitless negative zero must be accepted as special cases.)
				if ((s === "0") || (s === "-0") || (s === "+0")) {return true;}
				return false;
			}

			// When asked to parse a sizes attribute from an element, parse a
			// comma-separated list of component values from the value of the element's
			// sizes attribute (or the empty string, if the attribute is absent), and let
			// unparsed sizes list be the result.
			// http://dev.w3.org/csswg/css-syntax/#parse-comma-separated-list-of-component-values

			unparsedSizesList = parseComponentValues(strValue);
			unparsedSizesListLength = unparsedSizesList.length;

			// For each unparsed size in unparsed sizes list:
			for (i = 0; i < unparsedSizesListLength; i++) {
				unparsedSize = unparsedSizesList[i];

				// 1. Remove all consecutive <whitespace-token>s from the end of unparsed size.
				// ( parseComponentValues() already omits spaces outside of parens. )

				// If unparsed size is now empty, that is a parse error; continue to the next
				// iteration of this algorithm.
				// ( parseComponentValues() won't push an empty array. )

				// 2. If the last component value in unparsed size is a valid non-negative
				// <source-size-value>, let size be its value and remove the component value
				// from unparsed size. Any CSS function other than the calc() function is
				// invalid. Otherwise, there is a parse error; continue to the next iteration
				// of this algorithm.
				// http://dev.w3.org/csswg/css-syntax/#parse-component-value
				lastComponentValue = unparsedSize[unparsedSize.length - 1];

				if (isValidNonNegativeSourceSizeValue(lastComponentValue)) {
					size = lastComponentValue;
					unparsedSize.pop();
				} else {
					continue;
				}

				// 3. Remove all consecutive <whitespace-token>s from the end of unparsed
				// size. If unparsed size is now empty, return size and exit this algorithm.
				// If this was not the last item in unparsed sizes list, that is a parse error.
				if (unparsedSize.length === 0) {
					return size;
				}

				// 4. Parse the remaining component values in unparsed size as a
				// <media-condition>. If it does not parse correctly, or it does parse
				// correctly but the <media-condition> evaluates to false, continue to the
				// next iteration of this algorithm.
				// (Parsing all possible compound media conditions in JS is heavy, complicated,
				// and the payoff is unclear. Is there ever an situation where the
				// media condition parses incorrectly but still somehow evaluates to true?
				// Can we just rely on the browser/polyfill to do it?)
				unparsedSize = unparsedSize.join(" ");
				if (!(pf.matchesMedia( unparsedSize ) ) ) {
					continue;
				}

				// 5. Return size and exit this algorithm.
				return size;
			}

			// If the above algorithm exhausts unparsed sizes list without returning a
			// size value, return 100vw.
			return "100vw";
		}

		// namespace
		pf.ns = ("pf" + new Date().getTime()).substr(0, 9);

		// srcset support test
		pf.supSrcset = "srcset" in image;
		pf.supSizes = "sizes" in image;
		pf.supPicture = !!window.HTMLPictureElement;

		// UC browser does claim to support srcset and picture, but not sizes,
		// this extended test reveals the browser does support nothing
		if (pf.supSrcset && pf.supPicture && !pf.supSizes) {
			(function(image2) {
				image.srcset = "data:,a";
				image2.src = "data:,a";
				pf.supSrcset = image.complete === image2.complete;
				pf.supPicture = pf.supSrcset && pf.supPicture;
			})(document.createElement("img"));
		}

		// Safari9 has basic support for sizes, but does't expose the `sizes` idl attribute
		if (pf.supSrcset && !pf.supSizes) {

			(function() {
				var width2 = "data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw==";
				var width1 = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";
				var img = document.createElement("img");
				var test = function() {
					var width = img.width;

					if (width === 2) {
						pf.supSizes = true;
					}

					alwaysCheckWDescriptor = pf.supSrcset && !pf.supSizes;

					isSupportTestReady = true;
					// force async
					setTimeout(picturefill);
				};

				img.onload = test;
				img.onerror = test;
				img.setAttribute("sizes", "9px");

				img.srcset = width1 + " 1w," + width2 + " 9w";
				img.src = width1;
			})();

		} else {
			isSupportTestReady = true;
		}

		// using pf.qsa instead of dom traversing does scale much better,
		// especially on sites mixing responsive and non-responsive images
		pf.selShort = "picture>img,img[srcset]";
		pf.sel = pf.selShort;
		pf.cfg = cfg;

		/**
		 * Shortcut property for `devicePixelRatio` ( for easy overriding in tests )
		 */
		pf.DPR = (DPR  || 1 );
		pf.u = units;

		// container of supported mime types that one might need to qualify before using
		pf.types =  types;

		pf.setSize = noop;

		/**
		 * Gets a string and returns the absolute URL
		 * @param src
		 * @returns {String} absolute URL
		 */

		pf.makeUrl = memoize(function(src) {
			anchor.href = src;
			return anchor.href;
		});

		/**
		 * Gets a DOM element or document and a selctor and returns the found matches
		 * Can be extended with jQuery/Sizzle for IE7 support
		 * @param context
		 * @param sel
		 * @returns {NodeList|Array}
		 */
		pf.qsa = function(context, sel) {
			return ( "querySelector" in context ) ? context.querySelectorAll(sel) : [];
		};

		/**
		 * Shortcut method for matchMedia ( for easy overriding in tests )
		 * wether native or pf.mMQ is used will be decided lazy on first call
		 * @returns {boolean}
		 */
		pf.matchesMedia = function() {
			if ( window.matchMedia && (matchMedia( "(min-width: 0.1em)" ) || {}).matches ) {
				pf.matchesMedia = function( media ) {
					return !media || ( matchMedia( media ).matches );
				};
			} else {
				pf.matchesMedia = pf.mMQ;
			}

			return pf.matchesMedia.apply( this, arguments );
		};

		/**
		 * A simplified matchMedia implementation for IE8 and IE9
		 * handles only min-width/max-width with px or em values
		 * @param media
		 * @returns {boolean}
		 */
		pf.mMQ = function( media ) {
			return media ? evalCSS(media) : true;
		};

		/**
		 * Returns the calculated length in css pixel from the given sourceSizeValue
		 * http://dev.w3.org/csswg/css-values-3/#length-value
		 * intended Spec mismatches:
		 * * Does not check for invalid use of CSS functions
		 * * Does handle a computed length of 0 the same as a negative and therefore invalid value
		 * @param sourceSizeValue
		 * @returns {Number}
		 */
		pf.calcLength = function( sourceSizeValue ) {

			var value = evalCSS(sourceSizeValue, true) || false;
			if (value < 0) {
				value = false;
			}

			return value;
		};

		/**
		 * Takes a type string and checks if its supported
		 */

		pf.supportsType = function( type ) {
			return ( type ) ? types[ type ] : true;
		};

		/**
		 * Parses a sourceSize into mediaCondition (media) and sourceSizeValue (length)
		 * @param sourceSizeStr
		 * @returns {*}
		 */
		pf.parseSize = memoize(function( sourceSizeStr ) {
			var match = ( sourceSizeStr || "" ).match(regSize);
			return {
				media: match && match[1],
				length: match && match[2]
			};
		});

		pf.parseSet = function( set ) {
			if ( !set.cands ) {
				set.cands = parseSrcset(set.srcset, set);
			}
			return set.cands;
		};

		/**
		 * returns 1em in css px for html/body default size
		 * function taken from respondjs
		 * @returns {*|number}
		 */
		pf.getEmValue = function() {
			var body;
			if ( !eminpx && (body = document.body) ) {
				var div = document.createElement( "div" ),
					originalHTMLCSS = docElem.style.cssText,
					originalBodyCSS = body.style.cssText;

				div.style.cssText = baseStyle;

				// 1em in a media query is the value of the default font size of the browser
				// reset docElem and body to ensure the correct value is returned
				docElem.style.cssText = fsCss;
				body.style.cssText = fsCss;

				body.appendChild( div );
				eminpx = div.offsetWidth;
				body.removeChild( div );

				//also update eminpx before returning
				eminpx = parseFloat( eminpx, 10 );

				// restore the original values
				docElem.style.cssText = originalHTMLCSS;
				body.style.cssText = originalBodyCSS;

			}
			return eminpx || 16;
		};

		/**
		 * Takes a string of sizes and returns the width in pixels as a number
		 */
		pf.calcListLength = function( sourceSizeListStr ) {
			// Split up source size list, ie ( max-width: 30em ) 100%, ( max-width: 50em ) 50%, 33%
			//
			//                           or (min-width:30em) calc(30% - 15px)
			if ( !(sourceSizeListStr in sizeLengthCache) || cfg.uT ) {
				var winningLength = pf.calcLength( parseSizes( sourceSizeListStr ) );

				sizeLengthCache[ sourceSizeListStr ] = !winningLength ? units.width : winningLength;
			}

			return sizeLengthCache[ sourceSizeListStr ];
		};

		/**
		 * Takes a candidate object with a srcset property in the form of url/
		 * ex. "images/pic-medium.png 1x, images/pic-medium-2x.png 2x" or
		 *     "images/pic-medium.png 400w, images/pic-medium-2x.png 800w" or
		 *     "images/pic-small.png"
		 * Get an array of image candidates in the form of
		 *      {url: "/foo/bar.png", resolution: 1}
		 * where resolution is http://dev.w3.org/csswg/css-values-3/#resolution-value
		 * If sizes is specified, res is calculated
		 */
		pf.setRes = function( set ) {
			var candidates;
			if ( set ) {

				candidates = pf.parseSet( set );

				for ( var i = 0, len = candidates.length; i < len; i++ ) {
					setResolution( candidates[ i ], set.sizes );
				}
			}
			return candidates;
		};

		pf.setRes.res = setResolution;

		pf.applySetCandidate = function( candidates, img ) {
			if ( !candidates.length ) {return;}
			var candidate,
				i,
				j,
				length,
				bestCandidate,
				curSrc,
				curCan,
				candidateSrc,
				abortCurSrc;

			var imageData = img[ pf.ns ];
			var dpr = pf.DPR;

			curSrc = imageData.curSrc || img[curSrcProp];

			curCan = imageData.curCan || setSrcToCur(img, curSrc, candidates[0].set);

			// if we have a current source, we might either become lazy or give this source some advantage
			if ( curCan && curCan.set === candidates[ 0 ].set ) {

				// if browser can abort image request and the image has a higher pixel density than needed
				// and this image isn't downloaded yet, we skip next part and try to save bandwidth
				abortCurSrc = (supportAbort && !img.complete && curCan.res - 0.1 > dpr);

				if ( !abortCurSrc ) {
					curCan.cached = true;

					// if current candidate is "best", "better" or "okay",
					// set it to bestCandidate
					if ( curCan.res >= dpr ) {
						bestCandidate = curCan;
					}
				}
			}

			if ( !bestCandidate ) {

				candidates.sort( ascendingSort );

				length = candidates.length;
				bestCandidate = candidates[ length - 1 ];

				for ( i = 0; i < length; i++ ) {
					candidate = candidates[ i ];
					if ( candidate.res >= dpr ) {
						j = i - 1;

						// we have found the perfect candidate,
						// but let's improve this a little bit with some assumptions ;-)
						if (candidates[ j ] &&
							(abortCurSrc || curSrc !== pf.makeUrl( candidate.url )) &&
							chooseLowRes(candidates[ j ].res, candidate.res, dpr, candidates[ j ].cached)) {

							bestCandidate = candidates[ j ];

						} else {
							bestCandidate = candidate;
						}
						break;
					}
				}
			}

			if ( bestCandidate ) {

				candidateSrc = pf.makeUrl( bestCandidate.url );

				imageData.curSrc = candidateSrc;
				imageData.curCan = bestCandidate;

				if ( candidateSrc !== curSrc ) {
					pf.setSrc( img, bestCandidate );
				}
				pf.setSize( img );
			}
		};

		pf.setSrc = function( img, bestCandidate ) {
			var origWidth;
			img.src = bestCandidate.url;

			// although this is a specific Safari issue, we don't want to take too much different code paths
			if ( bestCandidate.set.type === "image/svg+xml" ) {
				origWidth = img.style.width;
				img.style.width = (img.offsetWidth + 1) + "px";

				// next line only should trigger a repaint
				// if... is only done to trick dead code removal
				if ( img.offsetWidth + 1 ) {
					img.style.width = origWidth;
				}
			}
		};

		pf.getSet = function( img ) {
			var i, set, supportsType;
			var match = false;
			var sets = img [ pf.ns ].sets;

			for ( i = 0; i < sets.length && !match; i++ ) {
				set = sets[i];

				if ( !set.srcset || !pf.matchesMedia( set.media ) || !(supportsType = pf.supportsType( set.type )) ) {
					continue;
				}

				if ( supportsType === "pending" ) {
					set = supportsType;
				}

				match = set;
				break;
			}

			return match;
		};

		pf.parseSets = function( element, parent, options ) {
			var srcsetAttribute, imageSet, isWDescripor, srcsetParsed;

			var hasPicture = parent && parent.nodeName.toUpperCase() === "PICTURE";
			var imageData = element[ pf.ns ];

			if ( imageData.src === undefined$1 || options.src ) {
				imageData.src = getImgAttr.call( element, "src" );
				if ( imageData.src ) {
					setImgAttr.call( element, srcAttr, imageData.src );
				} else {
					removeImgAttr.call( element, srcAttr );
				}
			}

			if ( imageData.srcset === undefined$1 || options.srcset || !pf.supSrcset || element.srcset ) {
				srcsetAttribute = getImgAttr.call( element, "srcset" );
				imageData.srcset = srcsetAttribute;
				srcsetParsed = true;
			}

			imageData.sets = [];

			if ( hasPicture ) {
				imageData.pic = true;
				getAllSourceElements( parent, imageData.sets );
			}

			if ( imageData.srcset ) {
				imageSet = {
					srcset: imageData.srcset,
					sizes: getImgAttr.call( element, "sizes" )
				};

				imageData.sets.push( imageSet );

				isWDescripor = (alwaysCheckWDescriptor || imageData.src) && regWDesc.test(imageData.srcset || "");

				// add normal src as candidate, if source has no w descriptor
				if ( !isWDescripor && imageData.src && !getCandidateForSrc(imageData.src, imageSet) && !imageSet.has1x ) {
					imageSet.srcset += ", " + imageData.src;
					imageSet.cands.push({
						url: imageData.src,
						d: 1,
						set: imageSet
					});
				}

			} else if ( imageData.src ) {
				imageData.sets.push( {
					srcset: imageData.src,
					sizes: null
				} );
			}

			imageData.curCan = null;
			imageData.curSrc = undefined$1;

			// if img has picture or the srcset was removed or has a srcset and does not support srcset at all
			// or has a w descriptor (and does not support sizes) set support to false to evaluate
			imageData.supported = !( hasPicture || ( imageSet && !pf.supSrcset ) || (isWDescripor && !pf.supSizes) );

			if ( srcsetParsed && pf.supSrcset && !imageData.supported ) {
				if ( srcsetAttribute ) {
					setImgAttr.call( element, srcsetAttr, srcsetAttribute );
					element.srcset = "";
				} else {
					removeImgAttr.call( element, srcsetAttr );
				}
			}

			if (imageData.supported && !imageData.srcset && ((!imageData.src && element.src) ||  element.src !== pf.makeUrl(imageData.src))) {
				if (imageData.src === null) {
					element.removeAttribute("src");
				} else {
					element.src = imageData.src;
				}
			}

			imageData.parsed = true;
		};

		pf.fillImg = function(element, options) {
			var imageData;
			var extreme = options.reselect || options.reevaluate;

			// expando for caching data on the img
			if ( !element[ pf.ns ] ) {
				element[ pf.ns ] = {};
			}

			imageData = element[ pf.ns ];

			// if the element has already been evaluated, skip it
			// unless `options.reevaluate` is set to true ( this, for example,
			// is set to true when running `picturefill` on `resize` ).
			if ( !extreme && imageData.evaled === evalId ) {
				return;
			}

			if ( !imageData.parsed || options.reevaluate ) {
				pf.parseSets( element, element.parentNode, options );
			}

			if ( !imageData.supported ) {
				applyBestCandidate( element );
			} else {
				imageData.evaled = evalId;
			}
		};

		pf.setupRun = function() {
			if ( !alreadyRun || isVwDirty || (DPR !== window.devicePixelRatio) ) {
				updateMetrics();
			}
		};

		// If picture is supported, well, that's awesome.
		if ( pf.supPicture ) {
			picturefill = noop;
			pf.fillImg = noop;
		} else {

			 // Set up picture polyfill by polling the document
			(function() {
				var isDomReady;
				var regReady = window.attachEvent ? /d$|^c/ : /d$|^c|^i/;

				var run = function() {
					var readyState = document.readyState || "";

					timerId = setTimeout(run, readyState === "loading" ? 200 :  999);
					if ( document.body ) {
						pf.fillImgs();
						isDomReady = isDomReady || regReady.test(readyState);
						if ( isDomReady ) {
							clearTimeout( timerId );
						}

					}
				};

				var timerId = setTimeout(run, document.body ? 9 : 99);

				// Also attach picturefill on resize and readystatechange
				// http://modernjavascript.blogspot.com/2013/08/building-better-debounce.html
				var debounce = function(func, wait) {
					var timeout, timestamp;
					var later = function() {
						var last = (new Date()) - timestamp;

						if (last < wait) {
							timeout = setTimeout(later, wait - last);
						} else {
							timeout = null;
							func();
						}
					};

					return function() {
						timestamp = new Date();

						if (!timeout) {
							timeout = setTimeout(later, wait);
						}
					};
				};
				var lastClientWidth = docElem.clientHeight;
				var onResize = function() {
					isVwDirty = Math.max(window.innerWidth || 0, docElem.clientWidth) !== units.width || docElem.clientHeight !== lastClientWidth;
					lastClientWidth = docElem.clientHeight;
					if ( isVwDirty ) {
						pf.fillImgs();
					}
				};

				on( window, "resize", debounce(onResize, 99 ) );
				on( document, "readystatechange", run );
			})();
		}

		pf.picturefill = picturefill;
		//use this internally for easy monkey patching/performance testing
		pf.fillImgs = picturefill;
		pf.teardownRun = noop;

		/* expose methods for testing */
		picturefill._ = pf;

		window.picturefillCFG = {
			pf: pf,
			push: function(args) {
				var name = args.shift();
				if (typeof pf[name] === "function") {
					pf[name].apply(pf, args);
				} else {
					cfg[name] = args[0];
					if (alreadyRun) {
						pf.fillImgs( { reselect: true } );
					}
				}
			}
		};

		while (setOptions && setOptions.length) {
			window.picturefillCFG.push(setOptions.shift());
		}

		/* expose picturefill */
		window.picturefill = picturefill;

		/* expose picturefill */
		{
			// CommonJS, just export
			module.exports = picturefill;
		}

		// IE8 evals this sync, so it must be the last thing we do
		if ( !pf.supPicture ) {
			types[ "image/webp" ] = detectTypeSupport("image/webp", "data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==" );
		}

	} )( window, document );
	});

	/*
	 * classList.js: Cross-browser full element.classList implementation.
	 * 1.1.20170427
	 *
	 * By Eli Grey, http://eligrey.com
	 * License: Dedicated to the public domain.
	 *   See https://github.com/eligrey/classList.js/blob/master/LICENSE.md
	 */

	/*global self, document, DOMException */

	/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */

	if ("document" in window.self) {

	// Full polyfill for browsers with no classList support
	// Including IE < Edge missing SVGElement.classList
	if (!("classList" in document.createElement("_")) 
		|| document.createElementNS && !("classList" in document.createElementNS("http://www.w3.org/2000/svg","g"))) {

	(function (view) {

	if (!('Element' in view)) return;

	var
		  classListProp = "classList"
		, protoProp = "prototype"
		, elemCtrProto = view.Element[protoProp]
		, objCtr = Object
		, strTrim = String[protoProp].trim || function () {
			return this.replace(/^\s+|\s+$/g, "");
		}
		, arrIndexOf = Array[protoProp].indexOf || function (item) {
			var
				  i = 0
				, len = this.length
			;
			for (; i < len; i++) {
				if (i in this && this[i] === item) {
					return i;
				}
			}
			return -1;
		}
		// Vendors: please allow content code to instantiate DOMExceptions
		, DOMEx = function (type, message) {
			this.name = type;
			this.code = DOMException[type];
			this.message = message;
		}
		, checkTokenAndGetIndex = function (classList, token) {
			if (token === "") {
				throw new DOMEx(
					  "SYNTAX_ERR"
					, "An invalid or illegal string was specified"
				);
			}
			if (/\s/.test(token)) {
				throw new DOMEx(
					  "INVALID_CHARACTER_ERR"
					, "String contains an invalid character"
				);
			}
			return arrIndexOf.call(classList, token);
		}
		, ClassList = function (elem) {
			var
				  trimmedClasses = strTrim.call(elem.getAttribute("class") || "")
				, classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
				, i = 0
				, len = classes.length
			;
			for (; i < len; i++) {
				this.push(classes[i]);
			}
			this._updateClassName = function () {
				elem.setAttribute("class", this.toString());
			};
		}
		, classListProto = ClassList[protoProp] = []
		, classListGetter = function () {
			return new ClassList(this);
		}
	;
	// Most DOMException implementations don't allow calling DOMException's toString()
	// on non-DOMExceptions. Error's toString() is sufficient here.
	DOMEx[protoProp] = Error[protoProp];
	classListProto.item = function (i) {
		return this[i] || null;
	};
	classListProto.contains = function (token) {
		token += "";
		return checkTokenAndGetIndex(this, token) !== -1;
	};
	classListProto.add = function () {
		var
			  tokens = arguments
			, i = 0
			, l = tokens.length
			, token
			, updated = false
		;
		do {
			token = tokens[i] + "";
			if (checkTokenAndGetIndex(this, token) === -1) {
				this.push(token);
				updated = true;
			}
		}
		while (++i < l);

		if (updated) {
			this._updateClassName();
		}
	};
	classListProto.remove = function () {
		var
			  tokens = arguments
			, i = 0
			, l = tokens.length
			, token
			, updated = false
			, index
		;
		do {
			token = tokens[i] + "";
			index = checkTokenAndGetIndex(this, token);
			while (index !== -1) {
				this.splice(index, 1);
				updated = true;
				index = checkTokenAndGetIndex(this, token);
			}
		}
		while (++i < l);

		if (updated) {
			this._updateClassName();
		}
	};
	classListProto.toggle = function (token, force) {
		token += "";

		var
			  result = this.contains(token)
			, method = result ?
				force !== true && "remove"
			:
				force !== false && "add"
		;

		if (method) {
			this[method](token);
		}

		if (force === true || force === false) {
			return force;
		} else {
			return !result;
		}
	};
	classListProto.toString = function () {
		return this.join(" ");
	};

	if (objCtr.defineProperty) {
		var classListPropDesc = {
			  get: classListGetter
			, enumerable: true
			, configurable: true
		};
		try {
			objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
		} catch (ex) { // IE 8 doesn't support enumerable:true
			// adding undefined to fight this issue https://github.com/eligrey/classList.js/issues/36
			// modernie IE8-MSW7 machine has IE8 8.0.6001.18702 and is affected
			if (ex.number === undefined || ex.number === -0x7FF5EC54) {
				classListPropDesc.enumerable = false;
				objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
			}
		}
	} else if (objCtr[protoProp].__defineGetter__) {
		elemCtrProto.__defineGetter__(classListProp, classListGetter);
	}

	}(window.self));

	}

	// There is full or partial native classList support, so just check if we need
	// to normalize the add/remove and toggle APIs.

	(function () {

		var testElement = document.createElement("_");

		testElement.classList.add("c1", "c2");

		// Polyfill for IE 10/11 and Firefox <26, where classList.add and
		// classList.remove exist but support only one argument at a time.
		if (!testElement.classList.contains("c2")) {
			var createMethod = function(method) {
				var original = DOMTokenList.prototype[method];

				DOMTokenList.prototype[method] = function(token) {
					var i, len = arguments.length;

					for (i = 0; i < len; i++) {
						token = arguments[i];
						original.call(this, token);
					}
				};
			};
			createMethod('add');
			createMethod('remove');
		}

		testElement.classList.toggle("c3", false);

		// Polyfill for IE 10 and Firefox <24, where classList.toggle does not
		// support the second argument.
		if (testElement.classList.contains("c3")) {
			var _toggle = DOMTokenList.prototype.toggle;

			DOMTokenList.prototype.toggle = function(token, force) {
				if (1 in arguments && !this.contains(token) === !force) {
					return force;
				} else {
					return _toggle.call(this, token);
				}
			};

		}

		testElement = null;
	}());

	}

	// Polyfill for creating CustomEvents on IE9/10/11

	// code pulled from:
	// https://github.com/d4tocchini/customevent-polyfill
	// https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent#Polyfill

	(function() {
	  if (typeof window === 'undefined') {
	    return;
	  }

	  try {
	    var ce = new window.CustomEvent('test', { cancelable: true });
	    ce.preventDefault();
	    if (ce.defaultPrevented !== true) {
	      // IE has problems with .preventDefault() on custom events
	      // http://stackoverflow.com/questions/23349191
	      throw new Error('Could not prevent default');
	    }
	  } catch (e) {
	    var CustomEvent = function(event, params) {
	      var evt, origPrevent;
	      params = params || {};
	      params.bubbles = !!params.bubbles;
	      params.cancelable = !!params.cancelable;

	      evt = document.createEvent('CustomEvent');
	      evt.initCustomEvent(
	        event,
	        params.bubbles,
	        params.cancelable,
	        params.detail
	      );
	      origPrevent = evt.preventDefault;
	      evt.preventDefault = function() {
	        origPrevent.call(this);
	        try {
	          Object.defineProperty(this, 'defaultPrevented', {
	            get: function() {
	              return true;
	            }
	          });
	        } catch (e) {
	          this.defaultPrevented = true;
	        }
	      };
	      return evt;
	    };

	    CustomEvent.prototype = window.Event.prototype;
	    window.CustomEvent = CustomEvent; // expose definition to window
	  }
	})();

	(function () {
	    if (!Element.prototype.matches) {
	        Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
	    }

	    if (!Element.prototype.closest) {
	        Element.prototype.closest = function (s) {
	            var el = this;

	            do {
	                if (el.matches(s)) return el;
	                el = el.parentElement || el.parentNode;
	            } while (el !== null && el.nodeType === 1);
	            return null;
	        };
	    }
	})();

	var support = {
	  searchParams: 'URLSearchParams' in self,
	  iterable: 'Symbol' in self && 'iterator' in Symbol,
	  blob:
	    'FileReader' in self &&
	    'Blob' in self &&
	    (function() {
	      try {
	        new Blob();
	        return true
	      } catch (e) {
	        return false
	      }
	    })(),
	  formData: 'FormData' in self,
	  arrayBuffer: 'ArrayBuffer' in self
	};

	function isDataView(obj) {
	  return obj && DataView.prototype.isPrototypeOf(obj)
	}

	if (support.arrayBuffer) {
	  var viewClasses = [
	    '[object Int8Array]',
	    '[object Uint8Array]',
	    '[object Uint8ClampedArray]',
	    '[object Int16Array]',
	    '[object Uint16Array]',
	    '[object Int32Array]',
	    '[object Uint32Array]',
	    '[object Float32Array]',
	    '[object Float64Array]'
	  ];

	  var isArrayBufferView =
	    ArrayBuffer.isView ||
	    function(obj) {
	      return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
	    };
	}

	function normalizeName(name) {
	  if (typeof name !== 'string') {
	    name = String(name);
	  }
	  if (/[^a-z0-9\-#$%&'*+.^_`|~]/i.test(name)) {
	    throw new TypeError('Invalid character in header field name')
	  }
	  return name.toLowerCase()
	}

	function normalizeValue(value) {
	  if (typeof value !== 'string') {
	    value = String(value);
	  }
	  return value
	}

	// Build a destructive iterator for the value list
	function iteratorFor(items) {
	  var iterator = {
	    next: function() {
	      var value = items.shift();
	      return {done: value === undefined, value: value}
	    }
	  };

	  if (support.iterable) {
	    iterator[Symbol.iterator] = function() {
	      return iterator
	    };
	  }

	  return iterator
	}

	function Headers(headers) {
	  this.map = {};

	  if (headers instanceof Headers) {
	    headers.forEach(function(value, name) {
	      this.append(name, value);
	    }, this);
	  } else if (Array.isArray(headers)) {
	    headers.forEach(function(header) {
	      this.append(header[0], header[1]);
	    }, this);
	  } else if (headers) {
	    Object.getOwnPropertyNames(headers).forEach(function(name) {
	      this.append(name, headers[name]);
	    }, this);
	  }
	}

	Headers.prototype.append = function(name, value) {
	  name = normalizeName(name);
	  value = normalizeValue(value);
	  var oldValue = this.map[name];
	  this.map[name] = oldValue ? oldValue + ', ' + value : value;
	};

	Headers.prototype['delete'] = function(name) {
	  delete this.map[normalizeName(name)];
	};

	Headers.prototype.get = function(name) {
	  name = normalizeName(name);
	  return this.has(name) ? this.map[name] : null
	};

	Headers.prototype.has = function(name) {
	  return this.map.hasOwnProperty(normalizeName(name))
	};

	Headers.prototype.set = function(name, value) {
	  this.map[normalizeName(name)] = normalizeValue(value);
	};

	Headers.prototype.forEach = function(callback, thisArg) {
	  for (var name in this.map) {
	    if (this.map.hasOwnProperty(name)) {
	      callback.call(thisArg, this.map[name], name, this);
	    }
	  }
	};

	Headers.prototype.keys = function() {
	  var items = [];
	  this.forEach(function(value, name) {
	    items.push(name);
	  });
	  return iteratorFor(items)
	};

	Headers.prototype.values = function() {
	  var items = [];
	  this.forEach(function(value) {
	    items.push(value);
	  });
	  return iteratorFor(items)
	};

	Headers.prototype.entries = function() {
	  var items = [];
	  this.forEach(function(value, name) {
	    items.push([name, value]);
	  });
	  return iteratorFor(items)
	};

	if (support.iterable) {
	  Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
	}

	function consumed(body) {
	  if (body.bodyUsed) {
	    return Promise.reject(new TypeError('Already read'))
	  }
	  body.bodyUsed = true;
	}

	function fileReaderReady(reader) {
	  return new Promise(function(resolve, reject) {
	    reader.onload = function() {
	      resolve(reader.result);
	    };
	    reader.onerror = function() {
	      reject(reader.error);
	    };
	  })
	}

	function readBlobAsArrayBuffer(blob) {
	  var reader = new FileReader();
	  var promise = fileReaderReady(reader);
	  reader.readAsArrayBuffer(blob);
	  return promise
	}

	function readBlobAsText(blob) {
	  var reader = new FileReader();
	  var promise = fileReaderReady(reader);
	  reader.readAsText(blob);
	  return promise
	}

	function readArrayBufferAsText(buf) {
	  var view = new Uint8Array(buf);
	  var chars = new Array(view.length);

	  for (var i = 0; i < view.length; i++) {
	    chars[i] = String.fromCharCode(view[i]);
	  }
	  return chars.join('')
	}

	function bufferClone(buf) {
	  if (buf.slice) {
	    return buf.slice(0)
	  } else {
	    var view = new Uint8Array(buf.byteLength);
	    view.set(new Uint8Array(buf));
	    return view.buffer
	  }
	}

	function Body() {
	  this.bodyUsed = false;

	  this._initBody = function(body) {
	    this._bodyInit = body;
	    if (!body) {
	      this._bodyText = '';
	    } else if (typeof body === 'string') {
	      this._bodyText = body;
	    } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
	      this._bodyBlob = body;
	    } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
	      this._bodyFormData = body;
	    } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	      this._bodyText = body.toString();
	    } else if (support.arrayBuffer && support.blob && isDataView(body)) {
	      this._bodyArrayBuffer = bufferClone(body.buffer);
	      // IE 10-11 can't handle a DataView body.
	      this._bodyInit = new Blob([this._bodyArrayBuffer]);
	    } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
	      this._bodyArrayBuffer = bufferClone(body);
	    } else {
	      this._bodyText = body = Object.prototype.toString.call(body);
	    }

	    if (!this.headers.get('content-type')) {
	      if (typeof body === 'string') {
	        this.headers.set('content-type', 'text/plain;charset=UTF-8');
	      } else if (this._bodyBlob && this._bodyBlob.type) {
	        this.headers.set('content-type', this._bodyBlob.type);
	      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	        this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
	      }
	    }
	  };

	  if (support.blob) {
	    this.blob = function() {
	      var rejected = consumed(this);
	      if (rejected) {
	        return rejected
	      }

	      if (this._bodyBlob) {
	        return Promise.resolve(this._bodyBlob)
	      } else if (this._bodyArrayBuffer) {
	        return Promise.resolve(new Blob([this._bodyArrayBuffer]))
	      } else if (this._bodyFormData) {
	        throw new Error('could not read FormData body as blob')
	      } else {
	        return Promise.resolve(new Blob([this._bodyText]))
	      }
	    };

	    this.arrayBuffer = function() {
	      if (this._bodyArrayBuffer) {
	        return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
	      } else {
	        return this.blob().then(readBlobAsArrayBuffer)
	      }
	    };
	  }

	  this.text = function() {
	    var rejected = consumed(this);
	    if (rejected) {
	      return rejected
	    }

	    if (this._bodyBlob) {
	      return readBlobAsText(this._bodyBlob)
	    } else if (this._bodyArrayBuffer) {
	      return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
	    } else if (this._bodyFormData) {
	      throw new Error('could not read FormData body as text')
	    } else {
	      return Promise.resolve(this._bodyText)
	    }
	  };

	  if (support.formData) {
	    this.formData = function() {
	      return this.text().then(decode)
	    };
	  }

	  this.json = function() {
	    return this.text().then(JSON.parse)
	  };

	  return this
	}

	// HTTP methods whose capitalization should be normalized
	var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];

	function normalizeMethod(method) {
	  var upcased = method.toUpperCase();
	  return methods.indexOf(upcased) > -1 ? upcased : method
	}

	function Request(input, options) {
	  options = options || {};
	  var body = options.body;

	  if (input instanceof Request) {
	    if (input.bodyUsed) {
	      throw new TypeError('Already read')
	    }
	    this.url = input.url;
	    this.credentials = input.credentials;
	    if (!options.headers) {
	      this.headers = new Headers(input.headers);
	    }
	    this.method = input.method;
	    this.mode = input.mode;
	    this.signal = input.signal;
	    if (!body && input._bodyInit != null) {
	      body = input._bodyInit;
	      input.bodyUsed = true;
	    }
	  } else {
	    this.url = String(input);
	  }

	  this.credentials = options.credentials || this.credentials || 'same-origin';
	  if (options.headers || !this.headers) {
	    this.headers = new Headers(options.headers);
	  }
	  this.method = normalizeMethod(options.method || this.method || 'GET');
	  this.mode = options.mode || this.mode || null;
	  this.signal = options.signal || this.signal;
	  this.referrer = null;

	  if ((this.method === 'GET' || this.method === 'HEAD') && body) {
	    throw new TypeError('Body not allowed for GET or HEAD requests')
	  }
	  this._initBody(body);
	}

	Request.prototype.clone = function() {
	  return new Request(this, {body: this._bodyInit})
	};

	function decode(body) {
	  var form = new FormData();
	  body
	    .trim()
	    .split('&')
	    .forEach(function(bytes) {
	      if (bytes) {
	        var split = bytes.split('=');
	        var name = split.shift().replace(/\+/g, ' ');
	        var value = split.join('=').replace(/\+/g, ' ');
	        form.append(decodeURIComponent(name), decodeURIComponent(value));
	      }
	    });
	  return form
	}

	function parseHeaders(rawHeaders) {
	  var headers = new Headers();
	  // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
	  // https://tools.ietf.org/html/rfc7230#section-3.2
	  var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
	  preProcessedHeaders.split(/\r?\n/).forEach(function(line) {
	    var parts = line.split(':');
	    var key = parts.shift().trim();
	    if (key) {
	      var value = parts.join(':').trim();
	      headers.append(key, value);
	    }
	  });
	  return headers
	}

	Body.call(Request.prototype);

	function Response(bodyInit, options) {
	  if (!options) {
	    options = {};
	  }

	  this.type = 'default';
	  this.status = options.status === undefined ? 200 : options.status;
	  this.ok = this.status >= 200 && this.status < 300;
	  this.statusText = 'statusText' in options ? options.statusText : 'OK';
	  this.headers = new Headers(options.headers);
	  this.url = options.url || '';
	  this._initBody(bodyInit);
	}

	Body.call(Response.prototype);

	Response.prototype.clone = function() {
	  return new Response(this._bodyInit, {
	    status: this.status,
	    statusText: this.statusText,
	    headers: new Headers(this.headers),
	    url: this.url
	  })
	};

	Response.error = function() {
	  var response = new Response(null, {status: 0, statusText: ''});
	  response.type = 'error';
	  return response
	};

	var redirectStatuses = [301, 302, 303, 307, 308];

	Response.redirect = function(url, status) {
	  if (redirectStatuses.indexOf(status) === -1) {
	    throw new RangeError('Invalid status code')
	  }

	  return new Response(null, {status: status, headers: {location: url}})
	};

	var DOMException$1 = self.DOMException;
	try {
	  new DOMException$1();
	} catch (err) {
	  DOMException$1 = function(message, name) {
	    this.message = message;
	    this.name = name;
	    var error = Error(message);
	    this.stack = error.stack;
	  };
	  DOMException$1.prototype = Object.create(Error.prototype);
	  DOMException$1.prototype.constructor = DOMException$1;
	}

	function fetch(input, init) {
	  return new Promise(function(resolve, reject) {
	    var request = new Request(input, init);

	    if (request.signal && request.signal.aborted) {
	      return reject(new DOMException$1('Aborted', 'AbortError'))
	    }

	    var xhr = new XMLHttpRequest();

	    function abortXhr() {
	      xhr.abort();
	    }

	    xhr.onload = function() {
	      var options = {
	        status: xhr.status,
	        statusText: xhr.statusText,
	        headers: parseHeaders(xhr.getAllResponseHeaders() || '')
	      };
	      options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
	      var body = 'response' in xhr ? xhr.response : xhr.responseText;
	      resolve(new Response(body, options));
	    };

	    xhr.onerror = function() {
	      reject(new TypeError('Network request failed'));
	    };

	    xhr.ontimeout = function() {
	      reject(new TypeError('Network request failed'));
	    };

	    xhr.onabort = function() {
	      reject(new DOMException$1('Aborted', 'AbortError'));
	    };

	    xhr.open(request.method, request.url, true);

	    if (request.credentials === 'include') {
	      xhr.withCredentials = true;
	    } else if (request.credentials === 'omit') {
	      xhr.withCredentials = false;
	    }

	    if ('responseType' in xhr && support.blob) {
	      xhr.responseType = 'blob';
	    }

	    request.headers.forEach(function(value, name) {
	      xhr.setRequestHeader(name, value);
	    });

	    if (request.signal) {
	      request.signal.addEventListener('abort', abortXhr);

	      xhr.onreadystatechange = function() {
	        // DONE (success or failure)
	        if (xhr.readyState === 4) {
	          request.signal.removeEventListener('abort', abortXhr);
	        }
	      };
	    }

	    xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
	  })
	}

	fetch.polyfill = true;

	if (!self.fetch) {
	  self.fetch = fetch;
	  self.Headers = Headers;
	  self.Request = Request;
	  self.Response = Response;
	}

	/**
	 * Copyright 2016 Google Inc. All Rights Reserved.
	 *
	 * Licensed under the W3C SOFTWARE AND DOCUMENT NOTICE AND LICENSE.
	 *
	 *  https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
	 *
	 */
	(function() {

	// Exit early if we're not running in a browser.
	if (typeof window !== 'object') {
	  return;
	}

	// Exit early if all IntersectionObserver and IntersectionObserverEntry
	// features are natively supported.
	if ('IntersectionObserver' in window &&
	    'IntersectionObserverEntry' in window &&
	    'intersectionRatio' in window.IntersectionObserverEntry.prototype) {

	  // Minimal polyfill for Edge 15's lack of `isIntersecting`
	  // See: https://github.com/w3c/IntersectionObserver/issues/211
	  if (!('isIntersecting' in window.IntersectionObserverEntry.prototype)) {
	    Object.defineProperty(window.IntersectionObserverEntry.prototype,
	      'isIntersecting', {
	      get: function () {
	        return this.intersectionRatio > 0;
	      }
	    });
	  }
	  return;
	}


	/**
	 * A local reference to the document.
	 */
	var document = window.document;


	/**
	 * Creates the global IntersectionObserverEntry constructor.
	 * https://w3c.github.io/IntersectionObserver/#intersection-observer-entry
	 * @param {Object} entry A dictionary of instance properties.
	 * @constructor
	 */
	function IntersectionObserverEntry(entry) {
	  this.time = entry.time;
	  this.target = entry.target;
	  this.rootBounds = entry.rootBounds;
	  this.boundingClientRect = entry.boundingClientRect;
	  this.intersectionRect = entry.intersectionRect || getEmptyRect();
	  this.isIntersecting = !!entry.intersectionRect;

	  // Calculates the intersection ratio.
	  var targetRect = this.boundingClientRect;
	  var targetArea = targetRect.width * targetRect.height;
	  var intersectionRect = this.intersectionRect;
	  var intersectionArea = intersectionRect.width * intersectionRect.height;

	  // Sets intersection ratio.
	  if (targetArea) {
	    // Round the intersection ratio to avoid floating point math issues:
	    // https://github.com/w3c/IntersectionObserver/issues/324
	    this.intersectionRatio = Number((intersectionArea / targetArea).toFixed(4));
	  } else {
	    // If area is zero and is intersecting, sets to 1, otherwise to 0
	    this.intersectionRatio = this.isIntersecting ? 1 : 0;
	  }
	}


	/**
	 * Creates the global IntersectionObserver constructor.
	 * https://w3c.github.io/IntersectionObserver/#intersection-observer-interface
	 * @param {Function} callback The function to be invoked after intersection
	 *     changes have queued. The function is not invoked if the queue has
	 *     been emptied by calling the `takeRecords` method.
	 * @param {Object=} opt_options Optional configuration options.
	 * @constructor
	 */
	function IntersectionObserver(callback, opt_options) {

	  var options = opt_options || {};

	  if (typeof callback != 'function') {
	    throw new Error('callback must be a function');
	  }

	  if (options.root && options.root.nodeType != 1) {
	    throw new Error('root must be an Element');
	  }

	  // Binds and throttles `this._checkForIntersections`.
	  this._checkForIntersections = throttle(
	      this._checkForIntersections.bind(this), this.THROTTLE_TIMEOUT);

	  // Private properties.
	  this._callback = callback;
	  this._observationTargets = [];
	  this._queuedEntries = [];
	  this._rootMarginValues = this._parseRootMargin(options.rootMargin);

	  // Public properties.
	  this.thresholds = this._initThresholds(options.threshold);
	  this.root = options.root || null;
	  this.rootMargin = this._rootMarginValues.map(function(margin) {
	    return margin.value + margin.unit;
	  }).join(' ');
	}


	/**
	 * The minimum interval within which the document will be checked for
	 * intersection changes.
	 */
	IntersectionObserver.prototype.THROTTLE_TIMEOUT = 100;


	/**
	 * The frequency in which the polyfill polls for intersection changes.
	 * this can be updated on a per instance basis and must be set prior to
	 * calling `observe` on the first target.
	 */
	IntersectionObserver.prototype.POLL_INTERVAL = null;

	/**
	 * Use a mutation observer on the root element
	 * to detect intersection changes.
	 */
	IntersectionObserver.prototype.USE_MUTATION_OBSERVER = true;


	/**
	 * Starts observing a target element for intersection changes based on
	 * the thresholds values.
	 * @param {Element} target The DOM element to observe.
	 */
	IntersectionObserver.prototype.observe = function(target) {
	  var isTargetAlreadyObserved = this._observationTargets.some(function(item) {
	    return item.element == target;
	  });

	  if (isTargetAlreadyObserved) {
	    return;
	  }

	  if (!(target && target.nodeType == 1)) {
	    throw new Error('target must be an Element');
	  }

	  this._registerInstance();
	  this._observationTargets.push({element: target, entry: null});
	  this._monitorIntersections();
	  this._checkForIntersections();
	};


	/**
	 * Stops observing a target element for intersection changes.
	 * @param {Element} target The DOM element to observe.
	 */
	IntersectionObserver.prototype.unobserve = function(target) {
	  this._observationTargets =
	      this._observationTargets.filter(function(item) {

	    return item.element != target;
	  });
	  if (!this._observationTargets.length) {
	    this._unmonitorIntersections();
	    this._unregisterInstance();
	  }
	};


	/**
	 * Stops observing all target elements for intersection changes.
	 */
	IntersectionObserver.prototype.disconnect = function() {
	  this._observationTargets = [];
	  this._unmonitorIntersections();
	  this._unregisterInstance();
	};


	/**
	 * Returns any queue entries that have not yet been reported to the
	 * callback and clears the queue. This can be used in conjunction with the
	 * callback to obtain the absolute most up-to-date intersection information.
	 * @return {Array} The currently queued entries.
	 */
	IntersectionObserver.prototype.takeRecords = function() {
	  var records = this._queuedEntries.slice();
	  this._queuedEntries = [];
	  return records;
	};


	/**
	 * Accepts the threshold value from the user configuration object and
	 * returns a sorted array of unique threshold values. If a value is not
	 * between 0 and 1 and error is thrown.
	 * @private
	 * @param {Array|number=} opt_threshold An optional threshold value or
	 *     a list of threshold values, defaulting to [0].
	 * @return {Array} A sorted list of unique and valid threshold values.
	 */
	IntersectionObserver.prototype._initThresholds = function(opt_threshold) {
	  var threshold = opt_threshold || [0];
	  if (!Array.isArray(threshold)) threshold = [threshold];

	  return threshold.sort().filter(function(t, i, a) {
	    if (typeof t != 'number' || isNaN(t) || t < 0 || t > 1) {
	      throw new Error('threshold must be a number between 0 and 1 inclusively');
	    }
	    return t !== a[i - 1];
	  });
	};


	/**
	 * Accepts the rootMargin value from the user configuration object
	 * and returns an array of the four margin values as an object containing
	 * the value and unit properties. If any of the values are not properly
	 * formatted or use a unit other than px or %, and error is thrown.
	 * @private
	 * @param {string=} opt_rootMargin An optional rootMargin value,
	 *     defaulting to '0px'.
	 * @return {Array<Object>} An array of margin objects with the keys
	 *     value and unit.
	 */
	IntersectionObserver.prototype._parseRootMargin = function(opt_rootMargin) {
	  var marginString = opt_rootMargin || '0px';
	  var margins = marginString.split(/\s+/).map(function(margin) {
	    var parts = /^(-?\d*\.?\d+)(px|%)$/.exec(margin);
	    if (!parts) {
	      throw new Error('rootMargin must be specified in pixels or percent');
	    }
	    return {value: parseFloat(parts[1]), unit: parts[2]};
	  });

	  // Handles shorthand.
	  margins[1] = margins[1] || margins[0];
	  margins[2] = margins[2] || margins[0];
	  margins[3] = margins[3] || margins[1];

	  return margins;
	};


	/**
	 * Starts polling for intersection changes if the polling is not already
	 * happening, and if the page's visibility state is visible.
	 * @private
	 */
	IntersectionObserver.prototype._monitorIntersections = function() {
	  if (!this._monitoringIntersections) {
	    this._monitoringIntersections = true;

	    // If a poll interval is set, use polling instead of listening to
	    // resize and scroll events or DOM mutations.
	    if (this.POLL_INTERVAL) {
	      this._monitoringInterval = setInterval(
	          this._checkForIntersections, this.POLL_INTERVAL);
	    }
	    else {
	      addEvent(window, 'resize', this._checkForIntersections, true);
	      addEvent(document, 'scroll', this._checkForIntersections, true);

	      if (this.USE_MUTATION_OBSERVER && 'MutationObserver' in window) {
	        this._domObserver = new MutationObserver(this._checkForIntersections);
	        this._domObserver.observe(document, {
	          attributes: true,
	          childList: true,
	          characterData: true,
	          subtree: true
	        });
	      }
	    }
	  }
	};


	/**
	 * Stops polling for intersection changes.
	 * @private
	 */
	IntersectionObserver.prototype._unmonitorIntersections = function() {
	  if (this._monitoringIntersections) {
	    this._monitoringIntersections = false;

	    clearInterval(this._monitoringInterval);
	    this._monitoringInterval = null;

	    removeEvent(window, 'resize', this._checkForIntersections, true);
	    removeEvent(document, 'scroll', this._checkForIntersections, true);

	    if (this._domObserver) {
	      this._domObserver.disconnect();
	      this._domObserver = null;
	    }
	  }
	};


	/**
	 * Scans each observation target for intersection changes and adds them
	 * to the internal entries queue. If new entries are found, it
	 * schedules the callback to be invoked.
	 * @private
	 */
	IntersectionObserver.prototype._checkForIntersections = function() {
	  var rootIsInDom = this._rootIsInDom();
	  var rootRect = rootIsInDom ? this._getRootRect() : getEmptyRect();

	  this._observationTargets.forEach(function(item) {
	    var target = item.element;
	    var targetRect = getBoundingClientRect(target);
	    var rootContainsTarget = this._rootContainsTarget(target);
	    var oldEntry = item.entry;
	    var intersectionRect = rootIsInDom && rootContainsTarget &&
	        this._computeTargetAndRootIntersection(target, rootRect);

	    var newEntry = item.entry = new IntersectionObserverEntry({
	      time: now(),
	      target: target,
	      boundingClientRect: targetRect,
	      rootBounds: rootRect,
	      intersectionRect: intersectionRect
	    });

	    if (!oldEntry) {
	      this._queuedEntries.push(newEntry);
	    } else if (rootIsInDom && rootContainsTarget) {
	      // If the new entry intersection ratio has crossed any of the
	      // thresholds, add a new entry.
	      if (this._hasCrossedThreshold(oldEntry, newEntry)) {
	        this._queuedEntries.push(newEntry);
	      }
	    } else {
	      // If the root is not in the DOM or target is not contained within
	      // root but the previous entry for this target had an intersection,
	      // add a new record indicating removal.
	      if (oldEntry && oldEntry.isIntersecting) {
	        this._queuedEntries.push(newEntry);
	      }
	    }
	  }, this);

	  if (this._queuedEntries.length) {
	    this._callback(this.takeRecords(), this);
	  }
	};


	/**
	 * Accepts a target and root rect computes the intersection between then
	 * following the algorithm in the spec.
	 * TODO(philipwalton): at this time clip-path is not considered.
	 * https://w3c.github.io/IntersectionObserver/#calculate-intersection-rect-algo
	 * @param {Element} target The target DOM element
	 * @param {Object} rootRect The bounding rect of the root after being
	 *     expanded by the rootMargin value.
	 * @return {?Object} The final intersection rect object or undefined if no
	 *     intersection is found.
	 * @private
	 */
	IntersectionObserver.prototype._computeTargetAndRootIntersection =
	    function(target, rootRect) {

	  // If the element isn't displayed, an intersection can't happen.
	  if (window.getComputedStyle(target).display == 'none') return;

	  var targetRect = getBoundingClientRect(target);
	  var intersectionRect = targetRect;
	  var parent = getParentNode(target);
	  var atRoot = false;

	  while (!atRoot) {
	    var parentRect = null;
	    var parentComputedStyle = parent.nodeType == 1 ?
	        window.getComputedStyle(parent) : {};

	    // If the parent isn't displayed, an intersection can't happen.
	    if (parentComputedStyle.display == 'none') return;

	    if (parent == this.root || parent == document) {
	      atRoot = true;
	      parentRect = rootRect;
	    } else {
	      // If the element has a non-visible overflow, and it's not the <body>
	      // or <html> element, update the intersection rect.
	      // Note: <body> and <html> cannot be clipped to a rect that's not also
	      // the document rect, so no need to compute a new intersection.
	      if (parent != document.body &&
	          parent != document.documentElement &&
	          parentComputedStyle.overflow != 'visible') {
	        parentRect = getBoundingClientRect(parent);
	      }
	    }

	    // If either of the above conditionals set a new parentRect,
	    // calculate new intersection data.
	    if (parentRect) {
	      intersectionRect = computeRectIntersection(parentRect, intersectionRect);

	      if (!intersectionRect) break;
	    }
	    parent = getParentNode(parent);
	  }
	  return intersectionRect;
	};


	/**
	 * Returns the root rect after being expanded by the rootMargin value.
	 * @return {Object} The expanded root rect.
	 * @private
	 */
	IntersectionObserver.prototype._getRootRect = function() {
	  var rootRect;
	  if (this.root) {
	    rootRect = getBoundingClientRect(this.root);
	  } else {
	    // Use <html>/<body> instead of window since scroll bars affect size.
	    var html = document.documentElement;
	    var body = document.body;
	    rootRect = {
	      top: 0,
	      left: 0,
	      right: html.clientWidth || body.clientWidth,
	      width: html.clientWidth || body.clientWidth,
	      bottom: html.clientHeight || body.clientHeight,
	      height: html.clientHeight || body.clientHeight
	    };
	  }
	  return this._expandRectByRootMargin(rootRect);
	};


	/**
	 * Accepts a rect and expands it by the rootMargin value.
	 * @param {Object} rect The rect object to expand.
	 * @return {Object} The expanded rect.
	 * @private
	 */
	IntersectionObserver.prototype._expandRectByRootMargin = function(rect) {
	  var margins = this._rootMarginValues.map(function(margin, i) {
	    return margin.unit == 'px' ? margin.value :
	        margin.value * (i % 2 ? rect.width : rect.height) / 100;
	  });
	  var newRect = {
	    top: rect.top - margins[0],
	    right: rect.right + margins[1],
	    bottom: rect.bottom + margins[2],
	    left: rect.left - margins[3]
	  };
	  newRect.width = newRect.right - newRect.left;
	  newRect.height = newRect.bottom - newRect.top;

	  return newRect;
	};


	/**
	 * Accepts an old and new entry and returns true if at least one of the
	 * threshold values has been crossed.
	 * @param {?IntersectionObserverEntry} oldEntry The previous entry for a
	 *    particular target element or null if no previous entry exists.
	 * @param {IntersectionObserverEntry} newEntry The current entry for a
	 *    particular target element.
	 * @return {boolean} Returns true if a any threshold has been crossed.
	 * @private
	 */
	IntersectionObserver.prototype._hasCrossedThreshold =
	    function(oldEntry, newEntry) {

	  // To make comparing easier, an entry that has a ratio of 0
	  // but does not actually intersect is given a value of -1
	  var oldRatio = oldEntry && oldEntry.isIntersecting ?
	      oldEntry.intersectionRatio || 0 : -1;
	  var newRatio = newEntry.isIntersecting ?
	      newEntry.intersectionRatio || 0 : -1;

	  // Ignore unchanged ratios
	  if (oldRatio === newRatio) return;

	  for (var i = 0; i < this.thresholds.length; i++) {
	    var threshold = this.thresholds[i];

	    // Return true if an entry matches a threshold or if the new ratio
	    // and the old ratio are on the opposite sides of a threshold.
	    if (threshold == oldRatio || threshold == newRatio ||
	        threshold < oldRatio !== threshold < newRatio) {
	      return true;
	    }
	  }
	};


	/**
	 * Returns whether or not the root element is an element and is in the DOM.
	 * @return {boolean} True if the root element is an element and is in the DOM.
	 * @private
	 */
	IntersectionObserver.prototype._rootIsInDom = function() {
	  return !this.root || containsDeep(document, this.root);
	};


	/**
	 * Returns whether or not the target element is a child of root.
	 * @param {Element} target The target element to check.
	 * @return {boolean} True if the target element is a child of root.
	 * @private
	 */
	IntersectionObserver.prototype._rootContainsTarget = function(target) {
	  return containsDeep(this.root || document, target);
	};


	/**
	 * Adds the instance to the global IntersectionObserver registry if it isn't
	 * already present.
	 * @private
	 */
	IntersectionObserver.prototype._registerInstance = function() {
	};


	/**
	 * Removes the instance from the global IntersectionObserver registry.
	 * @private
	 */
	IntersectionObserver.prototype._unregisterInstance = function() {
	};


	/**
	 * Returns the result of the performance.now() method or null in browsers
	 * that don't support the API.
	 * @return {number} The elapsed time since the page was requested.
	 */
	function now() {
	  return window.performance && performance.now && performance.now();
	}


	/**
	 * Throttles a function and delays its execution, so it's only called at most
	 * once within a given time period.
	 * @param {Function} fn The function to throttle.
	 * @param {number} timeout The amount of time that must pass before the
	 *     function can be called again.
	 * @return {Function} The throttled function.
	 */
	function throttle(fn, timeout) {
	  var timer = null;
	  return function () {
	    if (!timer) {
	      timer = setTimeout(function() {
	        fn();
	        timer = null;
	      }, timeout);
	    }
	  };
	}


	/**
	 * Adds an event handler to a DOM node ensuring cross-browser compatibility.
	 * @param {Node} node The DOM node to add the event handler to.
	 * @param {string} event The event name.
	 * @param {Function} fn The event handler to add.
	 * @param {boolean} opt_useCapture Optionally adds the even to the capture
	 *     phase. Note: this only works in modern browsers.
	 */
	function addEvent(node, event, fn, opt_useCapture) {
	  if (typeof node.addEventListener == 'function') {
	    node.addEventListener(event, fn, opt_useCapture || false);
	  }
	  else if (typeof node.attachEvent == 'function') {
	    node.attachEvent('on' + event, fn);
	  }
	}


	/**
	 * Removes a previously added event handler from a DOM node.
	 * @param {Node} node The DOM node to remove the event handler from.
	 * @param {string} event The event name.
	 * @param {Function} fn The event handler to remove.
	 * @param {boolean} opt_useCapture If the event handler was added with this
	 *     flag set to true, it should be set to true here in order to remove it.
	 */
	function removeEvent(node, event, fn, opt_useCapture) {
	  if (typeof node.removeEventListener == 'function') {
	    node.removeEventListener(event, fn, opt_useCapture || false);
	  }
	  else if (typeof node.detatchEvent == 'function') {
	    node.detatchEvent('on' + event, fn);
	  }
	}


	/**
	 * Returns the intersection between two rect objects.
	 * @param {Object} rect1 The first rect.
	 * @param {Object} rect2 The second rect.
	 * @return {?Object} The intersection rect or undefined if no intersection
	 *     is found.
	 */
	function computeRectIntersection(rect1, rect2) {
	  var top = Math.max(rect1.top, rect2.top);
	  var bottom = Math.min(rect1.bottom, rect2.bottom);
	  var left = Math.max(rect1.left, rect2.left);
	  var right = Math.min(rect1.right, rect2.right);
	  var width = right - left;
	  var height = bottom - top;

	  return (width >= 0 && height >= 0) && {
	    top: top,
	    bottom: bottom,
	    left: left,
	    right: right,
	    width: width,
	    height: height
	  };
	}


	/**
	 * Shims the native getBoundingClientRect for compatibility with older IE.
	 * @param {Element} el The element whose bounding rect to get.
	 * @return {Object} The (possibly shimmed) rect of the element.
	 */
	function getBoundingClientRect(el) {
	  var rect;

	  try {
	    rect = el.getBoundingClientRect();
	  } catch (err) {
	    // Ignore Windows 7 IE11 "Unspecified error"
	    // https://github.com/w3c/IntersectionObserver/pull/205
	  }

	  if (!rect) return getEmptyRect();

	  // Older IE
	  if (!(rect.width && rect.height)) {
	    rect = {
	      top: rect.top,
	      right: rect.right,
	      bottom: rect.bottom,
	      left: rect.left,
	      width: rect.right - rect.left,
	      height: rect.bottom - rect.top
	    };
	  }
	  return rect;
	}


	/**
	 * Returns an empty rect object. An empty rect is returned when an element
	 * is not in the DOM.
	 * @return {Object} The empty rect.
	 */
	function getEmptyRect() {
	  return {
	    top: 0,
	    bottom: 0,
	    left: 0,
	    right: 0,
	    width: 0,
	    height: 0
	  };
	}

	/**
	 * Checks to see if a parent element contains a child element (including inside
	 * shadow DOM).
	 * @param {Node} parent The parent element.
	 * @param {Node} child The child element.
	 * @return {boolean} True if the parent node contains the child node.
	 */
	function containsDeep(parent, child) {
	  var node = child;
	  while (node) {
	    if (node == parent) return true;

	    node = getParentNode(node);
	  }
	  return false;
	}


	/**
	 * Gets the parent node of an element or its host element if the parent node
	 * is a shadow root.
	 * @param {Node} node The node whose parent to get.
	 * @return {Node|null} The parent node or null if no parent exists.
	 */
	function getParentNode(node) {
	  var parent = node.parentNode;

	  if (parent && parent.nodeType == 11 && parent.host) {
	    // If the parent is a shadow root, return the host element.
	    return parent.host;
	  }

	  if (parent && parent.assignedSlot) {
	    // If the parent is distributed in a <slot>, return the parent of a slot.
	    return parent.assignedSlot.parentNode;
	  }

	  return parent;
	}


	// Exposes the constructors globally.
	window.IntersectionObserver = IntersectionObserver;
	window.IntersectionObserverEntry = IntersectionObserverEntry;

	}());

	var EventListenerInterceptor = (function() {

	  if(typeof EventTarget === 'undefined') {
	    window.EventTarget = Node;
	  }

	  /**
	   * Event listener interceptor
	   */

	  var EventListenerInterceptor = {
	    interceptors: [] // { target: EventTarget, interceptors: [{ add: Function, remove: Function }, ...] }
	  };


	  /**
	   * Returns if exists a previously registered listener from a target and the normalized arguments
	   * @param target
	   * @param normalizedArguments
	   * @return {*}
	   */
	  EventListenerInterceptor.getRegisteredEventListener = function(target, normalizedArguments) {
	    var key = normalizedArguments.type + '-' + (normalizedArguments.options.capture ? '1' : '0');
	    if(
	      (target.__eventListeners !== void 0) &&
	      (target.__eventListeners[key] !== void 0)
	    ) {
	      var map = target.__eventListeners[key];
	      for(var i = 0; i < map.length; i++) {
	        if(map[i].listener === normalizedArguments.listener) {
	          return map[i];
	        }
	      }
	    }
	    return null;
	  };

	  /**
	   * Registers a listener on a target with some options
	   * @param target
	   * @param normalizedArguments
	   */
	  EventListenerInterceptor.registerEventListener = function(target, normalizedArguments) {
	    var key = normalizedArguments.type + '-' + (normalizedArguments.options.capture ? '1' : '0');

	    if(target.__eventListeners === void 0) {
	      target.__eventListeners = {};
	    }

	    if(target.__eventListeners[key] === void 0) {
	      target.__eventListeners[key] = [];
	    }

	    target.__eventListeners[key].push(normalizedArguments);
	  };

	  /**
	   * Unregisters a listener on a target with some options
	   * @param target
	   * @param normalizedArguments
	   */
	  EventListenerInterceptor.unregisterEventListener = function(target, normalizedArguments) {
	    var key = normalizedArguments.type + '-' + (normalizedArguments.options.capture ? '1' : '0');
	    if(
	      (target.__eventListeners !==  void 0) &&
	      (target.__eventListeners[key] !== void 0)
	    ) {
	      var map = target.__eventListeners[key];
	      for(var i = 0; i < map.length; i++) {
	        if(map[i].listener === normalizedArguments.listener) {
	          map.splice(i, 1);
	        }
	      }

	      if(map.length === 0) {
	        delete target.__eventListeners[key];
	      }
	    }
	  };



	  EventListenerInterceptor.normalizeListenerCallback = function(listener) {
	    if((typeof listener === 'function') || (listener === null) || (listener === void 0)) {
	      return listener;
	    } else if((typeof listener === 'object') && (typeof listener.handleEvent === 'function')) {
	      return listener.handleEvent;
	    } else {
	      // to support Symbol
	      return function(event) {
	        listener(event);
	      };
	    }
	  };

	  EventListenerInterceptor.normalizeListenerOptions = function(options) {
	    switch(typeof options) {
	      case 'boolean':
	        options = { capture: options };
	        break;
	      case 'undefined':
	        options = { capture: false };
	        break;
	      case 'object':
	        if (options === null) {
	          options = { capture: false };
	        }
	        break;
	      default:
	        throw new Error('Unsupported options type for addEventListener');
	    }

	    options.once      = Boolean(options.once);
	    options.passive   = Boolean(options.passive);
	    options.capture   = Boolean(options.capture);

	    return options;
	  };

	  EventListenerInterceptor.normalizeListenerArguments = function(type, listener, options) {
	    return {
	      type: type,
	      listener: this.normalizeListenerCallback(listener),
	      options: this.normalizeListenerOptions(options)
	    };
	  };



	  EventListenerInterceptor.intercept = function(target, interceptors) {
	    // get an interceptor with this target or null
	    var interceptor = null;
	    for (var i = 0; i < this.interceptors.length; i++) {
	      if(this.interceptors[i].target === target) {
	        interceptor = this.interceptors[i];
	      }
	    }

	    // if no interceptor already set
	    if (interceptor === null) {
	      interceptor = { target: target, interceptors: [interceptors] };
	      this.interceptors.push(interceptor);

	      this.interceptAddEventListener(target, interceptor);
	      this.interceptRemoveEventListener(target, interceptor);
	    } else { // if an interceptor already set, simply add interceptors to the list
	      interceptor.interceptors.push(interceptors);
	    }

	    // var release = function() {
	    //   target.prototype.addEventListener = addEventListener;
	    //   target.prototype.removeEventListener = removeEventListener;
	    // };
	    // this.interceptors.push(release);
	    // return release;
	  };

	  EventListenerInterceptor.interceptAddEventListener = function(target, interceptor) {
	    var _this = this;

	    var addEventListener = target.prototype.addEventListener;
	    target.prototype.addEventListener = function(type, listener, options) {
	      var normalizedArguments = _this.normalizeListenerArguments(type, listener, options);
	      var registeredEventListener = _this.getRegisteredEventListener(this, normalizedArguments);

	      if (!registeredEventListener) {

	        normalizedArguments.polyfilled = {
	          type: normalizedArguments.type,
	          listener: normalizedArguments.listener,
	          options: {
	            capture: normalizedArguments.options.capture,
	            once: normalizedArguments.options.once,
	            passive: normalizedArguments.options.passive
	          }
	        };

	        for (var i = 0; i < interceptor.interceptors.length; i++) {
	          var interceptors = interceptor.interceptors[i];
	          if (typeof interceptors.add === 'function') {
	            interceptors.add(normalizedArguments);
	          }
	        }

	        // console.log('normalizedArguments', normalizedArguments.polyfilled);

	        _this.registerEventListener(this, normalizedArguments);

	        addEventListener.call(
	          this,
	          normalizedArguments.polyfilled.type,
	          normalizedArguments.polyfilled.listener,
	          normalizedArguments.polyfilled.options
	        );
	      }
	    };

	    return function() {
	      target.prototype.addEventListener = addEventListener;
	    };
	  };

	  EventListenerInterceptor.interceptRemoveEventListener = function(target, interceptor) {
	    var _this = this;

	    var removeEventListener = target.prototype.removeEventListener;
	    target.prototype.removeEventListener = function(type, listener, options) {
	      var normalizedArguments = _this.normalizeListenerArguments(type, listener, options);
	      var registeredEventListener = _this.getRegisteredEventListener(this, normalizedArguments);

	      if (registeredEventListener) {
	        _this.unregisterEventListener(this, normalizedArguments);
	        removeEventListener.call(
	          this,
	          registeredEventListener.polyfilled.type,
	          registeredEventListener.polyfilled.listener,
	          registeredEventListener.polyfilled.options
	        );
	      } else {
	        removeEventListener.call(this, type, listener, options);
	      }
	    };

	    return function() {
	      target.prototype.removeEventListener = removeEventListener;
	    };
	  };

	  EventListenerInterceptor.interceptAll = function(interceptors) {
	    this.intercept(EventTarget, interceptors);
	    if(!(window instanceof EventTarget)) {
	      this.intercept(Window, interceptors);
	    }
	  };

	  EventListenerInterceptor.releaseAll = function() {
	    for(var i = 0, l = this.interceptors.length; i < l; i++) {
	      this.interceptors();
	    }
	  };


	  EventListenerInterceptor.error = function(error) {
	    // throw error;
	    console.error(error);
	  };

	  return EventListenerInterceptor;
	})();

	(function(EventListenerInterceptor) {
	  /**
	   * Event listener options support
	   */

	  EventListenerInterceptor.detectSupportedOptions = function() {
	    var _this = this;

	    this.supportedOptions = {
	      once: false,
	      passive: false,
	      capture: false,

	      all: false,
	      some: false
	    };

	    document.createDocumentFragment().addEventListener('test', function() {}, {
	      get once() {
	        _this.supportedOptions.once = true;
	        return false;
	      },
	      get passive() {
	        _this.supportedOptions.passive = true;
	        return false;
	      },
	      get capture() {
	        _this.supportedOptions.capture = true;
	        return false;
	      }
	    });

	    // useful shortcuts to detect if options are all/some supported
	    this.supportedOptions.all  = this.supportedOptions.once && this.supportedOptions.passive && this.supportedOptions.capture;
	    this.supportedOptions.some = this.supportedOptions.once || this.supportedOptions.passive || this.supportedOptions.capture;
	  };

	  EventListenerInterceptor.polyfillListenerOptions = function() {
	    this.detectSupportedOptions();
	    if (!this.supportedOptions.all) {
	      var _this = this;

	      this.interceptAll({
	        add: function(normalizedArguments) {
	          // console.log('intercepted', normalizedArguments);

	          var once = normalizedArguments.options.once && !_this.supportedOptions.once;
	          var passive = normalizedArguments.options.passive && !_this.supportedOptions.passive;

	          if (once || passive) {
	            var listener = normalizedArguments.polyfilled.listener;

	            normalizedArguments.polyfilled.listener = function(event) {
	              if(once) {
	                this.removeEventListener(normalizedArguments.type, normalizedArguments.listener, normalizedArguments.options);
	              }

	              if(passive) {
	                event.preventDefault = function() {
	                  throw new Error('Unable to preventDefault inside passive event listener invocation.');
	                };
	              }

	              return listener.call(this, event);
	            };
	          }

	          if (!_this.supportedOptions.some) {
	            normalizedArguments.polyfilled.options = normalizedArguments.options.capture;
	          }
	        }
	      });
	    }
	  };


	  EventListenerInterceptor.polyfillListenerOptions();


	  // var onclick = function() {
	  //   console.log('click');
	  // };

	  // document.body.addEventListener('click', onclick, false);
	  // document.body.addEventListener('click', onclick, { once: true });
	  // document.body.addEventListener('click', onclick, { once: true });
	  // document.body.addEventListener('click', onclick, false);
	  // document.body.addEventListener('click', onclick, false);

	})(EventListenerInterceptor);

	var smoothscroll = createCommonjsModule(function (module, exports) {
	/* smoothscroll v0.4.4 - 2019 - Dustan Kasten, Jeremias Menichelli - MIT License */
	(function () {

	  // polyfill
	  function polyfill() {
	    // aliases
	    var w = window;
	    var d = document;

	    // return if scroll behavior is supported and polyfill is not forced
	    if (
	      'scrollBehavior' in d.documentElement.style &&
	      w.__forceSmoothScrollPolyfill__ !== true
	    ) {
	      return;
	    }

	    // globals
	    var Element = w.HTMLElement || w.Element;
	    var SCROLL_TIME = 468;

	    // object gathering original scroll methods
	    var original = {
	      scroll: w.scroll || w.scrollTo,
	      scrollBy: w.scrollBy,
	      elementScroll: Element.prototype.scroll || scrollElement,
	      scrollIntoView: Element.prototype.scrollIntoView
	    };

	    // define timing method
	    var now =
	      w.performance && w.performance.now
	        ? w.performance.now.bind(w.performance)
	        : Date.now;

	    /**
	     * indicates if a the current browser is made by Microsoft
	     * @method isMicrosoftBrowser
	     * @param {String} userAgent
	     * @returns {Boolean}
	     */
	    function isMicrosoftBrowser(userAgent) {
	      var userAgentPatterns = ['MSIE ', 'Trident/', 'Edge/'];

	      return new RegExp(userAgentPatterns.join('|')).test(userAgent);
	    }

	    /*
	     * IE has rounding bug rounding down clientHeight and clientWidth and
	     * rounding up scrollHeight and scrollWidth causing false positives
	     * on hasScrollableSpace
	     */
	    var ROUNDING_TOLERANCE = isMicrosoftBrowser(w.navigator.userAgent) ? 1 : 0;

	    /**
	     * changes scroll position inside an element
	     * @method scrollElement
	     * @param {Number} x
	     * @param {Number} y
	     * @returns {undefined}
	     */
	    function scrollElement(x, y) {
	      this.scrollLeft = x;
	      this.scrollTop = y;
	    }

	    /**
	     * returns result of applying ease math function to a number
	     * @method ease
	     * @param {Number} k
	     * @returns {Number}
	     */
	    function ease(k) {
	      return 0.5 * (1 - Math.cos(Math.PI * k));
	    }

	    /**
	     * indicates if a smooth behavior should be applied
	     * @method shouldBailOut
	     * @param {Number|Object} firstArg
	     * @returns {Boolean}
	     */
	    function shouldBailOut(firstArg) {
	      if (
	        firstArg === null ||
	        typeof firstArg !== 'object' ||
	        firstArg.behavior === undefined ||
	        firstArg.behavior === 'auto' ||
	        firstArg.behavior === 'instant'
	      ) {
	        // first argument is not an object/null
	        // or behavior is auto, instant or undefined
	        return true;
	      }

	      if (typeof firstArg === 'object' && firstArg.behavior === 'smooth') {
	        // first argument is an object and behavior is smooth
	        return false;
	      }

	      // throw error when behavior is not supported
	      throw new TypeError(
	        'behavior member of ScrollOptions ' +
	          firstArg.behavior +
	          ' is not a valid value for enumeration ScrollBehavior.'
	      );
	    }

	    /**
	     * indicates if an element has scrollable space in the provided axis
	     * @method hasScrollableSpace
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function hasScrollableSpace(el, axis) {
	      if (axis === 'Y') {
	        return el.clientHeight + ROUNDING_TOLERANCE < el.scrollHeight;
	      }

	      if (axis === 'X') {
	        return el.clientWidth + ROUNDING_TOLERANCE < el.scrollWidth;
	      }
	    }

	    /**
	     * indicates if an element has a scrollable overflow property in the axis
	     * @method canOverflow
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function canOverflow(el, axis) {
	      var overflowValue = w.getComputedStyle(el, null)['overflow' + axis];

	      return overflowValue === 'auto' || overflowValue === 'scroll';
	    }

	    /**
	     * indicates if an element can be scrolled in either axis
	     * @method isScrollable
	     * @param {Node} el
	     * @param {String} axis
	     * @returns {Boolean}
	     */
	    function isScrollable(el) {
	      var isScrollableY = hasScrollableSpace(el, 'Y') && canOverflow(el, 'Y');
	      var isScrollableX = hasScrollableSpace(el, 'X') && canOverflow(el, 'X');

	      return isScrollableY || isScrollableX;
	    }

	    /**
	     * finds scrollable parent of an element
	     * @method findScrollableParent
	     * @param {Node} el
	     * @returns {Node} el
	     */
	    function findScrollableParent(el) {
	      while (el !== d.body && isScrollable(el) === false) {
	        el = el.parentNode || el.host;
	      }

	      return el;
	    }

	    /**
	     * self invoked function that, given a context, steps through scrolling
	     * @method step
	     * @param {Object} context
	     * @returns {undefined}
	     */
	    function step(context) {
	      var time = now();
	      var value;
	      var currentX;
	      var currentY;
	      var elapsed = (time - context.startTime) / SCROLL_TIME;

	      // avoid elapsed times higher than one
	      elapsed = elapsed > 1 ? 1 : elapsed;

	      // apply easing to elapsed time
	      value = ease(elapsed);

	      currentX = context.startX + (context.x - context.startX) * value;
	      currentY = context.startY + (context.y - context.startY) * value;

	      context.method.call(context.scrollable, currentX, currentY);

	      // scroll more if we have not reached our destination
	      if (currentX !== context.x || currentY !== context.y) {
	        w.requestAnimationFrame(step.bind(w, context));
	      }
	    }

	    /**
	     * scrolls window or element with a smooth behavior
	     * @method smoothScroll
	     * @param {Object|Node} el
	     * @param {Number} x
	     * @param {Number} y
	     * @returns {undefined}
	     */
	    function smoothScroll(el, x, y) {
	      var scrollable;
	      var startX;
	      var startY;
	      var method;
	      var startTime = now();

	      // define scroll context
	      if (el === d.body) {
	        scrollable = w;
	        startX = w.scrollX || w.pageXOffset;
	        startY = w.scrollY || w.pageYOffset;
	        method = original.scroll;
	      } else {
	        scrollable = el;
	        startX = el.scrollLeft;
	        startY = el.scrollTop;
	        method = scrollElement;
	      }

	      // scroll looping over a frame
	      step({
	        scrollable: scrollable,
	        method: method,
	        startTime: startTime,
	        startX: startX,
	        startY: startY,
	        x: x,
	        y: y
	      });
	    }

	    // ORIGINAL METHODS OVERRIDES
	    // w.scroll and w.scrollTo
	    w.scroll = w.scrollTo = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.scroll.call(
	          w,
	          arguments[0].left !== undefined
	            ? arguments[0].left
	            : typeof arguments[0] !== 'object'
	              ? arguments[0]
	              : w.scrollX || w.pageXOffset,
	          // use top prop, second argument if present or fallback to scrollY
	          arguments[0].top !== undefined
	            ? arguments[0].top
	            : arguments[1] !== undefined
	              ? arguments[1]
	              : w.scrollY || w.pageYOffset
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        w,
	        d.body,
	        arguments[0].left !== undefined
	          ? ~~arguments[0].left
	          : w.scrollX || w.pageXOffset,
	        arguments[0].top !== undefined
	          ? ~~arguments[0].top
	          : w.scrollY || w.pageYOffset
	      );
	    };

	    // w.scrollBy
	    w.scrollBy = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0])) {
	        original.scrollBy.call(
	          w,
	          arguments[0].left !== undefined
	            ? arguments[0].left
	            : typeof arguments[0] !== 'object' ? arguments[0] : 0,
	          arguments[0].top !== undefined
	            ? arguments[0].top
	            : arguments[1] !== undefined ? arguments[1] : 0
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        w,
	        d.body,
	        ~~arguments[0].left + (w.scrollX || w.pageXOffset),
	        ~~arguments[0].top + (w.scrollY || w.pageYOffset)
	      );
	    };

	    // Element.prototype.scroll and Element.prototype.scrollTo
	    Element.prototype.scroll = Element.prototype.scrollTo = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        // if one number is passed, throw error to match Firefox implementation
	        if (typeof arguments[0] === 'number' && arguments[1] === undefined) {
	          throw new SyntaxError('Value could not be converted');
	        }

	        original.elementScroll.call(
	          this,
	          // use left prop, first number argument or fallback to scrollLeft
	          arguments[0].left !== undefined
	            ? ~~arguments[0].left
	            : typeof arguments[0] !== 'object' ? ~~arguments[0] : this.scrollLeft,
	          // use top prop, second argument or fallback to scrollTop
	          arguments[0].top !== undefined
	            ? ~~arguments[0].top
	            : arguments[1] !== undefined ? ~~arguments[1] : this.scrollTop
	        );

	        return;
	      }

	      var left = arguments[0].left;
	      var top = arguments[0].top;

	      // LET THE SMOOTHNESS BEGIN!
	      smoothScroll.call(
	        this,
	        this,
	        typeof left === 'undefined' ? this.scrollLeft : ~~left,
	        typeof top === 'undefined' ? this.scrollTop : ~~top
	      );
	    };

	    // Element.prototype.scrollBy
	    Element.prototype.scrollBy = function() {
	      // avoid action when no arguments are passed
	      if (arguments[0] === undefined) {
	        return;
	      }

	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.elementScroll.call(
	          this,
	          arguments[0].left !== undefined
	            ? ~~arguments[0].left + this.scrollLeft
	            : ~~arguments[0] + this.scrollLeft,
	          arguments[0].top !== undefined
	            ? ~~arguments[0].top + this.scrollTop
	            : ~~arguments[1] + this.scrollTop
	        );

	        return;
	      }

	      this.scroll({
	        left: ~~arguments[0].left + this.scrollLeft,
	        top: ~~arguments[0].top + this.scrollTop,
	        behavior: arguments[0].behavior
	      });
	    };

	    // Element.prototype.scrollIntoView
	    Element.prototype.scrollIntoView = function() {
	      // avoid smooth behavior if not required
	      if (shouldBailOut(arguments[0]) === true) {
	        original.scrollIntoView.call(
	          this,
	          arguments[0] === undefined ? true : arguments[0]
	        );

	        return;
	      }

	      // LET THE SMOOTHNESS BEGIN!
	      var scrollableParent = findScrollableParent(this);
	      var parentRects = scrollableParent.getBoundingClientRect();
	      var clientRects = this.getBoundingClientRect();

	      if (scrollableParent !== d.body) {
	        // reveal element inside parent
	        smoothScroll.call(
	          this,
	          scrollableParent,
	          scrollableParent.scrollLeft + clientRects.left - parentRects.left,
	          scrollableParent.scrollTop + clientRects.top - parentRects.top
	        );

	        // reveal parent in viewport unless is fixed
	        if (w.getComputedStyle(scrollableParent).position !== 'fixed') {
	          w.scrollBy({
	            left: parentRects.left,
	            top: parentRects.top,
	            behavior: 'smooth'
	          });
	        }
	      } else {
	        // reveal element in viewport
	        w.scrollBy({
	          left: clientRects.left,
	          top: clientRects.top,
	          behavior: 'smooth'
	        });
	      }
	    };
	  }

	  {
	    // commonjs
	    module.exports = { polyfill: polyfill };
	  }

	}());
	});
	var smoothscroll_1 = smoothscroll.polyfill;

	// required to activate smoothscroll polyfill
	smoothscroll.polyfill();

})));
