
(function(l, r) { return; })(window.document);
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('jquery')) :
	typeof define === 'function' && define.amd ? define(['exports', 'jquery'], factory) :
	(global = global || self, factory(global.mwf = {}, global.jQuery));
}(this, (function (exports, $) { 'use strict';

	$ = $ && $.hasOwnProperty('default') ? $['default'] : $;

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var check = function (it) {
	  return it && it.Math == Math && it;
	};

	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global_1 =
	  // eslint-disable-next-line no-undef
	  check(typeof globalThis == 'object' && globalThis) ||
	  check(typeof window == 'object' && window) ||
	  check(typeof self == 'object' && self) ||
	  check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
	  // eslint-disable-next-line no-new-func
	  Function('return this')();

	var fails = function (exec) {
	  try {
	    return !!exec();
	  } catch (error) {
	    return true;
	  }
	};

	// Thank's IE8 for his funny defineProperty
	var descriptors = !fails(function () {
	  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
	});

	var nativePropertyIsEnumerable = {}.propertyIsEnumerable;
	var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

	// Nashorn ~ JDK8 bug
	var NASHORN_BUG = getOwnPropertyDescriptor && !nativePropertyIsEnumerable.call({ 1: 2 }, 1);

	// `Object.prototype.propertyIsEnumerable` method implementation
	// https://tc39.github.io/ecma262/#sec-object.prototype.propertyisenumerable
	var f = NASHORN_BUG ? function propertyIsEnumerable(V) {
	  var descriptor = getOwnPropertyDescriptor(this, V);
	  return !!descriptor && descriptor.enumerable;
	} : nativePropertyIsEnumerable;

	var objectPropertyIsEnumerable = {
		f: f
	};

	var createPropertyDescriptor = function (bitmap, value) {
	  return {
	    enumerable: !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable: !(bitmap & 4),
	    value: value
	  };
	};

	var toString = {}.toString;

	var classofRaw = function (it) {
	  return toString.call(it).slice(8, -1);
	};

	var split = ''.split;

	// fallback for non-array-like ES3 and non-enumerable old V8 strings
	var indexedObject = fails(function () {
	  // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
	  // eslint-disable-next-line no-prototype-builtins
	  return !Object('z').propertyIsEnumerable(0);
	}) ? function (it) {
	  return classofRaw(it) == 'String' ? split.call(it, '') : Object(it);
	} : Object;

	// `RequireObjectCoercible` abstract operation
	// https://tc39.github.io/ecma262/#sec-requireobjectcoercible
	var requireObjectCoercible = function (it) {
	  if (it == undefined) throw TypeError("Can't call method on " + it);
	  return it;
	};

	// toObject with fallback for non-array-like ES3 strings



	var toIndexedObject = function (it) {
	  return indexedObject(requireObjectCoercible(it));
	};

	var isObject = function (it) {
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};

	// `ToPrimitive` abstract operation
	// https://tc39.github.io/ecma262/#sec-toprimitive
	// instead of the ES6 spec version, we didn't implement @@toPrimitive case
	// and the second argument - flag - preferred type is a string
	var toPrimitive = function (input, PREFERRED_STRING) {
	  if (!isObject(input)) return input;
	  var fn, val;
	  if (PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
	  if (typeof (fn = input.valueOf) == 'function' && !isObject(val = fn.call(input))) return val;
	  if (!PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
	  throw TypeError("Can't convert object to primitive value");
	};

	var hasOwnProperty = {}.hasOwnProperty;

	var has = function (it, key) {
	  return hasOwnProperty.call(it, key);
	};

	var document$1 = global_1.document;
	// typeof document.createElement is 'object' in old IE
	var EXISTS = isObject(document$1) && isObject(document$1.createElement);

	var documentCreateElement = function (it) {
	  return EXISTS ? document$1.createElement(it) : {};
	};

	// Thank's IE8 for his funny defineProperty
	var ie8DomDefine = !descriptors && !fails(function () {
	  return Object.defineProperty(documentCreateElement('div'), 'a', {
	    get: function () { return 7; }
	  }).a != 7;
	});

	var nativeGetOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

	// `Object.getOwnPropertyDescriptor` method
	// https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptor
	var f$1 = descriptors ? nativeGetOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
	  O = toIndexedObject(O);
	  P = toPrimitive(P, true);
	  if (ie8DomDefine) try {
	    return nativeGetOwnPropertyDescriptor(O, P);
	  } catch (error) { /* empty */ }
	  if (has(O, P)) return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(O, P), O[P]);
	};

	var objectGetOwnPropertyDescriptor = {
		f: f$1
	};

	var anObject = function (it) {
	  if (!isObject(it)) {
	    throw TypeError(String(it) + ' is not an object');
	  } return it;
	};

	var nativeDefineProperty = Object.defineProperty;

	// `Object.defineProperty` method
	// https://tc39.github.io/ecma262/#sec-object.defineproperty
	var f$2 = descriptors ? nativeDefineProperty : function defineProperty(O, P, Attributes) {
	  anObject(O);
	  P = toPrimitive(P, true);
	  anObject(Attributes);
	  if (ie8DomDefine) try {
	    return nativeDefineProperty(O, P, Attributes);
	  } catch (error) { /* empty */ }
	  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
	  if ('value' in Attributes) O[P] = Attributes.value;
	  return O;
	};

	var objectDefineProperty = {
		f: f$2
	};

	var createNonEnumerableProperty = descriptors ? function (object, key, value) {
	  return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
	} : function (object, key, value) {
	  object[key] = value;
	  return object;
	};

	var setGlobal = function (key, value) {
	  try {
	    createNonEnumerableProperty(global_1, key, value);
	  } catch (error) {
	    global_1[key] = value;
	  } return value;
	};

	var SHARED = '__core-js_shared__';
	var store = global_1[SHARED] || setGlobal(SHARED, {});

	var sharedStore = store;

	var functionToString = Function.toString;

	// this helper broken in `3.4.1-3.4.4`, so we can't use `shared` helper
	if (typeof sharedStore.inspectSource != 'function') {
	  sharedStore.inspectSource = function (it) {
	    return functionToString.call(it);
	  };
	}

	var inspectSource = sharedStore.inspectSource;

	var WeakMap$1 = global_1.WeakMap;

	var nativeWeakMap = typeof WeakMap$1 === 'function' && /native code/.test(inspectSource(WeakMap$1));

	var shared = createCommonjsModule(function (module) {
	(module.exports = function (key, value) {
	  return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
	})('versions', []).push({
	  version: '3.6.4',
	  mode:  'global',
	  copyright: '© 2020 Denis Pushkarev (zloirock.ru)'
	});
	});

	var id = 0;
	var postfix = Math.random();

	var uid = function (key) {
	  return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
	};

	var keys = shared('keys');

	var sharedKey = function (key) {
	  return keys[key] || (keys[key] = uid(key));
	};

	var hiddenKeys = {};

	var WeakMap$2 = global_1.WeakMap;
	var set, get, has$1;

	var enforce = function (it) {
	  return has$1(it) ? get(it) : set(it, {});
	};

	var getterFor = function (TYPE) {
	  return function (it) {
	    var state;
	    if (!isObject(it) || (state = get(it)).type !== TYPE) {
	      throw TypeError('Incompatible receiver, ' + TYPE + ' required');
	    } return state;
	  };
	};

	if (nativeWeakMap) {
	  var store$1 = new WeakMap$2();
	  var wmget = store$1.get;
	  var wmhas = store$1.has;
	  var wmset = store$1.set;
	  set = function (it, metadata) {
	    wmset.call(store$1, it, metadata);
	    return metadata;
	  };
	  get = function (it) {
	    return wmget.call(store$1, it) || {};
	  };
	  has$1 = function (it) {
	    return wmhas.call(store$1, it);
	  };
	} else {
	  var STATE = sharedKey('state');
	  hiddenKeys[STATE] = true;
	  set = function (it, metadata) {
	    createNonEnumerableProperty(it, STATE, metadata);
	    return metadata;
	  };
	  get = function (it) {
	    return has(it, STATE) ? it[STATE] : {};
	  };
	  has$1 = function (it) {
	    return has(it, STATE);
	  };
	}

	var internalState = {
	  set: set,
	  get: get,
	  has: has$1,
	  enforce: enforce,
	  getterFor: getterFor
	};

	var redefine = createCommonjsModule(function (module) {
	var getInternalState = internalState.get;
	var enforceInternalState = internalState.enforce;
	var TEMPLATE = String(String).split('String');

	(module.exports = function (O, key, value, options) {
	  var unsafe = options ? !!options.unsafe : false;
	  var simple = options ? !!options.enumerable : false;
	  var noTargetGet = options ? !!options.noTargetGet : false;
	  if (typeof value == 'function') {
	    if (typeof key == 'string' && !has(value, 'name')) createNonEnumerableProperty(value, 'name', key);
	    enforceInternalState(value).source = TEMPLATE.join(typeof key == 'string' ? key : '');
	  }
	  if (O === global_1) {
	    if (simple) O[key] = value;
	    else setGlobal(key, value);
	    return;
	  } else if (!unsafe) {
	    delete O[key];
	  } else if (!noTargetGet && O[key]) {
	    simple = true;
	  }
	  if (simple) O[key] = value;
	  else createNonEnumerableProperty(O, key, value);
	// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
	})(Function.prototype, 'toString', function toString() {
	  return typeof this == 'function' && getInternalState(this).source || inspectSource(this);
	});
	});

	var path = global_1;

	var aFunction = function (variable) {
	  return typeof variable == 'function' ? variable : undefined;
	};

	var getBuiltIn = function (namespace, method) {
	  return arguments.length < 2 ? aFunction(path[namespace]) || aFunction(global_1[namespace])
	    : path[namespace] && path[namespace][method] || global_1[namespace] && global_1[namespace][method];
	};

	var ceil = Math.ceil;
	var floor = Math.floor;

	// `ToInteger` abstract operation
	// https://tc39.github.io/ecma262/#sec-tointeger
	var toInteger = function (argument) {
	  return isNaN(argument = +argument) ? 0 : (argument > 0 ? floor : ceil)(argument);
	};

	var min = Math.min;

	// `ToLength` abstract operation
	// https://tc39.github.io/ecma262/#sec-tolength
	var toLength = function (argument) {
	  return argument > 0 ? min(toInteger(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
	};

	var max = Math.max;
	var min$1 = Math.min;

	// Helper for a popular repeating case of the spec:
	// Let integer be ? ToInteger(index).
	// If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
	var toAbsoluteIndex = function (index, length) {
	  var integer = toInteger(index);
	  return integer < 0 ? max(integer + length, 0) : min$1(integer, length);
	};

	// `Array.prototype.{ indexOf, includes }` methods implementation
	var createMethod = function (IS_INCLUDES) {
	  return function ($this, el, fromIndex) {
	    var O = toIndexedObject($this);
	    var length = toLength(O.length);
	    var index = toAbsoluteIndex(fromIndex, length);
	    var value;
	    // Array#includes uses SameValueZero equality algorithm
	    // eslint-disable-next-line no-self-compare
	    if (IS_INCLUDES && el != el) while (length > index) {
	      value = O[index++];
	      // eslint-disable-next-line no-self-compare
	      if (value != value) return true;
	    // Array#indexOf ignores holes, Array#includes - not
	    } else for (;length > index; index++) {
	      if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
	    } return !IS_INCLUDES && -1;
	  };
	};

	var arrayIncludes = {
	  // `Array.prototype.includes` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.includes
	  includes: createMethod(true),
	  // `Array.prototype.indexOf` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.indexof
	  indexOf: createMethod(false)
	};

	var indexOf = arrayIncludes.indexOf;


	var objectKeysInternal = function (object, names) {
	  var O = toIndexedObject(object);
	  var i = 0;
	  var result = [];
	  var key;
	  for (key in O) !has(hiddenKeys, key) && has(O, key) && result.push(key);
	  // Don't enum bug & hidden keys
	  while (names.length > i) if (has(O, key = names[i++])) {
	    ~indexOf(result, key) || result.push(key);
	  }
	  return result;
	};

	// IE8- don't enum bug keys
	var enumBugKeys = [
	  'constructor',
	  'hasOwnProperty',
	  'isPrototypeOf',
	  'propertyIsEnumerable',
	  'toLocaleString',
	  'toString',
	  'valueOf'
	];

	var hiddenKeys$1 = enumBugKeys.concat('length', 'prototype');

	// `Object.getOwnPropertyNames` method
	// https://tc39.github.io/ecma262/#sec-object.getownpropertynames
	var f$3 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
	  return objectKeysInternal(O, hiddenKeys$1);
	};

	var objectGetOwnPropertyNames = {
		f: f$3
	};

	var f$4 = Object.getOwnPropertySymbols;

	var objectGetOwnPropertySymbols = {
		f: f$4
	};

	// all object keys, includes non-enumerable and symbols
	var ownKeys = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
	  var keys = objectGetOwnPropertyNames.f(anObject(it));
	  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
	  return getOwnPropertySymbols ? keys.concat(getOwnPropertySymbols(it)) : keys;
	};

	var copyConstructorProperties = function (target, source) {
	  var keys = ownKeys(source);
	  var defineProperty = objectDefineProperty.f;
	  var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
	  for (var i = 0; i < keys.length; i++) {
	    var key = keys[i];
	    if (!has(target, key)) defineProperty(target, key, getOwnPropertyDescriptor(source, key));
	  }
	};

	var replacement = /#|\.prototype\./;

	var isForced = function (feature, detection) {
	  var value = data[normalize(feature)];
	  return value == POLYFILL ? true
	    : value == NATIVE ? false
	    : typeof detection == 'function' ? fails(detection)
	    : !!detection;
	};

	var normalize = isForced.normalize = function (string) {
	  return String(string).replace(replacement, '.').toLowerCase();
	};

	var data = isForced.data = {};
	var NATIVE = isForced.NATIVE = 'N';
	var POLYFILL = isForced.POLYFILL = 'P';

	var isForced_1 = isForced;

	var getOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;






	/*
	  options.target      - name of the target object
	  options.global      - target is the global object
	  options.stat        - export as static methods of target
	  options.proto       - export as prototype methods of target
	  options.real        - real prototype method for the `pure` version
	  options.forced      - export even if the native feature is available
	  options.bind        - bind methods to the target, required for the `pure` version
	  options.wrap        - wrap constructors to preventing global pollution, required for the `pure` version
	  options.unsafe      - use the simple assignment of property instead of delete + defineProperty
	  options.sham        - add a flag to not completely full polyfills
	  options.enumerable  - export as enumerable property
	  options.noTargetGet - prevent calling a getter on target
	*/
	var _export = function (options, source) {
	  var TARGET = options.target;
	  var GLOBAL = options.global;
	  var STATIC = options.stat;
	  var FORCED, target, key, targetProperty, sourceProperty, descriptor;
	  if (GLOBAL) {
	    target = global_1;
	  } else if (STATIC) {
	    target = global_1[TARGET] || setGlobal(TARGET, {});
	  } else {
	    target = (global_1[TARGET] || {}).prototype;
	  }
	  if (target) for (key in source) {
	    sourceProperty = source[key];
	    if (options.noTargetGet) {
	      descriptor = getOwnPropertyDescriptor$1(target, key);
	      targetProperty = descriptor && descriptor.value;
	    } else targetProperty = target[key];
	    FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
	    // contained in target
	    if (!FORCED && targetProperty !== undefined) {
	      if (typeof sourceProperty === typeof targetProperty) continue;
	      copyConstructorProperties(sourceProperty, targetProperty);
	    }
	    // add a flag to not completely full polyfills
	    if (options.sham || (targetProperty && targetProperty.sham)) {
	      createNonEnumerableProperty(sourceProperty, 'sham', true);
	    }
	    // extend global
	    redefine(target, key, sourceProperty, options);
	  }
	};

	var arrayMethodIsStrict = function (METHOD_NAME, argument) {
	  var method = [][METHOD_NAME];
	  return !!method && fails(function () {
	    // eslint-disable-next-line no-useless-call,no-throw-literal
	    method.call(null, argument || function () { throw 1; }, 1);
	  });
	};

	var defineProperty = Object.defineProperty;
	var cache = {};

	var thrower = function (it) { throw it; };

	var arrayMethodUsesToLength = function (METHOD_NAME, options) {
	  if (has(cache, METHOD_NAME)) return cache[METHOD_NAME];
	  if (!options) options = {};
	  var method = [][METHOD_NAME];
	  var ACCESSORS = has(options, 'ACCESSORS') ? options.ACCESSORS : false;
	  var argument0 = has(options, 0) ? options[0] : thrower;
	  var argument1 = has(options, 1) ? options[1] : undefined;

	  return cache[METHOD_NAME] = !!method && !fails(function () {
	    if (ACCESSORS && !descriptors) return true;
	    var O = { length: -1 };

	    if (ACCESSORS) defineProperty(O, 1, { enumerable: true, get: thrower });
	    else O[1] = 1;

	    method.call(O, argument0, argument1);
	  });
	};

	var $indexOf = arrayIncludes.indexOf;



	var nativeIndexOf = [].indexOf;

	var NEGATIVE_ZERO = !!nativeIndexOf && 1 / [1].indexOf(1, -0) < 0;
	var STRICT_METHOD = arrayMethodIsStrict('indexOf');
	var USES_TO_LENGTH = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

	// `Array.prototype.indexOf` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.indexof
	_export({ target: 'Array', proto: true, forced: NEGATIVE_ZERO || !STRICT_METHOD || !USES_TO_LENGTH }, {
	  indexOf: function indexOf(searchElement /* , fromIndex = 0 */) {
	    return NEGATIVE_ZERO
	      // convert -0 to +0
	      ? nativeIndexOf.apply(this, arguments) || 0
	      : $indexOf(this, searchElement, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	// `ToObject` abstract operation
	// https://tc39.github.io/ecma262/#sec-toobject
	var toObject = function (argument) {
	  return Object(requireObjectCoercible(argument));
	};

	// `IsArray` abstract operation
	// https://tc39.github.io/ecma262/#sec-isarray
	var isArray = Array.isArray || function isArray(arg) {
	  return classofRaw(arg) == 'Array';
	};

	var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
	  // Chrome 38 Symbol has incorrect toString conversion
	  // eslint-disable-next-line no-undef
	  return !String(Symbol());
	});

	var useSymbolAsUid = nativeSymbol
	  // eslint-disable-next-line no-undef
	  && !Symbol.sham
	  // eslint-disable-next-line no-undef
	  && typeof Symbol.iterator == 'symbol';

	var WellKnownSymbolsStore = shared('wks');
	var Symbol$1 = global_1.Symbol;
	var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

	var wellKnownSymbol = function (name) {
	  if (!has(WellKnownSymbolsStore, name)) {
	    if (nativeSymbol && has(Symbol$1, name)) WellKnownSymbolsStore[name] = Symbol$1[name];
	    else WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
	  } return WellKnownSymbolsStore[name];
	};

	var SPECIES = wellKnownSymbol('species');

	// `ArraySpeciesCreate` abstract operation
	// https://tc39.github.io/ecma262/#sec-arrayspeciescreate
	var arraySpeciesCreate = function (originalArray, length) {
	  var C;
	  if (isArray(originalArray)) {
	    C = originalArray.constructor;
	    // cross-realm fallback
	    if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;
	    else if (isObject(C)) {
	      C = C[SPECIES];
	      if (C === null) C = undefined;
	    }
	  } return new (C === undefined ? Array : C)(length === 0 ? 0 : length);
	};

	var createProperty = function (object, key, value) {
	  var propertyKey = toPrimitive(key);
	  if (propertyKey in object) objectDefineProperty.f(object, propertyKey, createPropertyDescriptor(0, value));
	  else object[propertyKey] = value;
	};

	var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

	var process = global_1.process;
	var versions = process && process.versions;
	var v8 = versions && versions.v8;
	var match, version;

	if (v8) {
	  match = v8.split('.');
	  version = match[0] + match[1];
	} else if (engineUserAgent) {
	  match = engineUserAgent.match(/Edge\/(\d+)/);
	  if (!match || match[1] >= 74) {
	    match = engineUserAgent.match(/Chrome\/(\d+)/);
	    if (match) version = match[1];
	  }
	}

	var engineV8Version = version && +version;

	var SPECIES$1 = wellKnownSymbol('species');

	var arrayMethodHasSpeciesSupport = function (METHOD_NAME) {
	  // We can't use this feature detection in V8 since it causes
	  // deoptimization and serious performance degradation
	  // https://github.com/zloirock/core-js/issues/677
	  return engineV8Version >= 51 || !fails(function () {
	    var array = [];
	    var constructor = array.constructor = {};
	    constructor[SPECIES$1] = function () {
	      return { foo: 1 };
	    };
	    return array[METHOD_NAME](Boolean).foo !== 1;
	  });
	};

	var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('splice');
	var USES_TO_LENGTH$1 = arrayMethodUsesToLength('splice', { ACCESSORS: true, 0: 0, 1: 2 });

	var max$1 = Math.max;
	var min$2 = Math.min;
	var MAX_SAFE_INTEGER = 0x1FFFFFFFFFFFFF;
	var MAXIMUM_ALLOWED_LENGTH_EXCEEDED = 'Maximum allowed length exceeded';

	// `Array.prototype.splice` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.splice
	// with adding support of @@species
	_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH$1 }, {
	  splice: function splice(start, deleteCount /* , ...items */) {
	    var O = toObject(this);
	    var len = toLength(O.length);
	    var actualStart = toAbsoluteIndex(start, len);
	    var argumentsLength = arguments.length;
	    var insertCount, actualDeleteCount, A, k, from, to;
	    if (argumentsLength === 0) {
	      insertCount = actualDeleteCount = 0;
	    } else if (argumentsLength === 1) {
	      insertCount = 0;
	      actualDeleteCount = len - actualStart;
	    } else {
	      insertCount = argumentsLength - 2;
	      actualDeleteCount = min$2(max$1(toInteger(deleteCount), 0), len - actualStart);
	    }
	    if (len + insertCount - actualDeleteCount > MAX_SAFE_INTEGER) {
	      throw TypeError(MAXIMUM_ALLOWED_LENGTH_EXCEEDED);
	    }
	    A = arraySpeciesCreate(O, actualDeleteCount);
	    for (k = 0; k < actualDeleteCount; k++) {
	      from = actualStart + k;
	      if (from in O) createProperty(A, k, O[from]);
	    }
	    A.length = actualDeleteCount;
	    if (insertCount < actualDeleteCount) {
	      for (k = actualStart; k < len - actualDeleteCount; k++) {
	        from = k + actualDeleteCount;
	        to = k + insertCount;
	        if (from in O) O[to] = O[from];
	        else delete O[to];
	      }
	      for (k = len; k > len - actualDeleteCount + insertCount; k--) delete O[k - 1];
	    } else if (insertCount > actualDeleteCount) {
	      for (k = len - actualDeleteCount; k > actualStart; k--) {
	        from = k + actualDeleteCount - 1;
	        to = k + insertCount - 1;
	        if (from in O) O[to] = O[from];
	        else delete O[to];
	      }
	    }
	    for (k = 0; k < insertCount; k++) {
	      O[k + actualStart] = arguments[k + 2];
	    }
	    O.length = len - actualDeleteCount + insertCount;
	    return A;
	  }
	});

	// `Object.keys` method
	// https://tc39.github.io/ecma262/#sec-object.keys
	var objectKeys = Object.keys || function keys(O) {
	  return objectKeysInternal(O, enumBugKeys);
	};

	var nativeAssign = Object.assign;
	var defineProperty$1 = Object.defineProperty;

	// `Object.assign` method
	// https://tc39.github.io/ecma262/#sec-object.assign
	var objectAssign = !nativeAssign || fails(function () {
	  // should have correct order of operations (Edge bug)
	  if (descriptors && nativeAssign({ b: 1 }, nativeAssign(defineProperty$1({}, 'a', {
	    enumerable: true,
	    get: function () {
	      defineProperty$1(this, 'b', {
	        value: 3,
	        enumerable: false
	      });
	    }
	  }), { b: 2 })).b !== 1) return true;
	  // should work with symbols and should have deterministic property order (V8 bug)
	  var A = {};
	  var B = {};
	  // eslint-disable-next-line no-undef
	  var symbol = Symbol();
	  var alphabet = 'abcdefghijklmnopqrst';
	  A[symbol] = 7;
	  alphabet.split('').forEach(function (chr) { B[chr] = chr; });
	  return nativeAssign({}, A)[symbol] != 7 || objectKeys(nativeAssign({}, B)).join('') != alphabet;
	}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
	  var T = toObject(target);
	  var argumentsLength = arguments.length;
	  var index = 1;
	  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
	  var propertyIsEnumerable = objectPropertyIsEnumerable.f;
	  while (argumentsLength > index) {
	    var S = indexedObject(arguments[index++]);
	    var keys = getOwnPropertySymbols ? objectKeys(S).concat(getOwnPropertySymbols(S)) : objectKeys(S);
	    var length = keys.length;
	    var j = 0;
	    var key;
	    while (length > j) {
	      key = keys[j++];
	      if (!descriptors || propertyIsEnumerable.call(S, key)) T[key] = S[key];
	    }
	  } return T;
	} : nativeAssign;

	// `Object.assign` method
	// https://tc39.github.io/ecma262/#sec-object.assign
	_export({ target: 'Object', stat: true, forced: Object.assign !== objectAssign }, {
	  assign: objectAssign
	});

	var TO_STRING_TAG = wellKnownSymbol('toStringTag');
	var test = {};

	test[TO_STRING_TAG] = 'z';

	var toStringTagSupport = String(test) === '[object z]';

	var TO_STRING_TAG$1 = wellKnownSymbol('toStringTag');
	// ES3 wrong here
	var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) == 'Arguments';

	// fallback for IE11 Script Access Denied error
	var tryGet = function (it, key) {
	  try {
	    return it[key];
	  } catch (error) { /* empty */ }
	};

	// getting tag from ES6+ `Object.prototype.toString`
	var classof = toStringTagSupport ? classofRaw : function (it) {
	  var O, tag, result;
	  return it === undefined ? 'Undefined' : it === null ? 'Null'
	    // @@toStringTag case
	    : typeof (tag = tryGet(O = Object(it), TO_STRING_TAG$1)) == 'string' ? tag
	    // builtinTag case
	    : CORRECT_ARGUMENTS ? classofRaw(O)
	    // ES3 arguments fallback
	    : (result = classofRaw(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : result;
	};

	// `Object.prototype.toString` method implementation
	// https://tc39.github.io/ecma262/#sec-object.prototype.tostring
	var objectToString = toStringTagSupport ? {}.toString : function toString() {
	  return '[object ' + classof(this) + ']';
	};

	// `Object.prototype.toString` method
	// https://tc39.github.io/ecma262/#sec-object.prototype.tostring
	if (!toStringTagSupport) {
	  redefine(Object.prototype, 'toString', objectToString, { unsafe: true });
	}

	// a string of all valid unicode whitespaces
	// eslint-disable-next-line max-len
	var whitespaces = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

	var whitespace = '[' + whitespaces + ']';
	var ltrim = RegExp('^' + whitespace + whitespace + '*');
	var rtrim = RegExp(whitespace + whitespace + '*$');

	// `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation
	var createMethod$1 = function (TYPE) {
	  return function ($this) {
	    var string = String(requireObjectCoercible($this));
	    if (TYPE & 1) string = string.replace(ltrim, '');
	    if (TYPE & 2) string = string.replace(rtrim, '');
	    return string;
	  };
	};

	var stringTrim = {
	  // `String.prototype.{ trimLeft, trimStart }` methods
	  // https://tc39.github.io/ecma262/#sec-string.prototype.trimstart
	  start: createMethod$1(1),
	  // `String.prototype.{ trimRight, trimEnd }` methods
	  // https://tc39.github.io/ecma262/#sec-string.prototype.trimend
	  end: createMethod$1(2),
	  // `String.prototype.trim` method
	  // https://tc39.github.io/ecma262/#sec-string.prototype.trim
	  trim: createMethod$1(3)
	};

	var trim = stringTrim.trim;


	var $parseFloat = global_1.parseFloat;
	var FORCED = 1 / $parseFloat(whitespaces + '-0') !== -Infinity;

	// `parseFloat` method
	// https://tc39.github.io/ecma262/#sec-parsefloat-string
	var numberParseFloat = FORCED ? function parseFloat(string) {
	  var trimmedString = trim(String(string));
	  var result = $parseFloat(trimmedString);
	  return result === 0 && trimmedString.charAt(0) == '-' ? -0 : result;
	} : $parseFloat;

	// `parseFloat` method
	// https://tc39.github.io/ecma262/#sec-parsefloat-string
	_export({ global: true, forced: parseFloat != numberParseFloat }, {
	  parseFloat: numberParseFloat
	});

	var aPossiblePrototype = function (it) {
	  if (!isObject(it) && it !== null) {
	    throw TypeError("Can't set " + String(it) + ' as a prototype');
	  } return it;
	};

	// `Object.setPrototypeOf` method
	// https://tc39.github.io/ecma262/#sec-object.setprototypeof
	// Works with __proto__ only. Old v8 can't work with null proto objects.
	/* eslint-disable no-proto */
	var objectSetPrototypeOf = Object.setPrototypeOf || ('__proto__' in {} ? function () {
	  var CORRECT_SETTER = false;
	  var test = {};
	  var setter;
	  try {
	    setter = Object.getOwnPropertyDescriptor(Object.prototype, '__proto__').set;
	    setter.call(test, []);
	    CORRECT_SETTER = test instanceof Array;
	  } catch (error) { /* empty */ }
	  return function setPrototypeOf(O, proto) {
	    anObject(O);
	    aPossiblePrototype(proto);
	    if (CORRECT_SETTER) setter.call(O, proto);
	    else O.__proto__ = proto;
	    return O;
	  };
	}() : undefined);

	// makes subclassing work correct for wrapped built-ins
	var inheritIfRequired = function ($this, dummy, Wrapper) {
	  var NewTarget, NewTargetPrototype;
	  if (
	    // it can work only with native `setPrototypeOf`
	    objectSetPrototypeOf &&
	    // we haven't completely correct pre-ES6 way for getting `new.target`, so use this
	    typeof (NewTarget = dummy.constructor) == 'function' &&
	    NewTarget !== Wrapper &&
	    isObject(NewTargetPrototype = NewTarget.prototype) &&
	    NewTargetPrototype !== Wrapper.prototype
	  ) objectSetPrototypeOf($this, NewTargetPrototype);
	  return $this;
	};

	var MATCH = wellKnownSymbol('match');

	// `IsRegExp` abstract operation
	// https://tc39.github.io/ecma262/#sec-isregexp
	var isRegexp = function (it) {
	  var isRegExp;
	  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : classofRaw(it) == 'RegExp');
	};

	// `RegExp.prototype.flags` getter implementation
	// https://tc39.github.io/ecma262/#sec-get-regexp.prototype.flags
	var regexpFlags = function () {
	  var that = anObject(this);
	  var result = '';
	  if (that.global) result += 'g';
	  if (that.ignoreCase) result += 'i';
	  if (that.multiline) result += 'm';
	  if (that.dotAll) result += 's';
	  if (that.unicode) result += 'u';
	  if (that.sticky) result += 'y';
	  return result;
	};

	// babel-minify transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError,
	// so we use an intermediate function.
	function RE(s, f) {
	  return RegExp(s, f);
	}

	var UNSUPPORTED_Y = fails(function () {
	  // babel-minify transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError
	  var re = RE('a', 'y');
	  re.lastIndex = 2;
	  return re.exec('abcd') != null;
	});

	var BROKEN_CARET = fails(function () {
	  // https://bugzilla.mozilla.org/show_bug.cgi?id=773687
	  var re = RE('^r', 'gy');
	  re.lastIndex = 2;
	  return re.exec('str') != null;
	});

	var regexpStickyHelpers = {
		UNSUPPORTED_Y: UNSUPPORTED_Y,
		BROKEN_CARET: BROKEN_CARET
	};

	var SPECIES$2 = wellKnownSymbol('species');

	var setSpecies = function (CONSTRUCTOR_NAME) {
	  var Constructor = getBuiltIn(CONSTRUCTOR_NAME);
	  var defineProperty = objectDefineProperty.f;

	  if (descriptors && Constructor && !Constructor[SPECIES$2]) {
	    defineProperty(Constructor, SPECIES$2, {
	      configurable: true,
	      get: function () { return this; }
	    });
	  }
	};

	var defineProperty$2 = objectDefineProperty.f;
	var getOwnPropertyNames = objectGetOwnPropertyNames.f;





	var setInternalState = internalState.set;



	var MATCH$1 = wellKnownSymbol('match');
	var NativeRegExp = global_1.RegExp;
	var RegExpPrototype = NativeRegExp.prototype;
	var re1 = /a/g;
	var re2 = /a/g;

	// "new" should create a new object, old webkit bug
	var CORRECT_NEW = new NativeRegExp(re1) !== re1;

	var UNSUPPORTED_Y$1 = regexpStickyHelpers.UNSUPPORTED_Y;

	var FORCED$1 = descriptors && isForced_1('RegExp', (!CORRECT_NEW || UNSUPPORTED_Y$1 || fails(function () {
	  re2[MATCH$1] = false;
	  // RegExp constructor can alter flags and IsRegExp works correct with @@match
	  return NativeRegExp(re1) != re1 || NativeRegExp(re2) == re2 || NativeRegExp(re1, 'i') != '/a/i';
	})));

	// `RegExp` constructor
	// https://tc39.github.io/ecma262/#sec-regexp-constructor
	if (FORCED$1) {
	  var RegExpWrapper = function RegExp(pattern, flags) {
	    var thisIsRegExp = this instanceof RegExpWrapper;
	    var patternIsRegExp = isRegexp(pattern);
	    var flagsAreUndefined = flags === undefined;
	    var sticky;

	    if (!thisIsRegExp && patternIsRegExp && pattern.constructor === RegExpWrapper && flagsAreUndefined) {
	      return pattern;
	    }

	    if (CORRECT_NEW) {
	      if (patternIsRegExp && !flagsAreUndefined) pattern = pattern.source;
	    } else if (pattern instanceof RegExpWrapper) {
	      if (flagsAreUndefined) flags = regexpFlags.call(pattern);
	      pattern = pattern.source;
	    }

	    if (UNSUPPORTED_Y$1) {
	      sticky = !!flags && flags.indexOf('y') > -1;
	      if (sticky) flags = flags.replace(/y/g, '');
	    }

	    var result = inheritIfRequired(
	      CORRECT_NEW ? new NativeRegExp(pattern, flags) : NativeRegExp(pattern, flags),
	      thisIsRegExp ? this : RegExpPrototype,
	      RegExpWrapper
	    );

	    if (UNSUPPORTED_Y$1 && sticky) setInternalState(result, { sticky: sticky });

	    return result;
	  };
	  var proxy = function (key) {
	    key in RegExpWrapper || defineProperty$2(RegExpWrapper, key, {
	      configurable: true,
	      get: function () { return NativeRegExp[key]; },
	      set: function (it) { NativeRegExp[key] = it; }
	    });
	  };
	  var keys$1 = getOwnPropertyNames(NativeRegExp);
	  var index = 0;
	  while (keys$1.length > index) proxy(keys$1[index++]);
	  RegExpPrototype.constructor = RegExpWrapper;
	  RegExpWrapper.prototype = RegExpPrototype;
	  redefine(global_1, 'RegExp', RegExpWrapper);
	}

	// https://tc39.github.io/ecma262/#sec-get-regexp-@@species
	setSpecies('RegExp');

	var nativeExec = RegExp.prototype.exec;
	// This always refers to the native implementation, because the
	// String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
	// which loads this file before patching the method.
	var nativeReplace = String.prototype.replace;

	var patchedExec = nativeExec;

	var UPDATES_LAST_INDEX_WRONG = (function () {
	  var re1 = /a/;
	  var re2 = /b*/g;
	  nativeExec.call(re1, 'a');
	  nativeExec.call(re2, 'a');
	  return re1.lastIndex !== 0 || re2.lastIndex !== 0;
	})();

	var UNSUPPORTED_Y$2 = regexpStickyHelpers.UNSUPPORTED_Y || regexpStickyHelpers.BROKEN_CARET;

	// nonparticipating capturing group, copied from es5-shim's String#split patch.
	var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

	var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y$2;

	if (PATCH) {
	  patchedExec = function exec(str) {
	    var re = this;
	    var lastIndex, reCopy, match, i;
	    var sticky = UNSUPPORTED_Y$2 && re.sticky;
	    var flags = regexpFlags.call(re);
	    var source = re.source;
	    var charsAdded = 0;
	    var strCopy = str;

	    if (sticky) {
	      flags = flags.replace('y', '');
	      if (flags.indexOf('g') === -1) {
	        flags += 'g';
	      }

	      strCopy = String(str).slice(re.lastIndex);
	      // Support anchored sticky behavior.
	      if (re.lastIndex > 0 && (!re.multiline || re.multiline && str[re.lastIndex - 1] !== '\n')) {
	        source = '(?: ' + source + ')';
	        strCopy = ' ' + strCopy;
	        charsAdded++;
	      }
	      // ^(? + rx + ) is needed, in combination with some str slicing, to
	      // simulate the 'y' flag.
	      reCopy = new RegExp('^(?:' + source + ')', flags);
	    }

	    if (NPCG_INCLUDED) {
	      reCopy = new RegExp('^' + source + '$(?!\\s)', flags);
	    }
	    if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;

	    match = nativeExec.call(sticky ? reCopy : re, strCopy);

	    if (sticky) {
	      if (match) {
	        match.input = match.input.slice(charsAdded);
	        match[0] = match[0].slice(charsAdded);
	        match.index = re.lastIndex;
	        re.lastIndex += match[0].length;
	      } else re.lastIndex = 0;
	    } else if (UPDATES_LAST_INDEX_WRONG && match) {
	      re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
	    }
	    if (NPCG_INCLUDED && match && match.length > 1) {
	      // Fix browsers whose `exec` methods don't consistently return `undefined`
	      // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
	      nativeReplace.call(match[0], reCopy, function () {
	        for (i = 1; i < arguments.length - 2; i++) {
	          if (arguments[i] === undefined) match[i] = undefined;
	        }
	      });
	    }

	    return match;
	  };
	}

	var regexpExec = patchedExec;

	_export({ target: 'RegExp', proto: true, forced: /./.exec !== regexpExec }, {
	  exec: regexpExec
	});

	var TO_STRING = 'toString';
	var RegExpPrototype$1 = RegExp.prototype;
	var nativeToString = RegExpPrototype$1[TO_STRING];

	var NOT_GENERIC = fails(function () { return nativeToString.call({ source: 'a', flags: 'b' }) != '/a/b'; });
	// FF44- RegExp#toString has a wrong name
	var INCORRECT_NAME = nativeToString.name != TO_STRING;

	// `RegExp.prototype.toString` method
	// https://tc39.github.io/ecma262/#sec-regexp.prototype.tostring
	if (NOT_GENERIC || INCORRECT_NAME) {
	  redefine(RegExp.prototype, TO_STRING, function toString() {
	    var R = anObject(this);
	    var p = String(R.source);
	    var rf = R.flags;
	    var f = String(rf === undefined && R instanceof RegExp && !('flags' in RegExpPrototype$1) ? regexpFlags.call(R) : rf);
	    return '/' + p + '/' + f;
	  }, { unsafe: true });
	}

	// TODO: Remove from `core-js@4` since it's moved to entry points







	var SPECIES$3 = wellKnownSymbol('species');

	var REPLACE_SUPPORTS_NAMED_GROUPS = !fails(function () {
	  // #replace needs built-in support for named groups.
	  // #match works fine because it just return the exec results, even if it has
	  // a "grops" property.
	  var re = /./;
	  re.exec = function () {
	    var result = [];
	    result.groups = { a: '7' };
	    return result;
	  };
	  return ''.replace(re, '$<a>') !== '7';
	});

	// IE <= 11 replaces $0 with the whole match, as if it was $&
	// https://stackoverflow.com/questions/6024666/getting-ie-to-replace-a-regex-with-the-literal-string-0
	var REPLACE_KEEPS_$0 = (function () {
	  return 'a'.replace(/./, '$0') === '$0';
	})();

	var REPLACE = wellKnownSymbol('replace');
	// Safari <= 13.0.3(?) substitutes nth capture where n>m with an empty string
	var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = (function () {
	  if (/./[REPLACE]) {
	    return /./[REPLACE]('a', '$0') === '';
	  }
	  return false;
	})();

	// Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
	// Weex JS has frozen built-in prototypes, so use try / catch wrapper
	var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = !fails(function () {
	  var re = /(?:)/;
	  var originalExec = re.exec;
	  re.exec = function () { return originalExec.apply(this, arguments); };
	  var result = 'ab'.split(re);
	  return result.length !== 2 || result[0] !== 'a' || result[1] !== 'b';
	});

	var fixRegexpWellKnownSymbolLogic = function (KEY, length, exec, sham) {
	  var SYMBOL = wellKnownSymbol(KEY);

	  var DELEGATES_TO_SYMBOL = !fails(function () {
	    // String methods call symbol-named RegEp methods
	    var O = {};
	    O[SYMBOL] = function () { return 7; };
	    return ''[KEY](O) != 7;
	  });

	  var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails(function () {
	    // Symbol-named RegExp methods call .exec
	    var execCalled = false;
	    var re = /a/;

	    if (KEY === 'split') {
	      // We can't use real regex here since it causes deoptimization
	      // and serious performance degradation in V8
	      // https://github.com/zloirock/core-js/issues/306
	      re = {};
	      // RegExp[@@split] doesn't call the regex's exec method, but first creates
	      // a new one. We need to return the patched regex when creating the new one.
	      re.constructor = {};
	      re.constructor[SPECIES$3] = function () { return re; };
	      re.flags = '';
	      re[SYMBOL] = /./[SYMBOL];
	    }

	    re.exec = function () { execCalled = true; return null; };

	    re[SYMBOL]('');
	    return !execCalled;
	  });

	  if (
	    !DELEGATES_TO_SYMBOL ||
	    !DELEGATES_TO_EXEC ||
	    (KEY === 'replace' && !(
	      REPLACE_SUPPORTS_NAMED_GROUPS &&
	      REPLACE_KEEPS_$0 &&
	      !REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE
	    )) ||
	    (KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC)
	  ) {
	    var nativeRegExpMethod = /./[SYMBOL];
	    var methods = exec(SYMBOL, ''[KEY], function (nativeMethod, regexp, str, arg2, forceStringMethod) {
	      if (regexp.exec === regexpExec) {
	        if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
	          // The native String method already delegates to @@method (this
	          // polyfilled function), leasing to infinite recursion.
	          // We avoid it by directly calling the native @@method method.
	          return { done: true, value: nativeRegExpMethod.call(regexp, str, arg2) };
	        }
	        return { done: true, value: nativeMethod.call(str, regexp, arg2) };
	      }
	      return { done: false };
	    }, {
	      REPLACE_KEEPS_$0: REPLACE_KEEPS_$0,
	      REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE: REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE
	    });
	    var stringMethod = methods[0];
	    var regexMethod = methods[1];

	    redefine(String.prototype, KEY, stringMethod);
	    redefine(RegExp.prototype, SYMBOL, length == 2
	      // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
	      // 21.2.5.11 RegExp.prototype[@@split](string, limit)
	      ? function (string, arg) { return regexMethod.call(string, this, arg); }
	      // 21.2.5.6 RegExp.prototype[@@match](string)
	      // 21.2.5.9 RegExp.prototype[@@search](string)
	      : function (string) { return regexMethod.call(string, this); }
	    );
	  }

	  if (sham) createNonEnumerableProperty(RegExp.prototype[SYMBOL], 'sham', true);
	};

	// `String.prototype.{ codePointAt, at }` methods implementation
	var createMethod$2 = function (CONVERT_TO_STRING) {
	  return function ($this, pos) {
	    var S = String(requireObjectCoercible($this));
	    var position = toInteger(pos);
	    var size = S.length;
	    var first, second;
	    if (position < 0 || position >= size) return CONVERT_TO_STRING ? '' : undefined;
	    first = S.charCodeAt(position);
	    return first < 0xD800 || first > 0xDBFF || position + 1 === size
	      || (second = S.charCodeAt(position + 1)) < 0xDC00 || second > 0xDFFF
	        ? CONVERT_TO_STRING ? S.charAt(position) : first
	        : CONVERT_TO_STRING ? S.slice(position, position + 2) : (first - 0xD800 << 10) + (second - 0xDC00) + 0x10000;
	  };
	};

	var stringMultibyte = {
	  // `String.prototype.codePointAt` method
	  // https://tc39.github.io/ecma262/#sec-string.prototype.codepointat
	  codeAt: createMethod$2(false),
	  // `String.prototype.at` method
	  // https://github.com/mathiasbynens/String.prototype.at
	  charAt: createMethod$2(true)
	};

	var charAt = stringMultibyte.charAt;

	// `AdvanceStringIndex` abstract operation
	// https://tc39.github.io/ecma262/#sec-advancestringindex
	var advanceStringIndex = function (S, index, unicode) {
	  return index + (unicode ? charAt(S, index).length : 1);
	};

	// `RegExpExec` abstract operation
	// https://tc39.github.io/ecma262/#sec-regexpexec
	var regexpExecAbstract = function (R, S) {
	  var exec = R.exec;
	  if (typeof exec === 'function') {
	    var result = exec.call(R, S);
	    if (typeof result !== 'object') {
	      throw TypeError('RegExp exec method returned something other than an Object or null');
	    }
	    return result;
	  }

	  if (classofRaw(R) !== 'RegExp') {
	    throw TypeError('RegExp#exec called on incompatible receiver');
	  }

	  return regexpExec.call(R, S);
	};

	// @@match logic
	fixRegexpWellKnownSymbolLogic('match', 1, function (MATCH, nativeMatch, maybeCallNative) {
	  return [
	    // `String.prototype.match` method
	    // https://tc39.github.io/ecma262/#sec-string.prototype.match
	    function match(regexp) {
	      var O = requireObjectCoercible(this);
	      var matcher = regexp == undefined ? undefined : regexp[MATCH];
	      return matcher !== undefined ? matcher.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
	    },
	    // `RegExp.prototype[@@match]` method
	    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
	    function (regexp) {
	      var res = maybeCallNative(nativeMatch, regexp, this);
	      if (res.done) return res.value;

	      var rx = anObject(regexp);
	      var S = String(this);

	      if (!rx.global) return regexpExecAbstract(rx, S);

	      var fullUnicode = rx.unicode;
	      rx.lastIndex = 0;
	      var A = [];
	      var n = 0;
	      var result;
	      while ((result = regexpExecAbstract(rx, S)) !== null) {
	        var matchStr = String(result[0]);
	        A[n] = matchStr;
	        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
	        n++;
	      }
	      return n === 0 ? null : A;
	    }
	  ];
	});

	var aFunction$1 = function (it) {
	  if (typeof it != 'function') {
	    throw TypeError(String(it) + ' is not a function');
	  } return it;
	};

	var SPECIES$4 = wellKnownSymbol('species');

	// `SpeciesConstructor` abstract operation
	// https://tc39.github.io/ecma262/#sec-speciesconstructor
	var speciesConstructor = function (O, defaultConstructor) {
	  var C = anObject(O).constructor;
	  var S;
	  return C === undefined || (S = anObject(C)[SPECIES$4]) == undefined ? defaultConstructor : aFunction$1(S);
	};

	var arrayPush = [].push;
	var min$3 = Math.min;
	var MAX_UINT32 = 0xFFFFFFFF;

	// babel-minify transpiles RegExp('x', 'y') -> /x/y and it causes SyntaxError
	var SUPPORTS_Y = !fails(function () { return !RegExp(MAX_UINT32, 'y'); });

	// @@split logic
	fixRegexpWellKnownSymbolLogic('split', 2, function (SPLIT, nativeSplit, maybeCallNative) {
	  var internalSplit;
	  if (
	    'abbc'.split(/(b)*/)[1] == 'c' ||
	    'test'.split(/(?:)/, -1).length != 4 ||
	    'ab'.split(/(?:ab)*/).length != 2 ||
	    '.'.split(/(.?)(.?)/).length != 4 ||
	    '.'.split(/()()/).length > 1 ||
	    ''.split(/.?/).length
	  ) {
	    // based on es5-shim implementation, need to rework it
	    internalSplit = function (separator, limit) {
	      var string = String(requireObjectCoercible(this));
	      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
	      if (lim === 0) return [];
	      if (separator === undefined) return [string];
	      // If `separator` is not a regex, use native split
	      if (!isRegexp(separator)) {
	        return nativeSplit.call(string, separator, lim);
	      }
	      var output = [];
	      var flags = (separator.ignoreCase ? 'i' : '') +
	                  (separator.multiline ? 'm' : '') +
	                  (separator.unicode ? 'u' : '') +
	                  (separator.sticky ? 'y' : '');
	      var lastLastIndex = 0;
	      // Make `global` and avoid `lastIndex` issues by working with a copy
	      var separatorCopy = new RegExp(separator.source, flags + 'g');
	      var match, lastIndex, lastLength;
	      while (match = regexpExec.call(separatorCopy, string)) {
	        lastIndex = separatorCopy.lastIndex;
	        if (lastIndex > lastLastIndex) {
	          output.push(string.slice(lastLastIndex, match.index));
	          if (match.length > 1 && match.index < string.length) arrayPush.apply(output, match.slice(1));
	          lastLength = match[0].length;
	          lastLastIndex = lastIndex;
	          if (output.length >= lim) break;
	        }
	        if (separatorCopy.lastIndex === match.index) separatorCopy.lastIndex++; // Avoid an infinite loop
	      }
	      if (lastLastIndex === string.length) {
	        if (lastLength || !separatorCopy.test('')) output.push('');
	      } else output.push(string.slice(lastLastIndex));
	      return output.length > lim ? output.slice(0, lim) : output;
	    };
	  // Chakra, V8
	  } else if ('0'.split(undefined, 0).length) {
	    internalSplit = function (separator, limit) {
	      return separator === undefined && limit === 0 ? [] : nativeSplit.call(this, separator, limit);
	    };
	  } else internalSplit = nativeSplit;

	  return [
	    // `String.prototype.split` method
	    // https://tc39.github.io/ecma262/#sec-string.prototype.split
	    function split(separator, limit) {
	      var O = requireObjectCoercible(this);
	      var splitter = separator == undefined ? undefined : separator[SPLIT];
	      return splitter !== undefined
	        ? splitter.call(separator, O, limit)
	        : internalSplit.call(String(O), separator, limit);
	    },
	    // `RegExp.prototype[@@split]` method
	    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@split
	    //
	    // NOTE: This cannot be properly polyfilled in engines that don't support
	    // the 'y' flag.
	    function (regexp, limit) {
	      var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== nativeSplit);
	      if (res.done) return res.value;

	      var rx = anObject(regexp);
	      var S = String(this);
	      var C = speciesConstructor(rx, RegExp);

	      var unicodeMatching = rx.unicode;
	      var flags = (rx.ignoreCase ? 'i' : '') +
	                  (rx.multiline ? 'm' : '') +
	                  (rx.unicode ? 'u' : '') +
	                  (SUPPORTS_Y ? 'y' : 'g');

	      // ^(? + rx + ) is needed, in combination with some S slicing, to
	      // simulate the 'y' flag.
	      var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
	      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
	      if (lim === 0) return [];
	      if (S.length === 0) return regexpExecAbstract(splitter, S) === null ? [S] : [];
	      var p = 0;
	      var q = 0;
	      var A = [];
	      while (q < S.length) {
	        splitter.lastIndex = SUPPORTS_Y ? q : 0;
	        var z = regexpExecAbstract(splitter, SUPPORTS_Y ? S : S.slice(q));
	        var e;
	        if (
	          z === null ||
	          (e = min$3(toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p
	        ) {
	          q = advanceStringIndex(S, q, unicodeMatching);
	        } else {
	          A.push(S.slice(p, q));
	          if (A.length === lim) return A;
	          for (var i = 1; i <= z.length - 1; i++) {
	            A.push(z[i]);
	            if (A.length === lim) return A;
	          }
	          q = p = e;
	        }
	      }
	      A.push(S.slice(p));
	      return A;
	    }
	  ];
	}, !SUPPORTS_Y);

	var non = '\u200B\u0085\u180E';

	// check that a method works with the correct list
	// of whitespaces and has a correct name
	var stringTrimForced = function (METHOD_NAME) {
	  return fails(function () {
	    return !!whitespaces[METHOD_NAME]() || non[METHOD_NAME]() != non || whitespaces[METHOD_NAME].name !== METHOD_NAME;
	  });
	};

	var $trim = stringTrim.trim;


	// `String.prototype.trim` method
	// https://tc39.github.io/ecma262/#sec-string.prototype.trim
	_export({ target: 'String', proto: true, forced: stringTrimForced('trim') }, {
	  trim: function trim() {
	    return $trim(this);
	  }
	});

	/**
	 * ------------------------------------------------------------------------
	 * Private TransitionEnd Helpers
	 * ------------------------------------------------------------------------
	 */

	var TRANSITION_END = 'transitionend';
	var MAX_UID = 1000000;
	var MILLISECONDS_MULTIPLIER = 1000; // Shoutout AngusCroll (https://goo.gl/pxwQGp)

	function toType(obj) {
	  return {}.toString.call(obj).match(/\s([a-z]+)/i)[1].toLowerCase();
	}

	function getSpecialTransitionEndEvent() {
	  return {
	    bindType: TRANSITION_END,
	    delegateType: TRANSITION_END,
	    handle: function handle(event) {
	      if ($(event.target).is(this)) {
	        return event.handleObj.handler.apply(this, arguments); // eslint-disable-line prefer-rest-params
	      }

	      return undefined; // eslint-disable-line no-undefined
	    }
	  };
	}

	function transitionEndEmulator(duration) {
	  var _this = this;

	  var called = false;
	  $(this).one(Util.TRANSITION_END, function () {
	    called = true;
	  });
	  setTimeout(function () {
	    if (!called) {
	      Util.triggerTransitionEnd(_this);
	    }
	  }, duration);
	  return this;
	}

	function setTransitionEndSupport() {
	  $.fn.emulateTransitionEnd = transitionEndEmulator;
	  $.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
	}
	/**
	 * --------------------------------------------------------------------------
	 * Public Util Api
	 * --------------------------------------------------------------------------
	 */


	var Util = {
	  TRANSITION_END: 'bsTransitionEnd',
	  getUID: function getUID(prefix) {
	    do {
	      // eslint-disable-next-line no-bitwise
	      prefix += ~~(Math.random() * MAX_UID); // "~~" acts like a faster Math.floor() here
	    } while (document.getElementById(prefix));

	    return prefix;
	  },
	  getSelectorFromElement: function getSelectorFromElement(element) {
	    var selector = element.getAttribute('data-target');

	    if (!selector || selector === '#') {
	      var hrefAttr = element.getAttribute('href');
	      selector = hrefAttr && hrefAttr !== '#' ? hrefAttr.trim() : '';
	    }

	    try {
	      return document.querySelector(selector) ? selector : null;
	    } catch (err) {
	      return null;
	    }
	  },
	  getTransitionDurationFromElement: function getTransitionDurationFromElement(element) {
	    if (!element) {
	      return 0;
	    } // Get transition-duration of the element


	    var transitionDuration = $(element).css('transition-duration');
	    var transitionDelay = $(element).css('transition-delay');
	    var floatTransitionDuration = parseFloat(transitionDuration);
	    var floatTransitionDelay = parseFloat(transitionDelay); // Return 0 if element or transition duration is not found

	    if (!floatTransitionDuration && !floatTransitionDelay) {
	      return 0;
	    } // If multiple durations are defined, take the first


	    transitionDuration = transitionDuration.split(',')[0];
	    transitionDelay = transitionDelay.split(',')[0];
	    return (parseFloat(transitionDuration) + parseFloat(transitionDelay)) * MILLISECONDS_MULTIPLIER;
	  },
	  reflow: function reflow(element) {
	    return element.offsetHeight;
	  },
	  triggerTransitionEnd: function triggerTransitionEnd(element) {
	    $(element).trigger(TRANSITION_END);
	  },
	  // TODO: Remove in v5
	  supportsTransitionEnd: function supportsTransitionEnd() {
	    return Boolean(TRANSITION_END);
	  },
	  isElement: function isElement(obj) {
	    return (obj[0] || obj).nodeType;
	  },
	  typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
	    for (var property in configTypes) {
	      if (Object.prototype.hasOwnProperty.call(configTypes, property)) {
	        var expectedTypes = configTypes[property];
	        var value = config[property];
	        var valueType = value && Util.isElement(value) ? 'element' : toType(value);

	        if (!new RegExp(expectedTypes).test(valueType)) {
	          throw new Error(componentName.toUpperCase() + ": " + ("Option \"" + property + "\" provided type \"" + valueType + "\" ") + ("but expected type \"" + expectedTypes + "\"."));
	        }
	      }
	    }
	  },
	  findShadowRoot: function findShadowRoot(element) {
	    if (!document.documentElement.attachShadow) {
	      return null;
	    } // Can find the shadow root otherwise it'll return the document


	    if (typeof element.getRootNode === 'function') {
	      var root = element.getRootNode();
	      return root instanceof ShadowRoot ? root : null;
	    }

	    if (element instanceof ShadowRoot) {
	      return element;
	    } // when we don't find a shadow root


	    if (!element.parentNode) {
	      return null;
	    }

	    return Util.findShadowRoot(element.parentNode);
	  },
	  jQueryDetection: function jQueryDetection() {
	    if (typeof $ === 'undefined') {
	      throw new TypeError('Bootstrap\'s JavaScript requires jQuery. jQuery must be included before Bootstrap\'s JavaScript.');
	    }

	    var version = $.fn.jquery.split(' ')[0].split('.');
	    var minMajor = 1;
	    var ltMajor = 2;
	    var minMinor = 9;
	    var minPatch = 1;
	    var maxMajor = 4;

	    if (version[0] < ltMajor && version[1] < minMinor || version[0] === minMajor && version[1] === minMinor && version[2] < minPatch || version[0] >= maxMajor) {
	      throw new Error('Bootstrap\'s JavaScript requires at least jQuery v1.9.1 but less than v4.0.0');
	    }
	  }
	};
	Util.jQueryDetection();
	setTransitionEndSupport();

	var HAS_SPECIES_SUPPORT$1 = arrayMethodHasSpeciesSupport('slice');
	var USES_TO_LENGTH$2 = arrayMethodUsesToLength('slice', { ACCESSORS: true, 0: 0, 1: 2 });

	var SPECIES$5 = wellKnownSymbol('species');
	var nativeSlice = [].slice;
	var max$2 = Math.max;

	// `Array.prototype.slice` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.slice
	// fallback for not array-like ES3 strings and DOM objects
	_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$1 || !USES_TO_LENGTH$2 }, {
	  slice: function slice(start, end) {
	    var O = toIndexedObject(this);
	    var length = toLength(O.length);
	    var k = toAbsoluteIndex(start, length);
	    var fin = toAbsoluteIndex(end === undefined ? length : end, length);
	    // inline `ArraySpeciesCreate` for usage native `Array#slice` where it's possible
	    var Constructor, result, n;
	    if (isArray(O)) {
	      Constructor = O.constructor;
	      // cross-realm fallback
	      if (typeof Constructor == 'function' && (Constructor === Array || isArray(Constructor.prototype))) {
	        Constructor = undefined;
	      } else if (isObject(Constructor)) {
	        Constructor = Constructor[SPECIES$5];
	        if (Constructor === null) Constructor = undefined;
	      }
	      if (Constructor === Array || Constructor === undefined) {
	        return nativeSlice.call(O, k, fin);
	      }
	    }
	    result = new (Constructor === undefined ? Array : Constructor)(max$2(fin - k, 0));
	    for (n = 0; k < fin; k++, n++) if (k in O) createProperty(result, n, O[k]);
	    result.length = n;
	    return result;
	  }
	});

	var trim$1 = stringTrim.trim;


	var $parseInt = global_1.parseInt;
	var hex = /^[+-]?0[Xx]/;
	var FORCED$2 = $parseInt(whitespaces + '08') !== 8 || $parseInt(whitespaces + '0x16') !== 22;

	// `parseInt` method
	// https://tc39.github.io/ecma262/#sec-parseint-string-radix
	var numberParseInt = FORCED$2 ? function parseInt(string, radix) {
	  var S = trim$1(String(string));
	  return $parseInt(S, (radix >>> 0) || (hex.test(S) ? 16 : 10));
	} : $parseInt;

	// `parseInt` method
	// https://tc39.github.io/ecma262/#sec-parseint-string-radix
	_export({ global: true, forced: parseInt != numberParseInt }, {
	  parseInt: numberParseInt
	});

	var ViewPort = {
	  XS: 0,
	  SM: 540,
	  MD: 860,
	  LG: 1084,
	  XL: 1400
	};
	var DetectionUtil = {
	  detectIE: function detectIE() {
	    /**
	    * detect IE
	    * returns version of IE or false, if browser is not Internet Explorer
	    */
	    var UA = window.navigator.userAgent;
	    var MSIE = UA.indexOf('MSIE ');
	    var TRIDENT = UA.indexOf('Trident/');
	    var EDGE = UA.indexOf('Edge/'); // IE 10 or older => return version number

	    if (MSIE > 0) {
	      return parseInt(UA.slice(MSIE + 5, UA.indexOf('.', MSIE)), 10);
	    } // IE 11 => return version number


	    if (TRIDENT > 0) {
	      var RV = UA.indexOf('rv:');
	      return parseInt(UA.slice(RV + 3, UA.indexOf('.', RV)), 10);
	    } // Edge (IE 12+) => return version number


	    if (EDGE > 0) {
	      return parseInt(UA.slice(EDGE + 5, UA.indexOf('.', EDGE)), 10);
	    } // other browser


	    return false;
	  },

	  /* eslint-disable no-useless-escape, unicorn/regex-shorthand */
	  detectMobile: function detectMobile(includeTabletCheck) {
	    if (includeTabletCheck === void 0) {
	      includeTabletCheck = false;
	    }

	    /**
	    * detect if mobile and/or tablet device
	    * returns bool
	    */
	    var check = false;

	    if (includeTabletCheck) {
	      (function (a) {
	        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.slice(0, 4))) {
	          check = true;
	        }
	      })(navigator.userAgent || navigator.vendor || window.opera);
	    } else {
	      (function (a) {
	        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.slice(0, 4))) {
	          check = true;
	        }
	      })(navigator.userAgent || navigator.vendor || window.opera);
	    }

	    return check;
	  },

	  /**
	  * Gets viewport based on brower's window width.
	  * @return {string} Returns viewport
	  */
	  detectViewport: function detectViewport() {
	    var windowWidth = window.innerWidth;

	    if (windowWidth >= ViewPort.XS && windowWidth < ViewPort.SM) {
	      return 'xs';
	    }

	    if (windowWidth < ViewPort.MD && windowWidth >= ViewPort.SM) {
	      return 'sm';
	    }

	    if (windowWidth < ViewPort.LG && windowWidth >= ViewPort.MD) {
	      return 'md';
	    }

	    if (windowWidth < ViewPort.XL && windowWidth >= ViewPort.LG) {
	      return 'lg';
	    }

	    if (windowWidth >= ViewPort.XL) {
	      return 'xl';
	    }
	  },

	  /* eslint-enable no-useless-escape */
	  isBiDirectional: function isBiDirectional(el) {
	    if (!el) {
	      el = document.querySelector('html');
	    }

	    return el.getAttribute('dir') === 'rtl';
	  }
	};

	// `Object.defineProperties` method
	// https://tc39.github.io/ecma262/#sec-object.defineproperties
	var objectDefineProperties = descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
	  anObject(O);
	  var keys = objectKeys(Properties);
	  var length = keys.length;
	  var index = 0;
	  var key;
	  while (length > index) objectDefineProperty.f(O, key = keys[index++], Properties[key]);
	  return O;
	};

	var html = getBuiltIn('document', 'documentElement');

	var GT = '>';
	var LT = '<';
	var PROTOTYPE = 'prototype';
	var SCRIPT = 'script';
	var IE_PROTO = sharedKey('IE_PROTO');

	var EmptyConstructor = function () { /* empty */ };

	var scriptTag = function (content) {
	  return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
	};

	// Create object with fake `null` prototype: use ActiveX Object with cleared prototype
	var NullProtoObjectViaActiveX = function (activeXDocument) {
	  activeXDocument.write(scriptTag(''));
	  activeXDocument.close();
	  var temp = activeXDocument.parentWindow.Object;
	  activeXDocument = null; // avoid memory leak
	  return temp;
	};

	// Create object with fake `null` prototype: use iframe Object with cleared prototype
	var NullProtoObjectViaIFrame = function () {
	  // Thrash, waste and sodomy: IE GC bug
	  var iframe = documentCreateElement('iframe');
	  var JS = 'java' + SCRIPT + ':';
	  var iframeDocument;
	  iframe.style.display = 'none';
	  html.appendChild(iframe);
	  // https://github.com/zloirock/core-js/issues/475
	  iframe.src = String(JS);
	  iframeDocument = iframe.contentWindow.document;
	  iframeDocument.open();
	  iframeDocument.write(scriptTag('document.F=Object'));
	  iframeDocument.close();
	  return iframeDocument.F;
	};

	// Check for document.domain and active x support
	// No need to use active x approach when document.domain is not set
	// see https://github.com/es-shims/es5-shim/issues/150
	// variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
	// avoid IE GC bug
	var activeXDocument;
	var NullProtoObject = function () {
	  try {
	    /* global ActiveXObject */
	    activeXDocument = document.domain && new ActiveXObject('htmlfile');
	  } catch (error) { /* ignore */ }
	  NullProtoObject = activeXDocument ? NullProtoObjectViaActiveX(activeXDocument) : NullProtoObjectViaIFrame();
	  var length = enumBugKeys.length;
	  while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];
	  return NullProtoObject();
	};

	hiddenKeys[IE_PROTO] = true;

	// `Object.create` method
	// https://tc39.github.io/ecma262/#sec-object.create
	var objectCreate = Object.create || function create(O, Properties) {
	  var result;
	  if (O !== null) {
	    EmptyConstructor[PROTOTYPE] = anObject(O);
	    result = new EmptyConstructor();
	    EmptyConstructor[PROTOTYPE] = null;
	    // add "__proto__" for Object.getPrototypeOf polyfill
	    result[IE_PROTO] = O;
	  } else result = NullProtoObject();
	  return Properties === undefined ? result : objectDefineProperties(result, Properties);
	};

	var nativeGetOwnPropertyNames = objectGetOwnPropertyNames.f;

	var toString$1 = {}.toString;

	var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
	  ? Object.getOwnPropertyNames(window) : [];

	var getWindowNames = function (it) {
	  try {
	    return nativeGetOwnPropertyNames(it);
	  } catch (error) {
	    return windowNames.slice();
	  }
	};

	// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
	var f$5 = function getOwnPropertyNames(it) {
	  return windowNames && toString$1.call(it) == '[object Window]'
	    ? getWindowNames(it)
	    : nativeGetOwnPropertyNames(toIndexedObject(it));
	};

	var objectGetOwnPropertyNamesExternal = {
		f: f$5
	};

	var f$6 = wellKnownSymbol;

	var wellKnownSymbolWrapped = {
		f: f$6
	};

	var defineProperty$3 = objectDefineProperty.f;

	var defineWellKnownSymbol = function (NAME) {
	  var Symbol = path.Symbol || (path.Symbol = {});
	  if (!has(Symbol, NAME)) defineProperty$3(Symbol, NAME, {
	    value: wellKnownSymbolWrapped.f(NAME)
	  });
	};

	var defineProperty$4 = objectDefineProperty.f;



	var TO_STRING_TAG$2 = wellKnownSymbol('toStringTag');

	var setToStringTag = function (it, TAG, STATIC) {
	  if (it && !has(it = STATIC ? it : it.prototype, TO_STRING_TAG$2)) {
	    defineProperty$4(it, TO_STRING_TAG$2, { configurable: true, value: TAG });
	  }
	};

	// optional / simple context binding
	var functionBindContext = function (fn, that, length) {
	  aFunction$1(fn);
	  if (that === undefined) return fn;
	  switch (length) {
	    case 0: return function () {
	      return fn.call(that);
	    };
	    case 1: return function (a) {
	      return fn.call(that, a);
	    };
	    case 2: return function (a, b) {
	      return fn.call(that, a, b);
	    };
	    case 3: return function (a, b, c) {
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function (/* ...args */) {
	    return fn.apply(that, arguments);
	  };
	};

	var push = [].push;

	// `Array.prototype.{ forEach, map, filter, some, every, find, findIndex }` methods implementation
	var createMethod$3 = function (TYPE) {
	  var IS_MAP = TYPE == 1;
	  var IS_FILTER = TYPE == 2;
	  var IS_SOME = TYPE == 3;
	  var IS_EVERY = TYPE == 4;
	  var IS_FIND_INDEX = TYPE == 6;
	  var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
	  return function ($this, callbackfn, that, specificCreate) {
	    var O = toObject($this);
	    var self = indexedObject(O);
	    var boundFunction = functionBindContext(callbackfn, that, 3);
	    var length = toLength(self.length);
	    var index = 0;
	    var create = specificCreate || arraySpeciesCreate;
	    var target = IS_MAP ? create($this, length) : IS_FILTER ? create($this, 0) : undefined;
	    var value, result;
	    for (;length > index; index++) if (NO_HOLES || index in self) {
	      value = self[index];
	      result = boundFunction(value, index, O);
	      if (TYPE) {
	        if (IS_MAP) target[index] = result; // map
	        else if (result) switch (TYPE) {
	          case 3: return true;              // some
	          case 5: return value;             // find
	          case 6: return index;             // findIndex
	          case 2: push.call(target, value); // filter
	        } else if (IS_EVERY) return false;  // every
	      }
	    }
	    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
	  };
	};

	var arrayIteration = {
	  // `Array.prototype.forEach` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.foreach
	  forEach: createMethod$3(0),
	  // `Array.prototype.map` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.map
	  map: createMethod$3(1),
	  // `Array.prototype.filter` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.filter
	  filter: createMethod$3(2),
	  // `Array.prototype.some` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.some
	  some: createMethod$3(3),
	  // `Array.prototype.every` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.every
	  every: createMethod$3(4),
	  // `Array.prototype.find` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.find
	  find: createMethod$3(5),
	  // `Array.prototype.findIndex` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
	  findIndex: createMethod$3(6)
	};

	var $forEach = arrayIteration.forEach;

	var HIDDEN = sharedKey('hidden');
	var SYMBOL = 'Symbol';
	var PROTOTYPE$1 = 'prototype';
	var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');
	var setInternalState$1 = internalState.set;
	var getInternalState = internalState.getterFor(SYMBOL);
	var ObjectPrototype = Object[PROTOTYPE$1];
	var $Symbol = global_1.Symbol;
	var $stringify = getBuiltIn('JSON', 'stringify');
	var nativeGetOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;
	var nativeDefineProperty$1 = objectDefineProperty.f;
	var nativeGetOwnPropertyNames$1 = objectGetOwnPropertyNamesExternal.f;
	var nativePropertyIsEnumerable$1 = objectPropertyIsEnumerable.f;
	var AllSymbols = shared('symbols');
	var ObjectPrototypeSymbols = shared('op-symbols');
	var StringToSymbolRegistry = shared('string-to-symbol-registry');
	var SymbolToStringRegistry = shared('symbol-to-string-registry');
	var WellKnownSymbolsStore$1 = shared('wks');
	var QObject = global_1.QObject;
	// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
	var USE_SETTER = !QObject || !QObject[PROTOTYPE$1] || !QObject[PROTOTYPE$1].findChild;

	// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
	var setSymbolDescriptor = descriptors && fails(function () {
	  return objectCreate(nativeDefineProperty$1({}, 'a', {
	    get: function () { return nativeDefineProperty$1(this, 'a', { value: 7 }).a; }
	  })).a != 7;
	}) ? function (O, P, Attributes) {
	  var ObjectPrototypeDescriptor = nativeGetOwnPropertyDescriptor$1(ObjectPrototype, P);
	  if (ObjectPrototypeDescriptor) delete ObjectPrototype[P];
	  nativeDefineProperty$1(O, P, Attributes);
	  if (ObjectPrototypeDescriptor && O !== ObjectPrototype) {
	    nativeDefineProperty$1(ObjectPrototype, P, ObjectPrototypeDescriptor);
	  }
	} : nativeDefineProperty$1;

	var wrap = function (tag, description) {
	  var symbol = AllSymbols[tag] = objectCreate($Symbol[PROTOTYPE$1]);
	  setInternalState$1(symbol, {
	    type: SYMBOL,
	    tag: tag,
	    description: description
	  });
	  if (!descriptors) symbol.description = description;
	  return symbol;
	};

	var isSymbol = useSymbolAsUid ? function (it) {
	  return typeof it == 'symbol';
	} : function (it) {
	  return Object(it) instanceof $Symbol;
	};

	var $defineProperty = function defineProperty(O, P, Attributes) {
	  if (O === ObjectPrototype) $defineProperty(ObjectPrototypeSymbols, P, Attributes);
	  anObject(O);
	  var key = toPrimitive(P, true);
	  anObject(Attributes);
	  if (has(AllSymbols, key)) {
	    if (!Attributes.enumerable) {
	      if (!has(O, HIDDEN)) nativeDefineProperty$1(O, HIDDEN, createPropertyDescriptor(1, {}));
	      O[HIDDEN][key] = true;
	    } else {
	      if (has(O, HIDDEN) && O[HIDDEN][key]) O[HIDDEN][key] = false;
	      Attributes = objectCreate(Attributes, { enumerable: createPropertyDescriptor(0, false) });
	    } return setSymbolDescriptor(O, key, Attributes);
	  } return nativeDefineProperty$1(O, key, Attributes);
	};

	var $defineProperties = function defineProperties(O, Properties) {
	  anObject(O);
	  var properties = toIndexedObject(Properties);
	  var keys = objectKeys(properties).concat($getOwnPropertySymbols(properties));
	  $forEach(keys, function (key) {
	    if (!descriptors || $propertyIsEnumerable.call(properties, key)) $defineProperty(O, key, properties[key]);
	  });
	  return O;
	};

	var $create = function create(O, Properties) {
	  return Properties === undefined ? objectCreate(O) : $defineProperties(objectCreate(O), Properties);
	};

	var $propertyIsEnumerable = function propertyIsEnumerable(V) {
	  var P = toPrimitive(V, true);
	  var enumerable = nativePropertyIsEnumerable$1.call(this, P);
	  if (this === ObjectPrototype && has(AllSymbols, P) && !has(ObjectPrototypeSymbols, P)) return false;
	  return enumerable || !has(this, P) || !has(AllSymbols, P) || has(this, HIDDEN) && this[HIDDEN][P] ? enumerable : true;
	};

	var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(O, P) {
	  var it = toIndexedObject(O);
	  var key = toPrimitive(P, true);
	  if (it === ObjectPrototype && has(AllSymbols, key) && !has(ObjectPrototypeSymbols, key)) return;
	  var descriptor = nativeGetOwnPropertyDescriptor$1(it, key);
	  if (descriptor && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) {
	    descriptor.enumerable = true;
	  }
	  return descriptor;
	};

	var $getOwnPropertyNames = function getOwnPropertyNames(O) {
	  var names = nativeGetOwnPropertyNames$1(toIndexedObject(O));
	  var result = [];
	  $forEach(names, function (key) {
	    if (!has(AllSymbols, key) && !has(hiddenKeys, key)) result.push(key);
	  });
	  return result;
	};

	var $getOwnPropertySymbols = function getOwnPropertySymbols(O) {
	  var IS_OBJECT_PROTOTYPE = O === ObjectPrototype;
	  var names = nativeGetOwnPropertyNames$1(IS_OBJECT_PROTOTYPE ? ObjectPrototypeSymbols : toIndexedObject(O));
	  var result = [];
	  $forEach(names, function (key) {
	    if (has(AllSymbols, key) && (!IS_OBJECT_PROTOTYPE || has(ObjectPrototype, key))) {
	      result.push(AllSymbols[key]);
	    }
	  });
	  return result;
	};

	// `Symbol` constructor
	// https://tc39.github.io/ecma262/#sec-symbol-constructor
	if (!nativeSymbol) {
	  $Symbol = function Symbol() {
	    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor');
	    var description = !arguments.length || arguments[0] === undefined ? undefined : String(arguments[0]);
	    var tag = uid(description);
	    var setter = function (value) {
	      if (this === ObjectPrototype) setter.call(ObjectPrototypeSymbols, value);
	      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
	      setSymbolDescriptor(this, tag, createPropertyDescriptor(1, value));
	    };
	    if (descriptors && USE_SETTER) setSymbolDescriptor(ObjectPrototype, tag, { configurable: true, set: setter });
	    return wrap(tag, description);
	  };

	  redefine($Symbol[PROTOTYPE$1], 'toString', function toString() {
	    return getInternalState(this).tag;
	  });

	  redefine($Symbol, 'withoutSetter', function (description) {
	    return wrap(uid(description), description);
	  });

	  objectPropertyIsEnumerable.f = $propertyIsEnumerable;
	  objectDefineProperty.f = $defineProperty;
	  objectGetOwnPropertyDescriptor.f = $getOwnPropertyDescriptor;
	  objectGetOwnPropertyNames.f = objectGetOwnPropertyNamesExternal.f = $getOwnPropertyNames;
	  objectGetOwnPropertySymbols.f = $getOwnPropertySymbols;

	  wellKnownSymbolWrapped.f = function (name) {
	    return wrap(wellKnownSymbol(name), name);
	  };

	  if (descriptors) {
	    // https://github.com/tc39/proposal-Symbol-description
	    nativeDefineProperty$1($Symbol[PROTOTYPE$1], 'description', {
	      configurable: true,
	      get: function description() {
	        return getInternalState(this).description;
	      }
	    });
	    {
	      redefine(ObjectPrototype, 'propertyIsEnumerable', $propertyIsEnumerable, { unsafe: true });
	    }
	  }
	}

	_export({ global: true, wrap: true, forced: !nativeSymbol, sham: !nativeSymbol }, {
	  Symbol: $Symbol
	});

	$forEach(objectKeys(WellKnownSymbolsStore$1), function (name) {
	  defineWellKnownSymbol(name);
	});

	_export({ target: SYMBOL, stat: true, forced: !nativeSymbol }, {
	  // `Symbol.for` method
	  // https://tc39.github.io/ecma262/#sec-symbol.for
	  'for': function (key) {
	    var string = String(key);
	    if (has(StringToSymbolRegistry, string)) return StringToSymbolRegistry[string];
	    var symbol = $Symbol(string);
	    StringToSymbolRegistry[string] = symbol;
	    SymbolToStringRegistry[symbol] = string;
	    return symbol;
	  },
	  // `Symbol.keyFor` method
	  // https://tc39.github.io/ecma262/#sec-symbol.keyfor
	  keyFor: function keyFor(sym) {
	    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol');
	    if (has(SymbolToStringRegistry, sym)) return SymbolToStringRegistry[sym];
	  },
	  useSetter: function () { USE_SETTER = true; },
	  useSimple: function () { USE_SETTER = false; }
	});

	_export({ target: 'Object', stat: true, forced: !nativeSymbol, sham: !descriptors }, {
	  // `Object.create` method
	  // https://tc39.github.io/ecma262/#sec-object.create
	  create: $create,
	  // `Object.defineProperty` method
	  // https://tc39.github.io/ecma262/#sec-object.defineproperty
	  defineProperty: $defineProperty,
	  // `Object.defineProperties` method
	  // https://tc39.github.io/ecma262/#sec-object.defineproperties
	  defineProperties: $defineProperties,
	  // `Object.getOwnPropertyDescriptor` method
	  // https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptors
	  getOwnPropertyDescriptor: $getOwnPropertyDescriptor
	});

	_export({ target: 'Object', stat: true, forced: !nativeSymbol }, {
	  // `Object.getOwnPropertyNames` method
	  // https://tc39.github.io/ecma262/#sec-object.getownpropertynames
	  getOwnPropertyNames: $getOwnPropertyNames,
	  // `Object.getOwnPropertySymbols` method
	  // https://tc39.github.io/ecma262/#sec-object.getownpropertysymbols
	  getOwnPropertySymbols: $getOwnPropertySymbols
	});

	// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
	// https://bugs.chromium.org/p/v8/issues/detail?id=3443
	_export({ target: 'Object', stat: true, forced: fails(function () { objectGetOwnPropertySymbols.f(1); }) }, {
	  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
	    return objectGetOwnPropertySymbols.f(toObject(it));
	  }
	});

	// `JSON.stringify` method behavior with symbols
	// https://tc39.github.io/ecma262/#sec-json.stringify
	if ($stringify) {
	  var FORCED_JSON_STRINGIFY = !nativeSymbol || fails(function () {
	    var symbol = $Symbol();
	    // MS Edge converts symbol values to JSON as {}
	    return $stringify([symbol]) != '[null]'
	      // WebKit converts symbol values to JSON as null
	      || $stringify({ a: symbol }) != '{}'
	      // V8 throws on boxed symbols
	      || $stringify(Object(symbol)) != '{}';
	  });

	  _export({ target: 'JSON', stat: true, forced: FORCED_JSON_STRINGIFY }, {
	    // eslint-disable-next-line no-unused-vars
	    stringify: function stringify(it, replacer, space) {
	      var args = [it];
	      var index = 1;
	      var $replacer;
	      while (arguments.length > index) args.push(arguments[index++]);
	      $replacer = replacer;
	      if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
	      if (!isArray(replacer)) replacer = function (key, value) {
	        if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
	        if (!isSymbol(value)) return value;
	      };
	      args[1] = replacer;
	      return $stringify.apply(null, args);
	    }
	  });
	}

	// `Symbol.prototype[@@toPrimitive]` method
	// https://tc39.github.io/ecma262/#sec-symbol.prototype-@@toprimitive
	if (!$Symbol[PROTOTYPE$1][TO_PRIMITIVE]) {
	  createNonEnumerableProperty($Symbol[PROTOTYPE$1], TO_PRIMITIVE, $Symbol[PROTOTYPE$1].valueOf);
	}
	// `Symbol.prototype[@@toStringTag]` property
	// https://tc39.github.io/ecma262/#sec-symbol.prototype-@@tostringtag
	setToStringTag($Symbol, SYMBOL);

	hiddenKeys[HIDDEN] = true;

	var defineProperty$5 = objectDefineProperty.f;


	var NativeSymbol = global_1.Symbol;

	if (descriptors && typeof NativeSymbol == 'function' && (!('description' in NativeSymbol.prototype) ||
	  // Safari 12 bug
	  NativeSymbol().description !== undefined
	)) {
	  var EmptyStringDescriptionStore = {};
	  // wrap Symbol constructor for correct work with undefined description
	  var SymbolWrapper = function Symbol() {
	    var description = arguments.length < 1 || arguments[0] === undefined ? undefined : String(arguments[0]);
	    var result = this instanceof SymbolWrapper
	      ? new NativeSymbol(description)
	      // in Edge 13, String(Symbol(undefined)) === 'Symbol(undefined)'
	      : description === undefined ? NativeSymbol() : NativeSymbol(description);
	    if (description === '') EmptyStringDescriptionStore[result] = true;
	    return result;
	  };
	  copyConstructorProperties(SymbolWrapper, NativeSymbol);
	  var symbolPrototype = SymbolWrapper.prototype = NativeSymbol.prototype;
	  symbolPrototype.constructor = SymbolWrapper;

	  var symbolToString = symbolPrototype.toString;
	  var native = String(NativeSymbol('test')) == 'Symbol(test)';
	  var regexp = /^Symbol\((.*)\)[^)]+$/;
	  defineProperty$5(symbolPrototype, 'description', {
	    configurable: true,
	    get: function description() {
	      var symbol = isObject(this) ? this.valueOf() : this;
	      var string = symbolToString.call(symbol);
	      if (has(EmptyStringDescriptionStore, symbol)) return '';
	      var desc = native ? string.slice(7, -1) : string.replace(regexp, '$1');
	      return desc === '' ? undefined : desc;
	    }
	  });

	  _export({ global: true, forced: true }, {
	    Symbol: SymbolWrapper
	  });
	}

	// `Symbol.iterator` well-known symbol
	// https://tc39.github.io/ecma262/#sec-symbol.iterator
	defineWellKnownSymbol('iterator');

	var $forEach$1 = arrayIteration.forEach;



	var STRICT_METHOD$1 = arrayMethodIsStrict('forEach');
	var USES_TO_LENGTH$3 = arrayMethodUsesToLength('forEach');

	// `Array.prototype.forEach` method implementation
	// https://tc39.github.io/ecma262/#sec-array.prototype.foreach
	var arrayForEach = (!STRICT_METHOD$1 || !USES_TO_LENGTH$3) ? function forEach(callbackfn /* , thisArg */) {
	  return $forEach$1(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
	} : [].forEach;

	// `Array.prototype.forEach` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.foreach
	_export({ target: 'Array', proto: true, forced: [].forEach != arrayForEach }, {
	  forEach: arrayForEach
	});

	var UNSCOPABLES = wellKnownSymbol('unscopables');
	var ArrayPrototype = Array.prototype;

	// Array.prototype[@@unscopables]
	// https://tc39.github.io/ecma262/#sec-array.prototype-@@unscopables
	if (ArrayPrototype[UNSCOPABLES] == undefined) {
	  objectDefineProperty.f(ArrayPrototype, UNSCOPABLES, {
	    configurable: true,
	    value: objectCreate(null)
	  });
	}

	// add a key to Array.prototype[@@unscopables]
	var addToUnscopables = function (key) {
	  ArrayPrototype[UNSCOPABLES][key] = true;
	};

	var iterators = {};

	var correctPrototypeGetter = !fails(function () {
	  function F() { /* empty */ }
	  F.prototype.constructor = null;
	  return Object.getPrototypeOf(new F()) !== F.prototype;
	});

	var IE_PROTO$1 = sharedKey('IE_PROTO');
	var ObjectPrototype$1 = Object.prototype;

	// `Object.getPrototypeOf` method
	// https://tc39.github.io/ecma262/#sec-object.getprototypeof
	var objectGetPrototypeOf = correctPrototypeGetter ? Object.getPrototypeOf : function (O) {
	  O = toObject(O);
	  if (has(O, IE_PROTO$1)) return O[IE_PROTO$1];
	  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
	    return O.constructor.prototype;
	  } return O instanceof Object ? ObjectPrototype$1 : null;
	};

	var ITERATOR = wellKnownSymbol('iterator');
	var BUGGY_SAFARI_ITERATORS = false;

	var returnThis = function () { return this; };

	// `%IteratorPrototype%` object
	// https://tc39.github.io/ecma262/#sec-%iteratorprototype%-object
	var IteratorPrototype, PrototypeOfArrayIteratorPrototype, arrayIterator;

	if ([].keys) {
	  arrayIterator = [].keys();
	  // Safari 8 has buggy iterators w/o `next`
	  if (!('next' in arrayIterator)) BUGGY_SAFARI_ITERATORS = true;
	  else {
	    PrototypeOfArrayIteratorPrototype = objectGetPrototypeOf(objectGetPrototypeOf(arrayIterator));
	    if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype = PrototypeOfArrayIteratorPrototype;
	  }
	}

	if (IteratorPrototype == undefined) IteratorPrototype = {};

	// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
	if ( !has(IteratorPrototype, ITERATOR)) {
	  createNonEnumerableProperty(IteratorPrototype, ITERATOR, returnThis);
	}

	var iteratorsCore = {
	  IteratorPrototype: IteratorPrototype,
	  BUGGY_SAFARI_ITERATORS: BUGGY_SAFARI_ITERATORS
	};

	var IteratorPrototype$1 = iteratorsCore.IteratorPrototype;





	var returnThis$1 = function () { return this; };

	var createIteratorConstructor = function (IteratorConstructor, NAME, next) {
	  var TO_STRING_TAG = NAME + ' Iterator';
	  IteratorConstructor.prototype = objectCreate(IteratorPrototype$1, { next: createPropertyDescriptor(1, next) });
	  setToStringTag(IteratorConstructor, TO_STRING_TAG, false);
	  iterators[TO_STRING_TAG] = returnThis$1;
	  return IteratorConstructor;
	};

	var IteratorPrototype$2 = iteratorsCore.IteratorPrototype;
	var BUGGY_SAFARI_ITERATORS$1 = iteratorsCore.BUGGY_SAFARI_ITERATORS;
	var ITERATOR$1 = wellKnownSymbol('iterator');
	var KEYS = 'keys';
	var VALUES = 'values';
	var ENTRIES = 'entries';

	var returnThis$2 = function () { return this; };

	var defineIterator = function (Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
	  createIteratorConstructor(IteratorConstructor, NAME, next);

	  var getIterationMethod = function (KIND) {
	    if (KIND === DEFAULT && defaultIterator) return defaultIterator;
	    if (!BUGGY_SAFARI_ITERATORS$1 && KIND in IterablePrototype) return IterablePrototype[KIND];
	    switch (KIND) {
	      case KEYS: return function keys() { return new IteratorConstructor(this, KIND); };
	      case VALUES: return function values() { return new IteratorConstructor(this, KIND); };
	      case ENTRIES: return function entries() { return new IteratorConstructor(this, KIND); };
	    } return function () { return new IteratorConstructor(this); };
	  };

	  var TO_STRING_TAG = NAME + ' Iterator';
	  var INCORRECT_VALUES_NAME = false;
	  var IterablePrototype = Iterable.prototype;
	  var nativeIterator = IterablePrototype[ITERATOR$1]
	    || IterablePrototype['@@iterator']
	    || DEFAULT && IterablePrototype[DEFAULT];
	  var defaultIterator = !BUGGY_SAFARI_ITERATORS$1 && nativeIterator || getIterationMethod(DEFAULT);
	  var anyNativeIterator = NAME == 'Array' ? IterablePrototype.entries || nativeIterator : nativeIterator;
	  var CurrentIteratorPrototype, methods, KEY;

	  // fix native
	  if (anyNativeIterator) {
	    CurrentIteratorPrototype = objectGetPrototypeOf(anyNativeIterator.call(new Iterable()));
	    if (IteratorPrototype$2 !== Object.prototype && CurrentIteratorPrototype.next) {
	      if ( objectGetPrototypeOf(CurrentIteratorPrototype) !== IteratorPrototype$2) {
	        if (objectSetPrototypeOf) {
	          objectSetPrototypeOf(CurrentIteratorPrototype, IteratorPrototype$2);
	        } else if (typeof CurrentIteratorPrototype[ITERATOR$1] != 'function') {
	          createNonEnumerableProperty(CurrentIteratorPrototype, ITERATOR$1, returnThis$2);
	        }
	      }
	      // Set @@toStringTag to native iterators
	      setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true);
	    }
	  }

	  // fix Array#{values, @@iterator}.name in V8 / FF
	  if (DEFAULT == VALUES && nativeIterator && nativeIterator.name !== VALUES) {
	    INCORRECT_VALUES_NAME = true;
	    defaultIterator = function values() { return nativeIterator.call(this); };
	  }

	  // define iterator
	  if ( IterablePrototype[ITERATOR$1] !== defaultIterator) {
	    createNonEnumerableProperty(IterablePrototype, ITERATOR$1, defaultIterator);
	  }
	  iterators[NAME] = defaultIterator;

	  // export additional methods
	  if (DEFAULT) {
	    methods = {
	      values: getIterationMethod(VALUES),
	      keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
	      entries: getIterationMethod(ENTRIES)
	    };
	    if (FORCED) for (KEY in methods) {
	      if (BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) {
	        redefine(IterablePrototype, KEY, methods[KEY]);
	      }
	    } else _export({ target: NAME, proto: true, forced: BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME }, methods);
	  }

	  return methods;
	};

	var ARRAY_ITERATOR = 'Array Iterator';
	var setInternalState$2 = internalState.set;
	var getInternalState$1 = internalState.getterFor(ARRAY_ITERATOR);

	// `Array.prototype.entries` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.entries
	// `Array.prototype.keys` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.keys
	// `Array.prototype.values` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.values
	// `Array.prototype[@@iterator]` method
	// https://tc39.github.io/ecma262/#sec-array.prototype-@@iterator
	// `CreateArrayIterator` internal method
	// https://tc39.github.io/ecma262/#sec-createarrayiterator
	var es_array_iterator = defineIterator(Array, 'Array', function (iterated, kind) {
	  setInternalState$2(this, {
	    type: ARRAY_ITERATOR,
	    target: toIndexedObject(iterated), // target
	    index: 0,                          // next index
	    kind: kind                         // kind
	  });
	// `%ArrayIteratorPrototype%.next` method
	// https://tc39.github.io/ecma262/#sec-%arrayiteratorprototype%.next
	}, function () {
	  var state = getInternalState$1(this);
	  var target = state.target;
	  var kind = state.kind;
	  var index = state.index++;
	  if (!target || index >= target.length) {
	    state.target = undefined;
	    return { value: undefined, done: true };
	  }
	  if (kind == 'keys') return { value: index, done: false };
	  if (kind == 'values') return { value: target[index], done: false };
	  return { value: [index, target[index]], done: false };
	}, 'values');

	// argumentsList[@@iterator] is %ArrayProto_values%
	// https://tc39.github.io/ecma262/#sec-createunmappedargumentsobject
	// https://tc39.github.io/ecma262/#sec-createmappedargumentsobject
	iterators.Arguments = iterators.Array;

	// https://tc39.github.io/ecma262/#sec-array.prototype-@@unscopables
	addToUnscopables('keys');
	addToUnscopables('values');
	addToUnscopables('entries');

	var charAt$1 = stringMultibyte.charAt;



	var STRING_ITERATOR = 'String Iterator';
	var setInternalState$3 = internalState.set;
	var getInternalState$2 = internalState.getterFor(STRING_ITERATOR);

	// `String.prototype[@@iterator]` method
	// https://tc39.github.io/ecma262/#sec-string.prototype-@@iterator
	defineIterator(String, 'String', function (iterated) {
	  setInternalState$3(this, {
	    type: STRING_ITERATOR,
	    string: String(iterated),
	    index: 0
	  });
	// `%StringIteratorPrototype%.next` method
	// https://tc39.github.io/ecma262/#sec-%stringiteratorprototype%.next
	}, function next() {
	  var state = getInternalState$2(this);
	  var string = state.string;
	  var index = state.index;
	  var point;
	  if (index >= string.length) return { value: undefined, done: true };
	  point = charAt$1(string, index);
	  state.index += point.length;
	  return { value: point, done: false };
	});

	// iterable DOM collections
	// flag - `iterable` interface - 'entries', 'keys', 'values', 'forEach' methods
	var domIterables = {
	  CSSRuleList: 0,
	  CSSStyleDeclaration: 0,
	  CSSValueList: 0,
	  ClientRectList: 0,
	  DOMRectList: 0,
	  DOMStringList: 0,
	  DOMTokenList: 1,
	  DataTransferItemList: 0,
	  FileList: 0,
	  HTMLAllCollection: 0,
	  HTMLCollection: 0,
	  HTMLFormElement: 0,
	  HTMLSelectElement: 0,
	  MediaList: 0,
	  MimeTypeArray: 0,
	  NamedNodeMap: 0,
	  NodeList: 1,
	  PaintRequestList: 0,
	  Plugin: 0,
	  PluginArray: 0,
	  SVGLengthList: 0,
	  SVGNumberList: 0,
	  SVGPathSegList: 0,
	  SVGPointList: 0,
	  SVGStringList: 0,
	  SVGTransformList: 0,
	  SourceBufferList: 0,
	  StyleSheetList: 0,
	  TextTrackCueList: 0,
	  TextTrackList: 0,
	  TouchList: 0
	};

	for (var COLLECTION_NAME in domIterables) {
	  var Collection = global_1[COLLECTION_NAME];
	  var CollectionPrototype = Collection && Collection.prototype;
	  // some Chrome versions have non-configurable methods on DOMTokenList
	  if (CollectionPrototype && CollectionPrototype.forEach !== arrayForEach) try {
	    createNonEnumerableProperty(CollectionPrototype, 'forEach', arrayForEach);
	  } catch (error) {
	    CollectionPrototype.forEach = arrayForEach;
	  }
	}

	var ITERATOR$2 = wellKnownSymbol('iterator');
	var TO_STRING_TAG$3 = wellKnownSymbol('toStringTag');
	var ArrayValues = es_array_iterator.values;

	for (var COLLECTION_NAME$1 in domIterables) {
	  var Collection$1 = global_1[COLLECTION_NAME$1];
	  var CollectionPrototype$1 = Collection$1 && Collection$1.prototype;
	  if (CollectionPrototype$1) {
	    // some Chrome versions have non-configurable methods on DOMTokenList
	    if (CollectionPrototype$1[ITERATOR$2] !== ArrayValues) try {
	      createNonEnumerableProperty(CollectionPrototype$1, ITERATOR$2, ArrayValues);
	    } catch (error) {
	      CollectionPrototype$1[ITERATOR$2] = ArrayValues;
	    }
	    if (!CollectionPrototype$1[TO_STRING_TAG$3]) {
	      createNonEnumerableProperty(CollectionPrototype$1, TO_STRING_TAG$3, COLLECTION_NAME$1);
	    }
	    if (domIterables[COLLECTION_NAME$1]) for (var METHOD_NAME in es_array_iterator) {
	      // some Chrome versions have non-configurable methods on DOMTokenList
	      if (CollectionPrototype$1[METHOD_NAME] !== es_array_iterator[METHOD_NAME]) try {
	        createNonEnumerableProperty(CollectionPrototype$1, METHOD_NAME, es_array_iterator[METHOD_NAME]);
	      } catch (error) {
	        CollectionPrototype$1[METHOD_NAME] = es_array_iterator[METHOD_NAME];
	      }
	    }
	  }
	}

	var InitializationUtil = {
	  /**
	   * Initialize a component after DOM is loaded
	   * @param {string} selector - DOM selector for component
	   * @param {Function} cbFn - Callback function to initialize the component
	   */
	  initializeComponent: function initializeComponent(selector, cbFn) {
	    document.addEventListener('DOMContentLoaded', function () {
	      document.querySelectorAll(selector).forEach(function (node) {
	        return cbFn(node);
	      });
	    });
	  },

	  /**
	   * Iterate over list to add event listeners
	   * @param {array} eventList - List of event maps
	   */
	  addEvents: function addEvents(eventList) {
	    for (var _iterator = eventList, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
	      var _ref;

	      if (_isArray) {
	        if (_i >= _iterator.length) break;
	        _ref = _iterator[_i++];
	      } else {
	        _i = _iterator.next();
	        if (_i.done) break;
	        _ref = _i.value;
	      }

	      var obj = _ref;

	      if (typeof obj.options === 'undefined') {
	        obj.options = {};
	      }

	      obj.el.addEventListener(obj.type, obj.handler, obj.options);
	    }
	  },

	  /**
	   * Iterate over list to remove event listeners
	   * @param {array} eventList - List of event maps
	   */
	  removeEvents: function removeEvents(eventList) {
	    for (var _iterator2 = eventList, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
	      var _ref2;

	      if (_isArray2) {
	        if (_i2 >= _iterator2.length) break;
	        _ref2 = _iterator2[_i2++];
	      } else {
	        _i2 = _iterator2.next();
	        if (_i2.done) break;
	        _ref2 = _i2.value;
	      }

	      var obj = _ref2;
	      obj.el.removeEventListener(obj.type, obj.handler);
	    }
	  }
	};

	var nativeJoin = [].join;

	var ES3_STRINGS = indexedObject != Object;
	var STRICT_METHOD$2 = arrayMethodIsStrict('join', ',');

	// `Array.prototype.join` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.join
	_export({ target: 'Array', proto: true, forced: ES3_STRINGS || !STRICT_METHOD$2 }, {
	  join: function join(separator) {
	    return nativeJoin.call(toIndexedObject(this), separator === undefined ? ',' : separator);
	  }
	});

	var selectors = ['input:not([disabled])', 'select:not([disabled])', 'textarea:not([disabled])', 'a[href]', 'button:not([disabled])', '[tabindex]', 'audio[controls]', 'video[controls]', '[contenteditable]:not([contenteditable="false"])'];
	var HelpersUtil = {
	  getTabbableElements: function getTabbableElements(node) {
	    if (node === void 0) {
	      node = document;
	    }

	    var elements = [].slice.call(node.querySelectorAll(selectors.join(', ')));
	    var tabbable = [];

	    for (var i = 0; i < elements.length; i++) {
	      var el = elements[i];

	      if (!el.disabled) {
	        tabbable.push(el);
	      }
	    }

	    return tabbable;
	  },
	  isElementTabbable: function isElementTabbable(node) {
	    return node.matches(selectors.join(', '));
	  }
	};

	var ColorUtil = {
	  /**
	     * Calulates the YIQ of the color
	     * @param {object} rgb The RGB notation of the color
	     * @returns {null} null
	     */
	  getYiq: function getYiq(_ref) {
	    var r = _ref.r,
	        g = _ref.g,
	        b = _ref.b;
	    return (r * 299 + g * 587 + b * 114) / 1000;
	  },

	  /**
	     * Gets the RGB object notation for a string
	     * @param {string} str a string respresenting a css rgb value
	     * @returns {object} an object for rgb notation
	     */
	  getRGB: function getRGB(str) {
	    var match = str.match(/rgba?\((\d{1,3}), ?(\d{1,3}), ?(\d{1,3})\)?(?:, ?(\d\.\d?)\))?/);
	    return match ? {
	      r: match[1],
	      g: match[2],
	      b: match[3]
	    } : {};
	  }
	};

	var KeyboardUtil = {
	  keyCodes: {
	    ARROW_DOWN: 40,
	    ARROW_UP: 38,
	    ARROW_LEFT: 37,
	    ARROW_RIGHT: 39,
	    ENTER: 13,
	    ESC: 27,
	    SPACE: 32,
	    TAB: 9
	  },
	  getKeyCode: function getKeyCode(e) {
	    return e.which || e.keyCode || 0;
	  }
	};

	var max$3 = Math.max;
	var min$4 = Math.min;
	var floor$1 = Math.floor;
	var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d\d?|<[^>]*>)/g;
	var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d\d?)/g;

	var maybeToString = function (it) {
	  return it === undefined ? it : String(it);
	};

	// @@replace logic
	fixRegexpWellKnownSymbolLogic('replace', 2, function (REPLACE, nativeReplace, maybeCallNative, reason) {
	  var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = reason.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE;
	  var REPLACE_KEEPS_$0 = reason.REPLACE_KEEPS_$0;
	  var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? '$' : '$0';

	  return [
	    // `String.prototype.replace` method
	    // https://tc39.github.io/ecma262/#sec-string.prototype.replace
	    function replace(searchValue, replaceValue) {
	      var O = requireObjectCoercible(this);
	      var replacer = searchValue == undefined ? undefined : searchValue[REPLACE];
	      return replacer !== undefined
	        ? replacer.call(searchValue, O, replaceValue)
	        : nativeReplace.call(String(O), searchValue, replaceValue);
	    },
	    // `RegExp.prototype[@@replace]` method
	    // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@replace
	    function (regexp, replaceValue) {
	      if (
	        (!REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE && REPLACE_KEEPS_$0) ||
	        (typeof replaceValue === 'string' && replaceValue.indexOf(UNSAFE_SUBSTITUTE) === -1)
	      ) {
	        var res = maybeCallNative(nativeReplace, regexp, this, replaceValue);
	        if (res.done) return res.value;
	      }

	      var rx = anObject(regexp);
	      var S = String(this);

	      var functionalReplace = typeof replaceValue === 'function';
	      if (!functionalReplace) replaceValue = String(replaceValue);

	      var global = rx.global;
	      if (global) {
	        var fullUnicode = rx.unicode;
	        rx.lastIndex = 0;
	      }
	      var results = [];
	      while (true) {
	        var result = regexpExecAbstract(rx, S);
	        if (result === null) break;

	        results.push(result);
	        if (!global) break;

	        var matchStr = String(result[0]);
	        if (matchStr === '') rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
	      }

	      var accumulatedResult = '';
	      var nextSourcePosition = 0;
	      for (var i = 0; i < results.length; i++) {
	        result = results[i];

	        var matched = String(result[0]);
	        var position = max$3(min$4(toInteger(result.index), S.length), 0);
	        var captures = [];
	        // NOTE: This is equivalent to
	        //   captures = result.slice(1).map(maybeToString)
	        // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
	        // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
	        // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
	        for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
	        var namedCaptures = result.groups;
	        if (functionalReplace) {
	          var replacerArgs = [matched].concat(captures, position, S);
	          if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
	          var replacement = String(replaceValue.apply(undefined, replacerArgs));
	        } else {
	          replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
	        }
	        if (position >= nextSourcePosition) {
	          accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
	          nextSourcePosition = position + matched.length;
	        }
	      }
	      return accumulatedResult + S.slice(nextSourcePosition);
	    }
	  ];

	  // https://tc39.github.io/ecma262/#sec-getsubstitution
	  function getSubstitution(matched, str, position, captures, namedCaptures, replacement) {
	    var tailPos = position + matched.length;
	    var m = captures.length;
	    var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
	    if (namedCaptures !== undefined) {
	      namedCaptures = toObject(namedCaptures);
	      symbols = SUBSTITUTION_SYMBOLS;
	    }
	    return nativeReplace.call(replacement, symbols, function (match, ch) {
	      var capture;
	      switch (ch.charAt(0)) {
	        case '$': return '$';
	        case '&': return matched;
	        case '`': return str.slice(0, position);
	        case "'": return str.slice(tailPos);
	        case '<':
	          capture = namedCaptures[ch.slice(1, -1)];
	          break;
	        default: // \d\d?
	          var n = +ch;
	          if (n === 0) return match;
	          if (n > m) {
	            var f = floor$1(n / 10);
	            if (f === 0) return match;
	            if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
	            return match;
	          }
	          capture = captures[n - 1];
	      }
	      return capture === undefined ? '' : capture;
	    });
	  }
	});

	var StringUtil = {
	  /**
	   * Interpolate a string.
	   * @param {string} template - The template string to interpolate, with keys in the format %{key}.
	   * @param {object} data - An object containing the keys and values to replace in the template.
	   * @returns {string} - The interpolated string.
	   */
	  interpolateString: function interpolateString(template, data) {
	    return template.replace(/%{(\w+)}/g, function (match, key) {
	      if (Object.prototype.hasOwnProperty.call(data, key)) {
	        return data[key];
	      } // %{key} not found, show a warning in the console and return an empty string
	      // eslint-disable-next-line no-console


	      console.warn("Template error, %{" + key + "} not found:", template);
	      return '';
	    });
	  }
	};

	/**
	 * Class representing Focus Controls.
	 * Solve for Firefox bug where following on-page anchor links loses focus:
	 * https://bugzilla.mozilla.org/show_bug.cgi?id=308064
	 * https://bugzilla.mozilla.org/show_bug.cgi?id=277178
	 */

	var FocusControls = /*#__PURE__*/function () {
	  /**
	   * Initialize focus controls.
	   * @param {Object} opts - The focus control options.
	   * @param {Node} opts.el - The anchor element node, must have href attribute with fragment identifier.
	   */
	  function FocusControls(opts) {
	    var _this = this;

	    this.el = opts.el;
	    this.target = document.querySelector(this.el.getAttribute('href'));
	    this.events = [{
	      el: this.el,
	      type: 'click',
	      handler: function handler(e) {
	        _this.onClick(e);
	      }
	    }]; // Add event handlers.

	    InitializationUtil.addEvents(this.events); // Create custom events.

	    this[Event.ON_REMOVE] = new CustomEvent(Event.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	  }
	  /**
	   * Click event.
	   * @param {Event} e - The event object.
	   */


	  var _proto = FocusControls.prototype;

	  _proto.onClick = function onClick(e) {
	    e.preventDefault();
	    this.target.focus();
	  }
	  /**
	   * Remove the form validation.
	   */
	  ;

	  _proto.remove = function remove() {
	    // Remove event handlers
	    InitializationUtil.removeEvents(this.events);
	    this.el.dispatchEvent(this[Event.ON_REMOVE]);
	  };

	  return FocusControls;
	}();

	var Util$1 = Object.assign({}, Util, {}, DetectionUtil, {}, HelpersUtil, {}, InitializationUtil, {}, ColorUtil, {}, KeyboardUtil, {}, StringUtil, {
	  FocusControls: FocusControls
	});

	var instances = [];
	var Selector = {
	  ALERT: '.alert-dismissible, [data-mount="alert-dismissible"]',
	  DISMISS: '[data-dismiss="alert"]'
	};
	var Event$1 = {
	  CLOSE: 'onClose',
	  CLOSED: 'onClosed',
	  TRANSITION_END: 'transitionend',
	  ON_REMOVE: 'onRemove'
	};
	var ClassName = {
	  FADE: 'fade',
	  SHOW: 'show'
	};

	function _triggerCloseEvent(element) {
	  element.dispatchEvent(this[Event$1.CLOSE]);
	}

	function _removeElement(element) {
	  var _this = this;

	  element.classList.remove(ClassName.SHOW);

	  if (!element.classList.contains(ClassName.FADE)) {
	    _destroyElement.bind(this)(element);

	    return;
	  }

	  element.addEventListener(Event$1.TRANSITION_END, function (event) {
	    return _destroyElement.bind(_this)(element, event);
	  }, {
	    once: true
	  });
	}

	function _destroyElement(element) {
	  element.dispatchEvent(this[Event$1.CLOSED]);
	  element.parentNode.removeChild(element);
	}

	var Alert = /*#__PURE__*/function () {
	  function Alert(opts) {
	    var _this2 = this;

	    this.el = opts.el;
	    this.dismiss = this.el.querySelector(Selector.DISMISS); // Custom Events

	    this[Event$1.CLOSE] = new CustomEvent(Event$1.CLOSE);
	    this[Event$1.CLOSED] = new CustomEvent(Event$1.CLOSED);
	    this[Event$1.ON_REMOVE] = new CustomEvent(Event$1.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    }); // Add event handlers

	    if (this.dismiss) {
	      this.events = [{
	        el: this.dismiss,
	        type: 'click',
	        handler: function handler() {
	          _this2.close();
	        }
	      }];
	      Util$1.addEvents(this.events);
	    }

	    instances.push(this);
	  }
	  /**
	   * Perform a close action
	   */


	  var _proto = Alert.prototype;

	  _proto.close = function close() {
	    var rootElement = this.el;

	    _triggerCloseEvent.bind(this)(rootElement);

	    _removeElement.bind(this)(rootElement);
	  }
	  /**
	   * Remove the instance
	   */
	  ;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events);
	    this.el.dispatchEvent(this[Event$1.ON_REMOVE]);
	    var index = instances.indexOf(this);
	    instances.splice(index, 1);
	  }
	  /**
	   * Get alert instances.
	   * @returns {Object[]} An array of alert instances
	   */
	  ;

	  Alert.getInstances = function getInstances() {
	    return instances;
	  };

	  return Alert;
	}();

	(function () {
	  Util$1.initializeComponent(Selector.ALERT, function (node) {
	    return new Alert({
	      el: node
	    });
	  });
	})();

	function _defineProperties(target, props) {
	  for (var i = 0; i < props.length; i++) {
	    var descriptor = props[i];
	    descriptor.enumerable = descriptor.enumerable || false;
	    descriptor.configurable = true;
	    if ("value" in descriptor) descriptor.writable = true;
	    Object.defineProperty(target, descriptor.key, descriptor);
	  }
	}

	function _createClass(Constructor, protoProps, staticProps) {
	  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
	  if (staticProps) _defineProperties(Constructor, staticProps);
	  return Constructor;
	}

	function _inheritsLoose(subClass, superClass) {
	  subClass.prototype = Object.create(superClass.prototype);
	  subClass.prototype.constructor = subClass;
	  subClass.__proto__ = superClass;
	}

	function _objectWithoutPropertiesLoose(source, excluded) {
	  if (source == null) return {};
	  var target = {};
	  var sourceKeys = Object.keys(source);
	  var key, i;

	  for (i = 0; i < sourceKeys.length; i++) {
	    key = sourceKeys[i];
	    if (excluded.indexOf(key) >= 0) continue;
	    target[key] = source[key];
	  }

	  return target;
	}

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME = 'button';
	var VERSION = '4.4.1';
	var DATA_KEY = 'bs.button';
	var EVENT_KEY = "." + DATA_KEY;
	var DATA_API_KEY = '.data-api';
	var JQUERY_NO_CONFLICT = $.fn[NAME];
	var ClassName$1 = {
	  ACTIVE: 'active',
	  BUTTON: 'btn',
	  FOCUS: 'focus'
	};
	var Selector$1 = {
	  DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
	  DATA_TOGGLES: '[data-toggle="buttons"]',
	  DATA_TOGGLE: '[data-toggle="button"]',
	  DATA_TOGGLES_BUTTONS: '[data-toggle="buttons"] .btn',
	  INPUT: 'input:not([type="hidden"])',
	  ACTIVE: '.active',
	  BUTTON: '.btn'
	};
	var Event$2 = {
	  CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY,
	  FOCUS_BLUR_DATA_API: "focus" + EVENT_KEY + DATA_API_KEY + " " + ("blur" + EVENT_KEY + DATA_API_KEY),
	  LOAD_DATA_API: "load" + EVENT_KEY + DATA_API_KEY
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Button = /*#__PURE__*/function () {
	  function Button(element) {
	    this._element = element;
	  } // Getters


	  var _proto = Button.prototype;

	  // Public
	  _proto.toggle = function toggle() {
	    var triggerChangeEvent = true;
	    var addAriaPressed = true;
	    var rootElement = $(this._element).closest(Selector$1.DATA_TOGGLES)[0];

	    if (rootElement) {
	      var input = this._element.querySelector(Selector$1.INPUT);

	      if (input) {
	        if (input.type === 'radio') {
	          if (input.checked && this._element.classList.contains(ClassName$1.ACTIVE)) {
	            triggerChangeEvent = false;
	          } else {
	            var activeElement = rootElement.querySelector(Selector$1.ACTIVE);

	            if (activeElement) {
	              $(activeElement).removeClass(ClassName$1.ACTIVE);
	            }
	          }
	        } else if (input.type === 'checkbox') {
	          if (this._element.tagName === 'LABEL' && input.checked === this._element.classList.contains(ClassName$1.ACTIVE)) {
	            triggerChangeEvent = false;
	          }
	        } else {
	          // if it's not a radio button or checkbox don't add a pointless/invalid checked property to the input
	          triggerChangeEvent = false;
	        }

	        if (triggerChangeEvent) {
	          input.checked = !this._element.classList.contains(ClassName$1.ACTIVE);
	          $(input).trigger('change');
	        }

	        input.focus();
	        addAriaPressed = false;
	      }
	    }

	    if (!(this._element.hasAttribute('disabled') || this._element.classList.contains('disabled'))) {
	      if (addAriaPressed) {
	        this._element.setAttribute('aria-pressed', !this._element.classList.contains(ClassName$1.ACTIVE));
	      }

	      if (triggerChangeEvent) {
	        $(this._element).toggleClass(ClassName$1.ACTIVE);
	      }
	    }
	  };

	  _proto.dispose = function dispose() {
	    $.removeData(this._element, DATA_KEY);
	    this._element = null;
	  } // Static
	  ;

	  Button._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY);

	      if (!data) {
	        data = new Button(this);
	        $(this).data(DATA_KEY, data);
	      }

	      if (config === 'toggle') {
	        data[config]();
	      }
	    });
	  };

	  _createClass(Button, null, [{
	    key: "VERSION",
	    get: function get() {
	      return VERSION;
	    }
	  }]);

	  return Button;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * Data Api implementation
	 * ------------------------------------------------------------------------
	 */


	$(document).on(Event$2.CLICK_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
	  var button = event.target;

	  if (!$(button).hasClass(ClassName$1.BUTTON)) {
	    button = $(button).closest(Selector$1.BUTTON)[0];
	  }

	  if (!button || button.hasAttribute('disabled') || button.classList.contains('disabled')) {
	    event.preventDefault(); // work around Firefox bug #1540995
	  } else {
	    var inputBtn = button.querySelector(Selector$1.INPUT);

	    if (inputBtn && (inputBtn.hasAttribute('disabled') || inputBtn.classList.contains('disabled'))) {
	      event.preventDefault(); // work around Firefox bug #1540995

	      return;
	    }

	    Button._jQueryInterface.call($(button), 'toggle');
	  }
	}).on(Event$2.FOCUS_BLUR_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
	  var button = $(event.target).closest(Selector$1.BUTTON)[0];
	  $(button).toggleClass(ClassName$1.FOCUS, /^focus(in)?$/.test(event.type));
	});
	$(window).on(Event$2.LOAD_DATA_API, function () {
	  // ensure correct active class is set to match the controls' actual values/states
	  // find all checkboxes/readio buttons inside data-toggle groups
	  var buttons = [].slice.call(document.querySelectorAll(Selector$1.DATA_TOGGLES_BUTTONS));

	  for (var i = 0, len = buttons.length; i < len; i++) {
	    var button = buttons[i];
	    var input = button.querySelector(Selector$1.INPUT);

	    if (input.checked || input.hasAttribute('checked')) {
	      button.classList.add(ClassName$1.ACTIVE);
	    } else {
	      button.classList.remove(ClassName$1.ACTIVE);
	    }
	  } // find all button toggles


	  buttons = [].slice.call(document.querySelectorAll(Selector$1.DATA_TOGGLE));

	  for (var _i = 0, _len = buttons.length; _i < _len; _i++) {
	    var _button = buttons[_i];

	    if (_button.getAttribute('aria-pressed') === 'true') {
	      _button.classList.add(ClassName$1.ACTIVE);
	    } else {
	      _button.classList.remove(ClassName$1.ACTIVE);
	    }
	  }
	});
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */

	$.fn[NAME] = Button._jQueryInterface;
	$.fn[NAME].Constructor = Button;

	$.fn[NAME].noConflict = function () {
	  $.fn[NAME] = JQUERY_NO_CONFLICT;
	  return Button._jQueryInterface;
	};

	/* eslint-disable no-undefined,no-param-reassign,no-shadow */

	/**
	 * Throttle execution of a function. Especially useful for rate limiting
	 * execution of handlers on events like resize and scroll.
	 *
	 * @param  {Number}    delay          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {Boolean}   [noTrailing]   Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
	 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
	 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
	 *                                    the internal counter is reset)
	 * @param  {Function}  callback       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                    to `callback` when the throttled-function is executed.
	 * @param  {Boolean}   [debounceMode] If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
	 *                                    schedule `callback` to execute after `delay` ms.
	 *
	 * @return {Function}  A new, throttled, function.
	 */
	function throttle (delay, noTrailing, callback, debounceMode) {
	  /*
	   * After wrapper has stopped being called, this timeout ensures that
	   * `callback` is executed at the proper times in `throttle` and `end`
	   * debounce modes.
	   */
	  var timeoutID;
	  var cancelled = false; // Keep track of the last time `callback` was executed.

	  var lastExec = 0; // Function to clear existing timeout

	  function clearExistingTimeout() {
	    if (timeoutID) {
	      clearTimeout(timeoutID);
	    }
	  } // Function to cancel next exec


	  function cancel() {
	    clearExistingTimeout();
	    cancelled = true;
	  } // `noTrailing` defaults to falsy.


	  if (typeof noTrailing !== 'boolean') {
	    debounceMode = callback;
	    callback = noTrailing;
	    noTrailing = undefined;
	  }
	  /*
	   * The `wrapper` function encapsulates all of the throttling / debouncing
	   * functionality and when executed will limit the rate at which `callback`
	   * is executed.
	   */


	  function wrapper() {
	    var self = this;
	    var elapsed = Date.now() - lastExec;
	    var args = arguments;

	    if (cancelled) {
	      return;
	    } // Execute `callback` and update the `lastExec` timestamp.


	    function exec() {
	      lastExec = Date.now();
	      callback.apply(self, args);
	    }
	    /*
	     * If `debounceMode` is true (at begin) this is used to clear the flag
	     * to allow future `callback` executions.
	     */


	    function clear() {
	      timeoutID = undefined;
	    }

	    if (debounceMode && !timeoutID) {
	      /*
	       * Since `wrapper` is being called for the first time and
	       * `debounceMode` is true (at begin), execute `callback`.
	       */
	      exec();
	    }

	    clearExistingTimeout();

	    if (debounceMode === undefined && elapsed > delay) {
	      /*
	       * In throttle mode, if `delay` time has been exceeded, execute
	       * `callback`.
	       */
	      exec();
	    } else if (noTrailing !== true) {
	      /*
	       * In trailing throttle mode, since `delay` time has not been
	       * exceeded, schedule `callback` to execute `delay` ms after most
	       * recent execution.
	       *
	       * If `debounceMode` is true (at begin), schedule `clear` to execute
	       * after `delay` ms.
	       *
	       * If `debounceMode` is false (at end), schedule `callback` to
	       * execute after `delay` ms.
	       */
	      timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
	    }
	  }

	  wrapper.cancel = cancel; // Return the wrapper function.

	  return wrapper;
	}

	/* eslint-disable no-undefined */
	/**
	 * Debounce execution of a function. Debouncing, unlike throttling,
	 * guarantees that a function is only executed a single time, either at the
	 * very beginning of a series of calls, or at the very end.
	 *
	 * @param  {Number}   delay         A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {Boolean}  [atBegin]     Optional, defaults to false. If atBegin is false or unspecified, callback will only be executed `delay` milliseconds
	 *                                  after the last debounced-function call. If atBegin is true, callback will be executed only at the first debounced-function call.
	 *                                  (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset).
	 * @param  {Function} callback      A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                  to `callback` when the debounced-function is executed.
	 *
	 * @return {Function} A new, debounced function.
	 */

	function debounce (delay, atBegin, callback) {
	  return callback === undefined ? throttle(delay, atBegin, false) : throttle(delay, callback, atBegin !== false);
	}

	var evEmitter = createCommonjsModule(function (module) {
	/**
	 * EvEmitter v1.1.0
	 * Lil' event emitter
	 * MIT License
	 */

	/* jshint unused: true, undef: true, strict: true */

	( function( global, factory ) {
	  // universal module definition
	  /* jshint strict: false */ /* globals define, module, window */
	  if (  module.exports ) {
	    // CommonJS - Browserify, Webpack
	    module.exports = factory();
	  } else {
	    // Browser globals
	    global.EvEmitter = factory();
	  }

	}( typeof window != 'undefined' ? window : commonjsGlobal, function() {

	function EvEmitter() {}

	var proto = EvEmitter.prototype;

	proto.on = function( eventName, listener ) {
	  if ( !eventName || !listener ) {
	    return;
	  }
	  // set events hash
	  var events = this._events = this._events || {};
	  // set listeners array
	  var listeners = events[ eventName ] = events[ eventName ] || [];
	  // only add once
	  if ( listeners.indexOf( listener ) == -1 ) {
	    listeners.push( listener );
	  }

	  return this;
	};

	proto.once = function( eventName, listener ) {
	  if ( !eventName || !listener ) {
	    return;
	  }
	  // add event
	  this.on( eventName, listener );
	  // set once flag
	  // set onceEvents hash
	  var onceEvents = this._onceEvents = this._onceEvents || {};
	  // set onceListeners object
	  var onceListeners = onceEvents[ eventName ] = onceEvents[ eventName ] || {};
	  // set flag
	  onceListeners[ listener ] = true;

	  return this;
	};

	proto.off = function( eventName, listener ) {
	  var listeners = this._events && this._events[ eventName ];
	  if ( !listeners || !listeners.length ) {
	    return;
	  }
	  var index = listeners.indexOf( listener );
	  if ( index != -1 ) {
	    listeners.splice( index, 1 );
	  }

	  return this;
	};

	proto.emitEvent = function( eventName, args ) {
	  var listeners = this._events && this._events[ eventName ];
	  if ( !listeners || !listeners.length ) {
	    return;
	  }
	  // copy over to avoid interference if .off() in listener
	  listeners = listeners.slice(0);
	  args = args || [];
	  // once stuff
	  var onceListeners = this._onceEvents && this._onceEvents[ eventName ];

	  for ( var i=0; i < listeners.length; i++ ) {
	    var listener = listeners[i];
	    var isOnce = onceListeners && onceListeners[ listener ];
	    if ( isOnce ) {
	      // remove listener
	      // remove before trigger to prevent recursion
	      this.off( eventName, listener );
	      // unset once flag
	      delete onceListeners[ listener ];
	    }
	    // trigger listener
	    listener.apply( this, args );
	  }

	  return this;
	};

	proto.allOff = function() {
	  delete this._events;
	  delete this._onceEvents;
	};

	return EvEmitter;

	}));
	});

	var imagesloaded = createCommonjsModule(function (module) {
	/*!
	 * imagesLoaded v4.1.4
	 * JavaScript is all like "You images are done yet or what?"
	 * MIT License
	 */

	( function( window, factory ) {  // universal module definition

	  /*global define: false, module: false, require: false */

	  if (  module.exports ) {
	    // CommonJS
	    module.exports = factory(
	      window,
	      evEmitter
	    );
	  } else {
	    // browser global
	    window.imagesLoaded = factory(
	      window,
	      window.EvEmitter
	    );
	  }

	})( typeof window !== 'undefined' ? window : commonjsGlobal,

	// --------------------------  factory -------------------------- //

	function factory( window, EvEmitter ) {

	var $ = window.jQuery;
	var console = window.console;

	// -------------------------- helpers -------------------------- //

	// extend objects
	function extend( a, b ) {
	  for ( var prop in b ) {
	    a[ prop ] = b[ prop ];
	  }
	  return a;
	}

	var arraySlice = Array.prototype.slice;

	// turn element or nodeList into an array
	function makeArray( obj ) {
	  if ( Array.isArray( obj ) ) {
	    // use object if already an array
	    return obj;
	  }

	  var isArrayLike = typeof obj == 'object' && typeof obj.length == 'number';
	  if ( isArrayLike ) {
	    // convert nodeList to array
	    return arraySlice.call( obj );
	  }

	  // array of single index
	  return [ obj ];
	}

	// -------------------------- imagesLoaded -------------------------- //

	/**
	 * @param {Array, Element, NodeList, String} elem
	 * @param {Object or Function} options - if function, use as callback
	 * @param {Function} onAlways - callback function
	 */
	function ImagesLoaded( elem, options, onAlways ) {
	  // coerce ImagesLoaded() without new, to be new ImagesLoaded()
	  if ( !( this instanceof ImagesLoaded ) ) {
	    return new ImagesLoaded( elem, options, onAlways );
	  }
	  // use elem as selector string
	  var queryElem = elem;
	  if ( typeof elem == 'string' ) {
	    queryElem = document.querySelectorAll( elem );
	  }
	  // bail if bad element
	  if ( !queryElem ) {
	    console.error( 'Bad element for imagesLoaded ' + ( queryElem || elem ) );
	    return;
	  }

	  this.elements = makeArray( queryElem );
	  this.options = extend( {}, this.options );
	  // shift arguments if no options set
	  if ( typeof options == 'function' ) {
	    onAlways = options;
	  } else {
	    extend( this.options, options );
	  }

	  if ( onAlways ) {
	    this.on( 'always', onAlways );
	  }

	  this.getImages();

	  if ( $ ) {
	    // add jQuery Deferred object
	    this.jqDeferred = new $.Deferred();
	  }

	  // HACK check async to allow time to bind listeners
	  setTimeout( this.check.bind( this ) );
	}

	ImagesLoaded.prototype = Object.create( EvEmitter.prototype );

	ImagesLoaded.prototype.options = {};

	ImagesLoaded.prototype.getImages = function() {
	  this.images = [];

	  // filter & find items if we have an item selector
	  this.elements.forEach( this.addElementImages, this );
	};

	/**
	 * @param {Node} element
	 */
	ImagesLoaded.prototype.addElementImages = function( elem ) {
	  // filter siblings
	  if ( elem.nodeName == 'IMG' ) {
	    this.addImage( elem );
	  }
	  // get background image on element
	  if ( this.options.background === true ) {
	    this.addElementBackgroundImages( elem );
	  }

	  // find children
	  // no non-element nodes, #143
	  var nodeType = elem.nodeType;
	  if ( !nodeType || !elementNodeTypes[ nodeType ] ) {
	    return;
	  }
	  var childImgs = elem.querySelectorAll('img');
	  // concat childElems to filterFound array
	  for ( var i=0; i < childImgs.length; i++ ) {
	    var img = childImgs[i];
	    this.addImage( img );
	  }

	  // get child background images
	  if ( typeof this.options.background == 'string' ) {
	    var children = elem.querySelectorAll( this.options.background );
	    for ( i=0; i < children.length; i++ ) {
	      var child = children[i];
	      this.addElementBackgroundImages( child );
	    }
	  }
	};

	var elementNodeTypes = {
	  1: true,
	  9: true,
	  11: true
	};

	ImagesLoaded.prototype.addElementBackgroundImages = function( elem ) {
	  var style = getComputedStyle( elem );
	  if ( !style ) {
	    // Firefox returns null if in a hidden iframe https://bugzil.la/548397
	    return;
	  }
	  // get url inside url("...")
	  var reURL = /url\((['"])?(.*?)\1\)/gi;
	  var matches = reURL.exec( style.backgroundImage );
	  while ( matches !== null ) {
	    var url = matches && matches[2];
	    if ( url ) {
	      this.addBackground( url, elem );
	    }
	    matches = reURL.exec( style.backgroundImage );
	  }
	};

	/**
	 * @param {Image} img
	 */
	ImagesLoaded.prototype.addImage = function( img ) {
	  var loadingImage = new LoadingImage( img );
	  this.images.push( loadingImage );
	};

	ImagesLoaded.prototype.addBackground = function( url, elem ) {
	  var background = new Background( url, elem );
	  this.images.push( background );
	};

	ImagesLoaded.prototype.check = function() {
	  var _this = this;
	  this.progressedCount = 0;
	  this.hasAnyBroken = false;
	  // complete if no images
	  if ( !this.images.length ) {
	    this.complete();
	    return;
	  }

	  function onProgress( image, elem, message ) {
	    // HACK - Chrome triggers event before object properties have changed. #83
	    setTimeout( function() {
	      _this.progress( image, elem, message );
	    });
	  }

	  this.images.forEach( function( loadingImage ) {
	    loadingImage.once( 'progress', onProgress );
	    loadingImage.check();
	  });
	};

	ImagesLoaded.prototype.progress = function( image, elem, message ) {
	  this.progressedCount++;
	  this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
	  // progress event
	  this.emitEvent( 'progress', [ this, image, elem ] );
	  if ( this.jqDeferred && this.jqDeferred.notify ) {
	    this.jqDeferred.notify( this, image );
	  }
	  // check if completed
	  if ( this.progressedCount == this.images.length ) {
	    this.complete();
	  }

	  if ( this.options.debug && console ) {
	    console.log( 'progress: ' + message, image, elem );
	  }
	};

	ImagesLoaded.prototype.complete = function() {
	  var eventName = this.hasAnyBroken ? 'fail' : 'done';
	  this.isComplete = true;
	  this.emitEvent( eventName, [ this ] );
	  this.emitEvent( 'always', [ this ] );
	  if ( this.jqDeferred ) {
	    var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
	    this.jqDeferred[ jqMethod ]( this );
	  }
	};

	// --------------------------  -------------------------- //

	function LoadingImage( img ) {
	  this.img = img;
	}

	LoadingImage.prototype = Object.create( EvEmitter.prototype );

	LoadingImage.prototype.check = function() {
	  // If complete is true and browser supports natural sizes,
	  // try to check for image status manually.
	  var isComplete = this.getIsImageComplete();
	  if ( isComplete ) {
	    // report based on naturalWidth
	    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
	    return;
	  }

	  // If none of the checks above matched, simulate loading on detached element.
	  this.proxyImage = new Image();
	  this.proxyImage.addEventListener( 'load', this );
	  this.proxyImage.addEventListener( 'error', this );
	  // bind to image as well for Firefox. #191
	  this.img.addEventListener( 'load', this );
	  this.img.addEventListener( 'error', this );
	  this.proxyImage.src = this.img.src;
	};

	LoadingImage.prototype.getIsImageComplete = function() {
	  // check for non-zero, non-undefined naturalWidth
	  // fixes Safari+InfiniteScroll+Masonry bug infinite-scroll#671
	  return this.img.complete && this.img.naturalWidth;
	};

	LoadingImage.prototype.confirm = function( isLoaded, message ) {
	  this.isLoaded = isLoaded;
	  this.emitEvent( 'progress', [ this, this.img, message ] );
	};

	// ----- events ----- //

	// trigger specified handler for event type
	LoadingImage.prototype.handleEvent = function( event ) {
	  var method = 'on' + event.type;
	  if ( this[ method ] ) {
	    this[ method ]( event );
	  }
	};

	LoadingImage.prototype.onload = function() {
	  this.confirm( true, 'onload' );
	  this.unbindEvents();
	};

	LoadingImage.prototype.onerror = function() {
	  this.confirm( false, 'onerror' );
	  this.unbindEvents();
	};

	LoadingImage.prototype.unbindEvents = function() {
	  this.proxyImage.removeEventListener( 'load', this );
	  this.proxyImage.removeEventListener( 'error', this );
	  this.img.removeEventListener( 'load', this );
	  this.img.removeEventListener( 'error', this );
	};

	// -------------------------- Background -------------------------- //

	function Background( url, element ) {
	  this.url = url;
	  this.element = element;
	  this.img = new Image();
	}

	// inherit LoadingImage prototype
	Background.prototype = Object.create( LoadingImage.prototype );

	Background.prototype.check = function() {
	  this.img.addEventListener( 'load', this );
	  this.img.addEventListener( 'error', this );
	  this.img.src = this.url;
	  // check if image is already complete
	  var isComplete = this.getIsImageComplete();
	  if ( isComplete ) {
	    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
	    this.unbindEvents();
	  }
	};

	Background.prototype.unbindEvents = function() {
	  this.img.removeEventListener( 'load', this );
	  this.img.removeEventListener( 'error', this );
	};

	Background.prototype.confirm = function( isLoaded, message ) {
	  this.isLoaded = isLoaded;
	  this.emitEvent( 'progress', [ this, this.element, message ] );
	};

	// -------------------------- jQuery -------------------------- //

	ImagesLoaded.makeJQueryPlugin = function( jQuery ) {
	  jQuery = jQuery || window.jQuery;
	  if ( !jQuery ) {
	    return;
	  }
	  // set local variable
	  $ = jQuery;
	  // $().imagesLoaded()
	  $.fn.imagesLoaded = function( options, callback ) {
	    var instance = new ImagesLoaded( this, options, callback );
	    return instance.jqDeferred.promise( $(this) );
	  };
	};
	// try making plugin
	ImagesLoaded.makeJQueryPlugin();

	// --------------------------  -------------------------- //

	return ImagesLoaded;

	});
	});

	var PointerType = {
	  TOUCH: 'touch',
	  PEN: 'pen'
	};

	function _handleSwipe() {
	  var absDeltax = Math.abs(this.touchDeltaX);

	  if (absDeltax <= this.swipeThreshold) {
	    return;
	  }

	  var direction = absDeltax / this.touchDeltaX; // swipe left

	  if (direction > 0) {
	    this.negativeCallback();
	  } // swipe right


	  if (direction < 0) {
	    this.positiveCallback();
	  }
	}

	function _swipeStart(event) {
	  if (this.pointerEvent && PointerType[event.pointerType.toUpperCase()]) {
	    this.touchStartX = event.clientX;
	  } else if (!this.pointerEvent) {
	    this.touchStartX = event.touches[0].clientX;
	  }
	}

	function _swipeMove(event) {
	  // ensure swiping with one touch and not pinching
	  if (event.touches && event.touches.length > 1) {
	    this.touchDeltaX = 0;
	  } else {
	    this.touchDeltaX = event.touches[0].clientX - this.touchStartX;
	  }
	}

	function _swipeEnd(event) {
	  if (this.pointerEvent && PointerType[event.pointerType.toUpperCase()]) {
	    this.touchDeltaX = event.clientX - this.touchStartX;
	  }

	  _handleSwipe.bind(this)();
	}
	/**
	 * Class for handling touch events.
	 */


	var TouchUtil = /*#__PURE__*/function () {
	  /**
	   * Create the touch events handler.
	   * @param {Object} opts - The touch events options.
	   * @param {Node} opts.el - The swipeable DOM node.
	   * @param {Function} opts.positiveCallback - Callback function to be called after swiping in a positive direction.
	   * @param {Function} opts.negativeCallback - Callback function to be called after swiping in a negative direction.
	   * @param {number} [opts.swipeThreshold] - The minimum swipe size
	   * @param {string} [opts.pointerEventClassName] - The classname to add for pointer events
	   */
	  function TouchUtil(opts) {
	    this.el = opts.el;
	    this.positiveCallback = opts.positiveCallback;
	    this.negativeCallback = opts.negativeCallback;
	    this.swipeThreshold = opts.swipeThreshold || 40;
	    this.pointerEventClassName = opts.pointerEventClassName || 'pointer-event';
	    this.touchStartX = 0;
	    this.touchDeltaX = 0;
	    this.touchSupported = 'ontouchstart' in document.documentElement || navigator.maxTouchPoints > 0;
	    this.pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent);
	  }
	  /**
	   * Add the touch event listeners.
	   */


	  var _proto = TouchUtil.prototype;

	  _proto.addEventListeners = function addEventListeners() {
	    if (this.touchSupported) {
	      if (this.pointerEvent) {
	        this.el.addEventListener('pointerdown', _swipeStart.bind(this));
	        this.el.addEventListener('pointerup', _swipeEnd.bind(this));
	        this.el.classList.add(this.pointerEventClassName);
	      } else {
	        this.el.addEventListener('touchstart', _swipeStart.bind(this));
	        this.el.addEventListener('touchmove', _swipeMove.bind(this));
	        this.el.addEventListener('touchend', _swipeEnd.bind(this));
	      }
	    }
	  }
	  /**
	   * Remove the touch event listeners.
	   */
	  ;

	  _proto.removeEventListeners = function removeEventListeners() {
	    if (this.touchSupported) {
	      if (this.pointerEvent) {
	        this.el.removeEventListener('pointerdown', _swipeStart.bind(this));
	        this.el.removeEventListener('pointerup', _swipeEnd.bind(this));
	        this.el.classList.remove(this.pointerEventClassName);
	      } else {
	        this.el.removeEventListener('touchstart', _swipeStart.bind(this));
	        this.el.removeEventListener('touchmove', _swipeMove.bind(this));
	        this.el.removeEventListener('touchend', _swipeEnd.bind(this));
	      }
	    }
	  };

	  return TouchUtil;
	}();

	var ClassName$2 = {
	  ACTIVE: 'active',
	  SLIDE: 'slide',
	  SLIDE_IN: 'sliding-in',
	  LAYERED: 'carousel-layered',
	  PRODUCT_PLACEMENT: 'carousel-product-placement',
	  RIGHT: 'carousel-item-right',
	  LEFT: 'carousel-item-left',
	  NEXT: 'carousel-item-next',
	  PREV: 'carousel-item-prev',
	  GET_HEIGHT: 'get-height'
	};
	var Direction = {
	  NEXT: 'next',
	  PREV: 'prev',
	  LEFT: 'left',
	  RIGHT: 'right'
	};
	var Selector$2 = {
	  ACTIVE: '.active',
	  ACTIVE_ITEM: '.active.carousel-item',
	  ITEM: '.carousel-item',
	  ITEM_IMG: '.carousel-item img',
	  INDICATORS: '.carousel-indicators',
	  DATA_SLIDE_PREV: '[data-slide="prev"]',
	  DATA_SLIDE_NEXT: '[data-slide="next"]',
	  DATA_MOUNT: '[data-mount="carousel"],[data-ride="carousel"]',
	  DATA_LOOP: 'data-loop',
	  DATA_STATUS: 'data-status',
	  CAROUSEL_INNER: '.carousel-inner',
	  ROW: '.row',
	  SLIDE_ITEM: '.slide-item'
	};
	var Event$3 = {
	  ON_CHANGE: 'onChange',
	  ON_UPDATE: 'onUpdate',
	  ON_REMOVE: 'onRemove'
	};
	/**
	 * Private functions.
	 */

	function _getItemIndex(element) {
	  var items = element && element.parentNode ? [].slice.call(element.parentNode.querySelectorAll(Selector$2.ITEM)) : [];
	  return items.indexOf(element);
	}

	function _getInitialSlideIndex() {
	  var activeItem = this.el.querySelector(Selector$2.ACTIVE_ITEM);
	  return _getItemIndex.bind(this)(activeItem);
	}

	function _getNextSlide() {
	  var index = this.currentSlideIndex + 1; // If index exceeds slide length, return to index 0

	  return index > this.slides.length - 1 ? 0 : index;
	}

	function _getPrevSlide() {
	  var index = this.currentSlideIndex - 1; // If index is less than 0, move to last slide index

	  return index < 0 ? this.slides.length - 1 : index;
	}

	function _getSlide(num) {
	  // Record highest number, 0 or passed-in value
	  var max = Math.max(num, 0); // Return lowest number, either previous number or the maximum slide index

	  return Math.min(max, this.slides.length - 1);
	}

	function _getStatusContainer() {
	  // Check if we are maintaing a status message for this carousel
	  // and that the element exists on the page
	  var statusContainer = this.el.getAttribute(Selector$2.DATA_STATUS);
	  return statusContainer ? document.getElementById(statusContainer) : null;
	}

	function _shouldLoopSlides() {
	  // Loop by default unless data-loop is set to false
	  return !(this.el.getAttribute(Selector$2.DATA_LOOP) === 'false');
	}

	function _onFirstSlide() {
	  return this.currentSlideIndex === 0;
	}

	function _onLastSlide() {
	  return this.currentSlideIndex === this.slides.length - 1;
	}

	function _shouldGoForward() {
	  return _onLastSlide.bind(this)() ? this.loopSlides : true;
	}

	function _shouldGoBack() {
	  return _onFirstSlide.bind(this)() ? this.loopSlides : true;
	}

	function _prevBtnOnClick() {
	  this.goToPrevSlide();
	}

	function _nextBtnOnClick() {
	  // Add events to manage focus order for accessibility
	  Util$1.addEvents(this.nextBtnEvents);
	  this.goToNextSlide();
	}

	function _imgOnDrag(event) {
	  // Prevent images inside slides from being dragged and interfering with touch interaction
	  event.preventDefault();
	}

	function _slide(direction, nextElementIndex) {
	  var _this = this;

	  var activeElement = this.slides[this.currentSlideIndex];
	  var nextElement = this.slides[nextElementIndex];
	  var directionalClassName;
	  var orderClassName;

	  if (direction === Direction.NEXT) {
	    directionalClassName = ClassName$2.LEFT;
	    orderClassName = ClassName$2.NEXT;
	  } else {
	    directionalClassName = ClassName$2.RIGHT;
	    orderClassName = ClassName$2.PREV;
	  }

	  if (nextElement && nextElement.classList.contains(ClassName$2.ACTIVE)) {
	    this.isSliding = false;
	    return;
	  }

	  if (!activeElement || !nextElement) {
	    // Some weirdness is happening, so we bail
	    return;
	  }

	  this.isSliding = true;

	  _setActiveIndicatorElement.bind(this)(nextElementIndex);

	  if (this.el.classList.contains(ClassName$2.LAYERED)) {
	    _removeNextPrevClasses.bind(this)();
	  }

	  if (this.el.classList.contains(ClassName$2.SLIDE)) {
	    nextElement.classList.add(orderClassName, ClassName$2.SLIDE_IN);
	    Util$1.reflow(nextElement);
	    activeElement.classList.add(directionalClassName);
	    nextElement.classList.add(directionalClassName);
	    var transitionDuration = Util$1.getTransitionDurationFromElement(activeElement);
	    setTimeout(function () {
	      nextElement.classList.remove(directionalClassName, orderClassName, ClassName$2.SLIDE_IN);
	      nextElement.classList.add(ClassName$2.ACTIVE);
	      activeElement.classList.remove(ClassName$2.ACTIVE, orderClassName, directionalClassName);
	      _this.isSliding = false;
	    }, transitionDuration);
	  } else {
	    activeElement.classList.remove(ClassName$2.ACTIVE);
	    nextElement.classList.add(ClassName$2.ACTIVE);
	    this.isSliding = false;
	  }

	  _setSlideAttributes.bind(this)(nextElementIndex);

	  this.didSlide = true;
	  this.currentSlideIndex = nextElementIndex;

	  if (this.el.classList.contains(ClassName$2.LAYERED)) {
	    _addNextPrevClasses.bind(this)();
	  }

	  _setButtonAttributes.bind(this)(); // Update the status message


	  if (this.statusContainer) {
	    _setStatusMessage.bind(this)(nextElementIndex);
	  }
	}

	function _setActiveIndicatorElement(index) {
	  if (this.indicators) {
	    var indicators = [].slice.call(this.indicators.querySelectorAll(Selector$2.ACTIVE));
	    indicators.forEach(function (indicator) {
	      indicator.classList.remove(ClassName$2.ACTIVE);
	    });
	    var nextIndicator = this.indicators.children[index];

	    if (nextIndicator) {
	      nextIndicator.classList.add(ClassName$2.ACTIVE);
	    }
	  }
	}

	function _removeNextPrevClasses() {
	  var nextElementIndex = _getNextSlide.bind(this)();

	  var prevElementIndex = _getPrevSlide.bind(this)();

	  this.slides[prevElementIndex].classList.remove(ClassName$2.PREV);
	  this.slides[nextElementIndex].classList.remove(ClassName$2.NEXT);
	}

	function _addNextPrevClasses() {
	  var nextElementIndex = _getNextSlide.bind(this)();

	  var prevElementIndex = _getPrevSlide.bind(this)();

	  this.slides[nextElementIndex].classList.add(ClassName$2.NEXT);
	  this.slides[prevElementIndex].classList.add(ClassName$2.PREV);
	}

	function _setSlideAttributes(index) {
	  for (var i = 0; i < this.slides.length; i++) {
	    if (i === index) {
	      this.slides[i].removeAttribute('aria-hidden');

	      if (this.el.classList.contains(ClassName$2.PRODUCT_PLACEMENT)) {
	        // Product placement carousel needs the first product placement focusable, not the whole slide
	        var slideItems = [].slice.call(this.slides[i].querySelectorAll(Selector$2.SLIDE_ITEM));
	        this.slides[i].removeAttribute('tabindex');
	        slideItems[0].firstElementChild.setAttribute('tabindex', 0);
	      } else {
	        this.slides[i].setAttribute('tabindex', 0);
	      }
	    } else {
	      this.slides[i].removeAttribute('tabindex');
	      this.slides[i].setAttribute('aria-hidden', 'true');
	    }
	  }
	}

	function _setActiveClass(index) {
	  for (var i = 0; i < this.slides.length; i++) {
	    if (i === index) {
	      this.slides[i].classList.add(ClassName$2.ACTIVE);
	    } else {
	      this.slides[i].classList.remove(ClassName$2.ACTIVE);
	    }
	  }
	}

	function _setButtonAttributes() {
	  if (!this.loopSlides) {
	    if (_onFirstSlide.bind(this)()) {
	      this.prevBtn.setAttribute('disabled', '');
	      this.prevBtn.setAttribute('tabindex', -1);
	      this.nextBtn.removeAttribute('disabled');
	    } else if (_onLastSlide.bind(this)()) {
	      this.prevBtn.removeAttribute('disabled');
	      this.prevBtn.removeAttribute('tabindex');
	      this.nextBtn.setAttribute('disabled', '');
	    } else {
	      this.prevBtn.removeAttribute('disabled');
	      this.prevBtn.removeAttribute('tabindex');
	      this.nextBtn.removeAttribute('disabled');
	    }
	  }
	}

	function _setStatusMessage(index) {
	  // Currently only product placement carousel supports a status message,
	  // but this could be expanded to other types of carousels
	  if (this.el.classList.contains(ClassName$2.PRODUCT_PLACEMENT)) {
	    var slideItems = [].slice.call(this.el.querySelectorAll(Selector$2.SLIDE_ITEM));
	    var activeSlide = this.slides[index];
	    var activeSlideItems = activeSlide.querySelectorAll(Selector$2.SLIDE_ITEM);
	    var start = slideItems.indexOf(activeSlideItems[0]) + 1;
	    var separator = '–';
	    var end = slideItems.indexOf(activeSlideItems[activeSlideItems.length - 1]) + 1;
	    var data = {
	      start: start,
	      separator: separator,
	      end: end,
	      total: slideItems.length
	    };
	    this.srStatusContainer.textContent = Util$1.interpolateString(this.srStatusTemplate, data); // If we are only showing one item, set separator and end to an empty string for the visible template

	    if (start === end) {
	      data.separator = '';
	      data.end = '';
	    }

	    this.visibleStatusContainer.textContent = Util$1.interpolateString(this.visibleStatusTemplate, data);
	  }
	}

	function _setSlideHeights() {
	  // Hack to enforce consistent height (flexbox messes with animation)
	  // TODO: remove this as part of carousel rewrite
	  var slideArray = [].slice.call(this.slides);
	  var maxHeight = slideArray[0].clientHeight;
	  slideArray.forEach(function (slide) {
	    if (!slide.classList.contains(ClassName$2.ACTIVE)) {
	      slide.classList.add(ClassName$2.GET_HEIGHT);
	    }

	    if (slide.clientHeight > maxHeight) {
	      maxHeight = slide.clientHeight;
	    }

	    slide.classList.remove(ClassName$2.GET_HEIGHT);
	  });
	  slideArray.forEach(function (slide) {
	    slide.style.height = maxHeight + "px";
	  });
	}

	function _removeSlideHeights() {
	  var slideArray = [].slice.call(this.slides);
	  slideArray.forEach(function (slide) {
	    slide.style.height = '';
	  });
	}

	function _handleKeyDown(event) {
	  var keycode = event.keycode || event.which;

	  if (keycode === Util$1.keyCodes.TAB && this.didSlide) {
	    _focusOnSlide.bind(this)(this.currentSlideIndex);

	    this.didSlide = false;
	    event.preventDefault();
	  }

	  _removeControlEventListeners.bind(this)();
	}

	function _focusOnSlide(index) {
	  this.slides[index].focus();
	}

	function _removeControlEventListeners() {
	  this.didSlide = false;
	  Util$1.removeEvents(this.nextBtnEvents);
	}

	function _reallocateSlideItems() {
	  var _this2 = this;

	  var inner = this.el.querySelector(Selector$2.CAROUSEL_INNER);
	  var activeSlide = this.el.querySelector(Selector$2.ACTIVE_ITEM);
	  var slideItemsContainer = activeSlide.querySelector(Selector$2.ROW);
	  var slideItems = [].slice.call(this.el.querySelectorAll(Selector$2.SLIDE_ITEM));
	  var activeSlideItems = activeSlide.querySelectorAll(Selector$2.SLIDE_ITEM);
	  var maxItems = Math.round(slideItemsContainer.clientWidth / activeSlideItems[0].clientWidth);
	  var slidesNeeded = Math.ceil(slideItems.length / maxItems);
	  var slidesToAdd = slidesNeeded - this.slides.length; // Reset CSS properties

	  _removeSlideHeights.bind(this)();

	  this.prevBtn.style.display = '';
	  this.nextBtn.style.display = '';

	  if (this.statusContainer) {
	    this.statusContainer.style.display = '';
	    this.statusContainer.nextElementSibling.style.display = '';
	  }

	  if (slidesToAdd > 0) {
	    // We need to add more slides
	    for (var i = 0; i < slidesToAdd; i++) {
	      var newNode = this.slides[this.slides.length - 1].cloneNode(true);
	      inner.appendChild(newNode);
	      var newParent = newNode.querySelector(Selector$2.ROW); // Clear out duplicated slide items

	      while (newParent.firstChild) {
	        newParent.removeChild(newParent.lastChild);
	      }
	    }
	  } else if (slidesToAdd < 0) {
	    // We need to remove some slides
	    for (var _i = 0; _i > slidesToAdd; _i--) {
	      inner.removeChild(inner.lastChild);
	    }
	  } // Reallocate the slide items among the slides


	  var slideItemsContainers = this.el.querySelectorAll(Selector$2.ROW);
	  var itemsToAppend;

	  var _loop = function _loop(_i2) {
	    var remainder = slideItems.length % maxItems;

	    if (remainder > 0) {
	      itemsToAppend = slideItems.splice(slideItems.length - remainder, remainder);
	    } else {
	      itemsToAppend = slideItems.splice(slideItems.length - maxItems, maxItems);
	    }

	    itemsToAppend.forEach(function (item) {
	      slideItemsContainers[_i2].appendChild(item);
	    });
	  };

	  for (var _i2 = slideItemsContainers.length - 1; _i2 >= 0; _i2--) {
	    _loop(_i2);
	  } // Update the slides property


	  this.slides = this.el.querySelectorAll(Selector$2.ITEM); // Reset current slide index if it's on a slide that's been removed

	  if (this.currentSlideIndex > this.slides.length - 1) {
	    this.currentSlideIndex = this.slides.length - 1;
	  } // If there is only one slide, hide the controls, status msg, and cta


	  if (this.slides.length === 1) {
	    this.prevBtn.style.display = 'none';
	    this.nextBtn.style.display = 'none';

	    if (this.statusContainer) {
	      this.statusContainer.style.display = 'none';
	      this.statusContainer.nextElementSibling.style.display = 'none';
	    }
	  } // TODO: remove this dependency as part of carousel refactor


	  imagesloaded(this.el, function () {
	    _setSlideHeights.bind(_this2)();
	  });
	}

	function _setupDom() {
	  // Reallocate slide items for product placement carousel
	  if (this.el.classList.contains(ClassName$2.PRODUCT_PLACEMENT)) {
	    _reallocateSlideItems.bind(this)();
	  } // Make sure slide attributes and indicators are up to date


	  _setSlideAttributes.bind(this)(this.currentSlideIndex);

	  _setActiveClass.bind(this)(this.currentSlideIndex);

	  _setActiveIndicatorElement.bind(this)(this.currentSlideIndex); // For layered carousel layouts, add prev and next classes to slides


	  if (this.el.classList.contains(ClassName$2.LAYERED)) {
	    _addNextPrevClasses.bind(this)();
	  } // Update button attributes, for non-looping carousels


	  _setButtonAttributes.bind(this)(); // Update the status message


	  if (this.statusContainer) {
	    _setStatusMessage.bind(this)(this.currentSlideIndex);
	  }
	}
	/**
	 * Class representing carousel controls.
	 */


	var CarouselControls = /*#__PURE__*/function () {
	  /**
	   * Create the carousel controls.
	   * @param {Object} opts - The carousel controls options.
	   * @param {Node} opts.el - The carousel DOM node.
	   * @param {Node[]} opts.slides - Array of carousel slides.
	   * @param {number} [opts.initialSlideIndex] - Index of the first carousel slide.
	   * @param {boolean} [opts.loopSlides=true] - Whether the carousel should loop. Defaults to true.
	   * @param {Node} [opts.statusContainer] - Node that contains the status message templates.
	   * @param {Function} [opts.prevOnClick] - Function to override the previous button click handler.
	   * @param {Function} [opts.nextOnClick] - Function to override the next button click handler.
	   */
	  function CarouselControls(opts) {
	    this.el = opts.el;
	    this.slides = opts.slides;
	    this.currentSlideIndex = opts.initialSlideIndex || _getInitialSlideIndex.bind(this)();
	    this.loopSlides = typeof opts.loopSlides === 'boolean' ? opts.loopSlides : _shouldLoopSlides.bind(this)();
	    this.statusContainer = opts.statusContainer || _getStatusContainer.bind(this)();
	    this.prevOnClick = opts.prevOnClick || _prevBtnOnClick.bind(this);
	    this.nextOnClick = opts.nextOnClick || _nextBtnOnClick.bind(this); // Internal variables

	    this.isSliding = false;
	    this.didSlide = false;
	    this.touchUtil = new TouchUtil({
	      el: this.el,
	      positiveCallback: this.goToNextSlide.bind(this),
	      negativeCallback: this.goToPrevSlide.bind(this)
	    }); // Select control nodes

	    this.prevBtn = this.el.querySelector(Selector$2.DATA_SLIDE_PREV);
	    this.nextBtn = this.el.querySelector(Selector$2.DATA_SLIDE_NEXT);
	    this.indicators = this.el.querySelector(Selector$2.INDICATORS);
	    this.itemImg = this.el.querySelector(Selector$2.ITEM_IMG);

	    if (this.statusContainer) {
	      this.visibleStatusContainer = this.statusContainer.querySelector('[aria-hidden="true"]');
	      this.visibleStatusTemplate = this.visibleStatusContainer.textContent;
	      this.srStatusContainer = this.statusContainer.querySelector('.sr-only');
	      this.srStatusTemplate = this.srStatusContainer.textContent;
	    } // Attach event listeners


	    this.events = [{
	      el: this.prevBtn,
	      type: 'click',
	      handler: this.prevOnClick
	    }, {
	      el: this.nextBtn,
	      type: 'click',
	      handler: this.nextOnClick
	    }, {
	      el: this.itemImg,
	      type: 'dragstart',
	      handler: _imgOnDrag
	    }]; // Product placement needs an additional event listener

	    if (this.el.classList.contains(ClassName$2.PRODUCT_PLACEMENT)) {
	      this.events.push({
	        el: window,
	        type: 'resize',
	        handler: debounce(300, this.update.bind(this)),
	        options: {
	          passive: true
	        }
	      });
	    }

	    Util$1.addEvents(this.events);
	    this.touchUtil.addEventListeners(); // Event listeners that need to be added/removed based on user interaction for accessibility
	    // After someone activates the next button, but before the slide animation is over, the next tab keypress
	    // needs to direct focus to the next slide

	    this.nextBtnEvents = [{
	      el: this.nextBtn,
	      type: 'keydown',
	      handler: _handleKeyDown.bind(this)
	    }, {
	      el: this.nextBtn,
	      type: 'blur',
	      handler: _removeControlEventListeners.bind(this)
	    }]; // Create custom events

	    this[Event$3.ON_CHANGE] = new CustomEvent(Event$3.ON_CHANGE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$3.ON_UPDATE] = new CustomEvent(Event$3.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$3.ON_REMOVE] = new CustomEvent(Event$3.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    }); // Setup DOM

	    _setupDom.bind(this)();
	  }
	  /**
	   * Remove the carousel controls event handlers.
	   */


	  var _proto = CarouselControls.prototype;

	  _proto.remove = function remove() {
	    // Remove event listeners
	    Util$1.removeEvents(this.events);
	    this.touchUtil.removeEventListeners();

	    _removeControlEventListeners.bind(this)();

	    this.el.dispatchEvent(this[Event$3.ON_REMOVE]);
	  }
	  /**
	   * Update the carousel controls instance.
	   */
	  ;

	  _proto.update = function update() {
	    // For layered carousel layouts, remove prev and next classes from existing slides
	    if (this.el.classList.contains(ClassName$2.LAYERED)) {
	      _removeNextPrevClasses.bind(this)();
	    } // Update the slides property


	    this.slides = this.el.querySelectorAll(Selector$2.ITEM); // Setup DOM

	    _setupDom.bind(this)();

	    this.el.dispatchEvent(this[Event$3.ON_UPDATE]);
	  }
	  /**
	   * Go forward to the next slide.
	   */
	  ;

	  _proto.goToNextSlide = function goToNextSlide() {
	    if (!this.isSliding && _shouldGoForward.bind(this)()) {
	      _slide.bind(this)(Direction.NEXT, _getNextSlide.bind(this)());

	      this.el.dispatchEvent(this[Event$3.ON_CHANGE]);
	    }
	  }
	  /**
	   * Go back to the previous slide.
	   */
	  ;

	  _proto.goToPrevSlide = function goToPrevSlide() {
	    if (!this.isSliding && _shouldGoBack.bind(this)()) {
	      _slide.bind(this)(Direction.PREV, _getPrevSlide.bind(this)());

	      this.el.dispatchEvent(this[Event$3.ON_CHANGE]);
	    }
	  }
	  /**
	   * Go to a specific slide.
	   * @param {number} num - 0-based index of the slide to change to.
	   */
	  ;

	  _proto.goToSlide = function goToSlide(num) {
	    if (!this.isSliding) {
	      _slide.bind(this)(Direction.PREV, _getSlide.bind(this)(num));

	      this.el.dispatchEvent(this[Event$3.ON_CHANGE]);
	    }
	  };

	  return CarouselControls;
	}();

	var carousels = [];
	/**
	 * Class representing a carousel.
	 */

	var Carousel = /*#__PURE__*/function () {
	  /**
	   * Create the carousel.
	   * @param {Object} opts - The carousel options.
	   * @param {Node} opts.el - The carousel DOM node.
	   */
	  function Carousel(opts) {
	    this.el = opts.el;
	    this.controls = new CarouselControls(Object.assign({
	      slides: opts.el.querySelectorAll(Selector$2.ITEM)
	    }, opts));
	    carousels.push(this);
	  }
	  /**
	   * Remove the carousel.
	   */


	  var _proto = Carousel.prototype;

	  _proto.remove = function remove() {
	    // remove any references from controls
	    this.controls.remove();
	    delete this.controls; // remove this carousel reference from array of instances

	    var index = carousels.indexOf(this);
	    carousels.splice(index, 1);
	  }
	  /**
	   * Get an array of carousel instances.
	   * @returns {Object[]} Array of carousel instances.
	   */
	  ;

	  Carousel.getInstances = function getInstances() {
	    return carousels;
	  };

	  return Carousel;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$2.DATA_MOUNT, function (node) {
	    new Carousel({
	      el: node
	    });
	  });
	})();

	// Enables click event navigation on blocks of content
	// Activate with data-group="click" on parent element
	// Triggers click event on first link in content block by default or configure target element with data-target="{id}"
	// Parameters:
	// el: {required} Content container element
	// opts: {optional} Options object
	var Selector$3 = {
	  DATA_EVENT: '[data-group="click"]'
	};
	var clickGroups = [];

	var ClickGroup = /*#__PURE__*/function () {
	  function ClickGroup(opts) {
	    this.el = opts.el;
	    this.target = this.getTarget();
	    this.onClick = opts.onClick || this.onElClick.bind(this);
	    this.options = opts;

	    if (this.target) {
	      this.el.style.cursor = 'pointer';
	      this.el.addEventListener('click', this.onClick);
	    }

	    clickGroups.push(this);
	  }

	  var _proto = ClickGroup.prototype;

	  _proto.getTarget = function getTarget() {
	    var selector = this.el.dataset.target;

	    if (selector) {
	      return this.el.querySelector("#" + selector);
	    }

	    var firstLink = this.el.getElementsByTagName('a')[0];
	    return firstLink ? firstLink : null;
	  };

	  _proto.onElClick = function onElClick(e) {
	    if (e.target.tagName !== 'A' && e.target.tagName !== 'BUTTON') {
	      if (this.options && this.options.onClickCallback) {
	        this.options.onClickCallback();
	      }

	      this.target.click();
	    }
	  };

	  _proto.remove = function remove() {
	    if (this.target) {
	      this.el.style.cursor = '';
	      this.el.removeEventListener('click', this.onClick);
	    }

	    var index = clickGroups.indexOf(this);
	    clickGroups.splice(index, 1);
	  };

	  ClickGroup.getInstances = function getInstances() {
	    return clickGroups;
	  };

	  return ClickGroup;
	}();

	(function () {
	  Array.prototype.forEach.call(document.querySelectorAll(Selector$3.DATA_EVENT), function (element) {
	    new ClickGroup({
	      el: element
	    });
	  });
	})();

	var $filter = arrayIteration.filter;



	var HAS_SPECIES_SUPPORT$2 = arrayMethodHasSpeciesSupport('filter');
	// Edge 14- issue
	var USES_TO_LENGTH$4 = arrayMethodUsesToLength('filter');

	// `Array.prototype.filter` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.filter
	// with adding support of @@species
	_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$2 || !USES_TO_LENGTH$4 }, {
	  filter: function filter(callbackfn /* , thisArg */) {
	    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$1 = 'collapse';
	var VERSION$1 = '4.4.1';
	var DATA_KEY$1 = 'bs.collapse';
	var EVENT_KEY$1 = "." + DATA_KEY$1;
	var DATA_API_KEY$1 = '.data-api';
	var JQUERY_NO_CONFLICT$1 = $.fn[NAME$1];
	var Default = {
	  toggle: true,
	  parent: ''
	};
	var DefaultType = {
	  toggle: 'boolean',
	  parent: '(string|element)'
	};
	var Event$4 = {
	  SHOW: "show" + EVENT_KEY$1,
	  SHOWN: "shown" + EVENT_KEY$1,
	  HIDE: "hide" + EVENT_KEY$1,
	  HIDDEN: "hidden" + EVENT_KEY$1,
	  CLICK_DATA_API: "click" + EVENT_KEY$1 + DATA_API_KEY$1
	};
	var ClassName$3 = {
	  SHOW: 'show',
	  COLLAPSE: 'collapse',
	  COLLAPSING: 'collapsing',
	  COLLAPSED: 'collapsed'
	};
	var Dimension = {
	  WIDTH: 'width',
	  HEIGHT: 'height'
	};
	var Selector$4 = {
	  ACTIVES: '.show, .collapsing',
	  DATA_TOGGLE: '[data-toggle="collapse"]'
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Collapse = /*#__PURE__*/function () {
	  function Collapse(element, config) {
	    this._isTransitioning = false;
	    this._element = element;
	    this._config = this._getConfig(config);
	    this._triggerArray = [].slice.call(document.querySelectorAll("[data-toggle=\"collapse\"][href=\"#" + element.id + "\"]," + ("[data-toggle=\"collapse\"][data-target=\"#" + element.id + "\"]")));
	    var toggleList = [].slice.call(document.querySelectorAll(Selector$4.DATA_TOGGLE));

	    for (var i = 0, len = toggleList.length; i < len; i++) {
	      var elem = toggleList[i];
	      var selector = Util.getSelectorFromElement(elem);
	      var filterElement = [].slice.call(document.querySelectorAll(selector)).filter(function (foundElem) {
	        return foundElem === element;
	      });

	      if (selector !== null && filterElement.length > 0) {
	        this._selector = selector;

	        this._triggerArray.push(elem);
	      }
	    }

	    this._parent = this._config.parent ? this._getParent() : null;

	    if (!this._config.parent) {
	      this._addAriaAndCollapsedClass(this._element, this._triggerArray);
	    }

	    if (this._config.toggle) {
	      this.toggle();
	    }
	  } // Getters


	  var _proto = Collapse.prototype;

	  // Public
	  _proto.toggle = function toggle() {
	    if ($(this._element).hasClass(ClassName$3.SHOW)) {
	      this.hide();
	    } else {
	      this.show();
	    }
	  };

	  _proto.show = function show() {
	    var _this = this;

	    if (this._isTransitioning || $(this._element).hasClass(ClassName$3.SHOW)) {
	      return;
	    }

	    var actives;
	    var activesData;

	    if (this._parent) {
	      actives = [].slice.call(this._parent.querySelectorAll(Selector$4.ACTIVES)).filter(function (elem) {
	        if (typeof _this._config.parent === 'string') {
	          return elem.getAttribute('data-parent') === _this._config.parent;
	        }

	        return elem.classList.contains(ClassName$3.COLLAPSE);
	      });

	      if (actives.length === 0) {
	        actives = null;
	      }
	    }

	    if (actives) {
	      activesData = $(actives).not(this._selector).data(DATA_KEY$1);

	      if (activesData && activesData._isTransitioning) {
	        return;
	      }
	    }

	    var startEvent = $.Event(Event$4.SHOW);
	    $(this._element).trigger(startEvent);

	    if (startEvent.isDefaultPrevented()) {
	      return;
	    }

	    if (actives) {
	      Collapse._jQueryInterface.call($(actives).not(this._selector), 'hide');

	      if (!activesData) {
	        $(actives).data(DATA_KEY$1, null);
	      }
	    }

	    var dimension = this._getDimension();

	    $(this._element).removeClass(ClassName$3.COLLAPSE).addClass(ClassName$3.COLLAPSING);
	    this._element.style[dimension] = 0;

	    if (this._triggerArray.length) {
	      $(this._triggerArray).removeClass(ClassName$3.COLLAPSED).attr('aria-expanded', true);
	    }

	    this.setTransitioning(true);

	    var complete = function complete() {
	      $(_this._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).addClass(ClassName$3.SHOW);
	      _this._element.style[dimension] = '';

	      _this.setTransitioning(false);

	      $(_this._element).trigger(Event$4.SHOWN);
	    };

	    var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
	    var scrollSize = "scroll" + capitalizedDimension;
	    var transitionDuration = Util.getTransitionDurationFromElement(this._element);
	    $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	    this._element.style[dimension] = this._element[scrollSize] + "px";
	  };

	  _proto.hide = function hide() {
	    var _this2 = this;

	    if (this._isTransitioning || !$(this._element).hasClass(ClassName$3.SHOW)) {
	      return;
	    }

	    var startEvent = $.Event(Event$4.HIDE);
	    $(this._element).trigger(startEvent);

	    if (startEvent.isDefaultPrevented()) {
	      return;
	    }

	    var dimension = this._getDimension();

	    this._element.style[dimension] = this._element.getBoundingClientRect()[dimension] + "px";
	    Util.reflow(this._element);
	    $(this._element).addClass(ClassName$3.COLLAPSING).removeClass(ClassName$3.COLLAPSE).removeClass(ClassName$3.SHOW);
	    var triggerArrayLength = this._triggerArray.length;

	    if (triggerArrayLength > 0) {
	      for (var i = 0; i < triggerArrayLength; i++) {
	        var trigger = this._triggerArray[i];
	        var selector = Util.getSelectorFromElement(trigger);

	        if (selector !== null) {
	          var $elem = $([].slice.call(document.querySelectorAll(selector)));

	          if (!$elem.hasClass(ClassName$3.SHOW)) {
	            $(trigger).addClass(ClassName$3.COLLAPSED).attr('aria-expanded', false);
	          }
	        }
	      }
	    }

	    this.setTransitioning(true);

	    var complete = function complete() {
	      _this2.setTransitioning(false);

	      $(_this2._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).trigger(Event$4.HIDDEN);
	    };

	    this._element.style[dimension] = '';
	    var transitionDuration = Util.getTransitionDurationFromElement(this._element);
	    $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	  };

	  _proto.setTransitioning = function setTransitioning(isTransitioning) {
	    this._isTransitioning = isTransitioning;
	  };

	  _proto.dispose = function dispose() {
	    $.removeData(this._element, DATA_KEY$1);
	    this._config = null;
	    this._parent = null;
	    this._element = null;
	    this._triggerArray = null;
	    this._isTransitioning = null;
	  } // Private
	  ;

	  _proto._getConfig = function _getConfig(config) {
	    config = Object.assign({}, Default, {}, config);
	    config.toggle = Boolean(config.toggle); // Coerce string values

	    Util.typeCheckConfig(NAME$1, config, DefaultType);
	    return config;
	  };

	  _proto._getDimension = function _getDimension() {
	    var hasWidth = $(this._element).hasClass(Dimension.WIDTH);
	    return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
	  };

	  _proto._getParent = function _getParent() {
	    var _this3 = this;

	    var parent;

	    if (Util.isElement(this._config.parent)) {
	      parent = this._config.parent; // It's a jQuery object

	      if (typeof this._config.parent.jquery !== 'undefined') {
	        parent = this._config.parent[0];
	      }
	    } else {
	      parent = document.querySelector(this._config.parent);
	    }

	    var selector = "[data-toggle=\"collapse\"][data-parent=\"" + this._config.parent + "\"]";
	    var children = [].slice.call(parent.querySelectorAll(selector));
	    $(children).each(function (i, element) {
	      _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
	    });
	    return parent;
	  };

	  _proto._addAriaAndCollapsedClass = function _addAriaAndCollapsedClass(element, triggerArray) {
	    var isOpen = $(element).hasClass(ClassName$3.SHOW);

	    if (triggerArray.length) {
	      $(triggerArray).toggleClass(ClassName$3.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
	    }
	  } // Static
	  ;

	  Collapse._getTargetFromElement = function _getTargetFromElement(element) {
	    var selector = Util.getSelectorFromElement(element);
	    return selector ? document.querySelector(selector) : null;
	  };

	  Collapse._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var $this = $(this);
	      var data = $this.data(DATA_KEY$1);

	      var _config = Object.assign({}, Default, {}, $this.data(), {}, typeof config === 'object' && config ? config : {});

	      if (!data && _config.toggle && /show|hide/.test(config)) {
	        _config.toggle = false;
	      }

	      if (!data) {
	        data = new Collapse(this, _config);
	        $this.data(DATA_KEY$1, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(Collapse, null, [{
	    key: "VERSION",
	    get: function get() {
	      return VERSION$1;
	    }
	  }, {
	    key: "Default",
	    get: function get() {
	      return Default;
	    }
	  }]);

	  return Collapse;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * Data Api implementation
	 * ------------------------------------------------------------------------
	 */


	$(document).on(Event$4.CLICK_DATA_API, Selector$4.DATA_TOGGLE, function (event) {
	  // preventDefault only for <a> elements (which change the URL) not inside the collapsible element
	  if (event.currentTarget.tagName === 'A') {
	    event.preventDefault();
	  }

	  var $trigger = $(this);
	  var selector = Util.getSelectorFromElement(this);
	  var selectors = [].slice.call(document.querySelectorAll(selector));
	  $(selectors).each(function () {
	    var $target = $(this);
	    var data = $target.data(DATA_KEY$1);
	    var config = data ? 'toggle' : $trigger.data();

	    Collapse._jQueryInterface.call($target, config);
	  });
	});
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */

	$.fn[NAME$1] = Collapse._jQueryInterface;
	$.fn[NAME$1].Constructor = Collapse;

	$.fn[NAME$1].noConflict = function () {
	  $.fn[NAME$1] = JQUERY_NO_CONFLICT$1;
	  return Collapse._jQueryInterface;
	};

	var Selector$5 = {
	  DATA_TOGGLE: '[data-toggle="collapseAll"]'
	};
	var ClassName$4 = {
	  DISABLED_TEXT_COLOR: 'text-neutral-300',
	  DISABLED_TEXT_DECORATION: 'text-decoration-none'
	};

	var CollapseControls = /*#__PURE__*/function () {
	  function CollapseControls(el) {
	    this.el = el;
	    this.accordion = this.getTarget()[0];
	    this.collapse = el.querySelector('[data-action="collapse"]');
	    this.expand = el.querySelector('[data-action="expand"]');
	    this.collapseList = this.accordion.querySelectorAll('.collapse');
	    this.collapseBtnList = this.accordion.querySelectorAll('[data-toggle="collapse"]');
	    this.collapseListCount = this.collapseBtnList.length;
	    this.openCount = 0;
	  }

	  var _proto = CollapseControls.prototype;

	  _proto.getTarget = function getTarget() {
	    var selector = Util$1.getSelectorFromElement(this.el);
	    return [].slice.call(document.querySelectorAll(selector));
	  };

	  _proto.collapseAllClick = function collapseAllClick() {
	    var _this = this;

	    this.collapseList.forEach(function (element) {
	      $(element).collapse('hide');
	      _this.openCount = 0;
	    });
	    this.syncDisabledStyle();
	  };

	  _proto.expandAllClick = function expandAllClick() {
	    var _this2 = this;

	    this.collapseList.forEach(function (element) {
	      $(element).collapse('show');
	      _this2.openCount = _this2.collapseListCount;
	    });
	    this.syncDisabledStyle();
	  };

	  _proto.collapseBtnClick = function collapseBtnClick(e) {
	    this.openCount = e.target.getAttribute('aria-expanded').toString() === 'true' ? this.openCount - 1 : this.openCount + 1;
	    this.syncDisabledStyle();
	  };

	  _proto.disableButton = function disableButton(elem) {
	    elem.setAttribute('aria-pressed', true);
	    elem.classList.add(ClassName$4.DISABLED_TEXT_DECORATION, ClassName$4.DISABLED_TEXT_COLOR);
	  };

	  _proto.enableButton = function enableButton(elem) {
	    elem.setAttribute('aria-pressed', false);
	    elem.classList.remove(ClassName$4.DISABLED_TEXT_DECORATION, ClassName$4.DISABLED_TEXT_COLOR);
	  };

	  _proto.syncDisabledStyle = function syncDisabledStyle() {
	    if (this.openCount === this.collapseListCount) {
	      this.enableButton(this.collapse);
	      this.disableButton(this.expand);
	    } else if (this.openCount === 0) {
	      this.enableButton(this.expand);
	      this.disableButton(this.collapse);
	    } else {
	      this.enableButton(this.expand);
	      this.enableButton(this.collapse);
	    }
	  };

	  _proto.init = function init() {
	    var _this3 = this;

	    this.collapse.addEventListener('click', this.collapseAllClick.bind(this));
	    this.expand.addEventListener('click', this.expandAllClick.bind(this));
	    this.collapseBtnList.forEach(function (element) {
	      if (element.getAttribute('aria-expanded').toString() === 'true') {
	        _this3.openCount += 1;
	      }

	      element.addEventListener('click', _this3.collapseBtnClick.bind(_this3));
	    });
	    this.syncDisabledStyle();
	  };

	  return CollapseControls;
	}();

	(function () {
	  Array.prototype.forEach.call(document.querySelectorAll(Selector$5.DATA_TOGGLE), function (element) {
	    new CollapseControls(element).init();
	  });
	})();

	var Selector$6 = {
	  DATA_TOGGLE: '[data-toggle="dropdown"]',
	  MENU: '.dropdown-menu'
	};
	var ClassName$5 = {
	  SHOW: 'show',
	  ACTIVE: 'active',
	  DROPDOWN: 'dropdown',
	  DROPUP: 'dropup',
	  DROPRIGHT: 'dropright',
	  DROPLEFT: 'dropleft',
	  MENU_RIGHT: 'dropdown-menu-right',
	  MENU_LEFT: 'dropdown-menu-left'
	};
	var Event$5 = {
	  ON_HIDE: 'onHide',
	  ON_SHOW: 'onShow',
	  ON_UPDATE: 'onUpdate',
	  ON_REMOVE: 'onRemove'
	};
	var biDirectional = Util$1.isBiDirectional();
	var dropdowns = [];
	/**
	 * Private functions.
	 */

	/**
	 * The event handler for when the target element is clicked.
	 * @param {MouseEvent} event - The event object.
	 */

	function _elOnClick(event) {
	  // Prevent page from trying to scroll to a page anchor.
	  event.preventDefault();
	  this.toggle();
	}
	/**
	 * The event handler for when a key is pressed on the target element.
	 * @param {KeyboardEvent} event - The event object.
	 */


	function _elOnKeydown(event) {
	  // Override keyboard functionality if element is an anchor.
	  if (this.el.nodeName.toLowerCase() === 'a') {
	    if (event.keyCode === Util$1.keyCodes.SPACE || event.keyCode === Util$1.keyCodes.ENTER) {
	      // Trigger the same event as a click for consistency.
	      _elOnClick.bind(this)(event);
	    }
	  } // Events for when the menu is open.


	  if (this.shown) {
	    // Menu should close with the Esc key.
	    if (event.keyCode === Util$1.keyCodes.ESC) {
	      // Prevent focusing on next element in tab order.
	      event.preventDefault();
	      this.hide();
	    }

	    if (this.arrowableItems && (event.keyCode === Util$1.keyCodes.ARROW_DOWN || event.keyCode === Util$1.keyCodes.ARROW_UP)) {
	      // Prevent scrolling page on down arrow.
	      event.preventDefault(); // Set focus to first focusable element in menu.

	      var i = event.keyCode === Util$1.keyCodes.ARROW_DOWN ? 0 : this.arrowableItems.length - 1;
	      this.arrowableItems[i].focus();
	    }
	  }
	}
	/**
	 * The event handler for when a key is pressed on the menu.
	 * @param {KeyboardEvent} event - The event object.
	 */


	function _menuOnKeydown(event) {
	  if (event.keyCode === Util$1.keyCodes.ESC) {
	    // Prevent page from trying to scroll to a page anchor.
	    event.preventDefault();
	    this.hide();
	  }

	  if (this.arrowableItems && (event.keyCode === Util$1.keyCodes.ARROW_DOWN || event.keyCode === Util$1.keyCodes.ARROW_UP)) {
	    // Prevent scrolling page on down arrow.
	    event.preventDefault();
	    var nextElement;

	    if (event.keyCode === Util$1.keyCodes.ARROW_DOWN) {
	      nextElement = _getNextFocusableItem.bind(this)();
	    }

	    if (event.keyCode === Util$1.keyCodes.ARROW_UP) {
	      nextElement = _getPrevFocusableItem.bind(this)();
	    }

	    nextElement.focus();
	  }
	}
	/**
	 * The event handler for when the document is clicked.
	 * @param {Event} event - The event object.
	 */


	function _documentEventHandler(event) {
	  if (this.shown && !this.parent.contains(event.target)) {
	    this.hide({
	      setFocus: false
	    }); // fpcus back on triggering element if tabbing out of dropdown

	    if (event.type === 'focusin') {
	      this.el.focus();
	    }
	  }
	}
	/**
	 * Get the next focusable element from the dropdown instance's arrowableItems list.
	 * @returns {Node} The next focusable element.
	 */


	function _getNextFocusableItem() {
	  // If the current focused element is the last focusable element in the menu return focus to the top of the menu
	  if (document.activeElement === this.arrowableItems[this.arrowableItems.length - 1]) {
	    return this.arrowableItems[0];
	  }

	  return this.arrowableItems[this.arrowableItems.indexOf(document.activeElement) + 1];
	}
	/**
	 * Get the previous focusable element from the dropdown instance's arrowableItems list.
	 * @returns {Node} The previous focusable element.
	 */


	function _getPrevFocusableItem() {
	  // If the current focused element is the first focusable element in the menu send focus to the bottom of the menu
	  if (document.activeElement === this.arrowableItems[0]) {
	    return this.arrowableItems[this.arrowableItems.length - 1];
	  }

	  return this.arrowableItems[this.arrowableItems.indexOf(document.activeElement) - 1];
	}
	/**
	 * Get the direction of a dropdown.
	 * @param {Node} node - The element to check for a directional class.
	 * @returns {string} The direction of the dropdown.
	 */


	function _getDirection(node) {
	  for (var _iterator = node.classList, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
	    var _ref;

	    if (_isArray) {
	      if (_i >= _iterator.length) break;
	      _ref = _iterator[_i++];
	    } else {
	      _i = _iterator.next();
	      if (_i.done) break;
	      _ref = _i.value;
	    }

	    var str = _ref;

	    if (str === ClassName$5.DROPUP) {
	      return 'up';
	    }

	    if (str === ClassName$5.DROPLEFT) {
	      return biDirectional ? 'right' : 'left';
	    }

	    if (str === ClassName$5.DROPRIGHT) {
	      return biDirectional ? 'left' : 'right';
	    }
	  }

	  return 'down';
	}
	/**
	 * Get the related menu for an element.
	 * @param {Node} node - The element to find a related menu for, typically the dropdown instance target.
	 * @returns {Node} The dropdown menu element.
	 */


	function _getRelatedMenu(node) {
	  if (node.attributes['aria-controls']) {
	    return document.querySelector("#" + node.attributes['aria-controls'].value);
	  }

	  return node.parentElement.querySelector(Selector$6.MENU);
	}
	/**
	 * Get the X distance for menu positioning.
	 * @param {number} distFromRight - Menu's distance from the right side of the window.
	 * @param {number} distFromLeft - Menu's distance from the left side of the window.
	 * @returns {number} The X distance to translate the menu.
	 */


	function _getTranslateX(distFromRight, distFromLeft) {
	  var rightAlignedMenu = this.menu.classList.contains(ClassName$5.MENU_RIGHT);
	  var offsetLeft = distFromLeft + this.el.offsetWidth;
	  var offsetRight = distFromRight + this.el.offsetWidth;
	  var translateX = 0;
	  /*
	    Left and right aligned menus should be horizontally biased. Position the menu to
	    either the left or right of the toggle. If there isn't enough room in the viewport
	    to fit the menu, shift the placement just enough to accommodate.
	  */

	  if (this.direction === 'right') {
	    translateX = this.parent.offsetWidth; // If its too close to the right edge of the window

	    if (distFromRight < 0) {
	      translateX += distFromRight;
	    }
	  } else if (this.direction === 'left') {
	    translateX = -this.menu.offsetWidth; // If its too close to the left edge of the window

	    if (distFromLeft < 0) {
	      translateX += -distFromLeft;
	    }
	    /*
	      Up and down aligned menus should align horizontally to either the left or right
	      edge of the toggle, depending on the text-alignment of the menu. If there isn't
	      enough room in the viewport to fit the menu, shift the placement just enough
	      to accommodate.
	    */

	  } else if (this.direction === 'up' || this.direction === 'down') {
	    // Adjust positioning for right versus left aligned menus
	    if (rightAlignedMenu) {
	      // translateX = this.el.offsetWidth - this.menu.offsetWidth;
	      translateX = this.parent.offsetWidth - this.el.offsetWidth - this.menu.offsetWidth + this.el.offsetWidth;
	    } else {
	      translateX = this.parent.offsetWidth - this.el.offsetWidth;
	    } // Invert positioning for RTL


	    if (biDirectional) {
	      translateX *= -1;
	    } // If its a right-aligned menu or left-aligned RTL menu


	    if (rightAlignedMenu && !biDirectional || !rightAlignedMenu && biDirectional) {
	      // and its too close to the left edge of the window
	      if (offsetLeft < 0) {
	        translateX -= offsetLeft;
	      } // If its a left-aligned menu or right-aligned RTL menu, and its too close to the right edge of the window

	    } else if (offsetRight < 0) {
	      translateX += offsetRight;
	    }
	  }

	  return translateX;
	}
	/**
	 * Get the Y distance for menu positioning.
	 * @param {number} distFromTop - Menu's distance from the top of the window.
	 * @param {number} distFromRight - Menu's distance from the right side of the window.
	 * @param {number} distFromBottom - Menu's distance from the bottom of the window.
	 * @param {number} distFromLeft - Menu's distance from the left side of the window.
	 * @returns {number} The Y distance to translate the menu.
	 */


	function _getTranslateY(distFromTop, distFromRight, distFromBottom, distFromLeft) {
	  var menuVertSpaceNeeded = this.menu.offsetHeight + this.el.offsetHeight / 2;
	  var translateY = 0;
	  /*
	    If the menu alone takes up over half the window height, the menu will expand
	    outside the viewport regardless. In this case, vertically position the menu
	    in whichever direction gives it the most space.
	  */

	  if (menuVertSpaceNeeded > window.innerHeight / 2) {
	    if (distFromBottom < distFromTop) {
	      // position menu above toggle
	      translateY = -this.menu.offsetHeight;
	    } else {
	      // position menu below toggle
	      translateY = this.el.offsetHeight;
	    }
	    /*
	      Left and right aligned menus shouldn't be vertically biased. Vertically
	      position the menu in whichever direction gives it the most space.
	    */

	  } else if (this.direction === 'left' || this.direction === 'right') {
	    if (distFromBottom < distFromTop) {
	      // position menu to fly out above
	      translateY = this.el.offsetHeight - this.menu.offsetHeight;

	      if (this.direction === 'left' && distFromLeft < 0 || this.direction === 'right' && distFromRight < 0) {
	        // position menu above toggle
	        translateY = -this.menu.offsetHeight;
	      }
	    } else if (this.direction === 'left' && distFromLeft < 0 || this.direction === 'right' && distFromRight < 0) {
	      // position menu below toggle
	      translateY = this.el.offsetHeight;
	    }
	    /*
	      Up and down aligned menus should be vertically biased. Position the menu
	      either above ('up') or below ('down') unless there isn't enough room for
	      the menu within the viewport.
	    */

	  } else if (this.direction === 'up' || this.direction === 'down') {
	    // Adjust distances to account for toggle height
	    distFromTop -= this.el.offsetHeight;
	    distFromBottom -= this.el.offsetHeight;

	    if (this.direction === 'down' && !(distFromBottom < 0) || this.direction === 'up' && distFromTop < 0) {
	      // position menu below toggle
	      translateY = this.el.offsetHeight;
	    } else {
	      // position menu above toggle
	      translateY = -this.menu.offsetHeight;
	    }
	  }

	  return translateY;
	}
	/**
	 * Sets the min-width of the menu depending on trigger width.
	 */


	function _setMenuWidth() {
	  if (this.direction === 'up' || this.direction === 'down') {
	    var minWidth = parseInt(window.getComputedStyle(this.menu).getPropertyValue('min-width'), 10);
	    minWidth = Math.max(minWidth, this.el.offsetWidth); // Adjust the menu width to be at least the width of the toggle.

	    this.menu.style.minWidth = minWidth + "px";
	  }
	}

	var Dropdown = /*#__PURE__*/function () {
	  /**
	   * Create a dropdown.
	   * @param {Object} opts - The dropdown options.
	   * @param {Node} opts.el - The element that toggles the dropdown.
	   * @param {Node} [opts.menu] - The element that defines the dropdown menu.
	   * @param {string} [opts.direction=down] - A string that defines the direction the menu opens.
	   */
	  function Dropdown(opts) {
	    this.el = opts.el; // the dropdown toggle

	    this.menu = opts.menu || _getRelatedMenu(this.el);
	    this.parent = this.el.offsetParent || this.el.parentElement;
	    this.direction = opts.direction || _getDirection(this.parent);
	    this.shown = false;

	    if (this.menu.nodeName.toLowerCase() === 'ul' || this.menu.nodeName.toLowerCase() === 'ol') {
	      this.arrowableItems = Util$1.getTabbableElements(this.menu);
	    } // Add event handlers.


	    this.events = [{
	      el: this.el,
	      type: 'click',
	      handler: _elOnClick.bind(this)
	    }, {
	      el: this.el,
	      type: 'keydown',
	      handler: _elOnKeydown.bind(this)
	    }, {
	      el: this.menu,
	      type: 'keydown',
	      handler: _menuOnKeydown.bind(this)
	    }, {
	      el: document,
	      type: 'click',
	      handler: _documentEventHandler.bind(this)
	    }, {
	      el: document,
	      type: 'focusin',
	      handler: _documentEventHandler.bind(this)
	    }];
	    Util$1.addEvents(this.events); // Add mutation observers.

	    this.menuObserver = new MutationObserver(this.update);
	    this.menuObserver.observe(this.menu, {
	      childList: true,
	      subtree: true
	    }); // Create custom events.

	    this[Event$5.ON_HIDE] = new CustomEvent(Event$5.ON_HIDE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$5.ON_SHOW] = new CustomEvent(Event$5.ON_SHOW, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$5.ON_UPDATE] = new CustomEvent(Event$5.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$5.ON_REMOVE] = new CustomEvent(Event$5.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    }); // Dynamically adjust menu width.

	    _setMenuWidth.bind(this)();

	    dropdowns.push(this);
	  }
	  /**
	   * Remove the dropdown instance.
	   */


	  var _proto = Dropdown.prototype;

	  _proto.remove = function remove() {
	    // Remove event handlers, observers, etc.
	    Util$1.removeEvents(this.events);
	    this.menuObserver.disconnect(); // Remove this reference from the array of instances.

	    var index = dropdowns.indexOf(this);
	    dropdowns.splice(index, 1);
	    this.el.dispatchEvent(this[Event$5.ON_REMOVE]);
	  }
	  /**
	   * Update the dropdown instance.
	   * @param {string} [opts={}] - Options for updating the dropdown.
	   * @param {string} [opts.direction] - New direction of the dropdown.
	   */
	  ;

	  _proto.update = function update(opts) {
	    if (opts === void 0) {
	      opts = {};
	    }

	    if (typeof this.arrowableItems !== 'undefined') {
	      // Update the list of known focusable items within the menu.
	      this.arrowableItems = Util$1.getTabbableElements(this.menu);
	    } // Change the direction of the dropdown if supplied.


	    if (opts.direction) {
	      var newClassName = ClassName$5[('drop' + opts.direction).toUpperCase()]; // Change menu position.

	      this.direction = opts.direction; // Change the class that determines the arrow position.

	      this.parent.classList.remove(ClassName$5.DROPDOWN, ClassName$5.DROPUP, ClassName$5.DROPRIGHT, ClassName$5.DROPLEFT);
	      this.parent.classList.add(newClassName);
	    } // Update menu width.


	    _setMenuWidth.bind(this)(); // Update the menu position if its open.


	    if (this.shown) {
	      this.positionMenu();
	    }

	    this.el.dispatchEvent(this[Event$5.ON_UPDATE]);
	  }
	  /**
	   * Position the dropdown menu.
	   */
	  ;

	  _proto.positionMenu = function positionMenu() {
	    // Calculate the distance of the menu from each side of the browser window.
	    var distFromLeft = this.el.getBoundingClientRect().left - this.menu.offsetWidth;
	    var distFromRight = document.body.clientWidth - (this.el.getBoundingClientRect().right + this.menu.offsetWidth);
	    var distFromTop = this.el.getBoundingClientRect().top + this.el.offsetHeight - this.menu.offsetHeight;
	    var distFromBottom = window.innerHeight - this.el.getBoundingClientRect().top - this.menu.offsetHeight; // Calculate the x and y distances needed to push the menu to the correct position.

	    var x = _getTranslateX.bind(this)(distFromRight, distFromLeft);

	    var y = _getTranslateY.bind(this)(distFromTop, distFromRight, distFromBottom, distFromLeft); // Set the transform style once.


	    this.menu.style.transform = "translate(" + x + "px, " + y + "px)";
	  }
	  /**
	   * Show the dropdown menu.
	   */
	  ;

	  _proto.show = function show() {
	    this.shown = true;
	    this.el.setAttribute('aria-expanded', this.shown);
	    this.el.classList.add(ClassName$5.ACTIVE);
	    this.menu.classList.add(ClassName$5.SHOW);
	    this.positionMenu();
	    this.el.dispatchEvent(this[Event$5.ON_SHOW]);
	  }
	  /**
	   * Hide the dropdown menu.
	   * @param {string} [opts={}] - Options for hiding the menu.
	   * @param {boolean} [opts.setFocus=true] - Whether or not the focus should be set on the toggling element; defaults to true.
	   */
	  ;

	  _proto.hide = function hide(opts) {
	    if (opts === void 0) {
	      opts = {};
	    }

	    // Default behavior should be to set focus on toggling element.
	    var setFocus = typeof opts.setFocus === 'boolean' ? opts.setFocus : true;
	    this.shown = false;
	    this.el.setAttribute('aria-expanded', this.shown);
	    this.el.classList.remove(ClassName$5.ACTIVE);
	    this.menu.classList.remove(ClassName$5.SHOW);

	    if (setFocus) {
	      // Set focus on the toggle.
	      this.el.focus();
	    }

	    this.el.dispatchEvent(this[Event$5.ON_HIDE]);
	  }
	  /**
	   * Toggle the dropdown menu state.
	   */
	  ;

	  _proto.toggle = function toggle() {
	    if (this.shown) {
	      this.hide();
	    } else {
	      this.show();
	    }
	  }
	  /**
	   * Get an array of dropdown instances.
	   * @returns {Object[]} Array of dropdown instances.
	   */
	  ;

	  Dropdown.getInstances = function getInstances() {
	    return dropdowns;
	  };

	  return Dropdown;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$6.DATA_TOGGLE, function (node) {
	    new Dropdown({
	      el: node
	    });
	  });
	})();

	// `Array.prototype.{ reduce, reduceRight }` methods implementation
	var createMethod$4 = function (IS_RIGHT) {
	  return function (that, callbackfn, argumentsLength, memo) {
	    aFunction$1(callbackfn);
	    var O = toObject(that);
	    var self = indexedObject(O);
	    var length = toLength(O.length);
	    var index = IS_RIGHT ? length - 1 : 0;
	    var i = IS_RIGHT ? -1 : 1;
	    if (argumentsLength < 2) while (true) {
	      if (index in self) {
	        memo = self[index];
	        index += i;
	        break;
	      }
	      index += i;
	      if (IS_RIGHT ? index < 0 : length <= index) {
	        throw TypeError('Reduce of empty array with no initial value');
	      }
	    }
	    for (;IS_RIGHT ? index >= 0 : length > index; index += i) if (index in self) {
	      memo = callbackfn(memo, self[index], index, O);
	    }
	    return memo;
	  };
	};

	var arrayReduce = {
	  // `Array.prototype.reduce` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.reduce
	  left: createMethod$4(false),
	  // `Array.prototype.reduceRight` method
	  // https://tc39.github.io/ecma262/#sec-array.prototype.reduceright
	  right: createMethod$4(true)
	};

	var $reduce = arrayReduce.left;



	var STRICT_METHOD$3 = arrayMethodIsStrict('reduce');
	var USES_TO_LENGTH$5 = arrayMethodUsesToLength('reduce', { 1: 0 });

	// `Array.prototype.reduce` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.reduce
	_export({ target: 'Array', proto: true, forced: !STRICT_METHOD$3 || !USES_TO_LENGTH$5 }, {
	  reduce: function reduce(callbackfn /* , initialValue */) {
	    return $reduce(this, callbackfn, arguments.length, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	var $some = arrayIteration.some;



	var STRICT_METHOD$4 = arrayMethodIsStrict('some');
	var USES_TO_LENGTH$6 = arrayMethodUsesToLength('some');

	// `Array.prototype.some` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.some
	_export({ target: 'Array', proto: true, forced: !STRICT_METHOD$4 || !USES_TO_LENGTH$6 }, {
	  some: function some(callbackfn /* , thisArg */) {
	    return $some(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	var defineProperty$6 = objectDefineProperty.f;

	var FunctionPrototype = Function.prototype;
	var FunctionPrototypeToString = FunctionPrototype.toString;
	var nameRE = /^\s*function ([^ (]*)/;
	var NAME$2 = 'name';

	// Function instances `.name` property
	// https://tc39.github.io/ecma262/#sec-function-instances-name
	if (descriptors && !(NAME$2 in FunctionPrototype)) {
	  defineProperty$6(FunctionPrototype, NAME$2, {
	    configurable: true,
	    get: function () {
	      try {
	        return FunctionPrototypeToString.call(this).match(nameRE)[1];
	      } catch (error) {
	        return '';
	      }
	    }
	  });
	}

	var getOwnPropertyNames$1 = objectGetOwnPropertyNames.f;
	var getOwnPropertyDescriptor$2 = objectGetOwnPropertyDescriptor.f;
	var defineProperty$7 = objectDefineProperty.f;
	var trim$2 = stringTrim.trim;

	var NUMBER = 'Number';
	var NativeNumber = global_1[NUMBER];
	var NumberPrototype = NativeNumber.prototype;

	// Opera ~12 has broken Object#toString
	var BROKEN_CLASSOF = classofRaw(objectCreate(NumberPrototype)) == NUMBER;

	// `ToNumber` abstract operation
	// https://tc39.github.io/ecma262/#sec-tonumber
	var toNumber = function (argument) {
	  var it = toPrimitive(argument, false);
	  var first, third, radix, maxCode, digits, length, index, code;
	  if (typeof it == 'string' && it.length > 2) {
	    it = trim$2(it);
	    first = it.charCodeAt(0);
	    if (first === 43 || first === 45) {
	      third = it.charCodeAt(2);
	      if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
	    } else if (first === 48) {
	      switch (it.charCodeAt(1)) {
	        case 66: case 98: radix = 2; maxCode = 49; break; // fast equal of /^0b[01]+$/i
	        case 79: case 111: radix = 8; maxCode = 55; break; // fast equal of /^0o[0-7]+$/i
	        default: return +it;
	      }
	      digits = it.slice(2);
	      length = digits.length;
	      for (index = 0; index < length; index++) {
	        code = digits.charCodeAt(index);
	        // parseInt parses a string to a first unavailable symbol
	        // but ToNumber should return NaN if a string contains unavailable symbols
	        if (code < 48 || code > maxCode) return NaN;
	      } return parseInt(digits, radix);
	    }
	  } return +it;
	};

	// `Number` constructor
	// https://tc39.github.io/ecma262/#sec-number-constructor
	if (isForced_1(NUMBER, !NativeNumber(' 0o1') || !NativeNumber('0b1') || NativeNumber('+0x1'))) {
	  var NumberWrapper = function Number(value) {
	    var it = arguments.length < 1 ? 0 : value;
	    var dummy = this;
	    return dummy instanceof NumberWrapper
	      // check on 1..constructor(foo) case
	      && (BROKEN_CLASSOF ? fails(function () { NumberPrototype.valueOf.call(dummy); }) : classofRaw(dummy) != NUMBER)
	        ? inheritIfRequired(new NativeNumber(toNumber(it)), dummy, NumberWrapper) : toNumber(it);
	  };
	  for (var keys$2 = descriptors ? getOwnPropertyNames$1(NativeNumber) : (
	    // ES3:
	    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
	    // ES2015 (in case, if modules with ES2015 Number statics required before):
	    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' +
	    'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger'
	  ).split(','), j = 0, key; keys$2.length > j; j++) {
	    if (has(NativeNumber, key = keys$2[j]) && !has(NumberWrapper, key)) {
	      defineProperty$7(NumberWrapper, key, getOwnPropertyDescriptor$2(NativeNumber, key));
	    }
	  }
	  NumberWrapper.prototype = NumberPrototype;
	  NumberPrototype.constructor = NumberWrapper;
	  redefine(global_1, NUMBER, NumberWrapper);
	}

	var quot = /"/g;

	// B.2.3.2.1 CreateHTML(string, tag, attribute, value)
	// https://tc39.github.io/ecma262/#sec-createhtml
	var createHtml = function (string, tag, attribute, value) {
	  var S = String(requireObjectCoercible(string));
	  var p1 = '<' + tag;
	  if (attribute !== '') p1 += ' ' + attribute + '="' + String(value).replace(quot, '&quot;') + '"';
	  return p1 + '>' + S + '</' + tag + '>';
	};

	// check the existence of a method, lowercase
	// of a tag and escaping quotes in arguments
	var stringHtmlForced = function (METHOD_NAME) {
	  return fails(function () {
	    var test = ''[METHOD_NAME]('"');
	    return test !== test.toLowerCase() || test.split('"').length > 3;
	  });
	};

	// `String.prototype.link` method
	// https://tc39.github.io/ecma262/#sec-string.prototype.link
	_export({ target: 'String', proto: true, forced: stringHtmlForced('link') }, {
	  link: function link(url) {
	    return createHtml(this, 'a', 'href', url);
	  }
	});

	var Selector$7 = {
	  DATA_MOUNT: '.needs-validation, [data-mount="validation"]',
	  INPUTS: 'input, select, textarea',
	  SUBMIT: '[type="submit"]',
	  FEEDBACK_LIST: '[data-mount="feedback-list"]',
	  FEEDBACK_EL: 'data-feedback',
	  FEEDBACK_CONTENT: 'data-feedback-content',
	  CHECKBOX_REQUIRED: 'data-form-check-required',
	  CHECKBOX_MAX: 'data-form-check-max'
	};
	var Event$6 = {
	  ON_VALIDATION: 'onValidation',
	  ON_VALID: 'onValid',
	  ON_INVALID: 'onInvalid',
	  ON_REMOVE: 'onRemove'
	};
	var ClassName$6 = {
	  DISPLAY: {
	    NONE: 'd-none'
	  },
	  IS_INVALID: 'is-invalid'
	};
	var formValidations = [];
	/**
	 * Private functions.
	 */

	/**
	 * Create link to input field with feedback at bottom of form
	 * @param {Node} input - The form input field.
	 */

	function _createFeedbackLink(input) {
	  if (!input.feedback.link) {
	    var feedbackItem = document.createElement('li');
	    var feedbackLink = document.createElement('a');
	    var feedbackTextNode = document.createTextNode(input.feedback.content);
	    feedbackLink.setAttribute('href', "#" + input.id);
	    input.feedback.focusControls = new Util$1.FocusControls({
	      el: feedbackLink
	    });
	    feedbackLink.appendChild(feedbackTextNode);
	    feedbackItem.appendChild(feedbackLink);
	    input.feedback.link = feedbackItem;

	    if (input.group) {
	      input.group.siblings.forEach(function (sibling) {
	        sibling.feedback.link = feedbackItem;
	        sibling.feedback.focusControls = input.feedback.focusControls;
	      });
	    }
	  }

	  this.feedbackList.appendChild(input.feedback.link);

	  if (!input.feedback.focusControls) {
	    input.feedback.focusControls = new Util$1.FocusControls({
	      el: input.feedback.link.querySelector('a')
	    });
	  }

	  input.feedback.linkRemoved = false;

	  if (input.group) {
	    input.group.siblings.forEach(function (sibling) {
	      sibling.feedback.linkRemoved = false;
	    });
	  }

	  this.feedbackListContainer.classList.remove(ClassName$6.DISPLAY.NONE);
	}
	/**
	 * Remove link to input field with feedback at bottom of form
	 * @param {Node} input - The form input field.
	 */


	function _removeFeedbackLink(input) {
	  if (input.group) {
	    input.group.siblings.forEach(function (sibling) {
	      sibling.feedback.linkRemoved = true;
	      sibling.feedback.focusControls.remove();
	    });
	  } else {
	    input.feedback.linkRemoved = true;
	    input.feedback.focusControls.remove();
	  }

	  this.feedbackList.removeChild(input.feedback.link);

	  if (this.feedbackList.children.length === 0) {
	    this.feedbackListContainer.classList.add(ClassName$6.DISPLAY.NONE);
	  }
	}
	/**
	 * Generate feedback data object from data attrbutes
	 * @param {Node} input - The form input field.
	 * @returns {Object} Object with feedback data.
	 */


	function _getFeedbackData(input) {
	  var feedback = {
	    id: input.getAttribute(Selector$7.FEEDBACK_EL)
	  };

	  if (feedback.id) {
	    feedback.content = input.getAttribute(Selector$7.FEEDBACK_CONTENT);
	    feedback.el = this.el.querySelector("#" + feedback.id);
	    feedback.linkRemoved = true;
	  }

	  return feedback;
	}
	/**
	 * Events for when input is valid
	 * @param {Node} input - The form input field.
	 */


	function _onValid(input) {
	  input.classList.remove(ClassName$6.IS_INVALID);
	  input.setAttribute('aria-invalid', false);

	  if (input.group) {
	    input.group.siblings.forEach(function (sibling) {
	      sibling.classList.remove(ClassName$6.IS_INVALID);
	      sibling.setAttribute('aria-invalid', false);
	    });
	  }

	  if (input.feedback.el) {
	    input.feedback.el.classList.remove(ClassName$6.IS_INVALID);
	    input.feedback.el.textContent = '';

	    if (this.feedbackList && input.feedback.link && !input.feedback.linkRemoved) {
	      _removeFeedbackLink.bind(this)(input);
	    }
	  }
	}
	/**
	 * Events for when input is invalid
	 * @param {Node} input - The form input field.
	 * @param {Object} feedback - The feedback options.
	 * @param {Node} feedback.el - The input feedback element.
	 * @param {string} feedback.content - The feedback content.
	 */


	function _onInvalid(input) {
	  input.classList.add(ClassName$6.IS_INVALID);
	  input.setAttribute('aria-invalid', true);

	  if (input.group) {
	    input.group.siblings.forEach(function (sibling) {
	      sibling.classList.add(ClassName$6.IS_INVALID);
	      sibling.setAttribute('aria-invalid', true);
	    });
	  }

	  if (input.feedback.el && input.feedback.content) {
	    input.feedback.el.classList.add(ClassName$6.IS_INVALID);
	    input.feedback.el.textContent = input.feedback.content;

	    if (this.feedbackList && input.feedback.linkRemoved) {
	      _createFeedbackLink.bind(this)(input);
	    }
	  }
	}
	/**
	 * Generate group data object from input
	 * @param {Node} input - The form input field.
	 * @returns {Object} Object with group data.
	 */


	function _inputCheckReducer(input) {
	  var name = input.name,
	      type = input.type;
	  return [].slice.call(this.inputs).reduce(function (obj, _input) {
	    if (_input.type === type && _input.name === name) {
	      if (obj.siblings) {
	        obj.siblings.push(_input);
	      } else {
	        obj.siblings = [_input];
	      }

	      var requiredMin = _input.getAttribute(Selector$7.CHECKBOX_REQUIRED);

	      var maxValid = _input.getAttribute(Selector$7.CHECKBOX_MAX); // Selector.CHECKBOX_REQUIRED attribute accepts either a boolean or integer
	      // If it's a boolean convert to an integer


	      if (requiredMin) {
	        var requiredMinInt = Number(requiredMin);

	        if (isNaN(requiredMinInt)) {
	          requiredMinInt = requiredMin === 'true' ? 1 : 0;
	        }

	        obj.requiredMin = requiredMinInt;
	      }

	      if (maxValid) {
	        var maxValidInt = Number(maxValid);
	        var maxValidIntIsNaN = isNaN(maxValidInt);

	        if (!maxValidIntIsNaN) {
	          obj.maxValid = maxValidInt;
	        }
	      }

	      if (_input.getAttribute(Selector$7.FEEDBACK_EL)) {
	        if (obj.feedback) {
	          obj.feedback.push(_input);
	        } else {
	          obj.feedback = [_input];
	        }
	      }
	    }

	    return obj;
	  }, {});
	}
	/**
	 * Setup inputs with required data.
	 * @param {Node} input - The form input field.
	 */


	function _inputInit(input) {
	  var type = input.type;
	  var feedbackEl = input;

	  if (type === 'radio' || type === 'checkbox') {
	    var group = _inputCheckReducer.bind(this)(input);

	    var feedback = group.feedback,
	        _group = _objectWithoutPropertiesLoose(group, ["feedback"]);

	    if (_group.siblings.length > 1) {
	      input.group = _group;
	    }

	    if (feedback) {
	      feedbackEl = feedback[0];
	    }
	  }

	  input.feedback = _getFeedbackData.bind(this)(feedbackEl);
	}
	/**
	 * Setup inputs with required data.
	 * @param {Node} input - The form input field.
	 */


	function _setFeedbackListFocusEl() {
	  var tagNames = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'P'];
	  var prevEl = this.feedbackList.previousElementSibling;
	  this.feedbackListFocusEl = this.feedbackListContainer;

	  if (prevEl && tagNames.indexOf(prevEl.tagName) > -1) {
	    this.feedbackListFocusEl = prevEl;
	  }

	  this.feedbackListFocusEl.tabIndex = -1;
	}
	/**
	 * Class representing form validation.
	 */


	var FormValidation = /*#__PURE__*/function () {
	  /**
	   * Initialize form validation.
	   * @param {Object} opts - The form validation options.
	   * @param {Node} opts.el - The form DOM node.
	   * @param {Node} opts.feedbackListContainer - The feedback list container DOM node.
	   */
	  function FormValidation(opts) {
	    var _this = this;

	    this.el = opts.el;
	    this.inputs = this.el.querySelectorAll(Selector$7.INPUTS);
	    this.submit = this.el.querySelector(Selector$7.SUBMIT);
	    this.feedbackListContainer = opts.feedbackListContainer || this.el.querySelector(Selector$7.FEEDBACK_LIST);

	    if (this.feedbackListContainer) {
	      this.feedbackList = this.feedbackListContainer.querySelector('ol');

	      _setFeedbackListFocusEl.bind(this)();
	    }

	    this.events = [{
	      el: this.el,
	      type: 'submit',
	      handler: function handler(e) {
	        _this.onSubmit(e);
	      }
	    }];
	    formValidations.push(this); // Hide empty feedback list

	    if (this.feedbackList && this.feedbackList.children.length === 0) {
	      this.feedbackListContainer.classList.add(ClassName$6.DISPLAY.NONE);
	    } // Set up inputs


	    this.inputs.forEach(function (input) {
	      _inputInit.bind(_this)(input);

	      _this.events.push({
	        el: input,
	        type: 'blur',
	        handler: function handler() {
	          setTimeout(function () {
	            _this.validate(input, true);
	          }, 0);
	        }
	      }, {
	        el: input,
	        type: 'change',
	        handler: function handler() {
	          _this.validate(input, true);
	        }
	      });
	    }); // Add event handlers.

	    Util$1.addEvents(this.events); // Create custom events.

	    this[Event$6.ON_REMOVE] = new CustomEvent(Event$6.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	  }
	  /**
	   * Validate form input
	   * @param {Node} input - The form input field.
	   * @param {boolean} onlyOnValid - Only runs if valid.
	   */


	  var _proto = FormValidation.prototype;

	  _proto.validate = function validate(input, onlyOnValid) {
	    var activeEl = document.activeElement;

	    if (!input.name || input.name !== activeEl.name) {
	      if (this.isInputValid(input)) {
	        _onValid.bind(this)(input);
	      } else if (!onlyOnValid) {
	        _onInvalid.bind(this)(input);
	      }
	    }
	  }
	  /**
	   * Check if input is valid
	   * @param {Node} input - The form input field.
	   * @returns {Boolean} - true if input is valid.
	   */
	  ;

	  _proto.isInputValid = function isInputValid(input) {
	    // Radio and check groups
	    if (input.group && (input.group.requiredMin || input.group.maxValid)) {
	      // get number of checked inputs in the group
	      var checked = input.group.siblings.filter(function (sibling) {
	        return sibling.checked === true;
	      }); // compare against required min or max

	      if (input.group.requiredMin && checked.length < input.group.requiredMin || input.group.maxValid && checked.length > input.group.maxValid) {
	        return false;
	      }

	      return true;
	    }

	    return input.checkValidity();
	  }
	  /**
	   * Check if form is valid
	   * @returns {Boolean} - true if all form inputs are valid.
	   */
	  ;

	  _proto.isFormValid = function isFormValid() {
	    var _this2 = this;

	    var checkValidity = [].slice.call(this.inputs).some(function (input) {
	      return _this2.isInputValid(input) === false;
	    });
	    return !checkValidity;
	  }
	  /**
	   * Check if form is empty
	   * @returns {Boolean} - false if any form inputs are checked or have a value.
	   */
	  ;

	  _proto.isFormEmpty = function isFormEmpty() {
	    var notEmpty = [].slice.call(this.inputs).some(function (input) {
	      var type = input.type;

	      if (type === 'radio' || type === 'checkbox') {
	        if (input.checked) {
	          return true;
	        }
	      } else if (input.value !== null && input.value !== undefined && input.value.trim().length) {
	        return true;
	      }

	      return false;
	    });
	    return !notEmpty;
	  }
	  /**
	   * Submit form
	   * @param {Event} e - The event object.
	   */
	  ;

	  _proto.onSubmit = function onSubmit(e) {
	    var _this3 = this;

	    e.preventDefault();
	    this.inputs.forEach(function (input) {
	      _this3.validate(input);
	    });

	    if (this.isFormValid()) {
	      if (!this.isFormEmpty()) {
	        this.el.submit();
	      }
	    } else {
	      this.feedbackListFocusEl.focus();
	    }
	  }
	  /**
	   * Remove the form validation.
	   */
	  ;

	  _proto.remove = function remove() {
	    // Remove event handlers
	    Util$1.removeEvents(this.events); // remove this form validation reference from array of instances

	    var index = formValidations.indexOf(this);
	    formValidations.splice(index, 1);
	    this.el.dispatchEvent(this[Event$6.ON_REMOVE]);
	  }
	  /**
	   * Get an array of form validation instances.
	   * @returns {Object[]} Array of form validation instances.
	   */
	  ;

	  FormValidation.getInstances = function getInstances() {
	    return formValidations;
	  };

	  return FormValidation;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$7.DATA_MOUNT, function (node) {
	    new FormValidation({
	      el: node
	    });
	  });
	})();

	var redefineAll = function (target, src, options) {
	  for (var key in src) redefine(target, key, src[key], options);
	  return target;
	};

	var freezing = !fails(function () {
	  return Object.isExtensible(Object.preventExtensions({}));
	});

	var internalMetadata = createCommonjsModule(function (module) {
	var defineProperty = objectDefineProperty.f;



	var METADATA = uid('meta');
	var id = 0;

	var isExtensible = Object.isExtensible || function () {
	  return true;
	};

	var setMetadata = function (it) {
	  defineProperty(it, METADATA, { value: {
	    objectID: 'O' + ++id, // object ID
	    weakData: {}          // weak collections IDs
	  } });
	};

	var fastKey = function (it, create) {
	  // return a primitive with prefix
	  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
	  if (!has(it, METADATA)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return 'F';
	    // not necessary to add metadata
	    if (!create) return 'E';
	    // add missing metadata
	    setMetadata(it);
	  // return object ID
	  } return it[METADATA].objectID;
	};

	var getWeakData = function (it, create) {
	  if (!has(it, METADATA)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return true;
	    // not necessary to add metadata
	    if (!create) return false;
	    // add missing metadata
	    setMetadata(it);
	  // return the store of weak collections IDs
	  } return it[METADATA].weakData;
	};

	// add metadata on freeze-family methods calling
	var onFreeze = function (it) {
	  if (freezing && meta.REQUIRED && isExtensible(it) && !has(it, METADATA)) setMetadata(it);
	  return it;
	};

	var meta = module.exports = {
	  REQUIRED: false,
	  fastKey: fastKey,
	  getWeakData: getWeakData,
	  onFreeze: onFreeze
	};

	hiddenKeys[METADATA] = true;
	});
	var internalMetadata_1 = internalMetadata.REQUIRED;
	var internalMetadata_2 = internalMetadata.fastKey;
	var internalMetadata_3 = internalMetadata.getWeakData;
	var internalMetadata_4 = internalMetadata.onFreeze;

	var ITERATOR$3 = wellKnownSymbol('iterator');
	var ArrayPrototype$1 = Array.prototype;

	// check on default Array iterator
	var isArrayIteratorMethod = function (it) {
	  return it !== undefined && (iterators.Array === it || ArrayPrototype$1[ITERATOR$3] === it);
	};

	var ITERATOR$4 = wellKnownSymbol('iterator');

	var getIteratorMethod = function (it) {
	  if (it != undefined) return it[ITERATOR$4]
	    || it['@@iterator']
	    || iterators[classof(it)];
	};

	// call something on iterator step with safe closing on error
	var callWithSafeIterationClosing = function (iterator, fn, value, ENTRIES) {
	  try {
	    return ENTRIES ? fn(anObject(value)[0], value[1]) : fn(value);
	  // 7.4.6 IteratorClose(iterator, completion)
	  } catch (error) {
	    var returnMethod = iterator['return'];
	    if (returnMethod !== undefined) anObject(returnMethod.call(iterator));
	    throw error;
	  }
	};

	var iterate_1 = createCommonjsModule(function (module) {
	var Result = function (stopped, result) {
	  this.stopped = stopped;
	  this.result = result;
	};

	var iterate = module.exports = function (iterable, fn, that, AS_ENTRIES, IS_ITERATOR) {
	  var boundFunction = functionBindContext(fn, that, AS_ENTRIES ? 2 : 1);
	  var iterator, iterFn, index, length, result, next, step;

	  if (IS_ITERATOR) {
	    iterator = iterable;
	  } else {
	    iterFn = getIteratorMethod(iterable);
	    if (typeof iterFn != 'function') throw TypeError('Target is not iterable');
	    // optimisation for array iterators
	    if (isArrayIteratorMethod(iterFn)) {
	      for (index = 0, length = toLength(iterable.length); length > index; index++) {
	        result = AS_ENTRIES
	          ? boundFunction(anObject(step = iterable[index])[0], step[1])
	          : boundFunction(iterable[index]);
	        if (result && result instanceof Result) return result;
	      } return new Result(false);
	    }
	    iterator = iterFn.call(iterable);
	  }

	  next = iterator.next;
	  while (!(step = next.call(iterator)).done) {
	    result = callWithSafeIterationClosing(iterator, boundFunction, step.value, AS_ENTRIES);
	    if (typeof result == 'object' && result && result instanceof Result) return result;
	  } return new Result(false);
	};

	iterate.stop = function (result) {
	  return new Result(true, result);
	};
	});

	var anInstance = function (it, Constructor, name) {
	  if (!(it instanceof Constructor)) {
	    throw TypeError('Incorrect ' + (name ? name + ' ' : '') + 'invocation');
	  } return it;
	};

	var ITERATOR$5 = wellKnownSymbol('iterator');
	var SAFE_CLOSING = false;

	try {
	  var called = 0;
	  var iteratorWithReturn = {
	    next: function () {
	      return { done: !!called++ };
	    },
	    'return': function () {
	      SAFE_CLOSING = true;
	    }
	  };
	  iteratorWithReturn[ITERATOR$5] = function () {
	    return this;
	  };
	  // eslint-disable-next-line no-throw-literal
	  Array.from(iteratorWithReturn, function () { throw 2; });
	} catch (error) { /* empty */ }

	var checkCorrectnessOfIteration = function (exec, SKIP_CLOSING) {
	  if (!SKIP_CLOSING && !SAFE_CLOSING) return false;
	  var ITERATION_SUPPORT = false;
	  try {
	    var object = {};
	    object[ITERATOR$5] = function () {
	      return {
	        next: function () {
	          return { done: ITERATION_SUPPORT = true };
	        }
	      };
	    };
	    exec(object);
	  } catch (error) { /* empty */ }
	  return ITERATION_SUPPORT;
	};

	var collection = function (CONSTRUCTOR_NAME, wrapper, common) {
	  var IS_MAP = CONSTRUCTOR_NAME.indexOf('Map') !== -1;
	  var IS_WEAK = CONSTRUCTOR_NAME.indexOf('Weak') !== -1;
	  var ADDER = IS_MAP ? 'set' : 'add';
	  var NativeConstructor = global_1[CONSTRUCTOR_NAME];
	  var NativePrototype = NativeConstructor && NativeConstructor.prototype;
	  var Constructor = NativeConstructor;
	  var exported = {};

	  var fixMethod = function (KEY) {
	    var nativeMethod = NativePrototype[KEY];
	    redefine(NativePrototype, KEY,
	      KEY == 'add' ? function add(value) {
	        nativeMethod.call(this, value === 0 ? 0 : value);
	        return this;
	      } : KEY == 'delete' ? function (key) {
	        return IS_WEAK && !isObject(key) ? false : nativeMethod.call(this, key === 0 ? 0 : key);
	      } : KEY == 'get' ? function get(key) {
	        return IS_WEAK && !isObject(key) ? undefined : nativeMethod.call(this, key === 0 ? 0 : key);
	      } : KEY == 'has' ? function has(key) {
	        return IS_WEAK && !isObject(key) ? false : nativeMethod.call(this, key === 0 ? 0 : key);
	      } : function set(key, value) {
	        nativeMethod.call(this, key === 0 ? 0 : key, value);
	        return this;
	      }
	    );
	  };

	  // eslint-disable-next-line max-len
	  if (isForced_1(CONSTRUCTOR_NAME, typeof NativeConstructor != 'function' || !(IS_WEAK || NativePrototype.forEach && !fails(function () {
	    new NativeConstructor().entries().next();
	  })))) {
	    // create collection constructor
	    Constructor = common.getConstructor(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER);
	    internalMetadata.REQUIRED = true;
	  } else if (isForced_1(CONSTRUCTOR_NAME, true)) {
	    var instance = new Constructor();
	    // early implementations not supports chaining
	    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
	    // V8 ~ Chromium 40- weak-collections throws on primitives, but should return false
	    var THROWS_ON_PRIMITIVES = fails(function () { instance.has(1); });
	    // most early implementations doesn't supports iterables, most modern - not close it correctly
	    // eslint-disable-next-line no-new
	    var ACCEPT_ITERABLES = checkCorrectnessOfIteration(function (iterable) { new NativeConstructor(iterable); });
	    // for early implementations -0 and +0 not the same
	    var BUGGY_ZERO = !IS_WEAK && fails(function () {
	      // V8 ~ Chromium 42- fails only with 5+ elements
	      var $instance = new NativeConstructor();
	      var index = 5;
	      while (index--) $instance[ADDER](index, index);
	      return !$instance.has(-0);
	    });

	    if (!ACCEPT_ITERABLES) {
	      Constructor = wrapper(function (dummy, iterable) {
	        anInstance(dummy, Constructor, CONSTRUCTOR_NAME);
	        var that = inheritIfRequired(new NativeConstructor(), dummy, Constructor);
	        if (iterable != undefined) iterate_1(iterable, that[ADDER], that, IS_MAP);
	        return that;
	      });
	      Constructor.prototype = NativePrototype;
	      NativePrototype.constructor = Constructor;
	    }

	    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
	      fixMethod('delete');
	      fixMethod('has');
	      IS_MAP && fixMethod('get');
	    }

	    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);

	    // weak collections should not contains .clear method
	    if (IS_WEAK && NativePrototype.clear) delete NativePrototype.clear;
	  }

	  exported[CONSTRUCTOR_NAME] = Constructor;
	  _export({ global: true, forced: Constructor != NativeConstructor }, exported);

	  setToStringTag(Constructor, CONSTRUCTOR_NAME);

	  if (!IS_WEAK) common.setStrong(Constructor, CONSTRUCTOR_NAME, IS_MAP);

	  return Constructor;
	};

	var getWeakData = internalMetadata.getWeakData;








	var setInternalState$4 = internalState.set;
	var internalStateGetterFor = internalState.getterFor;
	var find = arrayIteration.find;
	var findIndex = arrayIteration.findIndex;
	var id$1 = 0;

	// fallback for uncaught frozen keys
	var uncaughtFrozenStore = function (store) {
	  return store.frozen || (store.frozen = new UncaughtFrozenStore());
	};

	var UncaughtFrozenStore = function () {
	  this.entries = [];
	};

	var findUncaughtFrozen = function (store, key) {
	  return find(store.entries, function (it) {
	    return it[0] === key;
	  });
	};

	UncaughtFrozenStore.prototype = {
	  get: function (key) {
	    var entry = findUncaughtFrozen(this, key);
	    if (entry) return entry[1];
	  },
	  has: function (key) {
	    return !!findUncaughtFrozen(this, key);
	  },
	  set: function (key, value) {
	    var entry = findUncaughtFrozen(this, key);
	    if (entry) entry[1] = value;
	    else this.entries.push([key, value]);
	  },
	  'delete': function (key) {
	    var index = findIndex(this.entries, function (it) {
	      return it[0] === key;
	    });
	    if (~index) this.entries.splice(index, 1);
	    return !!~index;
	  }
	};

	var collectionWeak = {
	  getConstructor: function (wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
	    var C = wrapper(function (that, iterable) {
	      anInstance(that, C, CONSTRUCTOR_NAME);
	      setInternalState$4(that, {
	        type: CONSTRUCTOR_NAME,
	        id: id$1++,
	        frozen: undefined
	      });
	      if (iterable != undefined) iterate_1(iterable, that[ADDER], that, IS_MAP);
	    });

	    var getInternalState = internalStateGetterFor(CONSTRUCTOR_NAME);

	    var define = function (that, key, value) {
	      var state = getInternalState(that);
	      var data = getWeakData(anObject(key), true);
	      if (data === true) uncaughtFrozenStore(state).set(key, value);
	      else data[state.id] = value;
	      return that;
	    };

	    redefineAll(C.prototype, {
	      // 23.3.3.2 WeakMap.prototype.delete(key)
	      // 23.4.3.3 WeakSet.prototype.delete(value)
	      'delete': function (key) {
	        var state = getInternalState(this);
	        if (!isObject(key)) return false;
	        var data = getWeakData(key);
	        if (data === true) return uncaughtFrozenStore(state)['delete'](key);
	        return data && has(data, state.id) && delete data[state.id];
	      },
	      // 23.3.3.4 WeakMap.prototype.has(key)
	      // 23.4.3.4 WeakSet.prototype.has(value)
	      has: function has$1(key) {
	        var state = getInternalState(this);
	        if (!isObject(key)) return false;
	        var data = getWeakData(key);
	        if (data === true) return uncaughtFrozenStore(state).has(key);
	        return data && has(data, state.id);
	      }
	    });

	    redefineAll(C.prototype, IS_MAP ? {
	      // 23.3.3.3 WeakMap.prototype.get(key)
	      get: function get(key) {
	        var state = getInternalState(this);
	        if (isObject(key)) {
	          var data = getWeakData(key);
	          if (data === true) return uncaughtFrozenStore(state).get(key);
	          return data ? data[state.id] : undefined;
	        }
	      },
	      // 23.3.3.5 WeakMap.prototype.set(key, value)
	      set: function set(key, value) {
	        return define(this, key, value);
	      }
	    } : {
	      // 23.4.3.1 WeakSet.prototype.add(value)
	      add: function add(value) {
	        return define(this, value, true);
	      }
	    });

	    return C;
	  }
	};

	var es_weakMap = createCommonjsModule(function (module) {






	var enforceIternalState = internalState.enforce;


	var IS_IE11 = !global_1.ActiveXObject && 'ActiveXObject' in global_1;
	var isExtensible = Object.isExtensible;
	var InternalWeakMap;

	var wrapper = function (init) {
	  return function WeakMap() {
	    return init(this, arguments.length ? arguments[0] : undefined);
	  };
	};

	// `WeakMap` constructor
	// https://tc39.github.io/ecma262/#sec-weakmap-constructor
	var $WeakMap = module.exports = collection('WeakMap', wrapper, collectionWeak);

	// IE11 WeakMap frozen keys fix
	// We can't use feature detection because it crash some old IE builds
	// https://github.com/zloirock/core-js/issues/485
	if (nativeWeakMap && IS_IE11) {
	  InternalWeakMap = collectionWeak.getConstructor(wrapper, 'WeakMap', true);
	  internalMetadata.REQUIRED = true;
	  var WeakMapPrototype = $WeakMap.prototype;
	  var nativeDelete = WeakMapPrototype['delete'];
	  var nativeHas = WeakMapPrototype.has;
	  var nativeGet = WeakMapPrototype.get;
	  var nativeSet = WeakMapPrototype.set;
	  redefineAll(WeakMapPrototype, {
	    'delete': function (key) {
	      if (isObject(key) && !isExtensible(key)) {
	        var state = enforceIternalState(this);
	        if (!state.frozen) state.frozen = new InternalWeakMap();
	        return nativeDelete.call(this, key) || state.frozen['delete'](key);
	      } return nativeDelete.call(this, key);
	    },
	    has: function has(key) {
	      if (isObject(key) && !isExtensible(key)) {
	        var state = enforceIternalState(this);
	        if (!state.frozen) state.frozen = new InternalWeakMap();
	        return nativeHas.call(this, key) || state.frozen.has(key);
	      } return nativeHas.call(this, key);
	    },
	    get: function get(key) {
	      if (isObject(key) && !isExtensible(key)) {
	        var state = enforceIternalState(this);
	        if (!state.frozen) state.frozen = new InternalWeakMap();
	        return nativeHas.call(this, key) ? nativeGet.call(this, key) : state.frozen.get(key);
	      } return nativeGet.call(this, key);
	    },
	    set: function set(key, value) {
	      if (isObject(key) && !isExtensible(key)) {
	        var state = enforceIternalState(this);
	        if (!state.frozen) state.frozen = new InternalWeakMap();
	        nativeHas.call(this, key) ? nativeSet.call(this, key, value) : state.frozen.set(key, value);
	      } else nativeSet.call(this, key, value);
	      return this;
	    }
	  });
	}
	});

	var instances$1 = [];
	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$3 = 'modal';
	var Default$1 = {
	  backdrop: true,
	  keyboard: true,
	  focus: true,
	  show: true
	};
	var DefaultType$1 = {
	  backdrop: '(boolean|string)',
	  keyboard: 'boolean',
	  focus: 'boolean',
	  show: 'boolean'
	};
	var Event$7 = {
	  HIDE: 'onHide',
	  HIDDEN: 'onHidden',
	  SHOW: 'onShow',
	  SHOWN: 'onShown',
	  ON_REMOVE: 'onRemove',
	  FOCUSIN: 'focusin',
	  RESIZE: 'resize',
	  CLICK_DISMISS: 'click.dismiss',
	  KEYDOWN_DISMISS: 'keydown',
	  MOUSEUP_DISMISS: 'mouseup.dismiss',
	  MOUSEDOWN_DISMISS: 'mousedown.dismiss',
	  TRANSITION_END: 'transitionend'
	};
	var ClassName$7 = {
	  SCROLLABLE: 'modal-dialog-scrollable',
	  SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
	  BACKDROP: 'modal-backdrop',
	  OPEN: 'modal-open',
	  FADE: 'fade',
	  SHOW: 'show'
	};
	var Selector$8 = {
	  DIALOG: '.modal-dialog',
	  MODAL_BODY: '.modal-body',
	  MODAL: '[data-toggle="modal"], [data-mount="modal"]',
	  DATA_DISMISS: '[data-dismiss="modal"]',
	  FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
	  STICKY_CONTENT: '.sticky-top'
	};
	var Attribute = {
	  DATA_TARGET: 'data-target'
	}; // Event Handlers

	/**
	 * Handler for dismiss event
	 * @param {Event} event The event fired by the listener
	 */

	function dismissEvent(event) {
	  if (Util$1.getKeyCode(event) === Util$1.keyCodes.ESC) {
	    event.preventDefault();
	    this.hide();
	  }
	}
	/**
	 * Handler for Mousedown event
	 * @param {Event} event The event fired by the listener
	 */


	function mousedownEvent(event) {
	  var elem = document.querySelector(event.target);

	  if (elem.matches(this.el)) {
	    this.ignoreBackdropClick = true;
	  }
	}
	/**
	 * Handler for focus event
	 * @param {Event} event The event fired by the listener
	 */


	function focusEvent(event) {
	  if (document !== event.target && this.el !== event.target && !this.el.contains(event.target)) {
	    this.el.focus();
	  }
	}
	/**
	 * Handler for backdrop event
	 * @param {Event} event The event fired by the listener
	 */


	function backdropEvent(event) {
	  if (!this.dialog.contains(event.target)) {
	    // create and dispatch the event
	    this.el.dispatchEvent(this[Event$7.CLICK_DISMISS]);
	  }
	}
	/**
	 * Handler for dismiss click event
	 * @param {Event} event The event fired by the listener
	 */


	function clickDismissHandler(event) {
	  if (this.ignoreBackdropClick) {
	    this.ignoreBackdropClick = false;
	    return;
	  }

	  if (event.target !== event.currentTarget) {
	    return;
	  }

	  if (this.config.backdrop === 'static') {
	    this.el.focus();
	  } else {
	    this.hide();
	  }
	}
	/**
	 * Handler for mousedown event
	 * @param {Event} event The event fired by the listener
	 */


	function mousedownEventHandler() {
	  this.el.addEventListener(Event$7.MOUSEUP_DISMISS, mousedownEvent.bind(this), {
	    once: true
	  });
	} // Private


	function _getConfig(config) {
	  config = Object.assign({}, Default$1, {}, config);
	  Util$1.typeCheckConfig(NAME$3, config, DefaultType$1);
	  return config;
	}
	/**
	 * Handles the internal logic for showing an element
	 */


	function _showElement() {
	  var transition = this.el.classList.contains(ClassName$7.FADE);

	  if (!this.el.parentNode || this.el.parentNode.nodeType !== Node.ELEMENT_NODE) {
	    // Don't move modal's DOM position
	    document.body.appendChild(this.el);
	  }

	  this.el.style.display = 'block';
	  this.el.removeAttribute('aria-hidden');
	  this.el.setAttribute('aria-modal', true);

	  if (this.dialog.classList.contains(ClassName$7.SCROLLABLE)) {
	    this.dialog.querySelector(Selector$8.MODAL_BODY).scrollTop = 0;
	  } else {
	    this.el.scrollTop = 0;
	  }

	  if (transition) {
	    Util$1.reflow(this.el);
	  }

	  this.el.classList.add(ClassName$7.SHOW);

	  if (this.config.focus) {
	    _enforceFocus.bind(this)();
	  }

	  var transitionComplete = function transitionComplete() {
	    if (this.config.focus) {
	      this.el.focus();
	    }

	    this.isTransitioning = false;
	    this.el.dispatchEvent(this[Event$7.SHOWN]);
	  };

	  if (transition) {
	    this.dialog.addEventListener(Event$7.TRANSITION_END, transitionComplete.bind(this), {
	      once: true
	    });
	  } else {
	    transitionComplete.bind(this)();
	  }
	}
	/**
	 * Ensures the the focus is enforced on an element
	 */


	function _enforceFocus() {
	  document.removeEventListener(Event$7.FOCUSIN, focusEvent); // Guard against infinite focus loop

	  document.addEventListener(Event$7.FOCUSIN, focusEvent.bind(this));
	}
	/**
	 * Add and remove the event listeners for the escape event
	 */


	function _setEscapeEvent() {
	  if (this.isShown) {
	    this.el.addEventListener(Event$7.KEYDOWN_DISMISS, dismissEvent.bind(this));
	  } else {
	    this.el.removeEventListener(Event$7.KEYDOWN_DISMISS, dismissEvent);
	  }
	}
	/**
	 * Add and remove the resize event
	 */


	function _setResizeEvent() {
	  var _this = this;

	  if (this.isShown) {
	    window.addEventListener(Event$7.RESIZE, function (event) {
	      return _this.handleUpdate(event);
	    });
	  } else {
	    window.removeEventListener(Event$7.RESIZE, function (event) {
	      return _this.handleUpdate(event);
	    });
	  }
	}
	/**
	 * Private handler for hiding a modal
	 */


	function _hideModal() {
	  var _this2 = this;

	  this.el.style.display = 'none';
	  this.el.setAttribute('aria-hidden', true);
	  this.el.removeAttribute('aria-modal');
	  this.isTransitioning = false;

	  _showBackdrop.bind(this)(function () {
	    document.body.classList.remove(ClassName$7.OPEN);

	    _resetAdjustments.bind(_this2)();

	    _resetScrollbar.bind(_this2)();

	    _this2.el.dispatchEvent(_this2[Event$7.HIDDEN]);

	    document.body.removeEventListener('click', backdropEvent);
	  });
	}
	/**
	 * Remove backdrop from DOM
	 */


	function _removeBackdrop() {
	  if (this.backdrop) {
	    this.backdrop.parentNode.removeChild(this.backdrop);
	    this.backdrop = null;
	  }
	}
	/**
	 * Show Backdrop
	 * @param {*} callback Function to callback once backdrop is shown
	 */


	function _showBackdrop(callback) {
	  var _this3 = this;

	  var animate = this.el.classList.contains(ClassName$7.FADE) ? ClassName$7.FADE : '';

	  if (this.isShown && this.config.backdrop) {
	    this.backdrop = document.createElement('div');
	    this.backdrop.className = ClassName$7.BACKDROP;

	    if (animate) {
	      this.backdrop.classList.add(animate);
	    }

	    document.body.appendChild(this.backdrop);
	    document.body.addEventListener('click', backdropEvent.bind(this));
	    this.el.addEventListener(Event$7.CLICK_DISMISS, clickDismissHandler.bind(this));

	    if (animate) {
	      Util$1.reflow(this.backdrop);
	    }

	    this.backdrop.classList.add(ClassName$7.SHOW);

	    if (!callback) {
	      return;
	    }

	    if (!animate) {
	      callback();
	      return;
	    }

	    this.backdrop.addEventListener(Event$7.TRANSITION_END, callback, {
	      once: true
	    });
	  } else if (!this.isShown && this.backdrop) {
	    this.backdrop.classList.remove(ClassName$7.SHOW);

	    var callbackRemove = function callbackRemove() {
	      _removeBackdrop.bind(_this3)();

	      if (callback) {
	        callback();
	      }
	    };

	    if (this.el.classList.contains(ClassName$7.FADE)) {
	      this.backdrop.addEventListener(Event$7.TRANSITION_END, function () {
	        callbackRemove();
	      }, {
	        once: true
	      });
	    } else {
	      callbackRemove();
	    }
	  } else if (callback) {
	    callback();
	  }
	} // ----------------------------------------------------------------------
	// the following methods are used to handle overflowing modals
	// ----------------------------------------------------------------------


	function _adjustDialog() {
	  var isModalOverflowing = this.el.scrollHeight > document.documentElement.clientHeight;

	  if (!this.isBodyOverflowing && isModalOverflowing) {
	    this.el.style.paddingLeft = this.scrollbarWidth + "px";
	  }

	  if (this.isBodyOverflowing && !isModalOverflowing) {
	    this.el.style.paddingRight = this.scrollbarWidth + "px";
	  }
	}

	function _resetAdjustments() {
	  this.el.style.paddingLeft = '';
	  this.el.style.paddingRight = '';
	}

	function _checkScrollbar() {
	  var rect = document.body.getBoundingClientRect();
	  this.isBodyOverflowing = rect.left + rect.right < window.innerWidth;
	  this.scrollbarWidth = _getScrollbarWidth();
	}

	function _setScrollbar() {
	  var _this4 = this;

	  if (this.isBodyOverflowing) {
	    // Note: DOMNode.style.paddingRight returns the actual value or '' if not set
	    var fixedContent = [].slice.call(document.querySelectorAll(Selector$8.FIXED_CONTENT));
	    var stickyContent = [].slice.call(document.querySelectorAll(Selector$8.STICKY_CONTENT)); // Adjust fixed content padding

	    fixedContent.forEach(function (element) {
	      var actualPadding = element.style.paddingRight ? element.style.paddingRight : 0;
	      var calculatedPadding = getComputedStyle(element)['padding-right'];

	      _this4.data.set({
	        element: element,
	        attribute: 'padding-right'
	      }, actualPadding);

	      element.style.paddingRight = parseFloat(calculatedPadding) + _this4.scrollbarWidth + "px";
	    }); // Adjust sticky content margin

	    stickyContent.forEach(function (element) {
	      var actualMargin = element.style.marginRight ? element.style.marginRight : 0;
	      var calculatedMargin = getComputedStyle(element)['margin-right'];

	      _this4.data.set({
	        element: element,
	        attribute: 'margin-right'
	      }, actualMargin);

	      element.style.marginRight = parseFloat(calculatedMargin) - _this4.scrollbarWidth + "px";
	    }); // Adjust body padding

	    var actualPadding = document.body.style.paddingRight ? document.body.style.paddingRight : 0;
	    var calculatedPadding = getComputedStyle(document.body)['padding-right'];
	    this.data.set({
	      element: document.body,
	      attribute: 'padding-right'
	    }, actualPadding);
	    document.body.style.paddingRight = parseFloat(calculatedPadding) + this.scrollbarWidth + "px";
	  }

	  document.body.classList.add(ClassName$7.OPEN);
	}

	function _resetScrollbar() {
	  var _this5 = this;

	  // Restore fixed content padding
	  var fixedContent = [].slice.call(document.querySelectorAll(Selector$8.FIXED_CONTENT));
	  fixedContent.forEach(function (index, element) {
	    var key = {
	      element: element,
	      attribute: 'padding-right'
	    }; // Retrieve the element from the Map

	    var padding = _this5.data.get(key);

	    element.style.paddingRight = padding ? padding : ''; // Remove the item from the map

	    _this5.data.delete(key);
	  }); // Restore sticky content

	  var elements = [].slice.call(document.querySelectorAll("" + Selector$8.STICKY_CONTENT));
	  elements.forEach(function (element) {
	    var key = {
	      element: element,
	      attribute: 'margin-right'
	    }; // Retrieve the element from the Map

	    var margin = _this5.data.get(key);

	    if (typeof margin !== 'undefined') {
	      element.style.marginRight = margin;

	      _this5.data.delete(key);
	    }
	  }); // Restore body padding

	  var key = {
	    element: document.body,
	    attribute: 'padding-right'
	  };
	  var padding = this.data.get(key);
	  this.data.delete(key);
	  document.body.style.paddingRight = padding ? padding : '';
	}

	function _getScrollbarWidth() {
	  // thx d.walsh
	  var scrollDiv = document.createElement('div');
	  scrollDiv.className = ClassName$7.SCROLLBAR_MEASURER;
	  document.body.appendChild(scrollDiv);
	  var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
	  document.body.removeChild(scrollDiv);
	  return scrollbarWidth;
	}
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */


	var Modal = /*#__PURE__*/function () {
	  /**
	   * Create a Modal.
	   * @param {Object} opts - The modal options.
	   * @param {Node} opts.el - The element that toggles the modal.
	   */
	  function Modal(opts) {
	    var _this6 = this;

	    this.config = _getConfig(Default$1);
	    this.button = opts.el;
	    var relatedTarget = opts.el.getAttribute(Attribute.DATA_TARGET);
	    this.el = document.querySelector(relatedTarget);
	    this.dialog = this.el.querySelector(Selector$8.DIALOG);
	    this.backdrop = null;
	    this.isShown = false;
	    this.isBodyOverflowing = false;
	    this.ignoreBackdropClick = false;
	    this.isTransitioning = false;
	    this.scrollbarWidth = 0;
	    this.data = new WeakMap();
	    this[Event$7.ON_REMOVE] = new CustomEvent(Event$7.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$7.SHOWN] = new CustomEvent(Event$7.SHOWN, {
	      detail: {
	        relatedTarget: relatedTarget
	      }
	    });
	    this[Event$7.SHOW] = new CustomEvent(Event$7.SHOW, {
	      detail: {
	        relatedTarget: relatedTarget
	      }
	    });
	    this[Event$7.TRANSITION_END] = new CustomEvent(Util$1.TRANSITION_END);
	    this[Event$7.HIDE] = new CustomEvent(Event$7.HIDE);
	    this[Event$7.HIDDEN] = new CustomEvent(Event$7.HIDDEN);
	    this[Event$7.CLICK_DISMISS] = new CustomEvent(Event$7.CLICK_DISMISS); // Add event handlers

	    this.events = [{
	      el: this.button,
	      type: 'click',
	      handler: function handler() {
	        _this6.toggle();
	      }
	    }];
	    Util$1.addEvents(this.events);
	    instances$1.push(this);
	  }
	  /**
	   * Toggle hide and show states of the modal
	   */


	  var _proto = Modal.prototype;

	  _proto.toggle = function toggle() {
	    return this.isShown ? this.hide() : this.show();
	  }
	  /**
	   * Show the modal
	   */
	  ;

	  _proto.show = function show() {
	    var _this7 = this;

	    if (this.isShown || this.isTransitioning) {
	      return;
	    }

	    if (this.el.classList.contains(ClassName$7.FADE)) {
	      this.isTransitioning = true;
	    }

	    this.el.dispatchEvent(this[Event$7.SHOW]);

	    if (this.isShown || this[Event$7.SHOW].defaultPrevented) {
	      return;
	    }

	    this.isShown = true;

	    _checkScrollbar.bind(this)();

	    _setScrollbar.bind(this)();

	    _adjustDialog.bind(this)();

	    _setEscapeEvent.bind(this)();

	    _setResizeEvent.bind(this)(); // Add event listeners to the dismiss action


	    this.el.addEventListener(Event$7.CLICK_DISMISS, function (event) {
	      return _this7.hide(event);
	    }); // Find all the dismiss attribute elements and cause the modal to hide

	    this.el.querySelectorAll(Selector$8.DATA_DISMISS).forEach(function (_element) {
	      return _element.addEventListener('click', function (event) {
	        return _this7.hide(event);
	      });
	    });
	    this.dialog.addEventListener(Event$7.MOUSEDOWN_DISMISS, mousedownEventHandler.bind(this));

	    _showBackdrop.bind(this)(function () {
	      return _showElement.bind(_this7)(_this7.el);
	    });
	  }
	  /**
	   * Hide the modal
	   * @param {Event} event the event that triggered the hide
	   */
	  ;

	  _proto.hide = function hide(event) {
	    var _this8 = this;

	    if (event) {
	      event.preventDefault();
	    }

	    if (!this.isShown || this.isTransitioning) {
	      return;
	    }

	    this.el.dispatchEvent(this[Event$7.HIDE]);

	    if (!this.isShown || this[Event$7.HIDE].defaultPrevented) {
	      return;
	    }

	    this.isShown = false;
	    var transition = this.el.classList.contains(ClassName$7.FADE);

	    if (transition) {
	      this.isTransitioning = true;
	    }

	    _setEscapeEvent.bind(this)();

	    _setResizeEvent.bind(this)();

	    document.removeEventListener(Event$7.FOCUSIN, focusEvent);
	    this.el.classList.remove(ClassName$7.SHOW);
	    this.el.removeEventListener(Event$7.CLICK_DISMISS, clickDismissHandler);
	    this.dialog.removeEventListener(Event$7.MOUSEDOWN_DISMISS, mousedownEventHandler);

	    if (transition) {
	      this.el.addEventListener(Event$7.TRANSITION_END, function (event) {
	        return _hideModal.bind(_this8)(event);
	      }, {
	        once: true
	      });
	    } else {
	      _hideModal.bind(this)();
	    }
	  }
	  /**
	   * Handle update that happens with the modal
	   */
	  ;

	  _proto.handleUpdate = function handleUpdate() {
	    this._adjustDialog();
	  }
	  /**
	   * Remove the event handlers
	   */
	  ;

	  _proto.remove = function remove() {
	    // Remove event handlers, observers, etc.
	    Util$1.removeEvents(this.events);
	    document.removeEventListener(Event$7.FOCUSIN, focusEvent); // Remove this reference from the array of instances.

	    var index = instances$1.indexOf(this);
	    instances$1.splice(index, 1);
	    this.el.dispatchEvent(this[Event$7.ON_REMOVE]);
	  }
	  /**
	   * Get the modal instances.
	   * @returns {Object[]} An array of modal instances
	   */
	  ;

	  Modal.getInstances = function getInstances() {
	    return instances$1;
	  };

	  return Modal;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$8.MODAL, function (node) {
	    return new Modal({
	      el: node
	    });
	  });
	})();

	var $find = arrayIteration.find;



	var FIND = 'find';
	var SKIPS_HOLES = true;

	var USES_TO_LENGTH$7 = arrayMethodUsesToLength(FIND);

	// Shouldn't skip holes
	if (FIND in []) Array(1)[FIND](function () { SKIPS_HOLES = false; });

	// `Array.prototype.find` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.find
	_export({ target: 'Array', proto: true, forced: SKIPS_HOLES || !USES_TO_LENGTH$7 }, {
	  find: function find(callbackfn /* , that = undefined */) {
	    return $find(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	// https://tc39.github.io/ecma262/#sec-array.prototype-@@unscopables
	addToUnscopables(FIND);

	var IS_CONCAT_SPREADABLE = wellKnownSymbol('isConcatSpreadable');
	var MAX_SAFE_INTEGER$1 = 0x1FFFFFFFFFFFFF;
	var MAXIMUM_ALLOWED_INDEX_EXCEEDED = 'Maximum allowed index exceeded';

	// We can't use this feature detection in V8 since it causes
	// deoptimization and serious performance degradation
	// https://github.com/zloirock/core-js/issues/679
	var IS_CONCAT_SPREADABLE_SUPPORT = engineV8Version >= 51 || !fails(function () {
	  var array = [];
	  array[IS_CONCAT_SPREADABLE] = false;
	  return array.concat()[0] !== array;
	});

	var SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('concat');

	var isConcatSpreadable = function (O) {
	  if (!isObject(O)) return false;
	  var spreadable = O[IS_CONCAT_SPREADABLE];
	  return spreadable !== undefined ? !!spreadable : isArray(O);
	};

	var FORCED$3 = !IS_CONCAT_SPREADABLE_SUPPORT || !SPECIES_SUPPORT;

	// `Array.prototype.concat` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.concat
	// with adding support of @@isConcatSpreadable and @@species
	_export({ target: 'Array', proto: true, forced: FORCED$3 }, {
	  concat: function concat(arg) { // eslint-disable-line no-unused-vars
	    var O = toObject(this);
	    var A = arraySpeciesCreate(O, 0);
	    var n = 0;
	    var i, k, length, len, E;
	    for (i = -1, length = arguments.length; i < length; i++) {
	      E = i === -1 ? O : arguments[i];
	      if (isConcatSpreadable(E)) {
	        len = toLength(E.length);
	        if (n + len > MAX_SAFE_INTEGER$1) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);
	        for (k = 0; k < len; k++, n++) if (k in E) createProperty(A, n, E[k]);
	      } else {
	        if (n >= MAX_SAFE_INTEGER$1) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);
	        createProperty(A, n++, E);
	      }
	    }
	    A.length = n;
	    return A;
	  }
	});

	var FAILS_ON_PRIMITIVES = fails(function () { objectKeys(1); });

	// `Object.keys` method
	// https://tc39.github.io/ecma262/#sec-object.keys
	_export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES }, {
	  keys: function keys(it) {
	    return objectKeys(toObject(it));
	  }
	});

	/**
	 * --------------------------------------------------------------------------
	 * Bootstrap (v4.4.1): tools/sanitizer.js
	 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
	 * --------------------------------------------------------------------------
	 */
	var uriAttrs = ['background', 'cite', 'href', 'itemtype', 'longdesc', 'poster', 'src', 'xlink:href'];
	var ARIA_ATTRIBUTE_PATTERN = /^aria-[\w-]*$/i;
	var DefaultWhitelist = {
	  // Global attributes allowed on any supplied element below.
	  '*': ['class', 'dir', 'id', 'lang', 'role', ARIA_ATTRIBUTE_PATTERN],
	  a: ['target', 'href', 'title', 'rel'],
	  area: [],
	  b: [],
	  br: [],
	  col: [],
	  code: [],
	  div: [],
	  em: [],
	  hr: [],
	  h1: [],
	  h2: [],
	  h3: [],
	  h4: [],
	  h5: [],
	  h6: [],
	  i: [],
	  img: ['src', 'alt', 'title', 'width', 'height'],
	  li: [],
	  ol: [],
	  p: [],
	  pre: [],
	  s: [],
	  small: [],
	  span: [],
	  sub: [],
	  sup: [],
	  strong: [],
	  u: [],
	  ul: []
	};
	/**
	 * A pattern that recognizes a commonly useful subset of URLs that are safe.
	 *
	 * Shoutout to Angular 7 https://github.com/angular/angular/blob/7.2.4/packages/core/src/sanitization/url_sanitizer.ts
	 */

	var SAFE_URL_PATTERN = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi;
	/**
	 * A pattern that matches safe data URLs. Only matches image, video and audio types.
	 *
	 * Shoutout to Angular 7 https://github.com/angular/angular/blob/7.2.4/packages/core/src/sanitization/url_sanitizer.ts
	 */

	var DATA_URL_PATTERN = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

	function allowedAttribute(attr, allowedAttributeList) {
	  var attrName = attr.nodeName.toLowerCase();

	  if (allowedAttributeList.indexOf(attrName) !== -1) {
	    if (uriAttrs.indexOf(attrName) !== -1) {
	      return Boolean(attr.nodeValue.match(SAFE_URL_PATTERN) || attr.nodeValue.match(DATA_URL_PATTERN));
	    }

	    return true;
	  }

	  var regExp = allowedAttributeList.filter(function (attrRegex) {
	    return attrRegex instanceof RegExp;
	  }); // Check if a regular expression validates the attribute.

	  for (var i = 0, l = regExp.length; i < l; i++) {
	    if (attrName.match(regExp[i])) {
	      return true;
	    }
	  }

	  return false;
	}

	function sanitizeHtml(unsafeHtml, whiteList, sanitizeFn) {
	  if (unsafeHtml.length === 0) {
	    return unsafeHtml;
	  }

	  if (sanitizeFn && typeof sanitizeFn === 'function') {
	    return sanitizeFn(unsafeHtml);
	  }

	  var domParser = new window.DOMParser();
	  var createdDocument = domParser.parseFromString(unsafeHtml, 'text/html');
	  var whitelistKeys = Object.keys(whiteList);
	  var elements = [].slice.call(createdDocument.body.querySelectorAll('*'));

	  var _loop = function _loop(i, len) {
	    var el = elements[i];
	    var elName = el.nodeName.toLowerCase();

	    if (whitelistKeys.indexOf(el.nodeName.toLowerCase()) === -1) {
	      el.parentNode.removeChild(el);
	      return "continue";
	    }

	    var attributeList = [].slice.call(el.attributes);
	    var whitelistedAttributes = [].concat(whiteList['*'] || [], whiteList[elName] || []);
	    attributeList.forEach(function (attr) {
	      if (!allowedAttribute(attr, whitelistedAttributes)) {
	        el.removeAttribute(attr.nodeName);
	      }
	    });
	  };

	  for (var i = 0, len = elements.length; i < len; i++) {
	    var _ret = _loop(i);

	    if (_ret === "continue") continue;
	  }

	  return createdDocument.body.innerHTML;
	}

	/**!
	 * @fileOverview Kickass library to create and place poppers near their reference elements.
	 * @version 1.16.1
	 * @license
	 * Copyright (c) 2016 Federico Zivolo and contributors
	 *
	 * Permission is hereby granted, free of charge, to any person obtaining a copy
	 * of this software and associated documentation files (the "Software"), to deal
	 * in the Software without restriction, including without limitation the rights
	 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	 * copies of the Software, and to permit persons to whom the Software is
	 * furnished to do so, subject to the following conditions:
	 *
	 * The above copyright notice and this permission notice shall be included in all
	 * copies or substantial portions of the Software.
	 *
	 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	 * SOFTWARE.
	 */
	var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined' && typeof navigator !== 'undefined';

	var timeoutDuration = function () {
	  var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
	  for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
	    if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
	      return 1;
	    }
	  }
	  return 0;
	}();

	function microtaskDebounce(fn) {
	  var called = false;
	  return function () {
	    if (called) {
	      return;
	    }
	    called = true;
	    window.Promise.resolve().then(function () {
	      called = false;
	      fn();
	    });
	  };
	}

	function taskDebounce(fn) {
	  var scheduled = false;
	  return function () {
	    if (!scheduled) {
	      scheduled = true;
	      setTimeout(function () {
	        scheduled = false;
	        fn();
	      }, timeoutDuration);
	    }
	  };
	}

	var supportsMicroTasks = isBrowser && window.Promise;

	/**
	* Create a debounced version of a method, that's asynchronously deferred
	* but called in the minimum time possible.
	*
	* @method
	* @memberof Popper.Utils
	* @argument {Function} fn
	* @returns {Function}
	*/
	var debounce$1 = supportsMicroTasks ? microtaskDebounce : taskDebounce;

	/**
	 * Check if the given variable is a function
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Any} functionToCheck - variable to check
	 * @returns {Boolean} answer to: is a function?
	 */
	function isFunction(functionToCheck) {
	  var getType = {};
	  return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
	}

	/**
	 * Get CSS computed property of the given element
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Eement} element
	 * @argument {String} property
	 */
	function getStyleComputedProperty(element, property) {
	  if (element.nodeType !== 1) {
	    return [];
	  }
	  // NOTE: 1 DOM access here
	  var window = element.ownerDocument.defaultView;
	  var css = window.getComputedStyle(element, null);
	  return property ? css[property] : css;
	}

	/**
	 * Returns the parentNode or the host of the element
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @returns {Element} parent
	 */
	function getParentNode(element) {
	  if (element.nodeName === 'HTML') {
	    return element;
	  }
	  return element.parentNode || element.host;
	}

	/**
	 * Returns the scrolling parent of the given element
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @returns {Element} scroll parent
	 */
	function getScrollParent(element) {
	  // Return body, `getScroll` will take care to get the correct `scrollTop` from it
	  if (!element) {
	    return document.body;
	  }

	  switch (element.nodeName) {
	    case 'HTML':
	    case 'BODY':
	      return element.ownerDocument.body;
	    case '#document':
	      return element.body;
	  }

	  // Firefox want us to check `-x` and `-y` variations as well

	  var _getStyleComputedProp = getStyleComputedProperty(element),
	      overflow = _getStyleComputedProp.overflow,
	      overflowX = _getStyleComputedProp.overflowX,
	      overflowY = _getStyleComputedProp.overflowY;

	  if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) {
	    return element;
	  }

	  return getScrollParent(getParentNode(element));
	}

	/**
	 * Returns the reference node of the reference object, or the reference object itself.
	 * @method
	 * @memberof Popper.Utils
	 * @param {Element|Object} reference - the reference element (the popper will be relative to this)
	 * @returns {Element} parent
	 */
	function getReferenceNode(reference) {
	  return reference && reference.referenceNode ? reference.referenceNode : reference;
	}

	var isIE11 = isBrowser && !!(window.MSInputMethodContext && document.documentMode);
	var isIE10 = isBrowser && /MSIE 10/.test(navigator.userAgent);

	/**
	 * Determines if the browser is Internet Explorer
	 * @method
	 * @memberof Popper.Utils
	 * @param {Number} version to check
	 * @returns {Boolean} isIE
	 */
	function isIE(version) {
	  if (version === 11) {
	    return isIE11;
	  }
	  if (version === 10) {
	    return isIE10;
	  }
	  return isIE11 || isIE10;
	}

	/**
	 * Returns the offset parent of the given element
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @returns {Element} offset parent
	 */
	function getOffsetParent(element) {
	  if (!element) {
	    return document.documentElement;
	  }

	  var noOffsetParent = isIE(10) ? document.body : null;

	  // NOTE: 1 DOM access here
	  var offsetParent = element.offsetParent || null;
	  // Skip hidden elements which don't have an offsetParent
	  while (offsetParent === noOffsetParent && element.nextElementSibling) {
	    offsetParent = (element = element.nextElementSibling).offsetParent;
	  }

	  var nodeName = offsetParent && offsetParent.nodeName;

	  if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
	    return element ? element.ownerDocument.documentElement : document.documentElement;
	  }

	  // .offsetParent will return the closest TH, TD or TABLE in case
	  // no offsetParent is present, I hate this job...
	  if (['TH', 'TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
	    return getOffsetParent(offsetParent);
	  }

	  return offsetParent;
	}

	function isOffsetContainer(element) {
	  var nodeName = element.nodeName;

	  if (nodeName === 'BODY') {
	    return false;
	  }
	  return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
	}

	/**
	 * Finds the root node (document, shadowDOM root) of the given element
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} node
	 * @returns {Element} root node
	 */
	function getRoot(node) {
	  if (node.parentNode !== null) {
	    return getRoot(node.parentNode);
	  }

	  return node;
	}

	/**
	 * Finds the offset parent common to the two provided nodes
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element1
	 * @argument {Element} element2
	 * @returns {Element} common offset parent
	 */
	function findCommonOffsetParent(element1, element2) {
	  // This check is needed to avoid errors in case one of the elements isn't defined for any reason
	  if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
	    return document.documentElement;
	  }

	  // Here we make sure to give as "start" the element that comes first in the DOM
	  var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
	  var start = order ? element1 : element2;
	  var end = order ? element2 : element1;

	  // Get common ancestor container
	  var range = document.createRange();
	  range.setStart(start, 0);
	  range.setEnd(end, 0);
	  var commonAncestorContainer = range.commonAncestorContainer;

	  // Both nodes are inside #document

	  if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
	    if (isOffsetContainer(commonAncestorContainer)) {
	      return commonAncestorContainer;
	    }

	    return getOffsetParent(commonAncestorContainer);
	  }

	  // one of the nodes is inside shadowDOM, find which one
	  var element1root = getRoot(element1);
	  if (element1root.host) {
	    return findCommonOffsetParent(element1root.host, element2);
	  } else {
	    return findCommonOffsetParent(element1, getRoot(element2).host);
	  }
	}

	/**
	 * Gets the scroll value of the given element in the given side (top and left)
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @argument {String} side `top` or `left`
	 * @returns {number} amount of scrolled pixels
	 */
	function getScroll(element) {
	  var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

	  var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
	  var nodeName = element.nodeName;

	  if (nodeName === 'BODY' || nodeName === 'HTML') {
	    var html = element.ownerDocument.documentElement;
	    var scrollingElement = element.ownerDocument.scrollingElement || html;
	    return scrollingElement[upperSide];
	  }

	  return element[upperSide];
	}

	/*
	 * Sum or subtract the element scroll values (left and top) from a given rect object
	 * @method
	 * @memberof Popper.Utils
	 * @param {Object} rect - Rect object you want to change
	 * @param {HTMLElement} element - The element from the function reads the scroll values
	 * @param {Boolean} subtract - set to true if you want to subtract the scroll values
	 * @return {Object} rect - The modifier rect object
	 */
	function includeScroll(rect, element) {
	  var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	  var scrollTop = getScroll(element, 'top');
	  var scrollLeft = getScroll(element, 'left');
	  var modifier = subtract ? -1 : 1;
	  rect.top += scrollTop * modifier;
	  rect.bottom += scrollTop * modifier;
	  rect.left += scrollLeft * modifier;
	  rect.right += scrollLeft * modifier;
	  return rect;
	}

	/*
	 * Helper to detect borders of a given element
	 * @method
	 * @memberof Popper.Utils
	 * @param {CSSStyleDeclaration} styles
	 * Result of `getStyleComputedProperty` on the given element
	 * @param {String} axis - `x` or `y`
	 * @return {number} borders - The borders size of the given axis
	 */

	function getBordersSize(styles, axis) {
	  var sideA = axis === 'x' ? 'Left' : 'Top';
	  var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

	  return parseFloat(styles['border' + sideA + 'Width']) + parseFloat(styles['border' + sideB + 'Width']);
	}

	function getSize(axis, body, html, computedStyle) {
	  return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE(10) ? parseInt(html['offset' + axis]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')]) : 0);
	}

	function getWindowSizes(document) {
	  var body = document.body;
	  var html = document.documentElement;
	  var computedStyle = isIE(10) && getComputedStyle(html);

	  return {
	    height: getSize('Height', body, html, computedStyle),
	    width: getSize('Width', body, html, computedStyle)
	  };
	}

	var classCallCheck = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

	var createClass = function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      Object.defineProperty(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	}();





	var defineProperty$8 = function (obj, key, value) {
	  if (key in obj) {
	    Object.defineProperty(obj, key, {
	      value: value,
	      enumerable: true,
	      configurable: true,
	      writable: true
	    });
	  } else {
	    obj[key] = value;
	  }

	  return obj;
	};

	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];

	    for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }

	  return target;
	};

	/**
	 * Given element offsets, generate an output similar to getBoundingClientRect
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Object} offsets
	 * @returns {Object} ClientRect like output
	 */
	function getClientRect(offsets) {
	  return _extends({}, offsets, {
	    right: offsets.left + offsets.width,
	    bottom: offsets.top + offsets.height
	  });
	}

	/**
	 * Get bounding client rect of given element
	 * @method
	 * @memberof Popper.Utils
	 * @param {HTMLElement} element
	 * @return {Object} client rect
	 */
	function getBoundingClientRect(element) {
	  var rect = {};

	  // IE10 10 FIX: Please, don't ask, the element isn't
	  // considered in DOM in some circumstances...
	  // This isn't reproducible in IE10 compatibility mode of IE11
	  try {
	    if (isIE(10)) {
	      rect = element.getBoundingClientRect();
	      var scrollTop = getScroll(element, 'top');
	      var scrollLeft = getScroll(element, 'left');
	      rect.top += scrollTop;
	      rect.left += scrollLeft;
	      rect.bottom += scrollTop;
	      rect.right += scrollLeft;
	    } else {
	      rect = element.getBoundingClientRect();
	    }
	  } catch (e) {}

	  var result = {
	    left: rect.left,
	    top: rect.top,
	    width: rect.right - rect.left,
	    height: rect.bottom - rect.top
	  };

	  // subtract scrollbar size from sizes
	  var sizes = element.nodeName === 'HTML' ? getWindowSizes(element.ownerDocument) : {};
	  var width = sizes.width || element.clientWidth || result.width;
	  var height = sizes.height || element.clientHeight || result.height;

	  var horizScrollbar = element.offsetWidth - width;
	  var vertScrollbar = element.offsetHeight - height;

	  // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
	  // we make this check conditional for performance reasons
	  if (horizScrollbar || vertScrollbar) {
	    var styles = getStyleComputedProperty(element);
	    horizScrollbar -= getBordersSize(styles, 'x');
	    vertScrollbar -= getBordersSize(styles, 'y');

	    result.width -= horizScrollbar;
	    result.height -= vertScrollbar;
	  }

	  return getClientRect(result);
	}

	function getOffsetRectRelativeToArbitraryNode(children, parent) {
	  var fixedPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	  var isIE10 = isIE(10);
	  var isHTML = parent.nodeName === 'HTML';
	  var childrenRect = getBoundingClientRect(children);
	  var parentRect = getBoundingClientRect(parent);
	  var scrollParent = getScrollParent(children);

	  var styles = getStyleComputedProperty(parent);
	  var borderTopWidth = parseFloat(styles.borderTopWidth);
	  var borderLeftWidth = parseFloat(styles.borderLeftWidth);

	  // In cases where the parent is fixed, we must ignore negative scroll in offset calc
	  if (fixedPosition && isHTML) {
	    parentRect.top = Math.max(parentRect.top, 0);
	    parentRect.left = Math.max(parentRect.left, 0);
	  }
	  var offsets = getClientRect({
	    top: childrenRect.top - parentRect.top - borderTopWidth,
	    left: childrenRect.left - parentRect.left - borderLeftWidth,
	    width: childrenRect.width,
	    height: childrenRect.height
	  });
	  offsets.marginTop = 0;
	  offsets.marginLeft = 0;

	  // Subtract margins of documentElement in case it's being used as parent
	  // we do this only on HTML because it's the only element that behaves
	  // differently when margins are applied to it. The margins are included in
	  // the box of the documentElement, in the other cases not.
	  if (!isIE10 && isHTML) {
	    var marginTop = parseFloat(styles.marginTop);
	    var marginLeft = parseFloat(styles.marginLeft);

	    offsets.top -= borderTopWidth - marginTop;
	    offsets.bottom -= borderTopWidth - marginTop;
	    offsets.left -= borderLeftWidth - marginLeft;
	    offsets.right -= borderLeftWidth - marginLeft;

	    // Attach marginTop and marginLeft because in some circumstances we may need them
	    offsets.marginTop = marginTop;
	    offsets.marginLeft = marginLeft;
	  }

	  if (isIE10 && !fixedPosition ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
	    offsets = includeScroll(offsets, parent);
	  }

	  return offsets;
	}

	function getViewportOffsetRectRelativeToArtbitraryNode(element) {
	  var excludeScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var html = element.ownerDocument.documentElement;
	  var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
	  var width = Math.max(html.clientWidth, window.innerWidth || 0);
	  var height = Math.max(html.clientHeight, window.innerHeight || 0);

	  var scrollTop = !excludeScroll ? getScroll(html) : 0;
	  var scrollLeft = !excludeScroll ? getScroll(html, 'left') : 0;

	  var offset = {
	    top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
	    left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
	    width: width,
	    height: height
	  };

	  return getClientRect(offset);
	}

	/**
	 * Check if the given element is fixed or is inside a fixed parent
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @argument {Element} customContainer
	 * @returns {Boolean} answer to "isFixed?"
	 */
	function isFixed(element) {
	  var nodeName = element.nodeName;
	  if (nodeName === 'BODY' || nodeName === 'HTML') {
	    return false;
	  }
	  if (getStyleComputedProperty(element, 'position') === 'fixed') {
	    return true;
	  }
	  var parentNode = getParentNode(element);
	  if (!parentNode) {
	    return false;
	  }
	  return isFixed(parentNode);
	}

	/**
	 * Finds the first parent of an element that has a transformed property defined
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @returns {Element} first transformed parent or documentElement
	 */

	function getFixedPositionOffsetParent(element) {
	  // This check is needed to avoid errors in case one of the elements isn't defined for any reason
	  if (!element || !element.parentElement || isIE()) {
	    return document.documentElement;
	  }
	  var el = element.parentElement;
	  while (el && getStyleComputedProperty(el, 'transform') === 'none') {
	    el = el.parentElement;
	  }
	  return el || document.documentElement;
	}

	/**
	 * Computed the boundaries limits and return them
	 * @method
	 * @memberof Popper.Utils
	 * @param {HTMLElement} popper
	 * @param {HTMLElement} reference
	 * @param {number} padding
	 * @param {HTMLElement} boundariesElement - Element used to define the boundaries
	 * @param {Boolean} fixedPosition - Is in fixed position mode
	 * @returns {Object} Coordinates of the boundaries
	 */
	function getBoundaries(popper, reference, padding, boundariesElement) {
	  var fixedPosition = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

	  // NOTE: 1 DOM access here

	  var boundaries = { top: 0, left: 0 };
	  var offsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, getReferenceNode(reference));

	  // Handle viewport case
	  if (boundariesElement === 'viewport') {
	    boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent, fixedPosition);
	  } else {
	    // Handle other cases based on DOM element used as boundaries
	    var boundariesNode = void 0;
	    if (boundariesElement === 'scrollParent') {
	      boundariesNode = getScrollParent(getParentNode(reference));
	      if (boundariesNode.nodeName === 'BODY') {
	        boundariesNode = popper.ownerDocument.documentElement;
	      }
	    } else if (boundariesElement === 'window') {
	      boundariesNode = popper.ownerDocument.documentElement;
	    } else {
	      boundariesNode = boundariesElement;
	    }

	    var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent, fixedPosition);

	    // In case of HTML, we need a different computation
	    if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
	      var _getWindowSizes = getWindowSizes(popper.ownerDocument),
	          height = _getWindowSizes.height,
	          width = _getWindowSizes.width;

	      boundaries.top += offsets.top - offsets.marginTop;
	      boundaries.bottom = height + offsets.top;
	      boundaries.left += offsets.left - offsets.marginLeft;
	      boundaries.right = width + offsets.left;
	    } else {
	      // for all the other DOM elements, this one is good
	      boundaries = offsets;
	    }
	  }

	  // Add paddings
	  padding = padding || 0;
	  var isPaddingNumber = typeof padding === 'number';
	  boundaries.left += isPaddingNumber ? padding : padding.left || 0;
	  boundaries.top += isPaddingNumber ? padding : padding.top || 0;
	  boundaries.right -= isPaddingNumber ? padding : padding.right || 0;
	  boundaries.bottom -= isPaddingNumber ? padding : padding.bottom || 0;

	  return boundaries;
	}

	function getArea(_ref) {
	  var width = _ref.width,
	      height = _ref.height;

	  return width * height;
	}

	/**
	 * Utility used to transform the `auto` placement to the placement with more
	 * available space.
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
	  var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

	  if (placement.indexOf('auto') === -1) {
	    return placement;
	  }

	  var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

	  var rects = {
	    top: {
	      width: boundaries.width,
	      height: refRect.top - boundaries.top
	    },
	    right: {
	      width: boundaries.right - refRect.right,
	      height: boundaries.height
	    },
	    bottom: {
	      width: boundaries.width,
	      height: boundaries.bottom - refRect.bottom
	    },
	    left: {
	      width: refRect.left - boundaries.left,
	      height: boundaries.height
	    }
	  };

	  var sortedAreas = Object.keys(rects).map(function (key) {
	    return _extends({
	      key: key
	    }, rects[key], {
	      area: getArea(rects[key])
	    });
	  }).sort(function (a, b) {
	    return b.area - a.area;
	  });

	  var filteredAreas = sortedAreas.filter(function (_ref2) {
	    var width = _ref2.width,
	        height = _ref2.height;
	    return width >= popper.clientWidth && height >= popper.clientHeight;
	  });

	  var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

	  var variation = placement.split('-')[1];

	  return computedPlacement + (variation ? '-' + variation : '');
	}

	/**
	 * Get offsets to the reference element
	 * @method
	 * @memberof Popper.Utils
	 * @param {Object} state
	 * @param {Element} popper - the popper element
	 * @param {Element} reference - the reference element (the popper will be relative to this)
	 * @param {Element} fixedPosition - is in fixed position mode
	 * @returns {Object} An object containing the offsets which will be applied to the popper
	 */
	function getReferenceOffsets(state, popper, reference) {
	  var fixedPosition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

	  var commonOffsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, getReferenceNode(reference));
	  return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent, fixedPosition);
	}

	/**
	 * Get the outer sizes of the given element (offset size + margins)
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element
	 * @returns {Object} object containing width and height properties
	 */
	function getOuterSizes(element) {
	  var window = element.ownerDocument.defaultView;
	  var styles = window.getComputedStyle(element);
	  var x = parseFloat(styles.marginTop || 0) + parseFloat(styles.marginBottom || 0);
	  var y = parseFloat(styles.marginLeft || 0) + parseFloat(styles.marginRight || 0);
	  var result = {
	    width: element.offsetWidth + y,
	    height: element.offsetHeight + x
	  };
	  return result;
	}

	/**
	 * Get the opposite placement of the given one
	 * @method
	 * @memberof Popper.Utils
	 * @argument {String} placement
	 * @returns {String} flipped placement
	 */
	function getOppositePlacement(placement) {
	  var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
	  return placement.replace(/left|right|bottom|top/g, function (matched) {
	    return hash[matched];
	  });
	}

	/**
	 * Get offsets to the popper
	 * @method
	 * @memberof Popper.Utils
	 * @param {Object} position - CSS position the Popper will get applied
	 * @param {HTMLElement} popper - the popper element
	 * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
	 * @param {String} placement - one of the valid placement options
	 * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
	 */
	function getPopperOffsets(popper, referenceOffsets, placement) {
	  placement = placement.split('-')[0];

	  // Get popper node sizes
	  var popperRect = getOuterSizes(popper);

	  // Add position, width and height to our offsets object
	  var popperOffsets = {
	    width: popperRect.width,
	    height: popperRect.height
	  };

	  // depending by the popper placement we have to compute its offsets slightly differently
	  var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
	  var mainSide = isHoriz ? 'top' : 'left';
	  var secondarySide = isHoriz ? 'left' : 'top';
	  var measurement = isHoriz ? 'height' : 'width';
	  var secondaryMeasurement = !isHoriz ? 'height' : 'width';

	  popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
	  if (placement === secondarySide) {
	    popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
	  } else {
	    popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
	  }

	  return popperOffsets;
	}

	/**
	 * Mimics the `find` method of Array
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Array} arr
	 * @argument prop
	 * @argument value
	 * @returns index or -1
	 */
	function find$1(arr, check) {
	  // use native find if supported
	  if (Array.prototype.find) {
	    return arr.find(check);
	  }

	  // use `filter` to obtain the same behavior of `find`
	  return arr.filter(check)[0];
	}

	/**
	 * Return the index of the matching object
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Array} arr
	 * @argument prop
	 * @argument value
	 * @returns index or -1
	 */
	function findIndex$1(arr, prop, value) {
	  // use native findIndex if supported
	  if (Array.prototype.findIndex) {
	    return arr.findIndex(function (cur) {
	      return cur[prop] === value;
	    });
	  }

	  // use `find` + `indexOf` if `findIndex` isn't supported
	  var match = find$1(arr, function (obj) {
	    return obj[prop] === value;
	  });
	  return arr.indexOf(match);
	}

	/**
	 * Loop trough the list of modifiers and run them in order,
	 * each of them will then edit the data object.
	 * @method
	 * @memberof Popper.Utils
	 * @param {dataObject} data
	 * @param {Array} modifiers
	 * @param {String} ends - Optional modifier name used as stopper
	 * @returns {dataObject}
	 */
	function runModifiers(modifiers, data, ends) {
	  var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex$1(modifiers, 'name', ends));

	  modifiersToRun.forEach(function (modifier) {
	    if (modifier['function']) {
	      // eslint-disable-line dot-notation
	      console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
	    }
	    var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
	    if (modifier.enabled && isFunction(fn)) {
	      // Add properties to offsets to make them a complete clientRect object
	      // we do this before each modifier to make sure the previous one doesn't
	      // mess with these values
	      data.offsets.popper = getClientRect(data.offsets.popper);
	      data.offsets.reference = getClientRect(data.offsets.reference);

	      data = fn(data, modifier);
	    }
	  });

	  return data;
	}

	/**
	 * Updates the position of the popper, computing the new offsets and applying
	 * the new style.<br />
	 * Prefer `scheduleUpdate` over `update` because of performance reasons.
	 * @method
	 * @memberof Popper
	 */
	function update() {
	  // if popper is destroyed, don't perform any further update
	  if (this.state.isDestroyed) {
	    return;
	  }

	  var data = {
	    instance: this,
	    styles: {},
	    arrowStyles: {},
	    attributes: {},
	    flipped: false,
	    offsets: {}
	  };

	  // compute reference element offsets
	  data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference, this.options.positionFixed);

	  // compute auto placement, store placement inside the data object,
	  // modifiers will be able to edit `placement` if needed
	  // and refer to originalPlacement to know the original value
	  data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

	  // store the computed placement inside `originalPlacement`
	  data.originalPlacement = data.placement;

	  data.positionFixed = this.options.positionFixed;

	  // compute the popper offsets
	  data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);

	  data.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute';

	  // run the modifiers
	  data = runModifiers(this.modifiers, data);

	  // the first `update` will call `onCreate` callback
	  // the other ones will call `onUpdate` callback
	  if (!this.state.isCreated) {
	    this.state.isCreated = true;
	    this.options.onCreate(data);
	  } else {
	    this.options.onUpdate(data);
	  }
	}

	/**
	 * Helper used to know if the given modifier is enabled.
	 * @method
	 * @memberof Popper.Utils
	 * @returns {Boolean}
	 */
	function isModifierEnabled(modifiers, modifierName) {
	  return modifiers.some(function (_ref) {
	    var name = _ref.name,
	        enabled = _ref.enabled;
	    return enabled && name === modifierName;
	  });
	}

	/**
	 * Get the prefixed supported property name
	 * @method
	 * @memberof Popper.Utils
	 * @argument {String} property (camelCase)
	 * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
	 */
	function getSupportedPropertyName(property) {
	  var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
	  var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

	  for (var i = 0; i < prefixes.length; i++) {
	    var prefix = prefixes[i];
	    var toCheck = prefix ? '' + prefix + upperProp : property;
	    if (typeof document.body.style[toCheck] !== 'undefined') {
	      return toCheck;
	    }
	  }
	  return null;
	}

	/**
	 * Destroys the popper.
	 * @method
	 * @memberof Popper
	 */
	function destroy() {
	  this.state.isDestroyed = true;

	  // touch DOM only if `applyStyle` modifier is enabled
	  if (isModifierEnabled(this.modifiers, 'applyStyle')) {
	    this.popper.removeAttribute('x-placement');
	    this.popper.style.position = '';
	    this.popper.style.top = '';
	    this.popper.style.left = '';
	    this.popper.style.right = '';
	    this.popper.style.bottom = '';
	    this.popper.style.willChange = '';
	    this.popper.style[getSupportedPropertyName('transform')] = '';
	  }

	  this.disableEventListeners();

	  // remove the popper if user explicitly asked for the deletion on destroy
	  // do not use `remove` because IE11 doesn't support it
	  if (this.options.removeOnDestroy) {
	    this.popper.parentNode.removeChild(this.popper);
	  }
	  return this;
	}

	/**
	 * Get the window associated with the element
	 * @argument {Element} element
	 * @returns {Window}
	 */
	function getWindow(element) {
	  var ownerDocument = element.ownerDocument;
	  return ownerDocument ? ownerDocument.defaultView : window;
	}

	function attachToScrollParents(scrollParent, event, callback, scrollParents) {
	  var isBody = scrollParent.nodeName === 'BODY';
	  var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
	  target.addEventListener(event, callback, { passive: true });

	  if (!isBody) {
	    attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
	  }
	  scrollParents.push(target);
	}

	/**
	 * Setup needed event listeners used to update the popper position
	 * @method
	 * @memberof Popper.Utils
	 * @private
	 */
	function setupEventListeners(reference, options, state, updateBound) {
	  // Resize event listener on window
	  state.updateBound = updateBound;
	  getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

	  // Scroll event listener on scroll parents
	  var scrollElement = getScrollParent(reference);
	  attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
	  state.scrollElement = scrollElement;
	  state.eventsEnabled = true;

	  return state;
	}

	/**
	 * It will add resize/scroll events and start recalculating
	 * position of the popper element when they are triggered.
	 * @method
	 * @memberof Popper
	 */
	function enableEventListeners() {
	  if (!this.state.eventsEnabled) {
	    this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
	  }
	}

	/**
	 * Remove event listeners used to update the popper position
	 * @method
	 * @memberof Popper.Utils
	 * @private
	 */
	function removeEventListeners(reference, state) {
	  // Remove resize event listener on window
	  getWindow(reference).removeEventListener('resize', state.updateBound);

	  // Remove scroll event listener on scroll parents
	  state.scrollParents.forEach(function (target) {
	    target.removeEventListener('scroll', state.updateBound);
	  });

	  // Reset state
	  state.updateBound = null;
	  state.scrollParents = [];
	  state.scrollElement = null;
	  state.eventsEnabled = false;
	  return state;
	}

	/**
	 * It will remove resize/scroll events and won't recalculate popper position
	 * when they are triggered. It also won't trigger `onUpdate` callback anymore,
	 * unless you call `update` method manually.
	 * @method
	 * @memberof Popper
	 */
	function disableEventListeners() {
	  if (this.state.eventsEnabled) {
	    cancelAnimationFrame(this.scheduleUpdate);
	    this.state = removeEventListeners(this.reference, this.state);
	  }
	}

	/**
	 * Tells if a given input is a number
	 * @method
	 * @memberof Popper.Utils
	 * @param {*} input to check
	 * @return {Boolean}
	 */
	function isNumeric(n) {
	  return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
	}

	/**
	 * Set the style to the given popper
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element - Element to apply the style to
	 * @argument {Object} styles
	 * Object with a list of properties and values which will be applied to the element
	 */
	function setStyles(element, styles) {
	  Object.keys(styles).forEach(function (prop) {
	    var unit = '';
	    // add unit if the value is numeric and is one of the following
	    if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
	      unit = 'px';
	    }
	    element.style[prop] = styles[prop] + unit;
	  });
	}

	/**
	 * Set the attributes to the given popper
	 * @method
	 * @memberof Popper.Utils
	 * @argument {Element} element - Element to apply the attributes to
	 * @argument {Object} styles
	 * Object with a list of properties and values which will be applied to the element
	 */
	function setAttributes(element, attributes) {
	  Object.keys(attributes).forEach(function (prop) {
	    var value = attributes[prop];
	    if (value !== false) {
	      element.setAttribute(prop, attributes[prop]);
	    } else {
	      element.removeAttribute(prop);
	    }
	  });
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Object} data.styles - List of style properties - values to apply to popper element
	 * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The same data object
	 */
	function applyStyle(data) {
	  // any property present in `data.styles` will be applied to the popper,
	  // in this way we can make the 3rd party modifiers add custom styles to it
	  // Be aware, modifiers could override the properties defined in the previous
	  // lines of this modifier!
	  setStyles(data.instance.popper, data.styles);

	  // any property present in `data.attributes` will be applied to the popper,
	  // they will be set as HTML attributes of the element
	  setAttributes(data.instance.popper, data.attributes);

	  // if arrowElement is defined and arrowStyles has some properties
	  if (data.arrowElement && Object.keys(data.arrowStyles).length) {
	    setStyles(data.arrowElement, data.arrowStyles);
	  }

	  return data;
	}

	/**
	 * Set the x-placement attribute before everything else because it could be used
	 * to add margins to the popper margins needs to be calculated to get the
	 * correct popper offsets.
	 * @method
	 * @memberof Popper.modifiers
	 * @param {HTMLElement} reference - The reference element used to position the popper
	 * @param {HTMLElement} popper - The HTML element used as popper
	 * @param {Object} options - Popper.js options
	 */
	function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
	  // compute reference element offsets
	  var referenceOffsets = getReferenceOffsets(state, popper, reference, options.positionFixed);

	  // compute auto placement, store placement inside the data object,
	  // modifiers will be able to edit `placement` if needed
	  // and refer to originalPlacement to know the original value
	  var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

	  popper.setAttribute('x-placement', placement);

	  // Apply `position` to popper before anything else because
	  // without the position applied we can't guarantee correct computations
	  setStyles(popper, { position: options.positionFixed ? 'fixed' : 'absolute' });

	  return options;
	}

	/**
	 * @function
	 * @memberof Popper.Utils
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Boolean} shouldRound - If the offsets should be rounded at all
	 * @returns {Object} The popper's position offsets rounded
	 *
	 * The tale of pixel-perfect positioning. It's still not 100% perfect, but as
	 * good as it can be within reason.
	 * Discussion here: https://github.com/FezVrasta/popper.js/pull/715
	 *
	 * Low DPI screens cause a popper to be blurry if not using full pixels (Safari
	 * as well on High DPI screens).
	 *
	 * Firefox prefers no rounding for positioning and does not have blurriness on
	 * high DPI screens.
	 *
	 * Only horizontal placement and left/right values need to be considered.
	 */
	function getRoundedOffsets(data, shouldRound) {
	  var _data$offsets = data.offsets,
	      popper = _data$offsets.popper,
	      reference = _data$offsets.reference;
	  var round = Math.round,
	      floor = Math.floor;

	  var noRound = function noRound(v) {
	    return v;
	  };

	  var referenceWidth = round(reference.width);
	  var popperWidth = round(popper.width);

	  var isVertical = ['left', 'right'].indexOf(data.placement) !== -1;
	  var isVariation = data.placement.indexOf('-') !== -1;
	  var sameWidthParity = referenceWidth % 2 === popperWidth % 2;
	  var bothOddWidth = referenceWidth % 2 === 1 && popperWidth % 2 === 1;

	  var horizontalToInteger = !shouldRound ? noRound : isVertical || isVariation || sameWidthParity ? round : floor;
	  var verticalToInteger = !shouldRound ? noRound : round;

	  return {
	    left: horizontalToInteger(bothOddWidth && !isVariation && shouldRound ? popper.left - 1 : popper.left),
	    top: verticalToInteger(popper.top),
	    bottom: verticalToInteger(popper.bottom),
	    right: horizontalToInteger(popper.right)
	  };
	}

	var isFirefox = isBrowser && /Firefox/i.test(navigator.userAgent);

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function computeStyle(data, options) {
	  var x = options.x,
	      y = options.y;
	  var popper = data.offsets.popper;

	  // Remove this legacy support in Popper.js v2

	  var legacyGpuAccelerationOption = find$1(data.instance.modifiers, function (modifier) {
	    return modifier.name === 'applyStyle';
	  }).gpuAcceleration;
	  if (legacyGpuAccelerationOption !== undefined) {
	    console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
	  }
	  var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

	  var offsetParent = getOffsetParent(data.instance.popper);
	  var offsetParentRect = getBoundingClientRect(offsetParent);

	  // Styles
	  var styles = {
	    position: popper.position
	  };

	  var offsets = getRoundedOffsets(data, window.devicePixelRatio < 2 || !isFirefox);

	  var sideA = x === 'bottom' ? 'top' : 'bottom';
	  var sideB = y === 'right' ? 'left' : 'right';

	  // if gpuAcceleration is set to `true` and transform is supported,
	  //  we use `translate3d` to apply the position to the popper we
	  // automatically use the supported prefixed version if needed
	  var prefixedProperty = getSupportedPropertyName('transform');

	  // now, let's make a step back and look at this code closely (wtf?)
	  // If the content of the popper grows once it's been positioned, it
	  // may happen that the popper gets misplaced because of the new content
	  // overflowing its reference element
	  // To avoid this problem, we provide two options (x and y), which allow
	  // the consumer to define the offset origin.
	  // If we position a popper on top of a reference element, we can set
	  // `x` to `top` to make the popper grow towards its top instead of
	  // its bottom.
	  var left = void 0,
	      top = void 0;
	  if (sideA === 'bottom') {
	    // when offsetParent is <html> the positioning is relative to the bottom of the screen (excluding the scrollbar)
	    // and not the bottom of the html element
	    if (offsetParent.nodeName === 'HTML') {
	      top = -offsetParent.clientHeight + offsets.bottom;
	    } else {
	      top = -offsetParentRect.height + offsets.bottom;
	    }
	  } else {
	    top = offsets.top;
	  }
	  if (sideB === 'right') {
	    if (offsetParent.nodeName === 'HTML') {
	      left = -offsetParent.clientWidth + offsets.right;
	    } else {
	      left = -offsetParentRect.width + offsets.right;
	    }
	  } else {
	    left = offsets.left;
	  }
	  if (gpuAcceleration && prefixedProperty) {
	    styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
	    styles[sideA] = 0;
	    styles[sideB] = 0;
	    styles.willChange = 'transform';
	  } else {
	    // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
	    var invertTop = sideA === 'bottom' ? -1 : 1;
	    var invertLeft = sideB === 'right' ? -1 : 1;
	    styles[sideA] = top * invertTop;
	    styles[sideB] = left * invertLeft;
	    styles.willChange = sideA + ', ' + sideB;
	  }

	  // Attributes
	  var attributes = {
	    'x-placement': data.placement
	  };

	  // Update `data` attributes, styles and arrowStyles
	  data.attributes = _extends({}, attributes, data.attributes);
	  data.styles = _extends({}, styles, data.styles);
	  data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

	  return data;
	}

	/**
	 * Helper used to know if the given modifier depends from another one.<br />
	 * It checks if the needed modifier is listed and enabled.
	 * @method
	 * @memberof Popper.Utils
	 * @param {Array} modifiers - list of modifiers
	 * @param {String} requestingName - name of requesting modifier
	 * @param {String} requestedName - name of requested modifier
	 * @returns {Boolean}
	 */
	function isModifierRequired(modifiers, requestingName, requestedName) {
	  var requesting = find$1(modifiers, function (_ref) {
	    var name = _ref.name;
	    return name === requestingName;
	  });

	  var isRequired = !!requesting && modifiers.some(function (modifier) {
	    return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
	  });

	  if (!isRequired) {
	    var _requesting = '`' + requestingName + '`';
	    var requested = '`' + requestedName + '`';
	    console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
	  }
	  return isRequired;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function arrow(data, options) {
	  var _data$offsets$arrow;

	  // arrow depends on keepTogether in order to work
	  if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
	    return data;
	  }

	  var arrowElement = options.element;

	  // if arrowElement is a string, suppose it's a CSS selector
	  if (typeof arrowElement === 'string') {
	    arrowElement = data.instance.popper.querySelector(arrowElement);

	    // if arrowElement is not found, don't run the modifier
	    if (!arrowElement) {
	      return data;
	    }
	  } else {
	    // if the arrowElement isn't a query selector we must check that the
	    // provided DOM node is child of its popper node
	    if (!data.instance.popper.contains(arrowElement)) {
	      console.warn('WARNING: `arrow.element` must be child of its popper element!');
	      return data;
	    }
	  }

	  var placement = data.placement.split('-')[0];
	  var _data$offsets = data.offsets,
	      popper = _data$offsets.popper,
	      reference = _data$offsets.reference;

	  var isVertical = ['left', 'right'].indexOf(placement) !== -1;

	  var len = isVertical ? 'height' : 'width';
	  var sideCapitalized = isVertical ? 'Top' : 'Left';
	  var side = sideCapitalized.toLowerCase();
	  var altSide = isVertical ? 'left' : 'top';
	  var opSide = isVertical ? 'bottom' : 'right';
	  var arrowElementSize = getOuterSizes(arrowElement)[len];

	  //
	  // extends keepTogether behavior making sure the popper and its
	  // reference have enough pixels in conjunction
	  //

	  // top/left side
	  if (reference[opSide] - arrowElementSize < popper[side]) {
	    data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
	  }
	  // bottom/right side
	  if (reference[side] + arrowElementSize > popper[opSide]) {
	    data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
	  }
	  data.offsets.popper = getClientRect(data.offsets.popper);

	  // compute center of the popper
	  var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

	  // Compute the sideValue using the updated popper offsets
	  // take popper margin in account because we don't have this info available
	  var css = getStyleComputedProperty(data.instance.popper);
	  var popperMarginSide = parseFloat(css['margin' + sideCapitalized]);
	  var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width']);
	  var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

	  // prevent arrowElement from being placed not contiguously to its popper
	  sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

	  data.arrowElement = arrowElement;
	  data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty$8(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty$8(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

	  return data;
	}

	/**
	 * Get the opposite placement variation of the given one
	 * @method
	 * @memberof Popper.Utils
	 * @argument {String} placement variation
	 * @returns {String} flipped placement variation
	 */
	function getOppositeVariation(variation) {
	  if (variation === 'end') {
	    return 'start';
	  } else if (variation === 'start') {
	    return 'end';
	  }
	  return variation;
	}

	/**
	 * List of accepted placements to use as values of the `placement` option.<br />
	 * Valid placements are:
	 * - `auto`
	 * - `top`
	 * - `right`
	 * - `bottom`
	 * - `left`
	 *
	 * Each placement can have a variation from this list:
	 * - `-start`
	 * - `-end`
	 *
	 * Variations are interpreted easily if you think of them as the left to right
	 * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
	 * is right.<br />
	 * Vertically (`left` and `right`), `start` is top and `end` is bottom.
	 *
	 * Some valid examples are:
	 * - `top-end` (on top of reference, right aligned)
	 * - `right-start` (on right of reference, top aligned)
	 * - `bottom` (on bottom, centered)
	 * - `auto-end` (on the side with more space available, alignment depends by placement)
	 *
	 * @static
	 * @type {Array}
	 * @enum {String}
	 * @readonly
	 * @method placements
	 * @memberof Popper
	 */
	var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

	// Get rid of `auto` `auto-start` and `auto-end`
	var validPlacements = placements.slice(3);

	/**
	 * Given an initial placement, returns all the subsequent placements
	 * clockwise (or counter-clockwise).
	 *
	 * @method
	 * @memberof Popper.Utils
	 * @argument {String} placement - A valid placement (it accepts variations)
	 * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
	 * @returns {Array} placements including their variations
	 */
	function clockwise(placement) {
	  var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var index = validPlacements.indexOf(placement);
	  var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
	  return counter ? arr.reverse() : arr;
	}

	var BEHAVIORS = {
	  FLIP: 'flip',
	  CLOCKWISE: 'clockwise',
	  COUNTERCLOCKWISE: 'counterclockwise'
	};

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function flip(data, options) {
	  // if `inner` modifier is enabled, we can't use the `flip` modifier
	  if (isModifierEnabled(data.instance.modifiers, 'inner')) {
	    return data;
	  }

	  if (data.flipped && data.placement === data.originalPlacement) {
	    // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
	    return data;
	  }

	  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement, data.positionFixed);

	  var placement = data.placement.split('-')[0];
	  var placementOpposite = getOppositePlacement(placement);
	  var variation = data.placement.split('-')[1] || '';

	  var flipOrder = [];

	  switch (options.behavior) {
	    case BEHAVIORS.FLIP:
	      flipOrder = [placement, placementOpposite];
	      break;
	    case BEHAVIORS.CLOCKWISE:
	      flipOrder = clockwise(placement);
	      break;
	    case BEHAVIORS.COUNTERCLOCKWISE:
	      flipOrder = clockwise(placement, true);
	      break;
	    default:
	      flipOrder = options.behavior;
	  }

	  flipOrder.forEach(function (step, index) {
	    if (placement !== step || flipOrder.length === index + 1) {
	      return data;
	    }

	    placement = data.placement.split('-')[0];
	    placementOpposite = getOppositePlacement(placement);

	    var popperOffsets = data.offsets.popper;
	    var refOffsets = data.offsets.reference;

	    // using floor because the reference offsets may contain decimals we are not going to consider here
	    var floor = Math.floor;
	    var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

	    var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
	    var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
	    var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
	    var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

	    var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

	    // flip the variation if required
	    var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;

	    // flips variation if reference element overflows boundaries
	    var flippedVariationByRef = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

	    // flips variation if popper content overflows boundaries
	    var flippedVariationByContent = !!options.flipVariationsByContent && (isVertical && variation === 'start' && overflowsRight || isVertical && variation === 'end' && overflowsLeft || !isVertical && variation === 'start' && overflowsBottom || !isVertical && variation === 'end' && overflowsTop);

	    var flippedVariation = flippedVariationByRef || flippedVariationByContent;

	    if (overlapsRef || overflowsBoundaries || flippedVariation) {
	      // this boolean to detect any flip loop
	      data.flipped = true;

	      if (overlapsRef || overflowsBoundaries) {
	        placement = flipOrder[index + 1];
	      }

	      if (flippedVariation) {
	        variation = getOppositeVariation(variation);
	      }

	      data.placement = placement + (variation ? '-' + variation : '');

	      // this object contains `position`, we want to preserve it along with
	      // any additional property we may add in the future
	      data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

	      data = runModifiers(data.instance.modifiers, data, 'flip');
	    }
	  });
	  return data;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function keepTogether(data) {
	  var _data$offsets = data.offsets,
	      popper = _data$offsets.popper,
	      reference = _data$offsets.reference;

	  var placement = data.placement.split('-')[0];
	  var floor = Math.floor;
	  var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
	  var side = isVertical ? 'right' : 'bottom';
	  var opSide = isVertical ? 'left' : 'top';
	  var measurement = isVertical ? 'width' : 'height';

	  if (popper[side] < floor(reference[opSide])) {
	    data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
	  }
	  if (popper[opSide] > floor(reference[side])) {
	    data.offsets.popper[opSide] = floor(reference[side]);
	  }

	  return data;
	}

	/**
	 * Converts a string containing value + unit into a px value number
	 * @function
	 * @memberof {modifiers~offset}
	 * @private
	 * @argument {String} str - Value + unit string
	 * @argument {String} measurement - `height` or `width`
	 * @argument {Object} popperOffsets
	 * @argument {Object} referenceOffsets
	 * @returns {Number|String}
	 * Value in pixels, or original string if no values were extracted
	 */
	function toValue(str, measurement, popperOffsets, referenceOffsets) {
	  // separate value from unit
	  var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
	  var value = +split[1];
	  var unit = split[2];

	  // If it's not a number it's an operator, I guess
	  if (!value) {
	    return str;
	  }

	  if (unit.indexOf('%') === 0) {
	    var element = void 0;
	    switch (unit) {
	      case '%p':
	        element = popperOffsets;
	        break;
	      case '%':
	      case '%r':
	      default:
	        element = referenceOffsets;
	    }

	    var rect = getClientRect(element);
	    return rect[measurement] / 100 * value;
	  } else if (unit === 'vh' || unit === 'vw') {
	    // if is a vh or vw, we calculate the size based on the viewport
	    var size = void 0;
	    if (unit === 'vh') {
	      size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	    } else {
	      size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	    }
	    return size / 100 * value;
	  } else {
	    // if is an explicit pixel unit, we get rid of the unit and keep the value
	    // if is an implicit unit, it's px, and we return just the value
	    return value;
	  }
	}

	/**
	 * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
	 * @function
	 * @memberof {modifiers~offset}
	 * @private
	 * @argument {String} offset
	 * @argument {Object} popperOffsets
	 * @argument {Object} referenceOffsets
	 * @argument {String} basePlacement
	 * @returns {Array} a two cells array with x and y offsets in numbers
	 */
	function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
	  var offsets = [0, 0];

	  // Use height if placement is left or right and index is 0 otherwise use width
	  // in this way the first offset will use an axis and the second one
	  // will use the other one
	  var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

	  // Split the offset string to obtain a list of values and operands
	  // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
	  var fragments = offset.split(/(\+|\-)/).map(function (frag) {
	    return frag.trim();
	  });

	  // Detect if the offset string contains a pair of values or a single one
	  // they could be separated by comma or space
	  var divider = fragments.indexOf(find$1(fragments, function (frag) {
	    return frag.search(/,|\s/) !== -1;
	  }));

	  if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
	    console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
	  }

	  // If divider is found, we divide the list of values and operands to divide
	  // them by ofset X and Y.
	  var splitRegex = /\s*,\s*|\s+/;
	  var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

	  // Convert the values with units to absolute pixels to allow our computations
	  ops = ops.map(function (op, index) {
	    // Most of the units rely on the orientation of the popper
	    var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
	    var mergeWithPrevious = false;
	    return op
	    // This aggregates any `+` or `-` sign that aren't considered operators
	    // e.g.: 10 + +5 => [10, +, +5]
	    .reduce(function (a, b) {
	      if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
	        a[a.length - 1] = b;
	        mergeWithPrevious = true;
	        return a;
	      } else if (mergeWithPrevious) {
	        a[a.length - 1] += b;
	        mergeWithPrevious = false;
	        return a;
	      } else {
	        return a.concat(b);
	      }
	    }, [])
	    // Here we convert the string values into number values (in px)
	    .map(function (str) {
	      return toValue(str, measurement, popperOffsets, referenceOffsets);
	    });
	  });

	  // Loop trough the offsets arrays and execute the operations
	  ops.forEach(function (op, index) {
	    op.forEach(function (frag, index2) {
	      if (isNumeric(frag)) {
	        offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
	      }
	    });
	  });
	  return offsets;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @argument {Number|String} options.offset=0
	 * The offset value as described in the modifier description
	 * @returns {Object} The data object, properly modified
	 */
	function offset(data, _ref) {
	  var offset = _ref.offset;
	  var placement = data.placement,
	      _data$offsets = data.offsets,
	      popper = _data$offsets.popper,
	      reference = _data$offsets.reference;

	  var basePlacement = placement.split('-')[0];

	  var offsets = void 0;
	  if (isNumeric(+offset)) {
	    offsets = [+offset, 0];
	  } else {
	    offsets = parseOffset(offset, popper, reference, basePlacement);
	  }

	  if (basePlacement === 'left') {
	    popper.top += offsets[0];
	    popper.left -= offsets[1];
	  } else if (basePlacement === 'right') {
	    popper.top += offsets[0];
	    popper.left += offsets[1];
	  } else if (basePlacement === 'top') {
	    popper.left += offsets[0];
	    popper.top -= offsets[1];
	  } else if (basePlacement === 'bottom') {
	    popper.left += offsets[0];
	    popper.top += offsets[1];
	  }

	  data.popper = popper;
	  return data;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function preventOverflow(data, options) {
	  var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

	  // If offsetParent is the reference element, we really want to
	  // go one step up and use the next offsetParent as reference to
	  // avoid to make this modifier completely useless and look like broken
	  if (data.instance.reference === boundariesElement) {
	    boundariesElement = getOffsetParent(boundariesElement);
	  }

	  // NOTE: DOM access here
	  // resets the popper's position so that the document size can be calculated excluding
	  // the size of the popper element itself
	  var transformProp = getSupportedPropertyName('transform');
	  var popperStyles = data.instance.popper.style; // assignment to help minification
	  var top = popperStyles.top,
	      left = popperStyles.left,
	      transform = popperStyles[transformProp];

	  popperStyles.top = '';
	  popperStyles.left = '';
	  popperStyles[transformProp] = '';

	  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement, data.positionFixed);

	  // NOTE: DOM access here
	  // restores the original style properties after the offsets have been computed
	  popperStyles.top = top;
	  popperStyles.left = left;
	  popperStyles[transformProp] = transform;

	  options.boundaries = boundaries;

	  var order = options.priority;
	  var popper = data.offsets.popper;

	  var check = {
	    primary: function primary(placement) {
	      var value = popper[placement];
	      if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
	        value = Math.max(popper[placement], boundaries[placement]);
	      }
	      return defineProperty$8({}, placement, value);
	    },
	    secondary: function secondary(placement) {
	      var mainSide = placement === 'right' ? 'left' : 'top';
	      var value = popper[mainSide];
	      if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
	        value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
	      }
	      return defineProperty$8({}, mainSide, value);
	    }
	  };

	  order.forEach(function (placement) {
	    var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
	    popper = _extends({}, popper, check[side](placement));
	  });

	  data.offsets.popper = popper;

	  return data;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function shift(data) {
	  var placement = data.placement;
	  var basePlacement = placement.split('-')[0];
	  var shiftvariation = placement.split('-')[1];

	  // if shift shiftvariation is specified, run the modifier
	  if (shiftvariation) {
	    var _data$offsets = data.offsets,
	        reference = _data$offsets.reference,
	        popper = _data$offsets.popper;

	    var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
	    var side = isVertical ? 'left' : 'top';
	    var measurement = isVertical ? 'width' : 'height';

	    var shiftOffsets = {
	      start: defineProperty$8({}, side, reference[side]),
	      end: defineProperty$8({}, side, reference[side] + reference[measurement] - popper[measurement])
	    };

	    data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
	  }

	  return data;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by update method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function hide(data) {
	  if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
	    return data;
	  }

	  var refRect = data.offsets.reference;
	  var bound = find$1(data.instance.modifiers, function (modifier) {
	    return modifier.name === 'preventOverflow';
	  }).boundaries;

	  if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
	    // Avoid unnecessary DOM access if visibility hasn't changed
	    if (data.hide === true) {
	      return data;
	    }

	    data.hide = true;
	    data.attributes['x-out-of-boundaries'] = '';
	  } else {
	    // Avoid unnecessary DOM access if visibility hasn't changed
	    if (data.hide === false) {
	      return data;
	    }

	    data.hide = false;
	    data.attributes['x-out-of-boundaries'] = false;
	  }

	  return data;
	}

	/**
	 * @function
	 * @memberof Modifiers
	 * @argument {Object} data - The data object generated by `update` method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {Object} The data object, properly modified
	 */
	function inner(data) {
	  var placement = data.placement;
	  var basePlacement = placement.split('-')[0];
	  var _data$offsets = data.offsets,
	      popper = _data$offsets.popper,
	      reference = _data$offsets.reference;

	  var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

	  var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

	  popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

	  data.placement = getOppositePlacement(placement);
	  data.offsets.popper = getClientRect(popper);

	  return data;
	}

	/**
	 * Modifier function, each modifier can have a function of this type assigned
	 * to its `fn` property.<br />
	 * These functions will be called on each update, this means that you must
	 * make sure they are performant enough to avoid performance bottlenecks.
	 *
	 * @function ModifierFn
	 * @argument {dataObject} data - The data object generated by `update` method
	 * @argument {Object} options - Modifiers configuration and options
	 * @returns {dataObject} The data object, properly modified
	 */

	/**
	 * Modifiers are plugins used to alter the behavior of your poppers.<br />
	 * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
	 * needed by the library.
	 *
	 * Usually you don't want to override the `order`, `fn` and `onLoad` props.
	 * All the other properties are configurations that could be tweaked.
	 * @namespace modifiers
	 */
	var modifiers = {
	  /**
	   * Modifier used to shift the popper on the start or end of its reference
	   * element.<br />
	   * It will read the variation of the `placement` property.<br />
	   * It can be one either `-end` or `-start`.
	   * @memberof modifiers
	   * @inner
	   */
	  shift: {
	    /** @prop {number} order=100 - Index used to define the order of execution */
	    order: 100,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: shift
	  },

	  /**
	   * The `offset` modifier can shift your popper on both its axis.
	   *
	   * It accepts the following units:
	   * - `px` or unit-less, interpreted as pixels
	   * - `%` or `%r`, percentage relative to the length of the reference element
	   * - `%p`, percentage relative to the length of the popper element
	   * - `vw`, CSS viewport width unit
	   * - `vh`, CSS viewport height unit
	   *
	   * For length is intended the main axis relative to the placement of the popper.<br />
	   * This means that if the placement is `top` or `bottom`, the length will be the
	   * `width`. In case of `left` or `right`, it will be the `height`.
	   *
	   * You can provide a single value (as `Number` or `String`), or a pair of values
	   * as `String` divided by a comma or one (or more) white spaces.<br />
	   * The latter is a deprecated method because it leads to confusion and will be
	   * removed in v2.<br />
	   * Additionally, it accepts additions and subtractions between different units.
	   * Note that multiplications and divisions aren't supported.
	   *
	   * Valid examples are:
	   * ```
	   * 10
	   * '10%'
	   * '10, 10'
	   * '10%, 10'
	   * '10 + 10%'
	   * '10 - 5vh + 3%'
	   * '-10px + 5vh, 5px - 6%'
	   * ```
	   * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
	   * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
	   * > You can read more on this at this [issue](https://github.com/FezVrasta/popper.js/issues/373).
	   *
	   * @memberof modifiers
	   * @inner
	   */
	  offset: {
	    /** @prop {number} order=200 - Index used to define the order of execution */
	    order: 200,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: offset,
	    /** @prop {Number|String} offset=0
	     * The offset value as described in the modifier description
	     */
	    offset: 0
	  },

	  /**
	   * Modifier used to prevent the popper from being positioned outside the boundary.
	   *
	   * A scenario exists where the reference itself is not within the boundaries.<br />
	   * We can say it has "escaped the boundaries" — or just "escaped".<br />
	   * In this case we need to decide whether the popper should either:
	   *
	   * - detach from the reference and remain "trapped" in the boundaries, or
	   * - if it should ignore the boundary and "escape with its reference"
	   *
	   * When `escapeWithReference` is set to`true` and reference is completely
	   * outside its boundaries, the popper will overflow (or completely leave)
	   * the boundaries in order to remain attached to the edge of the reference.
	   *
	   * @memberof modifiers
	   * @inner
	   */
	  preventOverflow: {
	    /** @prop {number} order=300 - Index used to define the order of execution */
	    order: 300,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: preventOverflow,
	    /**
	     * @prop {Array} [priority=['left','right','top','bottom']]
	     * Popper will try to prevent overflow following these priorities by default,
	     * then, it could overflow on the left and on top of the `boundariesElement`
	     */
	    priority: ['left', 'right', 'top', 'bottom'],
	    /**
	     * @prop {number} padding=5
	     * Amount of pixel used to define a minimum distance between the boundaries
	     * and the popper. This makes sure the popper always has a little padding
	     * between the edges of its container
	     */
	    padding: 5,
	    /**
	     * @prop {String|HTMLElement} boundariesElement='scrollParent'
	     * Boundaries used by the modifier. Can be `scrollParent`, `window`,
	     * `viewport` or any DOM element.
	     */
	    boundariesElement: 'scrollParent'
	  },

	  /**
	   * Modifier used to make sure the reference and its popper stay near each other
	   * without leaving any gap between the two. Especially useful when the arrow is
	   * enabled and you want to ensure that it points to its reference element.
	   * It cares only about the first axis. You can still have poppers with margin
	   * between the popper and its reference element.
	   * @memberof modifiers
	   * @inner
	   */
	  keepTogether: {
	    /** @prop {number} order=400 - Index used to define the order of execution */
	    order: 400,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: keepTogether
	  },

	  /**
	   * This modifier is used to move the `arrowElement` of the popper to make
	   * sure it is positioned between the reference element and its popper element.
	   * It will read the outer size of the `arrowElement` node to detect how many
	   * pixels of conjunction are needed.
	   *
	   * It has no effect if no `arrowElement` is provided.
	   * @memberof modifiers
	   * @inner
	   */
	  arrow: {
	    /** @prop {number} order=500 - Index used to define the order of execution */
	    order: 500,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: arrow,
	    /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
	    element: '[x-arrow]'
	  },

	  /**
	   * Modifier used to flip the popper's placement when it starts to overlap its
	   * reference element.
	   *
	   * Requires the `preventOverflow` modifier before it in order to work.
	   *
	   * **NOTE:** this modifier will interrupt the current update cycle and will
	   * restart it if it detects the need to flip the placement.
	   * @memberof modifiers
	   * @inner
	   */
	  flip: {
	    /** @prop {number} order=600 - Index used to define the order of execution */
	    order: 600,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: flip,
	    /**
	     * @prop {String|Array} behavior='flip'
	     * The behavior used to change the popper's placement. It can be one of
	     * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
	     * placements (with optional variations)
	     */
	    behavior: 'flip',
	    /**
	     * @prop {number} padding=5
	     * The popper will flip if it hits the edges of the `boundariesElement`
	     */
	    padding: 5,
	    /**
	     * @prop {String|HTMLElement} boundariesElement='viewport'
	     * The element which will define the boundaries of the popper position.
	     * The popper will never be placed outside of the defined boundaries
	     * (except if `keepTogether` is enabled)
	     */
	    boundariesElement: 'viewport',
	    /**
	     * @prop {Boolean} flipVariations=false
	     * The popper will switch placement variation between `-start` and `-end` when
	     * the reference element overlaps its boundaries.
	     *
	     * The original placement should have a set variation.
	     */
	    flipVariations: false,
	    /**
	     * @prop {Boolean} flipVariationsByContent=false
	     * The popper will switch placement variation between `-start` and `-end` when
	     * the popper element overlaps its reference boundaries.
	     *
	     * The original placement should have a set variation.
	     */
	    flipVariationsByContent: false
	  },

	  /**
	   * Modifier used to make the popper flow toward the inner of the reference element.
	   * By default, when this modifier is disabled, the popper will be placed outside
	   * the reference element.
	   * @memberof modifiers
	   * @inner
	   */
	  inner: {
	    /** @prop {number} order=700 - Index used to define the order of execution */
	    order: 700,
	    /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
	    enabled: false,
	    /** @prop {ModifierFn} */
	    fn: inner
	  },

	  /**
	   * Modifier used to hide the popper when its reference element is outside of the
	   * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
	   * be used to hide with a CSS selector the popper when its reference is
	   * out of boundaries.
	   *
	   * Requires the `preventOverflow` modifier before it in order to work.
	   * @memberof modifiers
	   * @inner
	   */
	  hide: {
	    /** @prop {number} order=800 - Index used to define the order of execution */
	    order: 800,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: hide
	  },

	  /**
	   * Computes the style that will be applied to the popper element to gets
	   * properly positioned.
	   *
	   * Note that this modifier will not touch the DOM, it just prepares the styles
	   * so that `applyStyle` modifier can apply it. This separation is useful
	   * in case you need to replace `applyStyle` with a custom implementation.
	   *
	   * This modifier has `850` as `order` value to maintain backward compatibility
	   * with previous versions of Popper.js. Expect the modifiers ordering method
	   * to change in future major versions of the library.
	   *
	   * @memberof modifiers
	   * @inner
	   */
	  computeStyle: {
	    /** @prop {number} order=850 - Index used to define the order of execution */
	    order: 850,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: computeStyle,
	    /**
	     * @prop {Boolean} gpuAcceleration=true
	     * If true, it uses the CSS 3D transformation to position the popper.
	     * Otherwise, it will use the `top` and `left` properties
	     */
	    gpuAcceleration: true,
	    /**
	     * @prop {string} [x='bottom']
	     * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
	     * Change this if your popper should grow in a direction different from `bottom`
	     */
	    x: 'bottom',
	    /**
	     * @prop {string} [x='left']
	     * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
	     * Change this if your popper should grow in a direction different from `right`
	     */
	    y: 'right'
	  },

	  /**
	   * Applies the computed styles to the popper element.
	   *
	   * All the DOM manipulations are limited to this modifier. This is useful in case
	   * you want to integrate Popper.js inside a framework or view library and you
	   * want to delegate all the DOM manipulations to it.
	   *
	   * Note that if you disable this modifier, you must make sure the popper element
	   * has its position set to `absolute` before Popper.js can do its work!
	   *
	   * Just disable this modifier and define your own to achieve the desired effect.
	   *
	   * @memberof modifiers
	   * @inner
	   */
	  applyStyle: {
	    /** @prop {number} order=900 - Index used to define the order of execution */
	    order: 900,
	    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
	    enabled: true,
	    /** @prop {ModifierFn} */
	    fn: applyStyle,
	    /** @prop {Function} */
	    onLoad: applyStyleOnLoad,
	    /**
	     * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
	     * @prop {Boolean} gpuAcceleration=true
	     * If true, it uses the CSS 3D transformation to position the popper.
	     * Otherwise, it will use the `top` and `left` properties
	     */
	    gpuAcceleration: undefined
	  }
	};

	/**
	 * The `dataObject` is an object containing all the information used by Popper.js.
	 * This object is passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
	 * @name dataObject
	 * @property {Object} data.instance The Popper.js instance
	 * @property {String} data.placement Placement applied to popper
	 * @property {String} data.originalPlacement Placement originally defined on init
	 * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
	 * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper
	 * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
	 * @property {Object} data.styles Any CSS property defined here will be applied to the popper. It expects the JavaScript nomenclature (eg. `marginBottom`)
	 * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow. It expects the JavaScript nomenclature (eg. `marginBottom`)
	 * @property {Object} data.boundaries Offsets of the popper boundaries
	 * @property {Object} data.offsets The measurements of popper, reference and arrow elements
	 * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
	 * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
	 * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
	 */

	/**
	 * Default options provided to Popper.js constructor.<br />
	 * These can be overridden using the `options` argument of Popper.js.<br />
	 * To override an option, simply pass an object with the same
	 * structure of the `options` object, as the 3rd argument. For example:
	 * ```
	 * new Popper(ref, pop, {
	 *   modifiers: {
	 *     preventOverflow: { enabled: false }
	 *   }
	 * })
	 * ```
	 * @type {Object}
	 * @static
	 * @memberof Popper
	 */
	var Defaults = {
	  /**
	   * Popper's placement.
	   * @prop {Popper.placements} placement='bottom'
	   */
	  placement: 'bottom',

	  /**
	   * Set this to true if you want popper to position it self in 'fixed' mode
	   * @prop {Boolean} positionFixed=false
	   */
	  positionFixed: false,

	  /**
	   * Whether events (resize, scroll) are initially enabled.
	   * @prop {Boolean} eventsEnabled=true
	   */
	  eventsEnabled: true,

	  /**
	   * Set to true if you want to automatically remove the popper when
	   * you call the `destroy` method.
	   * @prop {Boolean} removeOnDestroy=false
	   */
	  removeOnDestroy: false,

	  /**
	   * Callback called when the popper is created.<br />
	   * By default, it is set to no-op.<br />
	   * Access Popper.js instance with `data.instance`.
	   * @prop {onCreate}
	   */
	  onCreate: function onCreate() {},

	  /**
	   * Callback called when the popper is updated. This callback is not called
	   * on the initialization/creation of the popper, but only on subsequent
	   * updates.<br />
	   * By default, it is set to no-op.<br />
	   * Access Popper.js instance with `data.instance`.
	   * @prop {onUpdate}
	   */
	  onUpdate: function onUpdate() {},

	  /**
	   * List of modifiers used to modify the offsets before they are applied to the popper.
	   * They provide most of the functionalities of Popper.js.
	   * @prop {modifiers}
	   */
	  modifiers: modifiers
	};

	/**
	 * @callback onCreate
	 * @param {dataObject} data
	 */

	/**
	 * @callback onUpdate
	 * @param {dataObject} data
	 */

	// Utils
	// Methods
	var Popper = function () {
	  /**
	   * Creates a new Popper.js instance.
	   * @class Popper
	   * @param {Element|referenceObject} reference - The reference element used to position the popper
	   * @param {Element} popper - The HTML / XML element used as the popper
	   * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
	   * @return {Object} instance - The generated Popper.js instance
	   */
	  function Popper(reference, popper) {
	    var _this = this;

	    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	    classCallCheck(this, Popper);

	    this.scheduleUpdate = function () {
	      return requestAnimationFrame(_this.update);
	    };

	    // make update() debounced, so that it only runs at most once-per-tick
	    this.update = debounce$1(this.update.bind(this));

	    // with {} we create a new object with the options inside it
	    this.options = _extends({}, Popper.Defaults, options);

	    // init state
	    this.state = {
	      isDestroyed: false,
	      isCreated: false,
	      scrollParents: []
	    };

	    // get reference and popper elements (allow jQuery wrappers)
	    this.reference = reference && reference.jquery ? reference[0] : reference;
	    this.popper = popper && popper.jquery ? popper[0] : popper;

	    // Deep merge modifiers options
	    this.options.modifiers = {};
	    Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
	      _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
	    });

	    // Refactoring modifiers' list (Object => Array)
	    this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
	      return _extends({
	        name: name
	      }, _this.options.modifiers[name]);
	    })
	    // sort the modifiers by order
	    .sort(function (a, b) {
	      return a.order - b.order;
	    });

	    // modifiers have the ability to execute arbitrary code when Popper.js get inited
	    // such code is executed in the same order of its modifier
	    // they could add new properties to their options configuration
	    // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
	    this.modifiers.forEach(function (modifierOptions) {
	      if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
	        modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
	      }
	    });

	    // fire the first update to position the popper in the right place
	    this.update();

	    var eventsEnabled = this.options.eventsEnabled;
	    if (eventsEnabled) {
	      // setup event listeners, they will take care of update the position in specific situations
	      this.enableEventListeners();
	    }

	    this.state.eventsEnabled = eventsEnabled;
	  }

	  // We can't use class properties because they don't get listed in the
	  // class prototype and break stuff like Sinon stubs


	  createClass(Popper, [{
	    key: 'update',
	    value: function update$$1() {
	      return update.call(this);
	    }
	  }, {
	    key: 'destroy',
	    value: function destroy$$1() {
	      return destroy.call(this);
	    }
	  }, {
	    key: 'enableEventListeners',
	    value: function enableEventListeners$$1() {
	      return enableEventListeners.call(this);
	    }
	  }, {
	    key: 'disableEventListeners',
	    value: function disableEventListeners$$1() {
	      return disableEventListeners.call(this);
	    }

	    /**
	     * Schedules an update. It will run on the next UI update available.
	     * @method scheduleUpdate
	     * @memberof Popper
	     */


	    /**
	     * Collection of utilities useful when writing custom modifiers.
	     * Starting from version 1.7, this method is available only if you
	     * include `popper-utils.js` before `popper.js`.
	     *
	     * **DEPRECATION**: This way to access PopperUtils is deprecated
	     * and will be removed in v2! Use the PopperUtils module directly instead.
	     * Due to the high instability of the methods contained in Utils, we can't
	     * guarantee them to follow semver. Use them at your own risk!
	     * @static
	     * @private
	     * @type {Object}
	     * @deprecated since version 1.8
	     * @member Utils
	     * @memberof Popper
	     */

	  }]);
	  return Popper;
	}();

	/**
	 * The `referenceObject` is an object that provides an interface compatible with Popper.js
	 * and lets you use it as replacement of a real DOM node.<br />
	 * You can use this method to position a popper relatively to a set of coordinates
	 * in case you don't have a DOM node to use as reference.
	 *
	 * ```
	 * new Popper(referenceObject, popperNode);
	 * ```
	 *
	 * NB: This feature isn't supported in Internet Explorer 10.
	 * @name referenceObject
	 * @property {Function} data.getBoundingClientRect
	 * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
	 * @property {number} data.clientWidth
	 * An ES6 getter that will return the width of the virtual reference element.
	 * @property {number} data.clientHeight
	 * An ES6 getter that will return the height of the virtual reference element.
	 */


	Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
	Popper.placements = placements;
	Popper.Defaults = Defaults;

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$4 = 'tooltip';
	var VERSION$2 = '4.4.1';
	var DATA_KEY$2 = 'bs.tooltip';
	var EVENT_KEY$2 = "." + DATA_KEY$2;
	var JQUERY_NO_CONFLICT$2 = $.fn[NAME$4];
	var CLASS_PREFIX = 'bs-tooltip';
	var BSCLS_PREFIX_REGEX = new RegExp("(^|\\s)" + CLASS_PREFIX + "\\S+", 'g');
	var DISALLOWED_ATTRIBUTES = ['sanitize', 'whiteList', 'sanitizeFn'];
	var DefaultType$2 = {
	  animation: 'boolean',
	  template: 'string',
	  title: '(string|element|function)',
	  trigger: 'string',
	  delay: '(number|object)',
	  html: 'boolean',
	  selector: '(string|boolean)',
	  placement: '(string|function)',
	  offset: '(number|string|function)',
	  container: '(string|element|boolean)',
	  fallbackPlacement: '(string|array)',
	  boundary: '(string|element)',
	  sanitize: 'boolean',
	  sanitizeFn: '(null|function)',
	  whiteList: 'object',
	  popperConfig: '(null|object)'
	};
	var AttachmentMap = {
	  AUTO: 'auto',
	  TOP: 'top',
	  RIGHT: 'right',
	  BOTTOM: 'bottom',
	  LEFT: 'left'
	};
	var Default$2 = {
	  animation: true,
	  template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div></div>',
	  trigger: 'hover focus',
	  title: '',
	  delay: 0,
	  html: false,
	  selector: false,
	  placement: 'top',
	  offset: 0,
	  container: false,
	  fallbackPlacement: 'flip',
	  boundary: 'scrollParent',
	  sanitize: true,
	  sanitizeFn: null,
	  whiteList: DefaultWhitelist,
	  popperConfig: null
	};
	var HoverState = {
	  SHOW: 'show',
	  OUT: 'out'
	};
	var Event$8 = {
	  HIDE: "hide" + EVENT_KEY$2,
	  HIDDEN: "hidden" + EVENT_KEY$2,
	  SHOW: "show" + EVENT_KEY$2,
	  SHOWN: "shown" + EVENT_KEY$2,
	  INSERTED: "inserted" + EVENT_KEY$2,
	  CLICK: "click" + EVENT_KEY$2,
	  FOCUSIN: "focusin" + EVENT_KEY$2,
	  FOCUSOUT: "focusout" + EVENT_KEY$2,
	  MOUSEENTER: "mouseenter" + EVENT_KEY$2,
	  MOUSELEAVE: "mouseleave" + EVENT_KEY$2
	};
	var ClassName$8 = {
	  FADE: 'fade',
	  SHOW: 'show'
	};
	var Selector$9 = {
	  TOOLTIP: '.tooltip',
	  TOOLTIP_INNER: '.tooltip-inner',
	  ARROW: '.arrow'
	};
	var Trigger = {
	  HOVER: 'hover',
	  FOCUS: 'focus',
	  CLICK: 'click',
	  MANUAL: 'manual'
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Tooltip = /*#__PURE__*/function () {
	  function Tooltip(element, config) {
	    if (typeof Popper === 'undefined') {
	      throw new TypeError('Bootstrap\'s tooltips require Popper.js (https://popper.js.org/)');
	    } // private


	    this._isEnabled = true;
	    this._timeout = 0;
	    this._hoverState = '';
	    this._activeTrigger = {};
	    this._popper = null; // Protected

	    this.element = element;
	    this.config = this._getConfig(config);
	    this.tip = null;

	    this._setListeners();
	  } // Getters


	  var _proto = Tooltip.prototype;

	  // Public
	  _proto.enable = function enable() {
	    this._isEnabled = true;
	  };

	  _proto.disable = function disable() {
	    this._isEnabled = false;
	  };

	  _proto.toggleEnabled = function toggleEnabled() {
	    this._isEnabled = !this._isEnabled;
	  };

	  _proto.toggle = function toggle(event) {
	    if (!this._isEnabled) {
	      return;
	    }

	    if (event) {
	      var dataKey = this.constructor.DATA_KEY;
	      var context = $(event.currentTarget).data(dataKey);

	      if (!context) {
	        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
	        $(event.currentTarget).data(dataKey, context);
	      }

	      context._activeTrigger.click = !context._activeTrigger.click;

	      if (context._isWithActiveTrigger()) {
	        context._enter(null, context);
	      } else {
	        context._leave(null, context);
	      }
	    } else {
	      if ($(this.getTipElement()).hasClass(ClassName$8.SHOW)) {
	        this._leave(null, this);

	        return;
	      }

	      this._enter(null, this);
	    }
	  };

	  _proto.dispose = function dispose() {
	    clearTimeout(this._timeout);
	    $.removeData(this.element, this.constructor.DATA_KEY);
	    $(this.element).off(this.constructor.EVENT_KEY);
	    $(this.element).closest('.modal').off('hide.bs.modal', this._hideModalHandler);

	    if (this.tip) {
	      $(this.tip).remove();
	    }

	    this._isEnabled = null;
	    this._timeout = null;
	    this._hoverState = null;
	    this._activeTrigger = null;

	    if (this._popper) {
	      this._popper.destroy();
	    }

	    this._popper = null;
	    this.element = null;
	    this.config = null;
	    this.tip = null;
	  };

	  _proto.show = function show() {
	    var _this = this;

	    if ($(this.element).css('display') === 'none') {
	      throw new Error('Please use show on visible elements');
	    }

	    var showEvent = $.Event(this.constructor.Event.SHOW);

	    if (this.isWithContent() && this._isEnabled) {
	      $(this.element).trigger(showEvent);
	      var shadowRoot = Util.findShadowRoot(this.element);
	      var isInTheDom = $.contains(shadowRoot !== null ? shadowRoot : this.element.ownerDocument.documentElement, this.element);

	      if (showEvent.isDefaultPrevented() || !isInTheDom) {
	        return;
	      }

	      var tip = this.getTipElement();
	      var tipId = Util.getUID(this.constructor.NAME);
	      tip.setAttribute('id', tipId);
	      this.element.setAttribute('aria-describedby', tipId);
	      this.setContent();

	      if (this.config.animation) {
	        $(tip).addClass(ClassName$8.FADE);
	      }

	      var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

	      var attachment = this._getAttachment(placement);

	      this.addAttachmentClass(attachment);

	      var container = this._getContainer();

	      $(tip).data(this.constructor.DATA_KEY, this);

	      if (!$.contains(this.element.ownerDocument.documentElement, this.tip)) {
	        $(tip).appendTo(container);
	      }

	      $(this.element).trigger(this.constructor.Event.INSERTED);
	      this._popper = new Popper(this.element, tip, this._getPopperConfig(attachment));
	      $(tip).addClass(ClassName$8.SHOW); // If this is a touch-enabled device we add extra
	      // empty mouseover listeners to the body's immediate children;
	      // only needed because of broken event delegation on iOS
	      // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html

	      if ('ontouchstart' in document.documentElement) {
	        $(document.body).children().on('mouseover', null, $.noop);
	      }

	      var complete = function complete() {
	        if (_this.config.animation) {
	          _this._fixTransition();
	        }

	        var prevHoverState = _this._hoverState;
	        _this._hoverState = null;
	        $(_this.element).trigger(_this.constructor.Event.SHOWN);

	        if (prevHoverState === HoverState.OUT) {
	          _this._leave(null, _this);
	        }
	      };

	      if ($(this.tip).hasClass(ClassName$8.FADE)) {
	        var transitionDuration = Util.getTransitionDurationFromElement(this.tip);
	        $(this.tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	      } else {
	        complete();
	      }
	    }
	  };

	  _proto.hide = function hide(callback) {
	    var _this2 = this;

	    var tip = this.getTipElement();
	    var hideEvent = $.Event(this.constructor.Event.HIDE);

	    var complete = function complete() {
	      if (_this2._hoverState !== HoverState.SHOW && tip.parentNode) {
	        tip.parentNode.removeChild(tip);
	      }

	      _this2._cleanTipClass();

	      _this2.element.removeAttribute('aria-describedby');

	      $(_this2.element).trigger(_this2.constructor.Event.HIDDEN);

	      if (_this2._popper !== null) {
	        _this2._popper.destroy();
	      }

	      if (callback) {
	        callback();
	      }
	    };

	    $(this.element).trigger(hideEvent);

	    if (hideEvent.isDefaultPrevented()) {
	      return;
	    }

	    $(tip).removeClass(ClassName$8.SHOW); // If this is a touch-enabled device we remove the extra
	    // empty mouseover listeners we added for iOS support

	    if ('ontouchstart' in document.documentElement) {
	      $(document.body).children().off('mouseover', null, $.noop);
	    }

	    this._activeTrigger[Trigger.CLICK] = false;
	    this._activeTrigger[Trigger.FOCUS] = false;
	    this._activeTrigger[Trigger.HOVER] = false;

	    if ($(this.tip).hasClass(ClassName$8.FADE)) {
	      var transitionDuration = Util.getTransitionDurationFromElement(tip);
	      $(tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	    } else {
	      complete();
	    }

	    this._hoverState = '';
	  };

	  _proto.update = function update() {
	    if (this._popper !== null) {
	      this._popper.scheduleUpdate();
	    }
	  } // Protected
	  ;

	  _proto.isWithContent = function isWithContent() {
	    return Boolean(this.getTitle());
	  };

	  _proto.addAttachmentClass = function addAttachmentClass(attachment) {
	    $(this.getTipElement()).addClass(CLASS_PREFIX + "-" + attachment);
	  };

	  _proto.getTipElement = function getTipElement() {
	    this.tip = this.tip || $(this.config.template)[0];
	    return this.tip;
	  };

	  _proto.setContent = function setContent() {
	    var tip = this.getTipElement();
	    this.setElementContent($(tip.querySelectorAll(Selector$9.TOOLTIP_INNER)), this.getTitle());
	    $(tip).removeClass(ClassName$8.FADE + " " + ClassName$8.SHOW);
	  };

	  _proto.setElementContent = function setElementContent($element, content) {
	    if (typeof content === 'object' && (content.nodeType || content.jquery)) {
	      // Content is a DOM node or a jQuery
	      if (this.config.html) {
	        if (!$(content).parent().is($element)) {
	          $element.empty().append(content);
	        }
	      } else {
	        $element.text($(content).text());
	      }

	      return;
	    }

	    if (this.config.html) {
	      if (this.config.sanitize) {
	        content = sanitizeHtml(content, this.config.whiteList, this.config.sanitizeFn);
	      }

	      $element.html(content);
	    } else {
	      $element.text(content);
	    }
	  };

	  _proto.getTitle = function getTitle() {
	    var title = this.element.getAttribute('data-original-title');

	    if (!title) {
	      title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
	    }

	    return title;
	  } // Private
	  ;

	  _proto._getPopperConfig = function _getPopperConfig(attachment) {
	    var _this3 = this;

	    var defaultBsConfig = {
	      placement: attachment,
	      modifiers: {
	        offset: this._getOffset(),
	        flip: {
	          behavior: this.config.fallbackPlacement
	        },
	        arrow: {
	          element: Selector$9.ARROW
	        },
	        preventOverflow: {
	          boundariesElement: this.config.boundary
	        }
	      },
	      onCreate: function onCreate(data) {
	        if (data.originalPlacement !== data.placement) {
	          _this3._handlePopperPlacementChange(data);
	        }
	      },
	      onUpdate: function onUpdate(data) {
	        return _this3._handlePopperPlacementChange(data);
	      }
	    };
	    return Object.assign({}, defaultBsConfig, {}, this.config.popperConfig);
	  };

	  _proto._getOffset = function _getOffset() {
	    var _this4 = this;

	    var offset = {};

	    if (typeof this.config.offset === 'function') {
	      offset.fn = function (data) {
	        data.offsets = Object.assign({}, data.offsets, {}, _this4.config.offset(data.offsets, _this4.element) || {});
	        return data;
	      };
	    } else {
	      offset.offset = this.config.offset;
	    }

	    return offset;
	  };

	  _proto._getContainer = function _getContainer() {
	    if (this.config.container === false) {
	      return document.body;
	    }

	    if (Util.isElement(this.config.container)) {
	      return $(this.config.container);
	    }

	    return $(document).find(this.config.container);
	  };

	  _proto._getAttachment = function _getAttachment(placement) {
	    return AttachmentMap[placement.toUpperCase()];
	  };

	  _proto._setListeners = function _setListeners() {
	    var _this5 = this;

	    var triggers = this.config.trigger.split(' ');
	    triggers.forEach(function (trigger) {
	      if (trigger === 'click') {
	        $(_this5.element).on(_this5.constructor.Event.CLICK, _this5.config.selector, function (event) {
	          return _this5.toggle(event);
	        });
	      } else if (trigger !== Trigger.MANUAL) {
	        var eventIn = trigger === Trigger.HOVER ? _this5.constructor.Event.MOUSEENTER : _this5.constructor.Event.FOCUSIN;
	        var eventOut = trigger === Trigger.HOVER ? _this5.constructor.Event.MOUSELEAVE : _this5.constructor.Event.FOCUSOUT;
	        $(_this5.element).on(eventIn, _this5.config.selector, function (event) {
	          return _this5._enter(event);
	        }).on(eventOut, _this5.config.selector, function (event) {
	          return _this5._leave(event);
	        });
	      }
	    });

	    this._hideModalHandler = function () {
	      if (_this5.element) {
	        _this5.hide();
	      }
	    };

	    $(this.element).closest('.modal').on('hide.bs.modal', this._hideModalHandler);

	    if (this.config.selector) {
	      this.config = Object.assign({}, this.config, {
	        trigger: 'manual',
	        selector: ''
	      });
	    } else {
	      this._fixTitle();
	    }
	  };

	  _proto._fixTitle = function _fixTitle() {
	    var titleType = typeof this.element.getAttribute('data-original-title');

	    if (this.element.getAttribute('title') || titleType !== 'string') {
	      this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
	      this.element.setAttribute('title', '');
	    }
	  };

	  _proto._enter = function _enter(event, context) {
	    var dataKey = this.constructor.DATA_KEY;
	    context = context || $(event.currentTarget).data(dataKey);

	    if (!context) {
	      context = new this.constructor(event.currentTarget, this._getDelegateConfig());
	      $(event.currentTarget).data(dataKey, context);
	    }

	    if (event) {
	      context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
	    }

	    if ($(context.getTipElement()).hasClass(ClassName$8.SHOW) || context._hoverState === HoverState.SHOW) {
	      context._hoverState = HoverState.SHOW;
	      return;
	    }

	    clearTimeout(context._timeout);
	    context._hoverState = HoverState.SHOW;

	    if (!context.config.delay || !context.config.delay.show) {
	      context.show();
	      return;
	    }

	    context._timeout = setTimeout(function () {
	      if (context._hoverState === HoverState.SHOW) {
	        context.show();
	      }
	    }, context.config.delay.show);
	  };

	  _proto._leave = function _leave(event, context) {
	    var dataKey = this.constructor.DATA_KEY;
	    context = context || $(event.currentTarget).data(dataKey);

	    if (!context) {
	      context = new this.constructor(event.currentTarget, this._getDelegateConfig());
	      $(event.currentTarget).data(dataKey, context);
	    }

	    if (event) {
	      context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
	    }

	    if (context._isWithActiveTrigger()) {
	      return;
	    }

	    clearTimeout(context._timeout);
	    context._hoverState = HoverState.OUT;

	    if (!context.config.delay || !context.config.delay.hide) {
	      context.hide();
	      return;
	    }

	    context._timeout = setTimeout(function () {
	      if (context._hoverState === HoverState.OUT) {
	        context.hide();
	      }
	    }, context.config.delay.hide);
	  };

	  _proto._isWithActiveTrigger = function _isWithActiveTrigger() {
	    for (var trigger in this._activeTrigger) {
	      if (this._activeTrigger[trigger]) {
	        return true;
	      }
	    }

	    return false;
	  };

	  _proto._getConfig = function _getConfig(config) {
	    var dataAttributes = $(this.element).data();
	    Object.keys(dataAttributes).forEach(function (dataAttr) {
	      if (DISALLOWED_ATTRIBUTES.indexOf(dataAttr) !== -1) {
	        delete dataAttributes[dataAttr];
	      }
	    });
	    config = Object.assign({}, this.constructor.Default, {}, dataAttributes, {}, typeof config === 'object' && config ? config : {});

	    if (typeof config.delay === 'number') {
	      config.delay = {
	        show: config.delay,
	        hide: config.delay
	      };
	    }

	    if (typeof config.title === 'number') {
	      config.title = config.title.toString();
	    }

	    if (typeof config.content === 'number') {
	      config.content = config.content.toString();
	    }

	    Util.typeCheckConfig(NAME$4, config, this.constructor.DefaultType);

	    if (config.sanitize) {
	      config.template = sanitizeHtml(config.template, config.whiteList, config.sanitizeFn);
	    }

	    return config;
	  };

	  _proto._getDelegateConfig = function _getDelegateConfig() {
	    var config = {};

	    if (this.config) {
	      for (var key in this.config) {
	        if (this.constructor.Default[key] !== this.config[key]) {
	          config[key] = this.config[key];
	        }
	      }
	    }

	    return config;
	  };

	  _proto._cleanTipClass = function _cleanTipClass() {
	    var $tip = $(this.getTipElement());
	    var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);

	    if (tabClass !== null && tabClass.length) {
	      $tip.removeClass(tabClass.join(''));
	    }
	  };

	  _proto._handlePopperPlacementChange = function _handlePopperPlacementChange(popperData) {
	    var popperInstance = popperData.instance;
	    this.tip = popperInstance.popper;

	    this._cleanTipClass();

	    this.addAttachmentClass(this._getAttachment(popperData.placement));
	  };

	  _proto._fixTransition = function _fixTransition() {
	    var tip = this.getTipElement();
	    var initConfigAnimation = this.config.animation;

	    if (tip.getAttribute('x-placement') !== null) {
	      return;
	    }

	    $(tip).removeClass(ClassName$8.FADE);
	    this.config.animation = false;
	    this.hide();
	    this.show();
	    this.config.animation = initConfigAnimation;
	  } // Static
	  ;

	  Tooltip._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY$2);

	      var _config = typeof config === 'object' && config;

	      if (!data && /dispose|hide/.test(config)) {
	        return;
	      }

	      if (!data) {
	        data = new Tooltip(this, _config);
	        $(this).data(DATA_KEY$2, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(Tooltip, null, [{
	    key: "VERSION",
	    get: function get() {
	      return VERSION$2;
	    }
	  }, {
	    key: "Default",
	    get: function get() {
	      return Default$2;
	    }
	  }, {
	    key: "NAME",
	    get: function get() {
	      return NAME$4;
	    }
	  }, {
	    key: "DATA_KEY",
	    get: function get() {
	      return DATA_KEY$2;
	    }
	  }, {
	    key: "Event",
	    get: function get() {
	      return Event$8;
	    }
	  }, {
	    key: "EVENT_KEY",
	    get: function get() {
	      return EVENT_KEY$2;
	    }
	  }, {
	    key: "DefaultType",
	    get: function get() {
	      return DefaultType$2;
	    }
	  }]);

	  return Tooltip;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */


	$.fn[NAME$4] = Tooltip._jQueryInterface;
	$.fn[NAME$4].Constructor = Tooltip;

	$.fn[NAME$4].noConflict = function () {
	  $.fn[NAME$4] = JQUERY_NO_CONFLICT$2;
	  return Tooltip._jQueryInterface;
	};

	var NAME$5 = Tooltip.NAME;
	var DATA_KEY$3 = Tooltip.DATA_KEY;
	var SCREENREADER_CLASS = '.sr-only';
	var JQUERY_NO_CONFLICT$3 = $.fn[NAME$5];
	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var DefaultType$3 = Object.assign({}, Tooltip.DefaultType, {
	  screenReaderContent: 'string' // new constant default type

	});
	var Default$3 = Object.assign({}, Tooltip.Default, {
	  template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div>' + '<span class="sr-only"></span></div>',
	  screenReaderContent: '' // new constant default value

	}); // ACCESSIBILITY FIX
	// IE11, Edge - does not need a tooltip delay
	// FireFox, Chrome - add slight delay so that the button is read first and then the tooltip

	var IE_VERSION = Util$1.detectIE();
	var DELAY_VALUE_OFFSET = IE_VERSION <= 11 && IE_VERSION !== false ? 0 : {
	  show: 250,
	  hide: 100
	};

	var clockwise$1 = function clockwise(dir) {
	  var arr = ['top', 'right', 'bottom', 'left'];
	  var index = arr.indexOf(dir);
	  var firstSlice = arr.slice(index);
	  return firstSlice.concat(arr.slice(0, index));
	};

	var instances$2 = [];

	var Tooltip$1 = /*#__PURE__*/function (_BootstrapTooltip) {
	  _inheritsLoose(Tooltip, _BootstrapTooltip);

	  function Tooltip(element, config) {
	    var _this;

	    _this = _BootstrapTooltip.call(this, element, config) || this;

	    _this._setRTLPlacement();

	    _this._offsetDelay();

	    _this._setFallbackPlacement(config);

	    _this._setKeyboardListeners();

	    _this._setScreenReaderContentTemplate();

	    return _this;
	  } // Getters


	  var _proto = Tooltip.prototype;

	  // Private
	  _proto._offsetDelay = function _offsetDelay() {
	    if (DELAY_VALUE_OFFSET && typeof DELAY_VALUE_OFFSET === 'object') {
	      this.config.delay.show += DELAY_VALUE_OFFSET.show;
	      this.config.delay.hide += DELAY_VALUE_OFFSET.hide;
	    }
	  };

	  _proto._setRTLPlacement = function _setRTLPlacement() {
	    if (Util$1.isBiDirectional()) {
	      // reverse the left/right placement
	      if (this.config.placement === 'left') {
	        this.config.placement = 'right';
	      } else if (this.config.placement === 'right') {
	        this.config.placement = 'left';
	      }
	    }
	  };

	  _proto._setFallbackPlacement = function _setFallbackPlacement(options) {
	    if (!options || typeof options.fallbackPlacement === 'undefined') {
	      this.config.fallbackPlacement = clockwise$1(this.config.placement);
	    }
	  };

	  _proto._setKeyboardListeners = function _setKeyboardListeners() {
	    this.element.addEventListener('keydown', this._handleKeydownEvent.bind(this));
	  };

	  _proto._handleKeydownEvent = function _handleKeydownEvent(event) {
	    var keycode = event.keycode || event.which;

	    if (keycode === Util$1.keyCodes.ESC) {
	      this.hide();
	    }

	    return false;
	  };

	  _proto._getScreenReaderCloseText = function _getScreenReaderCloseText() {
	    return this.element.getAttribute('data-sr-close') || this.config.screenReaderContent;
	  };

	  _proto._setScreenReaderContentTemplate = function _setScreenReaderContentTemplate() {
	    // update the content HTML template to add screen reader element from default or user config
	    var tempTemplate = $(this.config.template);

	    if (tempTemplate.find(SCREENREADER_CLASS).length > 0) {
	      tempTemplate.find(SCREENREADER_CLASS).text(this._getScreenReaderCloseText());
	      this.config.template = tempTemplate;
	    }
	  } // Static
	  ;

	  Tooltip._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY$3);

	      var _config = typeof config === 'object' && config;

	      if (!data && /dispose|hide/.test(config)) {
	        return;
	      }

	      if (!data) {
	        data = new Tooltip(this, _config);
	        $(this).data(DATA_KEY$3, data);
	      }

	      instances$2.push(data); // add all instances to an array for management

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(Tooltip, null, [{
	    key: "Default",
	    get: function get() {
	      return Default$3;
	    }
	  }, {
	    key: "DefaultType",
	    get: function get() {
	      return DefaultType$3;
	    }
	  }, {
	    key: "Instances",
	    get: function get() {
	      return instances$2;
	    }
	  }]);

	  return Tooltip;
	}(Tooltip);
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */


	$.fn[NAME$5] = Tooltip$1._jQueryInterface;
	$.fn[NAME$5].Constructor = Tooltip$1;

	$.fn[NAME$5].noConflict = function () {
	  $.fn[NAME$5] = JQUERY_NO_CONFLICT$3;
	  return Tooltip$1._jQueryInterface;
	};

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$6 = 'popover';
	var VERSION$3 = '4.4.1';
	var DATA_KEY$4 = 'bs.popover';
	var EVENT_KEY$3 = "." + DATA_KEY$4;
	var JQUERY_NO_CONFLICT$4 = $.fn[NAME$6];
	var CLASS_PREFIX$1 = 'bs-popover';
	var BSCLS_PREFIX_REGEX$1 = new RegExp("(^|\\s)" + CLASS_PREFIX$1 + "\\S+", 'g');
	var Default$4 = Object.assign({}, Tooltip.Default, {
	  placement: 'right',
	  trigger: 'click',
	  content: '',
	  template: '<div class="popover" role="tooltip">' + '<div class="arrow"></div>' + '<h3 class="popover-header"></h3>' + '<div class="popover-body"></div></div>'
	});
	var DefaultType$4 = Object.assign({}, Tooltip.DefaultType, {
	  content: '(string|element|function)'
	});
	var ClassName$9 = {
	  FADE: 'fade',
	  SHOW: 'show'
	};
	var Selector$a = {
	  TITLE: '.popover-header',
	  CONTENT: '.popover-body'
	};
	var Event$9 = {
	  HIDE: "hide" + EVENT_KEY$3,
	  HIDDEN: "hidden" + EVENT_KEY$3,
	  SHOW: "show" + EVENT_KEY$3,
	  SHOWN: "shown" + EVENT_KEY$3,
	  INSERTED: "inserted" + EVENT_KEY$3,
	  CLICK: "click" + EVENT_KEY$3,
	  FOCUSIN: "focusin" + EVENT_KEY$3,
	  FOCUSOUT: "focusout" + EVENT_KEY$3,
	  MOUSEENTER: "mouseenter" + EVENT_KEY$3,
	  MOUSELEAVE: "mouseleave" + EVENT_KEY$3
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Popover = /*#__PURE__*/function (_Tooltip) {
	  _inheritsLoose(Popover, _Tooltip);

	  function Popover() {
	    return _Tooltip.apply(this, arguments) || this;
	  }

	  var _proto = Popover.prototype;

	  // Overrides
	  _proto.isWithContent = function isWithContent() {
	    return this.getTitle() || this._getContent();
	  };

	  _proto.addAttachmentClass = function addAttachmentClass(attachment) {
	    $(this.getTipElement()).addClass(CLASS_PREFIX$1 + "-" + attachment);
	  };

	  _proto.getTipElement = function getTipElement() {
	    this.tip = this.tip || $(this.config.template)[0];
	    return this.tip;
	  };

	  _proto.setContent = function setContent() {
	    var $tip = $(this.getTipElement()); // We use append for html objects to maintain js events

	    this.setElementContent($tip.find(Selector$a.TITLE), this.getTitle());

	    var content = this._getContent();

	    if (typeof content === 'function') {
	      content = content.call(this.element);
	    }

	    this.setElementContent($tip.find(Selector$a.CONTENT), content);
	    $tip.removeClass(ClassName$9.FADE + " " + ClassName$9.SHOW);
	  } // Private
	  ;

	  _proto._getContent = function _getContent() {
	    return this.element.getAttribute('data-content') || this.config.content;
	  };

	  _proto._cleanTipClass = function _cleanTipClass() {
	    var $tip = $(this.getTipElement());
	    var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX$1);

	    if (tabClass !== null && tabClass.length > 0) {
	      $tip.removeClass(tabClass.join(''));
	    }
	  } // Static
	  ;

	  Popover._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY$4);

	      var _config = typeof config === 'object' ? config : null;

	      if (!data && /dispose|hide/.test(config)) {
	        return;
	      }

	      if (!data) {
	        data = new Popover(this, _config);
	        $(this).data(DATA_KEY$4, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(Popover, null, [{
	    key: "VERSION",
	    // Getters
	    get: function get() {
	      return VERSION$3;
	    }
	  }, {
	    key: "Default",
	    get: function get() {
	      return Default$4;
	    }
	  }, {
	    key: "NAME",
	    get: function get() {
	      return NAME$6;
	    }
	  }, {
	    key: "DATA_KEY",
	    get: function get() {
	      return DATA_KEY$4;
	    }
	  }, {
	    key: "Event",
	    get: function get() {
	      return Event$9;
	    }
	  }, {
	    key: "EVENT_KEY",
	    get: function get() {
	      return EVENT_KEY$3;
	    }
	  }, {
	    key: "DefaultType",
	    get: function get() {
	      return DefaultType$4;
	    }
	  }]);

	  return Popover;
	}(Tooltip);
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */


	$.fn[NAME$6] = Popover._jQueryInterface;
	$.fn[NAME$6].Constructor = Popover;

	$.fn[NAME$6].noConflict = function () {
	  $.fn[NAME$6] = JQUERY_NO_CONFLICT$4;
	  return Popover._jQueryInterface;
	};

	var NAME$7 = Popover.NAME;
	var DATA_KEY$5 = Popover.DATA_KEY;
	var EVENT_KEY$4 = "." + DATA_KEY$5;
	var JQUERY_NO_CONFLICT$5 = $.fn[NAME$7];
	var CLASS_PREFIX$2 = 'bs-popover';
	var BSCLS_PREFIX_REGEX$2 = new RegExp("(^|\\s)" + CLASS_PREFIX$2 + "\\S+", 'g');
	var Default$5 = Object.assign({}, Tooltip$1.Default, {
	  placement: 'right',
	  trigger: 'click',
	  content: '',
	  template: "\n  <div class=\"popover\" role=\"tooltip\">\n    <div class=\"arrow\"></div>\n    <button class=\"close\" aria-label=\"Close tooltip\" onclick=\"$(this).parent().popover('hide');\"></button>\n    <div class=\"popover-content\">\n      <h3 class=\"popover-header h4\"></h3>\n      <div class=\"popover-body\"></div>\n    </div>\n    <span class=\"sr-only\"></span>\n  </div>",
	  screenReaderContent: '' // new constant default value

	});
	var DefaultType$5 = Object.assign({}, Tooltip$1.DefaultType, {
	  content: '(string|element|function)',
	  screenReaderContent: 'string' // new constant default type

	});
	var ClassName$a = {
	  FADE: 'fade',
	  SHOW: 'show'
	};
	var Selector$b = {
	  TITLE: '.popover-header',
	  CONTENT: '.popover-body'
	};
	var Event$a = Popover.Event;

	var Popover$1 = /*#__PURE__*/function (_Tooltip) {
	  _inheritsLoose(Popover, _Tooltip);

	  function Popover() {
	    return _Tooltip.apply(this, arguments) || this;
	  }

	  var _proto = Popover.prototype;

	  // Overrides
	  _proto.isWithContent = function isWithContent() {
	    return this.getTitle() || this._getContent();
	  };

	  _proto.addAttachmentClass = function addAttachmentClass(attachment) {
	    $(this.getTipElement()).addClass(CLASS_PREFIX$2 + "-" + attachment);
	  };

	  _proto.getTipElement = function getTipElement() {
	    this.tip = this.tip || $(this.config.template)[0];
	    return this.tip;
	  };

	  _proto.setContent = function setContent() {
	    var $tip = $(this.getTipElement()); // We use append for html objects to maintain js events

	    this.setElementContent($tip.find(Selector$b.TITLE), this.getTitle());

	    var content = this._getContent();

	    if (typeof content === 'function') {
	      content = content.call(this.element);
	    }

	    this.setElementContent($tip.find(Selector$b.CONTENT), content);
	    $tip.removeClass(ClassName$a.FADE + " " + ClassName$a.SHOW);
	  } // Private
	  ;

	  _proto._getContent = function _getContent() {
	    return this.element.getAttribute('data-content') || this.config.content;
	  };

	  _proto._cleanTipClass = function _cleanTipClass() {
	    var $tip = $(this.getTipElement());
	    var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX$2);

	    if (tabClass !== null && tabClass.length > 0) {
	      $tip.removeClass(tabClass.join(''));
	    }
	  };

	  Popover._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY$5);

	      var _config = typeof config === 'object' ? config : null;

	      if (!data && /dispose|hide/.test(config)) {
	        return;
	      }

	      if (!data) {
	        data = new Popover(this, _config);
	        $(this).data(DATA_KEY$5, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(Popover, null, [{
	    key: "Default",
	    // Getters
	    get: function get() {
	      return Default$5;
	    }
	  }, {
	    key: "NAME",
	    get: function get() {
	      return NAME$7;
	    }
	  }, {
	    key: "DATA_KEY",
	    get: function get() {
	      return DATA_KEY$5;
	    }
	  }, {
	    key: "Event",
	    get: function get() {
	      return Event$a;
	    }
	  }, {
	    key: "EVENT_KEY",
	    get: function get() {
	      return EVENT_KEY$4;
	    }
	  }, {
	    key: "DefaultType",
	    get: function get() {
	      return DefaultType$5;
	    }
	  }]);

	  return Popover;
	}(Tooltip$1);
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */


	$.fn[NAME$7] = Popover$1._jQueryInterface;
	$.fn[NAME$7].Constructor = Popover$1;

	$.fn[NAME$7].noConflict = function () {
	  $.fn[NAME$7] = JQUERY_NO_CONFLICT$5;
	  return Popover$1._jQueryInterface;
	};

	var $map = arrayIteration.map;



	var HAS_SPECIES_SUPPORT$3 = arrayMethodHasSpeciesSupport('map');
	// FF49- issue
	var USES_TO_LENGTH$8 = arrayMethodUsesToLength('map');

	// `Array.prototype.map` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.map
	// with adding support of @@species
	_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT$3 || !USES_TO_LENGTH$8 }, {
	  map: function map(callbackfn /* , thisArg */) {
	    return $map(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
	  }
	});

	var test$1 = [];
	var nativeSort = test$1.sort;

	// IE8-
	var FAILS_ON_UNDEFINED = fails(function () {
	  test$1.sort(undefined);
	});
	// V8 bug
	var FAILS_ON_NULL = fails(function () {
	  test$1.sort(null);
	});
	// Old WebKit
	var STRICT_METHOD$5 = arrayMethodIsStrict('sort');

	var FORCED$4 = FAILS_ON_UNDEFINED || !FAILS_ON_NULL || !STRICT_METHOD$5;

	// `Array.prototype.sort` method
	// https://tc39.github.io/ecma262/#sec-array.prototype.sort
	_export({ target: 'Array', proto: true, forced: FORCED$4 }, {
	  sort: function sort(comparefn) {
	    return comparefn === undefined
	      ? nativeSort.call(toObject(this))
	      : nativeSort.call(toObject(this), aFunction$1(comparefn));
	  }
	});

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$8 = 'scrollspy';
	var VERSION$4 = '4.4.1';
	var DATA_KEY$6 = 'bs.scrollspy';
	var EVENT_KEY$5 = "." + DATA_KEY$6;
	var DATA_API_KEY$2 = '.data-api';
	var JQUERY_NO_CONFLICT$6 = $.fn[NAME$8];
	var Default$6 = {
	  offset: 10,
	  method: 'auto',
	  target: ''
	};
	var DefaultType$6 = {
	  offset: 'number',
	  method: 'string',
	  target: '(string|element)'
	};
	var Event$b = {
	  ACTIVATE: "activate" + EVENT_KEY$5,
	  SCROLL: "scroll" + EVENT_KEY$5,
	  LOAD_DATA_API: "load" + EVENT_KEY$5 + DATA_API_KEY$2
	};
	var ClassName$b = {
	  DROPDOWN_ITEM: 'dropdown-item',
	  DROPDOWN_MENU: 'dropdown-menu',
	  ACTIVE: 'active'
	};
	var Selector$c = {
	  DATA_SPY: '[data-spy="scroll"]',
	  ACTIVE: '.active',
	  NAV_LIST_GROUP: '.nav, .list-group',
	  NAV_LINKS: '.nav-link',
	  NAV_ITEMS: '.nav-item',
	  LIST_ITEMS: '.list-group-item',
	  DROPDOWN: '.dropdown',
	  DROPDOWN_ITEMS: '.dropdown-item',
	  DROPDOWN_TOGGLE: '.dropdown-toggle'
	};
	var OffsetMethod = {
	  OFFSET: 'offset',
	  POSITION: 'position'
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var ScrollSpy = /*#__PURE__*/function () {
	  function ScrollSpy(element, config) {
	    var _this = this;

	    this._element = element;
	    this._scrollElement = element.tagName === 'BODY' ? window : element;
	    this._config = this._getConfig(config);
	    this._selector = this._config.target + " " + Selector$c.NAV_LINKS + "," + (this._config.target + " " + Selector$c.LIST_ITEMS + ",") + (this._config.target + " " + Selector$c.DROPDOWN_ITEMS);
	    this._offsets = [];
	    this._targets = [];
	    this._activeTarget = null;
	    this._scrollHeight = 0;
	    $(this._scrollElement).on(Event$b.SCROLL, function (event) {
	      return _this._process(event);
	    });
	    this.refresh();

	    this._process();
	  } // Getters


	  var _proto = ScrollSpy.prototype;

	  // Public
	  _proto.refresh = function refresh() {
	    var _this2 = this;

	    var autoMethod = this._scrollElement === this._scrollElement.window ? OffsetMethod.OFFSET : OffsetMethod.POSITION;
	    var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;
	    var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;
	    this._offsets = [];
	    this._targets = [];
	    this._scrollHeight = this._getScrollHeight();
	    var targets = [].slice.call(document.querySelectorAll(this._selector));
	    targets.map(function (element) {
	      var target;
	      var targetSelector = Util.getSelectorFromElement(element);

	      if (targetSelector) {
	        target = document.querySelector(targetSelector);
	      }

	      if (target) {
	        var targetBCR = target.getBoundingClientRect();

	        if (targetBCR.width || targetBCR.height) {
	          // TODO (fat): remove sketch reliance on jQuery position/offset
	          return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
	        }
	      }

	      return null;
	    }).filter(function (item) {
	      return item;
	    }).sort(function (a, b) {
	      return a[0] - b[0];
	    }).forEach(function (item) {
	      _this2._offsets.push(item[0]);

	      _this2._targets.push(item[1]);
	    });
	  };

	  _proto.dispose = function dispose() {
	    $.removeData(this._element, DATA_KEY$6);
	    $(this._scrollElement).off(EVENT_KEY$5);
	    this._element = null;
	    this._scrollElement = null;
	    this._config = null;
	    this._selector = null;
	    this._offsets = null;
	    this._targets = null;
	    this._activeTarget = null;
	    this._scrollHeight = null;
	  } // Private
	  ;

	  _proto._getConfig = function _getConfig(config) {
	    config = Object.assign({}, Default$6, {}, typeof config === 'object' && config ? config : {});

	    if (typeof config.target !== 'string') {
	      var id = $(config.target).attr('id');

	      if (!id) {
	        id = Util.getUID(NAME$8);
	        $(config.target).attr('id', id);
	      }

	      config.target = "#" + id;
	    }

	    Util.typeCheckConfig(NAME$8, config, DefaultType$6);
	    return config;
	  };

	  _proto._getScrollTop = function _getScrollTop() {
	    return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
	  };

	  _proto._getScrollHeight = function _getScrollHeight() {
	    return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
	  };

	  _proto._getOffsetHeight = function _getOffsetHeight() {
	    return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
	  };

	  _proto._process = function _process() {
	    var scrollTop = this._getScrollTop() + this._config.offset;

	    var scrollHeight = this._getScrollHeight();

	    var maxScroll = this._config.offset + scrollHeight - this._getOffsetHeight();

	    if (this._scrollHeight !== scrollHeight) {
	      this.refresh();
	    }

	    if (scrollTop >= maxScroll) {
	      var target = this._targets[this._targets.length - 1];

	      if (this._activeTarget !== target) {
	        this._activate(target);
	      }

	      return;
	    }

	    if (this._activeTarget && scrollTop < this._offsets[0] && this._offsets[0] > 0) {
	      this._activeTarget = null;

	      this._clear();

	      return;
	    }

	    var offsetLength = this._offsets.length;

	    for (var i = offsetLength; i--;) {
	      var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (typeof this._offsets[i + 1] === 'undefined' || scrollTop < this._offsets[i + 1]);

	      if (isActiveTarget) {
	        this._activate(this._targets[i]);
	      }
	    }
	  };

	  _proto._activate = function _activate(target) {
	    this._activeTarget = target;

	    this._clear();

	    var queries = this._selector.split(',').map(function (selector) {
	      return selector + "[data-target=\"" + target + "\"]," + selector + "[href=\"" + target + "\"]";
	    });

	    var $link = $([].slice.call(document.querySelectorAll(queries.join(','))));

	    if ($link.hasClass(ClassName$b.DROPDOWN_ITEM)) {
	      $link.closest(Selector$c.DROPDOWN).find(Selector$c.DROPDOWN_TOGGLE).addClass(ClassName$b.ACTIVE);
	      $link.addClass(ClassName$b.ACTIVE);
	    } else {
	      // Set triggered link as active
	      $link.addClass(ClassName$b.ACTIVE); // Set triggered links parents as active
	      // With both <ul> and <nav> markup a parent is the previous sibling of any nav ancestor

	      $link.parents(Selector$c.NAV_LIST_GROUP).prev(Selector$c.NAV_LINKS + ", " + Selector$c.LIST_ITEMS).addClass(ClassName$b.ACTIVE); // Handle special case when .nav-link is inside .nav-item

	      $link.parents(Selector$c.NAV_LIST_GROUP).prev(Selector$c.NAV_ITEMS).children(Selector$c.NAV_LINKS).addClass(ClassName$b.ACTIVE);
	    }

	    $(this._scrollElement).trigger(Event$b.ACTIVATE, {
	      relatedTarget: target
	    });
	  };

	  _proto._clear = function _clear() {
	    [].slice.call(document.querySelectorAll(this._selector)).filter(function (node) {
	      return node.classList.contains(ClassName$b.ACTIVE);
	    }).forEach(function (node) {
	      return node.classList.remove(ClassName$b.ACTIVE);
	    });
	  } // Static
	  ;

	  ScrollSpy._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var data = $(this).data(DATA_KEY$6);

	      var _config = typeof config === 'object' && config;

	      if (!data) {
	        data = new ScrollSpy(this, _config);
	        $(this).data(DATA_KEY$6, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config]();
	      }
	    });
	  };

	  _createClass(ScrollSpy, null, [{
	    key: "VERSION",
	    get: function get() {
	      return VERSION$4;
	    }
	  }, {
	    key: "Default",
	    get: function get() {
	      return Default$6;
	    }
	  }]);

	  return ScrollSpy;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * Data Api implementation
	 * ------------------------------------------------------------------------
	 */


	$(window).on(Event$b.LOAD_DATA_API, function () {
	  var scrollSpys = [].slice.call(document.querySelectorAll(Selector$c.DATA_SPY));
	  var scrollSpysLength = scrollSpys.length;

	  for (var i = scrollSpysLength; i--;) {
	    var $spy = $(scrollSpys[i]);

	    ScrollSpy._jQueryInterface.call($spy, $spy.data());
	  }
	});
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */

	$.fn[NAME$8] = ScrollSpy._jQueryInterface;
	$.fn[NAME$8].Constructor = ScrollSpy;

	$.fn[NAME$8].noConflict = function () {
	  $.fn[NAME$8] = JQUERY_NO_CONFLICT$6;
	  return ScrollSpy._jQueryInterface;
	};

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var tabs = [];
	var Event$c = {
	  HIDE: 'hide',
	  HIDDEN: 'hidden',
	  SHOW: 'show',
	  SHOWN: 'shown',
	  CLICK_DATA_API: 'click',
	  KEYDOWN_DATA_API: 'keydown',
	  ON_REMOVE: 'onRemove'
	};
	var Attribute$1 = {
	  HIDDEN: 'hidden'
	};
	var ClassName$c = {
	  DROPDOWN_MENU: 'dropdown-menu',
	  ACTIVE: 'active',
	  DISABLED: 'disabled',
	  FADE: 'fade',
	  SHOW: 'show'
	};
	var Selector$d = {
	  DROPDOWN: '.dropdown',
	  NAV_LIST_GROUP: '.nav, .list-group, .tab-group',
	  ACTIVE: '.active',
	  ACTIVE_UL: 'li .active',
	  DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
	  DROPDOWN_TOGGLE: '.dropdown-toggle',
	  DROPDOWN_ACTIVE_CHILD: '.dropdown-menu .active'
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Tab = /*#__PURE__*/function () {
	  function Tab(opts) {
	    this.element = opts.el;
	    this.listGroup = this.element.closest(Selector$d.NAV_LIST_GROUP);
	    this.isRTL = document.dir === 'rtl'; // prevents error if tab is not within a list group

	    if (this.listGroup) {
	      this.listNodeList = this.listGroup.querySelectorAll("[data-toggle=" + this.element.dataset.toggle + "]") || [];
	      this.nodeListArray = [].slice.call(this.listNodeList);
	      this.tabIndex = this.nodeListArray.indexOf(this.element);
	    } // attach event listeners


	    this.events = [{
	      el: this.element,
	      type: Event$c.CLICK_DATA_API,
	      handler: this.show.bind(this)
	    }, {
	      el: this.element,
	      type: Event$c.KEYDOWN_DATA_API,
	      handler: this._onKeycodeEvent.bind(this)
	    }]; // add event listeners

	    Util$1.addEvents(this.events);
	    tabs.push(this); // Create custom events

	    this[Event$c.ON_REMOVE] = new CustomEvent(Event$c.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	  } // Public

	  /**
	  * Shows a tab panel based on the tab clicked and hides other panels
	  * @param {event}  e  event trigger
	  */


	  var _proto = Tab.prototype;

	  _proto.show = function show(e) {
	    var _this = this;

	    e.preventDefault();

	    if (this.element.parentNode && this.element.parentNode.nodeType === Node.ELEMENT_NODE && this.element.classList.contains(ClassName$c.ACTIVE) || this.element.classList.contains(ClassName$c.DISABLED)) {
	      return;
	    }

	    var target;
	    var previous;
	    var listElement = this.element.closest(Selector$d.NAV_LIST_GROUP);
	    var selector = Util$1.getSelectorFromElement(this.element);

	    if (listElement) {
	      var itemSelector = listElement.nodeName === 'UL' || listElement.nodeName === 'OL' ? Selector$d.ACTIVE_UL : Selector$d.ACTIVE;
	      previous = [].concat(document.querySelector(itemSelector));
	      previous = previous[previous.length - 1];
	    }

	    var hideEvent = new CustomEvent(Event$c.HIDE, {
	      detail: {
	        relatedTarget: this.element
	      }
	    });
	    var showEvent = new CustomEvent(Event$c.SHOW, {
	      detail: {
	        relatedTarget: previous
	      }
	    });

	    if (previous) {
	      previous.dispatchEvent(hideEvent);
	    }

	    this.element.dispatchEvent(showEvent);

	    if (showEvent.defaultPrevented || hideEvent.defaultPrevented) {
	      return;
	    }

	    if (selector) {
	      target = document.querySelector(selector);
	    }

	    this._activate(this.element, listElement);

	    var complete = function complete() {
	      var hiddenEvent = new CustomEvent(Event$c.HIDDEN, {
	        detail: {
	          relatedTarget: _this.element
	        }
	      });
	      var shownEvent = new CustomEvent(Event$c.SHOWN, {
	        detail: {
	          relatedTarget: previous
	        }
	      });
	      previous.dispatchEvent(hiddenEvent);

	      _this.element.dispatchEvent(shownEvent);
	    };

	    if (target) {
	      this._activate(target, target.parentNode, complete);
	    } else {
	      complete();
	    }
	  }
	  /**
	   * Bootstrap function to dispose of instance
	   * Recommended to use remove() function instead
	   */
	  ;

	  _proto.dispose = function dispose() {
	    this.element = null;
	  }
	  /**
	   * Remove event handlers.
	   */
	  ;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events); // remove this reference from array of instances

	    var index = tabs.indexOf(this);
	    tabs.splice(index, 1);
	    this.el.dispatchEvent(this[Event$c.ON_REMOVE]);
	  }
	  /**
	   * Get instances.
	   * @returns {Object} A object instance
	   */
	  ;

	  Tab.getInstances = function getInstances() {
	    return tabs;
	  } // Private
	  ;

	  _proto._activate = function _activate(element, container, callback) {
	    var _this2 = this;

	    var activeElements = container && (container.nodeName === 'UL' || container.nodeName === 'OL') ? container.querySelector(Selector$d.ACTIVE_UL) : container.querySelectorAll(Selector$d.ACTIVE);
	    var active = activeElements[0];
	    var isTransitioning = callback && active && active.classList.contains(ClassName$c.FADE);

	    var complete = function complete() {
	      return _this2._transitionComplete(element, active, callback);
	    };

	    if (active && isTransitioning) {
	      var transitionDuration = Util$1.getTransitionDurationFromElement(active);
	      active.addEventListener(Util$1.TRANSITION_END, complete, false);
	      var transitionEvent = new CustomEvent(Util$1.TRANSITION_END, {
	        detail: {
	          relatedTarget: active
	        }
	      });
	      setTimeout(function () {
	        active.classList.remove(ClassName$c.SHOW);
	        active.dispatchEvent(transitionEvent);
	        active.removeEventListener(Util$1.TRANSITION_END, complete);
	      }, transitionDuration);
	    } else {
	      complete();
	    }
	  };

	  _proto._transitionComplete = function _transitionComplete(element, active, callback) {
	    if (active) {
	      active.classList.remove(ClassName$c.ACTIVE);
	      var dropdownChild = active.parentNode.querySelector(Selector$d.DROPDOWN_ACTIVE_CHILD);

	      if (dropdownChild) {
	        dropdownChild[0].classList.remove(ClassName$c.ACTIVE);
	      }

	      if (active.getAttribute('role') === 'tab') {
	        active.setAttribute('aria-selected', false);
	        active.setAttribute('tabindex', '-1');
	      } else if (active.getAttribute('role') === 'tabpanel') {
	        active.hidden = true;
	      }
	    }

	    element.classList.add(ClassName$c.ACTIVE);

	    if (element.getAttribute('role') === 'tab') {
	      element.setAttribute('aria-selected', true);
	      element.setAttribute('tabindex', '0');
	    } else if (element.getAttribute('role') === 'tabpanel') {
	      element.removeAttribute(Attribute$1.HIDDEN);
	    }

	    Util$1.reflow(element);

	    if (element.classList.contains(ClassName$c.FADE)) {
	      element.classList.add(ClassName$c.SHOW);
	    }

	    if (element.parentNode && element.parentNode.classList.contains(ClassName$c.DROPDOWN_MENU)) {
	      var dropdownElement = element.closest(Selector$d.DROPDOWN)[0];

	      if (dropdownElement) {
	        var dropdownToggleList = [].slice.call(dropdownElement.querySelectorAll(Selector$d.DROPDOWN_TOGGLE));
	        dropdownToggleList.classList.add(ClassName$c.ACTIVE);
	      }

	      element.setAttribute('aria-expanded', true);
	    }

	    if (callback) {
	      callback();
	    }
	  };

	  _proto._onKeycodeEvent = function _onKeycodeEvent(event) {
	    if (event.keyCode === Util$1.keyCodes.SPACE || event.keyCode === Util$1.keyCodes.ENTER) {
	      event.preventDefault();
	      this.show(event);
	    } else if (event.keyCode === Util$1.keyCodes.ARROW_LEFT) {
	      if (this.isRTL) {
	        this._onKeycodeRight();
	      } else {
	        this._onKeycodeLeft();
	      }
	    } else if (event.keyCode === Util$1.keyCodes.ARROW_RIGHT) {
	      if (this.isRTL) {
	        this._onKeycodeLeft();
	      } else {
	        this._onKeycodeRight();
	      }
	    }
	  };

	  _proto._onKeycodeLeft = function _onKeycodeLeft() {
	    if (this.tabIndex === 0) {
	      return;
	    }

	    this.listNodeList[this.tabIndex - 1].focus();
	  };

	  _proto._onKeycodeRight = function _onKeycodeRight() {
	    if (this.tabIndex === this.listNodeList.length - 1) {
	      return;
	    }

	    this.listNodeList[this.tabIndex + 1].focus();
	  };

	  return Tab;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * Data Api implementation
	 * ------------------------------------------------------------------------
	 */


	(function () {
	  Util$1.initializeComponent(Selector$d.DATA_TOGGLE, function (node) {
	    new Tab({
	      el: node
	    });
	  });
	})();

	var tabSliders = [];
	var Event$d = {
	  CLICK_DATA_API: 'click',
	  RESIZE_DATA_API: 'resize',
	  FOCUS_DATA_API: 'focus',
	  SCROLL_DATA_API: 'scroll',
	  ON_SCROLL: 'onScroll',
	  ON_REMOVE: 'onRemove'
	};
	var Direction$1 = {
	  LEFT: 'left',
	  RIGHT: 'right'
	};
	var ClassName$d = {
	  ACTIVE: 'active',
	  ARROWS: 'tab-arrows',
	  ARROW_PREV: 'arrow-prev',
	  ARROW_NEXT: 'arrow-next',
	  TAB_OVERFLOW: 'tab-overflow',
	  TAB_WINDOW: 'tab-window',
	  TAB_GROUP: 'tab-group',
	  JUSTIFY_CENTER: 'justify-content-center',
	  MOBILE_ARROWS: 'mobile-arrows'
	};
	var Selector$e = {
	  ACTIVE: "." + ClassName$d.ACTIVE,
	  ARROWS: "." + ClassName$d.ARROWS,
	  ARROW_PREV: "." + ClassName$d.ARROW_PREV,
	  ARROW_NEXT: "." + ClassName$d.ARROW_NEXT,
	  TAB_OVERFLOW: "." + ClassName$d.TAB_OVERFLOW,
	  TAB_WINDOW: "." + ClassName$d.TAB_WINDOW,
	  TAB_GROUP: "." + ClassName$d.TAB_GROUP,
	  DATA_MOUNT: '[data-mount="tab"]',
	  DATA_TOGGLE: '[data-toggle="list"]'
	};
	/**
	 * Private functions.
	 */

	/**
	 * Helper function to check if single tab element is within tab window.
	 * @param {node}  tabBounds             Single tab element.
	 * @param {node}  tabListWindowBounds   Tab window.
	 * @return {boolean} Returns true if the tab element is visible within the tab window.
	 */

	function _inTabWindow(tab, tabListWindow) {
	  var tabBounds = tab.getBoundingClientRect();
	  var tabListWindowBounds = tabListWindow.getBoundingClientRect();
	  return Math.ceil(tabBounds.left) >= Math.ceil(tabListWindowBounds.left) && Math.ceil(tabBounds.right) < Math.ceil(tabListWindowBounds.right);
	}
	/**
	 * Helper function to hide and/or show arrows dependent on visible tabs.
	 */


	function _showHideArrow() {
	  var tabListWindowBounds = this.el;
	  var scrollLeftVal = this.scrollElement.scrollLeft;
	  var arrowTarget1 = this.isRTL ? this.arrowNext : this.arrowPrev;
	  var arrowTarget2 = this.isRTL ? this.arrowPrev : this.arrowNext;

	  if (_inTabWindow.bind(this)(this.tabListItems[0], tabListWindowBounds) || !this.isRTL && scrollLeftVal === 0) {
	    arrowTarget1.style.display = 'none';
	    arrowTarget2.style.display = 'block';
	  } else if (_inTabWindow.bind(this)(this.tabListItems[this.tabListItems.length - 1], tabListWindowBounds)) {
	    arrowTarget1.style.display = 'block';
	    arrowTarget2.style.display = 'none';
	  } else {
	    this.arrowNext.style.display = 'block';
	    this.arrowPrev.style.display = 'block';
	  }
	}
	/**
	 * Helper function to keep focus on arrow that is clicked when slider moves.
	 */


	function _onArrowFocus() {
	  var arrowTarget1 = this.isRTL ? this.arrowNext : this.arrowPrev;
	  var arrowTarget2 = this.isRTL ? this.arrowPrev : this.arrowNext;

	  if (this.arrowDirection === Direction$1.LEFT) {
	    if (arrowTarget1.style.display === 'block') {
	      arrowTarget1.focus();
	    } else {
	      arrowTarget2.focus();
	    }
	  } else if (this.arrowDirection === Direction$1.RIGHT) {
	    if (arrowTarget2.style.display === 'block') {
	      arrowTarget2.focus();
	    } else {
	      arrowTarget1.focus();
	    }
	  }
	}
	/**
	 * Event trigger on click to move the slide left or right depending on which arrow has been clicked.
	 * @param {event} event javascript event.
	 * @return {boolean} returns false if target is undefined
	 */


	function _onArrowClick(event) {
	  event.preventDefault();
	  this.isArrowClicked = true;

	  _updateTabWindowWidth.bind(this)(); // check for which arrow has been clicked


	  if (event.target.matches(Selector$e.ARROW_NEXT)) {
	    this.arrowDirection = this.isRTL ? Direction$1.LEFT : Direction$1.RIGHT;
	  } else {
	    this.arrowDirection = this.isRTL ? Direction$1.RIGHT : Direction$1.LEFT;
	  }

	  var slideToTarget = _getSlideToTarget.bind(this)();

	  if (!slideToTarget) {
	    return false;
	  }

	  _setScrollLeft.bind(this)(slideToTarget);
	}
	/**
	 * Set left position of tab window to left position of target element.
	 * @param {node} slideToTarget target element to set left position to.
	 */


	function _setScrollLeft(slideToTarget) {
	  var arrowPadding = parseInt(getComputedStyle(this.arrowPrev).paddingLeft, 10) || parseInt(getComputedStyle(this.arrowNext).paddingLeft, 10);
	  var scrollElementLeft = Math.floor(this.scrollElement.scrollLeft);
	  var slideToTargetVal = Math.floor(_getBiDirectionBoundingRectValue.bind(this)(slideToTarget, true));
	  var scrollElementVal = Math.floor(_getBiDirectionBoundingRectValue.bind(this)(this.scrollElement, true));
	  var scrollAmount;

	  if (this.isRTL) {
	    if (this.arrowDirection === Direction$1.LEFT) {
	      scrollAmount = scrollElementLeft + slideToTargetVal + scrollElementVal + arrowPadding;
	    } else {
	      scrollAmount = scrollElementLeft - slideToTargetVal - scrollElementVal + arrowPadding;
	    }
	  } else {
	    scrollAmount = scrollElementLeft + slideToTargetVal - scrollElementVal - arrowPadding;
	  }

	  try {
	    this.scrollElement.scrollTo({
	      left: scrollAmount,
	      behavior: 'smooth'
	    });
	  } catch (_unused) {
	    this.scrollElement.scrollLeft = scrollAmount;
	  }
	}
	/**
	 * Set left position of tab window to left position of target element.
	 * @return {node} returns node of element to set left position to or undefined if no element is found
	 */


	function _getSlideToTarget() {
	  var tabTarget;
	  var i;
	  var widthRemaining;
	  var tabBounds;
	  var tabListWindowBounds = this.el.getBoundingClientRect();

	  if (this.arrowDirection === Direction$1.RIGHT) {
	    i = this.tabListItems.length;
	    /**
	     * Start at right most tab and decrement until
	     * the first tab not in the tab window is found
	     * */

	    while (i--) {
	      tabBounds = this.tabListItems[i].getBoundingClientRect(); // break if last tab is within tab window

	      if (i === this.tabListItems.length - 1 && _inTabWindow.bind(this)(this.tabListItems[i], this.el)) {
	        break;
	      } // update to track the left most tab within the tab window


	      if (_getBiDirectionBoundingRectValue.bind(this)(this.tabListItems[i], false) >= _getBiDirectionBoundingRectValue.bind(this)(this.el, false)) {
	        tabTarget = this.tabListItems[i]; // update left most tab shown in tab window

	        this.tabSlideTarget.el = tabTarget;
	        this.tabSlideTarget.index = i;
	      } else {
	        break;
	      }
	    }
	  } else {
	    /**
	     * Start at left most tab in tab window, decrement and find
	     * out how many tabs can fit within the tab window.
	     * */
	    i = this.tabSlideTarget.index;
	    widthRemaining = tabListWindowBounds.width;

	    if (i === -1) {
	      return false;
	    }

	    while (i-- && widthRemaining >= 0) {
	      tabBounds = this.tabListItems[i].getBoundingClientRect(); // break if first tab is within tab window

	      if (i === 0 && _inTabWindow.bind(this)(this.tabListItems[i], this.el)) {
	        break;
	      }

	      widthRemaining -= tabBounds.width; // subtract tab width from tab window

	      tabTarget = this.tabListItems[i]; // update left most tab shown in tab window

	      this.tabSlideTarget.el = tabTarget;
	      this.tabSlideTarget.index = i; // break if the tab before this tab element creates a negative value

	      if (this.tabListItems[i - 1] && widthRemaining - this.tabListItems[i - 1].getBoundingClientRect().width < 0) {
	        break;
	      }
	    }
	  }

	  return tabTarget;
	}
	/**
	 * Function to initialize contstructor on load and also handle window resize.
	 * Sets container width, shows/hides arrows depending on visible tabs, and resets
	 * styles when slider is not needed.
	 */


	function _onWindowResize() {
	  // width of tab container - left/right padding
	  var tabContainerWidth = this.el.offsetWidth - parseInt(getComputedStyle(this.el).paddingLeft, 10) * 2;
	  var arrowsStyleDisplay = getComputedStyle(this.arrows).display; // don't do anything if container is large enough to hold tabs

	  if (tabContainerWidth >= this.tabListWidth) {
	    // reset values
	    if (arrowsStyleDisplay === 'block' || this.tabWindow.style.width) {
	      this.arrows.style.display = 'none';
	      this.tabWindow.style.width = ''; // add justify center class if it existed

	      if (this.tabContentCentered) {
	        this.tabGroup.classList.add(ClassName$d.JUSTIFY_CENTER);
	      }
	    }

	    return;
	  } // create container overflow for tabs


	  if (!this.tabWindow.style.width || this.tabWindow.style.width === '') {
	    this.tabWindow.style.width = this.tabListWidthBuffer + 'px'; // align tabs to the left when arrows appear

	    if (this.tabContentCentered) {
	      this.tabGroup.classList.remove(ClassName$d.JUSTIFY_CENTER);
	    } // update tab list and last tab bounds


	    this.tabListItems = this.el.querySelectorAll(Selector$e.DATA_TOGGLE);
	    this.lastTabBounds = this.tabListItems[this.tabListItems.length - 1].getBoundingClientRect();
	  } // show arrows when the right most tab is out of bounds of the container by 40px (arrow width)


	  var lastTabBoundsRightVal = _getBiDirectionBoundingRectValue.bind(this)(this.tabListItems[this.tabListItems.length - 1], false);

	  if (arrowsStyleDisplay === 'none' && lastTabBoundsRightVal - this.arrowOffsetWidth * 2 >= tabContainerWidth) {
	    this.arrows.style.display = 'block';
	  } // hide arrows before shifting left position


	  _showHideArrow.bind(this)();
	}
	/**
	 * Event trigger on focus to move slider position if focused tab is not visible within the tab window.
	 * @param {event}  event javascript event.
	 */


	function _onFocus(event) {
	  if (event.target.matches(Selector$e.DATA_TOGGLE)) {
	    _updateTabWindowWidth.bind(this)();

	    var slideToTarget = event.target;
	    var nodeListArray = [].slice.call(this.tabListItems); // update left most tab shown in tab window

	    this.tabSlideTarget.el = slideToTarget;
	    this.tabSlideTarget.index = nodeListArray.indexOf(slideToTarget);
	  }
	}
	/**
	 * Event trigger on scroll to capture scroll event and move slider if it is triggered by keyboard events: left/right, tab/shift+tab.
	 */


	function _onScroll() {
	  var _this = this;

	  if (this.scrollTimeout !== null) {
	    clearTimeout(this.scrollTimeout);
	  }

	  this.scrollTimeout = setTimeout(function () {
	    _updateTabWindowWidth.bind(_this)();

	    _showHideArrow.bind(_this)(); // focus on the arrow only if an arrow was clicked (prevents keyboard presses from activating arrow focus)


	    if (_this.arrowDirection && (document.activeElement === _this.arrowNext || document.activeElement === _this.arrowPrev)) {
	      _onArrowFocus.bind(_this)();
	    } // prevent scroll event from doing additional variable updates


	    if (_this.isArrowClicked) {
	      _this.isArrowClicked = false;
	      return false;
	    } // update left most tab shown in tab window


	    for (var i = _this.tabSlideTarget.index; i < _this.tabListItems.length; i++) {
	      if (_this.tabListItems[i].getBoundingClientRect().left > 0) {
	        _this.tabSlideTarget.el = _this.tabListItems[i];
	        _this.tabSlideTarget.index = i;
	        break;
	      }
	    }
	  }, 100);
	}
	/**
	 * Helper function to accurately calculate all elements that make up the tab width.
	 * @param {node}  tab  tab element
	 * @return {number} returns tab width value
	 */


	function _getTabWidth(tab) {
	  var eleStyleObj = getComputedStyle(tab);
	  var marginLeft = Math.abs(parseInt(eleStyleObj.marginLeft, 10)) || 0;
	  var marginRight = Math.abs(parseInt(eleStyleObj.marginRight, 10)) || 0;
	  var borderLeft = parseInt(eleStyleObj.borderLeftWidth, 10) || 0;
	  var borderRight = parseInt(eleStyleObj.borderRightWidth, 10) || 0;
	  return tab.offsetWidth + (marginLeft + marginRight) + (borderLeft + borderRight);
	}
	/**
	 * Update tab window width.
	 *
	 * On page load whitespace buffer is created to account for tab widths when letter-spacing increases,
	 * but tab window should be readjusted to remove whitespace
	 */


	function _updateTabWindowWidth() {
	  var _this2 = this;

	  // assumes that letter spacing will be toggled only once before tab interaction
	  if (!this.isTabWindowWidthAdjusted && this.tabWindow.style.width) {
	    this.tabListWidth = 0;
	    this.tabListItems.forEach(function (tab) {
	      _this2.tabListWidth += _getTabWidth(tab);
	    });
	    this.tabListWidthBuffer = this.tabListWidth; // create a buffer of the tab window width

	    this.tabWindow.style.width = this.tabListWidthBuffer + 'px';
	    this.isTabWindowWidthAdjusted = true;
	  }
	}
	/**
	 * Helper function to return left or right rectangle bounding values in LTR vs RTL.
	 * @param {node}  tab  tab element
	 * @param {boolean}  getLeftBoundsValue set to true if you want .left or fase if .right value
	 * @return {number} left or right value of element bounding rectangle
	 */


	function _getBiDirectionBoundingRectValue(element, getLeftBoundsValue) {
	  var tabBounds = element.getBoundingClientRect();

	  if (getLeftBoundsValue) {
	    if (this.isRTL) {
	      var elementStyles = getComputedStyle(element);
	      var borderRight = parseInt(elementStyles.borderRightWidth, 10);
	      var marginRight = parseInt(elementStyles.marginRight, 10);
	      return Math.abs(tabBounds.right + borderRight + marginRight - window.innerWidth);
	    }

	    return tabBounds.left;
	  }

	  if (this.isRTL) {
	    return Math.abs(tabBounds.left - window.innerWidth);
	  }

	  return tabBounds.right;
	}
	/**
	 * Tab slider
	 */


	var TabSlider = /*#__PURE__*/function () {
	  /**
	   * Create the tab slider controls.
	   * @param {Object} opts   The tab slider control options.
	   * @param {node} opts.el The tab slider DOM node.
	   * @param {function} [opts.onPrevArrowClick]  Function to override the previous button click handler.
	   * @param {function} [opts.onNextArrowClick]  Function to override the next button click handler.
	   * @param {function} [opts.onFocusEvent] Function to override the focus event handler.
	   * @param {function} [opts.onScrollEvent] Function to override the scroll event handler.
	   * @param {function} [opts.onWindowResize]  Function to override the resize handler.
	   */
	  function TabSlider(opts) {
	    var _this3 = this;

	    // select control nodes
	    this.el = opts.el;
	    this.tabListItems = this.el.querySelectorAll(Selector$e.DATA_TOGGLE);
	    this.scrollElement = this.el.querySelector(Selector$e.TAB_OVERFLOW);
	    this.tabWindow = this.el.querySelector(Selector$e.TAB_WINDOW);
	    this.tabGroup = this.el.querySelector(Selector$e.TAB_GROUP);
	    this.tabContentCentered = this.tabGroup.classList.contains(ClassName$d.JUSTIFY_CENTER);
	    this.arrows = this.el.querySelector(Selector$e.ARROWS);
	    this.arrowPrev = this.el.querySelector(Selector$e.ARROW_PREV);
	    this.arrowNext = this.el.querySelector(Selector$e.ARROW_NEXT);
	    this.arrowOffsetWidth = parseInt(this.el.querySelector(Selector$e.ARROW_NEXT).dataset.width, 10) || 40; // event controls

	    this.onPrevArrowClick = opts.onPrevArrowClick || _onArrowClick.bind(this);
	    this.onNextArrowClick = opts.onNextArrowClick || _onArrowClick.bind(this);
	    this.onFocusEvent = opts.onFocusEvent || _onFocus.bind(this);
	    this.onScrollEvent = opts.onScrollEvent || _onScroll.bind(this);
	    this.onWindowResize = opts.onWindowResize || _onWindowResize.bind(this); // internal variables

	    this.isRTL = document.dir === 'rtl';
	    this.isTabWindowWidthAdjusted = false;
	    this.isArrowClicked = false;
	    this.arrowDirection = Direction$1.LEFT;
	    this.scrollTimeout = null;
	    this.tabListWidth = 0;
	    this.tabListWidthBuffer = 0; // a11y fix to increase tab list window width to allow for increased letter spacing

	    this.lastTabBounds = this.tabListItems[this.tabListItems.length - 1].getBoundingClientRect(); // keep track of tab that is on the far left of the tab window

	    this.tabSlideTarget = {
	      el: this.isRTL ? this.tabListItems[this.tabListItems.length - 1] : this.tabListItems[0],
	      index: this.isRTL ? this.tabListItems.length - 1 : 0
	    }; // get width of all tabs; include borders and margins

	    this.tabListItems.forEach(function (tab) {
	      _this3.tabListWidth += _getTabWidth(tab);
	    }); // create a buffer of the tab window width on page load

	    this.tabListWidthBuffer = this.tabListWidth * 1.5; // add class name to arrows for mobile only

	    if (Util$1.detectMobile(true)) {
	      this.arrows.classList.add(ClassName$d.MOBILE_ARROWS);
	    } // attach event listeners


	    this.events = [{
	      el: this.arrowPrev,
	      type: Event$d.CLICK_DATA_API,
	      handler: this.onPrevArrowClick
	    }, {
	      el: this.arrowNext,
	      type: Event$d.CLICK_DATA_API,
	      handler: this.onNextArrowClick
	    }, {
	      el: window,
	      type: Event$d.RESIZE_DATA_API,
	      handler: throttle(100, this.onWindowResize)
	    }, {
	      el: this.scrollElement,
	      type: Event$d.SCROLL_DATA_API,
	      handler: throttle(100, this.onScrollEvent)
	    }];

	    for (var _iterator = this.tabListItems, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
	      var _ref;

	      if (_isArray) {
	        if (_i >= _iterator.length) break;
	        _ref = _iterator[_i++];
	      } else {
	        _i = _iterator.next();
	        if (_i.done) break;
	        _ref = _i.value;
	      }

	      var tab = _ref;
	      this.events.push({
	        el: tab,
	        type: Event$d.FOCUS_DATA_API,
	        handler: this.onFocusEvent
	      });
	    }

	    Util$1.addEvents(this.events);
	    tabSliders.push(this); // initialize slider if necessary on load

	    this.onWindowResize(); // Create custom events

	    this[Event$d.ON_SCROLL] = new CustomEvent(Event$d.ON_SCROLL, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$d.ON_REMOVE] = new CustomEvent(Event$d.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	  }
	  /**
	   * Remove event handlers.
	   */


	  var _proto = TabSlider.prototype;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events); // remove this reference from array of instances

	    var index = tabSliders.indexOf(this);
	    tabSliders.splice(index, 1);
	    this.el.dispatchEvent(this[Event$d.ON_REMOVE]);
	  }
	  /**
	   * Go to next tabs
	   */
	  ;

	  _proto.onClickNextArrow = function onClickNextArrow() {
	    this.arrowNext.click();
	    this.el.dispatchEvent(this[Event$d.ON_SCROLL]);
	  }
	  /**
	   * Go to previous tabs
	   */
	  ;

	  _proto.onClickPrevArrow = function onClickPrevArrow() {
	    this.arrowPrev.click();
	    this.el.dispatchEvent(this[Event$d.ON_SCROLL]);
	  }
	  /**
	   * Get instances.
	   * @returns {Object} A object instance
	   */
	  ;

	  TabSlider.getInstances = function getInstances() {
	    return tabSliders;
	  };

	  return TabSlider;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * Data Api implementation
	 * ------------------------------------------------------------------------
	 */


	(function () {
	  Util$1.initializeComponent(Selector$e.DATA_MOUNT, function (node) {
	    new TabSlider({
	      el: node
	    });
	  });
	})();

	var Selector$f = {
	  DATA_TOGGLE: '[data-toggle="showMoreShowLess"]',
	  ELLIPSIS: '.show-more-show-less-ellipsis',
	  TOGGLEABLE_CONTENT: '.show-more-show-less-toggleable-content',
	  CONTAINER: '[data-container="showMoreShowLess"]'
	};
	var ClassName$e = {
	  DISPLAY_NONE: 'd-none'
	};
	var Event$e = {
	  ON_HIDE: 'onHide',
	  ON_SHOW: 'onShow',
	  ON_REMOVE: 'onRemove',
	  ON_UPDATE: 'onUpdate'
	};
	var instances$3 = [];

	function _elOnClick$1() {
	  this.toggle();
	}

	function _toggleableContentOnFocusOut(element) {
	  element.removeAttribute('tabindex');
	}

	var ShowMoreShowLess = /*#__PURE__*/function () {
	  function ShowMoreShowLess(opts) {
	    var _this = this;

	    this.el = opts.el;
	    this.container = this.el.closest(Selector$f.CONTAINER);
	    this.showMoreText = opts.showMoreText || this.el.textContent;
	    this.showLessText = opts.showLessText || this.el.getAttribute('data-show-less');
	    this.ellipsis = this.container.querySelector(Selector$f.ELLIPSIS);
	    this.shown = false;
	    this.events = [{
	      el: this.el,
	      type: 'click',
	      handler: _elOnClick$1.bind(this)
	    }]; // Add mutation observers.

	    this.childObserver = new MutationObserver(function () {
	      _this.update({
	        _self: _this
	      });
	    });
	    this.childObserver.observe(this.container.querySelector(Selector$f.TOGGLEABLE_CONTENT), {
	      childList: true,
	      subtree: true
	    }); // Create custom events.

	    this[Event$e.ON_HIDE] = new CustomEvent(Event$e.ON_HIDE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$e.ON_SHOW] = new CustomEvent(Event$e.ON_SHOW, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$e.ON_REMOVE] = new CustomEvent(Event$e.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$e.ON_UPDATE] = new CustomEvent(Event$e.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this.el.setAttribute('aria-expanded', false);
	    instances$3.push(this);
	  }

	  var _proto = ShowMoreShowLess.prototype;

	  _proto.setFocusToElement = function setFocusToElement(element) {
	    element.setAttribute('tabindex', 0);
	    document.activeElement.blur();
	    element.focus();
	  };

	  _proto.show = function show() {
	    this.shown = true;

	    if (this.ellipsis) {
	      this.ellipsis.classList.add(ClassName$e.DISPLAY_NONE);
	    }

	    this.el.setAttribute('aria-expanded', true);
	    this.el.textContent = this.showLessText;
	    this.el.dispatchEvent(this[Event$e.ON_SHOW]);
	  };

	  _proto.hide = function hide() {
	    this.shown = false;

	    if (this.ellipsis) {
	      this.ellipsis.classList.remove(ClassName$e.DISPLAY_NONE);
	    }

	    this.el.setAttribute('aria-expanded', false);
	    this.el.textContent = this.showMoreText;
	    this.el.dispatchEvent(this[Event$e.ON_HIDE]);
	  };

	  _proto.toggle = function toggle() {
	    if (this.shown) {
	      this.hide();
	    } else {
	      this.show();
	    }
	  } // Removes event listeners
	  ;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events);
	    this.childObserver.disconnect(); // Remove this reference from the array of instances.

	    var index = instances$3.indexOf(this);
	    instances$3.splice(index, 1);
	    this.el.dispatchEvent(this[Event$e.ON_REMOVE]);
	  };

	  ShowMoreShowLess.getInstances = function getInstances() {
	    return instances$3;
	  };

	  ShowMoreShowLess.generate = function generate(opts) {
	    if (opts.el.hasAttribute('data-count')) {
	      new ShowMoreShowLessMultiElement({
	        el: opts.el,
	        hideAfter: opts.el.getAttribute('data-count')
	      });
	    } else {
	      new ShowMoreShowLessSingleElement({
	        el: opts.el
	      });
	    }
	  };

	  return ShowMoreShowLess;
	}();

	var ShowMoreShowLessSingleElement = /*#__PURE__*/function (_ShowMoreShowLess) {
	  _inheritsLoose(ShowMoreShowLessSingleElement, _ShowMoreShowLess);

	  function ShowMoreShowLessSingleElement(opts) {
	    var _this2;

	    _this2 = _ShowMoreShowLess.call(this, opts) || this;
	    _this2.toggleableContent = _this2.container.querySelector(Selector$f.TOGGLEABLE_CONTENT);

	    _this2.events.push({
	      el: _this2.toggleableContent,
	      type: 'focusout',
	      handler: _toggleableContentOnFocusOut.bind(null, _this2.toggleableContent)
	    });

	    Util$1.addEvents(_this2.events);

	    _this2.toggleableContent.setAttribute('tabindex', -1);

	    _this2.toggleableContent.classList.add(ClassName$e.DISPLAY_NONE);

	    return _this2;
	  }

	  var _proto2 = ShowMoreShowLessSingleElement.prototype;

	  _proto2.show = function show() {
	    _ShowMoreShowLess.prototype.show.call(this);

	    this.toggleableContent.classList.remove(ClassName$e.DISPLAY_NONE);

	    if (this.ellipsis) {
	      this.ellipsis.classList.add(ClassName$e.DISPLAY_NONE);
	    }

	    _ShowMoreShowLess.prototype.setFocusToElement.call(this, this.toggleableContent);
	  };

	  _proto2.hide = function hide() {
	    _ShowMoreShowLess.prototype.hide.call(this);

	    this.toggleableContent.classList.add(ClassName$e.DISPLAY_NONE);

	    if (this.ellipsis) {
	      this.ellipsis.classList.remove(ClassName$e.DISPLAY_NONE);
	    }

	    this.toggleableContent.setAttribute('tabindex', -1);
	  };

	  return ShowMoreShowLessSingleElement;
	}(ShowMoreShowLess);

	var ShowMoreShowLessMultiElement = /*#__PURE__*/function (_ShowMoreShowLess2) {
	  _inheritsLoose(ShowMoreShowLessMultiElement, _ShowMoreShowLess2);

	  function ShowMoreShowLessMultiElement(opts) {
	    var _this3;

	    _this3 = _ShowMoreShowLess2.call(this, opts) || this;
	    _this3.hideAfter = opts.hideAfter || null;

	    _this3.setChildren();

	    var focusOuttarget = _this3.toggleableContent[0];

	    _this3.events.push({
	      el: _this3.toggleableContent[0],
	      type: 'focusout',
	      handler: _toggleableContentOnFocusOut.bind(null, focusOuttarget)
	    });

	    Util$1.addEvents(_this3.events); // Set attributes on html elements
	    // Tabindex set to -1 so content can be focused when shown

	    _this3.toggleableContent[0].setAttribute('tabindex', -1); // Set default state to hidden


	    _this3.toggleableContent.forEach(function (node) {
	      node.classList.add(ClassName$e.DISPLAY_NONE);
	    });

	    return _this3;
	  }

	  var _proto3 = ShowMoreShowLessMultiElement.prototype;

	  _proto3.setChildren = function setChildren() {
	    this.visibleContent = this.container.querySelectorAll(Selector$f.TOGGLEABLE_CONTENT + ' > :nth-child(-n+' + (this.hideAfter - 1) + ')');
	    this.toggleableContent = this.container.querySelectorAll(Selector$f.TOGGLEABLE_CONTENT + ' > :nth-child(n+' + this.hideAfter + ')');
	  };

	  _proto3.show = function show() {
	    _ShowMoreShowLess2.prototype.show.call(this);

	    this.toggleableContent.forEach(function (node) {
	      node.classList.remove(ClassName$e.DISPLAY_NONE);
	    });

	    if (this.toggleableContent) {
	      _ShowMoreShowLess2.prototype.setFocusToElement.call(this, this.toggleableContent[0]);
	    }
	  };

	  _proto3.hide = function hide() {
	    _ShowMoreShowLess2.prototype.hide.call(this);

	    if (this.toggleableContent.length > 0) {
	      this.toggleableContent.forEach(function (node) {
	        node.classList.add(ClassName$e.DISPLAY_NONE);
	      });
	      this.toggleableContent[0].setAttribute('tabindex', -1);
	    }
	  };

	  _proto3.update = function update(opts) {
	    if (opts === void 0) {
	      opts = {};
	    }

	    var _self = opts._self || this;

	    _self.setChildren();

	    _self.visibleContent.forEach(function (node) {
	      if (node.classList.contains(ClassName$e.DISPLAY_NONE)) {
	        node.classList.remove(ClassName$e.DISPLAY_NONE);
	      }

	      if (node.hasAttribute('tabindex')) {
	        node.removeAttribute('tabindex');
	      }
	    });

	    if (_self.toggleableContent.length > 0) {
	      _self.hide();
	    }

	    if (_self.toggleableContent.length > 1) {
	      var hasTabIndex = false;

	      _self.toggleableContent.forEach(function (node) {
	        if (hasTabIndex) {
	          node.removeAttribute('tabindex');
	        }

	        if (node.hasAttribute('tabindex')) {
	          hasTabIndex = true;
	        }
	      });
	    }

	    if (_self.toggleableContent.length === 0 && !_self.el.classList.contains(ClassName$e.DISPLAY_NONE)) {
	      _self.el.classList.add(ClassName$e.DISPLAY_NONE);
	    }

	    if (_self.toggleableContent.length > 0 && _self.el.classList.contains(ClassName$e.DISPLAY_NONE)) {
	      _self.el.classList.remove(ClassName$e.DISPLAY_NONE);
	    }

	    _self.el.dispatchEvent(this[Event$e.ON_UPDATE]);
	  };

	  return ShowMoreShowLessMultiElement;
	}(ShowMoreShowLess);

	(function () {
	  Util$1.initializeComponent(Selector$f.DATA_TOGGLE, function (element) {
	    ShowMoreShowLess.generate({
	      el: element
	    });
	  });
	})();

	/**
	 * ------------------------------------------------------------------------
	 * Constants
	 * ------------------------------------------------------------------------
	 */

	var NAME$9 = 'toast';
	var VERSION$5 = '4.4.1';
	var DATA_KEY$7 = 'bs.toast';
	var EVENT_KEY$6 = "." + DATA_KEY$7;
	var JQUERY_NO_CONFLICT$7 = $.fn[NAME$9];
	var Event$f = {
	  CLICK_DISMISS: "click.dismiss" + EVENT_KEY$6,
	  HIDE: "hide" + EVENT_KEY$6,
	  HIDDEN: "hidden" + EVENT_KEY$6,
	  SHOW: "show" + EVENT_KEY$6,
	  SHOWN: "shown" + EVENT_KEY$6
	};
	var ClassName$f = {
	  FADE: 'fade',
	  HIDE: 'hide',
	  SHOW: 'show',
	  SHOWING: 'showing'
	};
	var DefaultType$7 = {
	  animation: 'boolean',
	  autohide: 'boolean',
	  delay: 'number'
	};
	var Default$7 = {
	  animation: true,
	  autohide: true,
	  delay: 500
	};
	var Selector$g = {
	  DATA_DISMISS: '[data-dismiss="toast"]'
	};
	/**
	 * ------------------------------------------------------------------------
	 * Class Definition
	 * ------------------------------------------------------------------------
	 */

	var Toast = /*#__PURE__*/function () {
	  function Toast(element, config) {
	    this._element = element;
	    this._config = this._getConfig(config);
	    this._timeout = null;

	    this._setListeners();
	  } // Getters


	  var _proto = Toast.prototype;

	  // Public
	  _proto.show = function show() {
	    var _this = this;

	    var showEvent = $.Event(Event$f.SHOW);
	    $(this._element).trigger(showEvent);

	    if (showEvent.isDefaultPrevented()) {
	      return;
	    }

	    if (this._config.animation) {
	      this._element.classList.add(ClassName$f.FADE);
	    }

	    var complete = function complete() {
	      _this._element.classList.remove(ClassName$f.SHOWING);

	      _this._element.classList.add(ClassName$f.SHOW);

	      $(_this._element).trigger(Event$f.SHOWN);

	      if (_this._config.autohide) {
	        _this._timeout = setTimeout(function () {
	          _this.hide();
	        }, _this._config.delay);
	      }
	    };

	    this._element.classList.remove(ClassName$f.HIDE);

	    Util.reflow(this._element);

	    this._element.classList.add(ClassName$f.SHOWING);

	    if (this._config.animation) {
	      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
	      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	    } else {
	      complete();
	    }
	  };

	  _proto.hide = function hide() {
	    if (!this._element.classList.contains(ClassName$f.SHOW)) {
	      return;
	    }

	    var hideEvent = $.Event(Event$f.HIDE);
	    $(this._element).trigger(hideEvent);

	    if (hideEvent.isDefaultPrevented()) {
	      return;
	    }

	    this._close();
	  };

	  _proto.dispose = function dispose() {
	    clearTimeout(this._timeout);
	    this._timeout = null;

	    if (this._element.classList.contains(ClassName$f.SHOW)) {
	      this._element.classList.remove(ClassName$f.SHOW);
	    }

	    $(this._element).off(Event$f.CLICK_DISMISS);
	    $.removeData(this._element, DATA_KEY$7);
	    this._element = null;
	    this._config = null;
	  } // Private
	  ;

	  _proto._getConfig = function _getConfig(config) {
	    config = Object.assign({}, Default$7, {}, $(this._element).data(), {}, typeof config === 'object' && config ? config : {});
	    Util.typeCheckConfig(NAME$9, config, this.constructor.DefaultType);
	    return config;
	  };

	  _proto._setListeners = function _setListeners() {
	    var _this2 = this;

	    $(this._element).on(Event$f.CLICK_DISMISS, Selector$g.DATA_DISMISS, function () {
	      return _this2.hide();
	    });
	  };

	  _proto._close = function _close() {
	    var _this3 = this;

	    var complete = function complete() {
	      _this3._element.classList.add(ClassName$f.HIDE);

	      $(_this3._element).trigger(Event$f.HIDDEN);
	    };

	    this._element.classList.remove(ClassName$f.SHOW);

	    if (this._config.animation) {
	      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
	      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
	    } else {
	      complete();
	    }
	  } // Static
	  ;

	  Toast._jQueryInterface = function _jQueryInterface(config) {
	    return this.each(function () {
	      var $element = $(this);
	      var data = $element.data(DATA_KEY$7);

	      var _config = typeof config === 'object' && config;

	      if (!data) {
	        data = new Toast(this, _config);
	        $element.data(DATA_KEY$7, data);
	      }

	      if (typeof config === 'string') {
	        if (typeof data[config] === 'undefined') {
	          throw new TypeError("No method named \"" + config + "\"");
	        }

	        data[config](this);
	      }
	    });
	  };

	  _createClass(Toast, null, [{
	    key: "VERSION",
	    get: function get() {
	      return VERSION$5;
	    }
	  }, {
	    key: "DefaultType",
	    get: function get() {
	      return DefaultType$7;
	    }
	  }, {
	    key: "Default",
	    get: function get() {
	      return Default$7;
	    }
	  }]);

	  return Toast;
	}();
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */


	$.fn[NAME$9] = Toast._jQueryInterface;
	$.fn[NAME$9].Constructor = Toast;

	$.fn[NAME$9].noConflict = function () {
	  $.fn[NAME$9] = JQUERY_NO_CONFLICT$7;
	  return Toast._jQueryInterface;
	};

	var Debug = {
	  focusedElement: function focusedElement() {
	    document.addEventListener('focus', function () {
	      /* eslint-disable-next-line no-console */
	      console.log('focused', document.activeElement);
	    }, true);
	  }
	};

	var nativePromiseConstructor = global_1.Promise;

	var engineIsIos = /(iphone|ipod|ipad).*applewebkit/i.test(engineUserAgent);

	var location = global_1.location;
	var set$1 = global_1.setImmediate;
	var clear = global_1.clearImmediate;
	var process$1 = global_1.process;
	var MessageChannel = global_1.MessageChannel;
	var Dispatch = global_1.Dispatch;
	var counter = 0;
	var queue = {};
	var ONREADYSTATECHANGE = 'onreadystatechange';
	var defer, channel, port;

	var run = function (id) {
	  // eslint-disable-next-line no-prototype-builtins
	  if (queue.hasOwnProperty(id)) {
	    var fn = queue[id];
	    delete queue[id];
	    fn();
	  }
	};

	var runner = function (id) {
	  return function () {
	    run(id);
	  };
	};

	var listener = function (event) {
	  run(event.data);
	};

	var post = function (id) {
	  // old engines have not location.origin
	  global_1.postMessage(id + '', location.protocol + '//' + location.host);
	};

	// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
	if (!set$1 || !clear) {
	  set$1 = function setImmediate(fn) {
	    var args = [];
	    var i = 1;
	    while (arguments.length > i) args.push(arguments[i++]);
	    queue[++counter] = function () {
	      // eslint-disable-next-line no-new-func
	      (typeof fn == 'function' ? fn : Function(fn)).apply(undefined, args);
	    };
	    defer(counter);
	    return counter;
	  };
	  clear = function clearImmediate(id) {
	    delete queue[id];
	  };
	  // Node.js 0.8-
	  if (classofRaw(process$1) == 'process') {
	    defer = function (id) {
	      process$1.nextTick(runner(id));
	    };
	  // Sphere (JS game engine) Dispatch API
	  } else if (Dispatch && Dispatch.now) {
	    defer = function (id) {
	      Dispatch.now(runner(id));
	    };
	  // Browsers with MessageChannel, includes WebWorkers
	  // except iOS - https://github.com/zloirock/core-js/issues/624
	  } else if (MessageChannel && !engineIsIos) {
	    channel = new MessageChannel();
	    port = channel.port2;
	    channel.port1.onmessage = listener;
	    defer = functionBindContext(port.postMessage, port, 1);
	  // Browsers with postMessage, skip WebWorkers
	  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
	  } else if (global_1.addEventListener && typeof postMessage == 'function' && !global_1.importScripts && !fails(post)) {
	    defer = post;
	    global_1.addEventListener('message', listener, false);
	  // IE8-
	  } else if (ONREADYSTATECHANGE in documentCreateElement('script')) {
	    defer = function (id) {
	      html.appendChild(documentCreateElement('script'))[ONREADYSTATECHANGE] = function () {
	        html.removeChild(this);
	        run(id);
	      };
	    };
	  // Rest old browsers
	  } else {
	    defer = function (id) {
	      setTimeout(runner(id), 0);
	    };
	  }
	}

	var task = {
	  set: set$1,
	  clear: clear
	};

	var getOwnPropertyDescriptor$3 = objectGetOwnPropertyDescriptor.f;

	var macrotask = task.set;


	var MutationObserver$1 = global_1.MutationObserver || global_1.WebKitMutationObserver;
	var process$2 = global_1.process;
	var Promise = global_1.Promise;
	var IS_NODE = classofRaw(process$2) == 'process';
	// Node.js 11 shows ExperimentalWarning on getting `queueMicrotask`
	var queueMicrotaskDescriptor = getOwnPropertyDescriptor$3(global_1, 'queueMicrotask');
	var queueMicrotask = queueMicrotaskDescriptor && queueMicrotaskDescriptor.value;

	var flush, head, last, notify, toggle, node, promise, then;

	// modern engines have queueMicrotask method
	if (!queueMicrotask) {
	  flush = function () {
	    var parent, fn;
	    if (IS_NODE && (parent = process$2.domain)) parent.exit();
	    while (head) {
	      fn = head.fn;
	      head = head.next;
	      try {
	        fn();
	      } catch (error) {
	        if (head) notify();
	        else last = undefined;
	        throw error;
	      }
	    } last = undefined;
	    if (parent) parent.enter();
	  };

	  // Node.js
	  if (IS_NODE) {
	    notify = function () {
	      process$2.nextTick(flush);
	    };
	  // browsers with MutationObserver, except iOS - https://github.com/zloirock/core-js/issues/339
	  } else if (MutationObserver$1 && !engineIsIos) {
	    toggle = true;
	    node = document.createTextNode('');
	    new MutationObserver$1(flush).observe(node, { characterData: true });
	    notify = function () {
	      node.data = toggle = !toggle;
	    };
	  // environments with maybe non-completely correct, but existent Promise
	  } else if (Promise && Promise.resolve) {
	    // Promise.resolve without an argument throws an error in LG WebOS 2
	    promise = Promise.resolve(undefined);
	    then = promise.then;
	    notify = function () {
	      then.call(promise, flush);
	    };
	  // for other environments - macrotask based on:
	  // - setImmediate
	  // - MessageChannel
	  // - window.postMessag
	  // - onreadystatechange
	  // - setTimeout
	  } else {
	    notify = function () {
	      // strange IE + webpack dev server bug - use .call(global)
	      macrotask.call(global_1, flush);
	    };
	  }
	}

	var microtask = queueMicrotask || function (fn) {
	  var task = { fn: fn, next: undefined };
	  if (last) last.next = task;
	  if (!head) {
	    head = task;
	    notify();
	  } last = task;
	};

	var PromiseCapability = function (C) {
	  var resolve, reject;
	  this.promise = new C(function ($$resolve, $$reject) {
	    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
	    resolve = $$resolve;
	    reject = $$reject;
	  });
	  this.resolve = aFunction$1(resolve);
	  this.reject = aFunction$1(reject);
	};

	// 25.4.1.5 NewPromiseCapability(C)
	var f$7 = function (C) {
	  return new PromiseCapability(C);
	};

	var newPromiseCapability = {
		f: f$7
	};

	var promiseResolve = function (C, x) {
	  anObject(C);
	  if (isObject(x) && x.constructor === C) return x;
	  var promiseCapability = newPromiseCapability.f(C);
	  var resolve = promiseCapability.resolve;
	  resolve(x);
	  return promiseCapability.promise;
	};

	var hostReportErrors = function (a, b) {
	  var console = global_1.console;
	  if (console && console.error) {
	    arguments.length === 1 ? console.error(a) : console.error(a, b);
	  }
	};

	var perform = function (exec) {
	  try {
	    return { error: false, value: exec() };
	  } catch (error) {
	    return { error: true, value: error };
	  }
	};

	var task$1 = task.set;










	var SPECIES$6 = wellKnownSymbol('species');
	var PROMISE = 'Promise';
	var getInternalState$3 = internalState.get;
	var setInternalState$5 = internalState.set;
	var getInternalPromiseState = internalState.getterFor(PROMISE);
	var PromiseConstructor = nativePromiseConstructor;
	var TypeError$1 = global_1.TypeError;
	var document$2 = global_1.document;
	var process$3 = global_1.process;
	var $fetch = getBuiltIn('fetch');
	var newPromiseCapability$1 = newPromiseCapability.f;
	var newGenericPromiseCapability = newPromiseCapability$1;
	var IS_NODE$1 = classofRaw(process$3) == 'process';
	var DISPATCH_EVENT = !!(document$2 && document$2.createEvent && global_1.dispatchEvent);
	var UNHANDLED_REJECTION = 'unhandledrejection';
	var REJECTION_HANDLED = 'rejectionhandled';
	var PENDING = 0;
	var FULFILLED = 1;
	var REJECTED = 2;
	var HANDLED = 1;
	var UNHANDLED = 2;
	var Internal, OwnPromiseCapability, PromiseWrapper, nativeThen;

	var FORCED$5 = isForced_1(PROMISE, function () {
	  var GLOBAL_CORE_JS_PROMISE = inspectSource(PromiseConstructor) !== String(PromiseConstructor);
	  if (!GLOBAL_CORE_JS_PROMISE) {
	    // V8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
	    // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
	    // We can't detect it synchronously, so just check versions
	    if (engineV8Version === 66) return true;
	    // Unhandled rejections tracking support, NodeJS Promise without it fails @@species test
	    if (!IS_NODE$1 && typeof PromiseRejectionEvent != 'function') return true;
	  }
	  // We can't use @@species feature detection in V8 since it causes
	  // deoptimization and performance degradation
	  // https://github.com/zloirock/core-js/issues/679
	  if (engineV8Version >= 51 && /native code/.test(PromiseConstructor)) return false;
	  // Detect correctness of subclassing with @@species support
	  var promise = PromiseConstructor.resolve(1);
	  var FakePromise = function (exec) {
	    exec(function () { /* empty */ }, function () { /* empty */ });
	  };
	  var constructor = promise.constructor = {};
	  constructor[SPECIES$6] = FakePromise;
	  return !(promise.then(function () { /* empty */ }) instanceof FakePromise);
	});

	var INCORRECT_ITERATION = FORCED$5 || !checkCorrectnessOfIteration(function (iterable) {
	  PromiseConstructor.all(iterable)['catch'](function () { /* empty */ });
	});

	// helpers
	var isThenable = function (it) {
	  var then;
	  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
	};

	var notify$1 = function (promise, state, isReject) {
	  if (state.notified) return;
	  state.notified = true;
	  var chain = state.reactions;
	  microtask(function () {
	    var value = state.value;
	    var ok = state.state == FULFILLED;
	    var index = 0;
	    // variable length - can't use forEach
	    while (chain.length > index) {
	      var reaction = chain[index++];
	      var handler = ok ? reaction.ok : reaction.fail;
	      var resolve = reaction.resolve;
	      var reject = reaction.reject;
	      var domain = reaction.domain;
	      var result, then, exited;
	      try {
	        if (handler) {
	          if (!ok) {
	            if (state.rejection === UNHANDLED) onHandleUnhandled(promise, state);
	            state.rejection = HANDLED;
	          }
	          if (handler === true) result = value;
	          else {
	            if (domain) domain.enter();
	            result = handler(value); // can throw
	            if (domain) {
	              domain.exit();
	              exited = true;
	            }
	          }
	          if (result === reaction.promise) {
	            reject(TypeError$1('Promise-chain cycle'));
	          } else if (then = isThenable(result)) {
	            then.call(result, resolve, reject);
	          } else resolve(result);
	        } else reject(value);
	      } catch (error) {
	        if (domain && !exited) domain.exit();
	        reject(error);
	      }
	    }
	    state.reactions = [];
	    state.notified = false;
	    if (isReject && !state.rejection) onUnhandled(promise, state);
	  });
	};

	var dispatchEvent = function (name, promise, reason) {
	  var event, handler;
	  if (DISPATCH_EVENT) {
	    event = document$2.createEvent('Event');
	    event.promise = promise;
	    event.reason = reason;
	    event.initEvent(name, false, true);
	    global_1.dispatchEvent(event);
	  } else event = { promise: promise, reason: reason };
	  if (handler = global_1['on' + name]) handler(event);
	  else if (name === UNHANDLED_REJECTION) hostReportErrors('Unhandled promise rejection', reason);
	};

	var onUnhandled = function (promise, state) {
	  task$1.call(global_1, function () {
	    var value = state.value;
	    var IS_UNHANDLED = isUnhandled(state);
	    var result;
	    if (IS_UNHANDLED) {
	      result = perform(function () {
	        if (IS_NODE$1) {
	          process$3.emit('unhandledRejection', value, promise);
	        } else dispatchEvent(UNHANDLED_REJECTION, promise, value);
	      });
	      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
	      state.rejection = IS_NODE$1 || isUnhandled(state) ? UNHANDLED : HANDLED;
	      if (result.error) throw result.value;
	    }
	  });
	};

	var isUnhandled = function (state) {
	  return state.rejection !== HANDLED && !state.parent;
	};

	var onHandleUnhandled = function (promise, state) {
	  task$1.call(global_1, function () {
	    if (IS_NODE$1) {
	      process$3.emit('rejectionHandled', promise);
	    } else dispatchEvent(REJECTION_HANDLED, promise, state.value);
	  });
	};

	var bind = function (fn, promise, state, unwrap) {
	  return function (value) {
	    fn(promise, state, value, unwrap);
	  };
	};

	var internalReject = function (promise, state, value, unwrap) {
	  if (state.done) return;
	  state.done = true;
	  if (unwrap) state = unwrap;
	  state.value = value;
	  state.state = REJECTED;
	  notify$1(promise, state, true);
	};

	var internalResolve = function (promise, state, value, unwrap) {
	  if (state.done) return;
	  state.done = true;
	  if (unwrap) state = unwrap;
	  try {
	    if (promise === value) throw TypeError$1("Promise can't be resolved itself");
	    var then = isThenable(value);
	    if (then) {
	      microtask(function () {
	        var wrapper = { done: false };
	        try {
	          then.call(value,
	            bind(internalResolve, promise, wrapper, state),
	            bind(internalReject, promise, wrapper, state)
	          );
	        } catch (error) {
	          internalReject(promise, wrapper, error, state);
	        }
	      });
	    } else {
	      state.value = value;
	      state.state = FULFILLED;
	      notify$1(promise, state, false);
	    }
	  } catch (error) {
	    internalReject(promise, { done: false }, error, state);
	  }
	};

	// constructor polyfill
	if (FORCED$5) {
	  // 25.4.3.1 Promise(executor)
	  PromiseConstructor = function Promise(executor) {
	    anInstance(this, PromiseConstructor, PROMISE);
	    aFunction$1(executor);
	    Internal.call(this);
	    var state = getInternalState$3(this);
	    try {
	      executor(bind(internalResolve, this, state), bind(internalReject, this, state));
	    } catch (error) {
	      internalReject(this, state, error);
	    }
	  };
	  // eslint-disable-next-line no-unused-vars
	  Internal = function Promise(executor) {
	    setInternalState$5(this, {
	      type: PROMISE,
	      done: false,
	      notified: false,
	      parent: false,
	      reactions: [],
	      rejection: false,
	      state: PENDING,
	      value: undefined
	    });
	  };
	  Internal.prototype = redefineAll(PromiseConstructor.prototype, {
	    // `Promise.prototype.then` method
	    // https://tc39.github.io/ecma262/#sec-promise.prototype.then
	    then: function then(onFulfilled, onRejected) {
	      var state = getInternalPromiseState(this);
	      var reaction = newPromiseCapability$1(speciesConstructor(this, PromiseConstructor));
	      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
	      reaction.fail = typeof onRejected == 'function' && onRejected;
	      reaction.domain = IS_NODE$1 ? process$3.domain : undefined;
	      state.parent = true;
	      state.reactions.push(reaction);
	      if (state.state != PENDING) notify$1(this, state, false);
	      return reaction.promise;
	    },
	    // `Promise.prototype.catch` method
	    // https://tc39.github.io/ecma262/#sec-promise.prototype.catch
	    'catch': function (onRejected) {
	      return this.then(undefined, onRejected);
	    }
	  });
	  OwnPromiseCapability = function () {
	    var promise = new Internal();
	    var state = getInternalState$3(promise);
	    this.promise = promise;
	    this.resolve = bind(internalResolve, promise, state);
	    this.reject = bind(internalReject, promise, state);
	  };
	  newPromiseCapability.f = newPromiseCapability$1 = function (C) {
	    return C === PromiseConstructor || C === PromiseWrapper
	      ? new OwnPromiseCapability(C)
	      : newGenericPromiseCapability(C);
	  };

	  if ( typeof nativePromiseConstructor == 'function') {
	    nativeThen = nativePromiseConstructor.prototype.then;

	    // wrap native Promise#then for native async functions
	    redefine(nativePromiseConstructor.prototype, 'then', function then(onFulfilled, onRejected) {
	      var that = this;
	      return new PromiseConstructor(function (resolve, reject) {
	        nativeThen.call(that, resolve, reject);
	      }).then(onFulfilled, onRejected);
	    // https://github.com/zloirock/core-js/issues/640
	    }, { unsafe: true });

	    // wrap fetch result
	    if (typeof $fetch == 'function') _export({ global: true, enumerable: true, forced: true }, {
	      // eslint-disable-next-line no-unused-vars
	      fetch: function fetch(input /* , init */) {
	        return promiseResolve(PromiseConstructor, $fetch.apply(global_1, arguments));
	      }
	    });
	  }
	}

	_export({ global: true, wrap: true, forced: FORCED$5 }, {
	  Promise: PromiseConstructor
	});

	setToStringTag(PromiseConstructor, PROMISE, false);
	setSpecies(PROMISE);

	PromiseWrapper = getBuiltIn(PROMISE);

	// statics
	_export({ target: PROMISE, stat: true, forced: FORCED$5 }, {
	  // `Promise.reject` method
	  // https://tc39.github.io/ecma262/#sec-promise.reject
	  reject: function reject(r) {
	    var capability = newPromiseCapability$1(this);
	    capability.reject.call(undefined, r);
	    return capability.promise;
	  }
	});

	_export({ target: PROMISE, stat: true, forced:  FORCED$5 }, {
	  // `Promise.resolve` method
	  // https://tc39.github.io/ecma262/#sec-promise.resolve
	  resolve: function resolve(x) {
	    return promiseResolve( this, x);
	  }
	});

	_export({ target: PROMISE, stat: true, forced: INCORRECT_ITERATION }, {
	  // `Promise.all` method
	  // https://tc39.github.io/ecma262/#sec-promise.all
	  all: function all(iterable) {
	    var C = this;
	    var capability = newPromiseCapability$1(C);
	    var resolve = capability.resolve;
	    var reject = capability.reject;
	    var result = perform(function () {
	      var $promiseResolve = aFunction$1(C.resolve);
	      var values = [];
	      var counter = 0;
	      var remaining = 1;
	      iterate_1(iterable, function (promise) {
	        var index = counter++;
	        var alreadyCalled = false;
	        values.push(undefined);
	        remaining++;
	        $promiseResolve.call(C, promise).then(function (value) {
	          if (alreadyCalled) return;
	          alreadyCalled = true;
	          values[index] = value;
	          --remaining || resolve(values);
	        }, reject);
	      });
	      --remaining || resolve(values);
	    });
	    if (result.error) reject(result.value);
	    return capability.promise;
	  },
	  // `Promise.race` method
	  // https://tc39.github.io/ecma262/#sec-promise.race
	  race: function race(iterable) {
	    var C = this;
	    var capability = newPromiseCapability$1(C);
	    var reject = capability.reject;
	    var result = perform(function () {
	      var $promiseResolve = aFunction$1(C.resolve);
	      iterate_1(iterable, function (promise) {
	        $promiseResolve.call(C, promise).then(capability.resolve, reject);
	      });
	    });
	    if (result.error) reject(result.value);
	    return capability.promise;
	  }
	});

	var locationData = [{
	  title: 'Alberta',
	  abbreviation: 'AB'
	}, {
	  title: 'British Columbia',
	  abbreviation: 'BC'
	}, {
	  title: 'Manitoba',
	  abbreviation: 'MB'
	}, {
	  title: 'New Brunswick',
	  abbreviation: 'NB'
	}, {
	  title: 'Newfoundland and Labrador',
	  abbreviation: 'NL'
	}, {
	  title: 'Nova Scotia',
	  abbreviation: 'NS'
	}, {
	  title: 'Northwest Territories',
	  abbreviation: 'NT'
	}, {
	  title: 'Nunavut',
	  abbreviation: 'NU'
	}, {
	  title: 'Ontario',
	  abbreviation: 'ON'
	}, {
	  title: 'Prince Edward Island',
	  abbreviation: 'PE'
	}, {
	  title: 'Quebec',
	  abbreviation: 'QC'
	}, {
	  title: 'Saskatchewan',
	  abbreviation: 'SK'
	}, {
	  title: 'Yukon',
	  abbreviation: 'YT'
	}];

	/*
	 * @constant
	 * @type {string}
	 */

	var Config = {
	  DEFAULT_SEARCH_RESULT: 10
	};
	var SEARCH_INSTANCES = [];
	var CUSTOM_MSG = 'results are available, use up and down arrow keys to navigate'; // default message is set, if custom message not set.

	var Event$g = {
	  ON_CLOSE: 'onClose',
	  ON_OPEN: 'onOpen',
	  ON_UPDATE: 'onUpdate',
	  ON_REMOVE: 'onRemove'
	};
	/*
	 * Class representing a Autocomplete.
	 */

	var AutoComplete = /*#__PURE__*/function () {
	  /*
	   * Create the Autocomplete.
	   * @param {Object} opts - The autocomplete options.
	   * @param {Node} opts.target - The autocomplete DOM node.
	   */
	  function AutoComplete(opts) {
	    this.container = opts.target;
	    this.customMessage = opts.msg || opts.target.getAttribute('data-message') || CUSTOM_MSG;
	    this.filter = opts.filter || opts.target.getAttribute('data-filter') || false;
	    this.suggestedData = opts.data || opts.target.getAttribute('data-suggestions') || locationData;
	    this.target = opts.target.querySelector('.result-list');
	    this.searchResultsContainer = this.container.querySelector('.search-results-container');
	    this.searchInput = this.container.querySelector('.search-input');

	    this._init();

	    SEARCH_INSTANCES.push(this); // Create custom events.

	    this[Event$g.ON_CLOSE] = new CustomEvent(Event$g.ON_CLOSE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$g.ON_OPEN] = new CustomEvent(Event$g.ON_OPEN, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$g.ON_UPDATE] = new CustomEvent(Event$g.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$g.ON_REMOVE] = new CustomEvent(Event$g.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	  }
	  /*
	   * Initiatlize the set of events.
	   */


	  var _proto = AutoComplete.prototype;

	  _proto._init = function _init() {
	    this._validate = this._validate.bind(this);
	    this._selectResult = this._selectResult.bind(this);
	    this._selectResultKey = this._selectResultKey.bind(this);
	    this._closeSearchList = this._closeSearchList.bind(this);
	    this.searchInput.addEventListener('input', this._validate);
	    this.target.addEventListener('click', this._selectResult);
	    document.addEventListener('keydown', this._selectResultKey);
	    document.addEventListener('click', this._closeSearchList);
	    this.target.classList.remove('active');
	  }
	  /*
	   * Get an array of autocomplete instances.
	   * @returns {Object[]} Array of search instances.
	   */
	  ;

	  AutoComplete.getInstances = function getInstances() {
	    return SEARCH_INSTANCES;
	  }
	  /*
	   * Show the suggested list.
	   */
	  ;

	  _proto.open = function open() {
	    var _target = this.target;

	    _target.classList.add('active');

	    this.searchInput.setAttribute('aria-expanded', 'true');

	    this._populateSelect();

	    this.container.dispatchEvent(this[Event$g.ON_OPEN]);
	  }
	  /*
	   * close the suggested list.
	   */
	  ;

	  _proto.close = function close() {
	    var _target = this.target;

	    _target.classList.remove('active');

	    this.searchInput.setAttribute('aria-expanded', 'false');
	    this.container.dispatchEvent(this[Event$g.ON_CLOSE]);
	  }
	  /*
	   * update the event listeners with a value as parameter
	   */
	  ;

	  _proto.update = function update(value) {
	    if (value) {
	      this.searchInput.value = value;

	      this._populateSelect();

	      this.container.dispatchEvent(this[Event$g.ON_UPDATE]);
	    }
	  }
	  /**
	   * remove all event listeners.
	   */
	  ;

	  _proto.remove = function remove() {
	    this.searchInput.removeEventListener('input', this._validate);
	    this.target.removeEventListener('click', this._selectResult);
	    document.removeEventListener('keydown', this._selectResultKey);
	    document.removeEventListener('click', this._closeSearchList); // remove this autocomplete reference from array of instances

	    var index = SEARCH_INSTANCES.indexOf(this);
	    SEARCH_INSTANCES.splice(index, 1);
	    this.container.dispatchEvent(this[Event$g.ON_REMOVE]);
	  }
	  /*
	   * close suggested list.
	   */
	  ;

	  _proto._closeSearchList = function _closeSearchList(e) {
	    if (e.target !== this.searchInput && e.target !== this.searchResultsContainer) {
	      var _target = this.target;

	      _target.classList.remove('active');

	      this.searchInput.setAttribute('aria-expanded', 'false');
	    }
	  }
	  /*
	   * get the data from URL
	   */
	  ;

	  _proto._getData = function _getData(url) {
	    return fetch(url).then(function (response) {
	      return response.json();
	    });
	  }
	  /*
	   * filter the data.
	   */
	  ;

	  _proto._filterData = function _filterData(data) {
	    var _this = this;

	    return data.filter(function (item) {
	      if (item.title.indexOf(_this.searchInput.value) !== -1) {
	        return item;
	      }

	      return false;
	    });
	  }
	  /*
	   * fetch the data to li tag.
	   */
	  ;

	  _proto._fetchData = function _fetchData(data) {
	    var _this2 = this;

	    var searchData = data.slice(0, Config.DEFAULT_SEARCH_RESULT);
	    var targetHtmlContainer = '';
	    searchData.forEach(function (item) {
	      _this2.target.classList.add('active');

	      _this2.searchInput.setAttribute('aria-expanded', 'true');

	      _this2.container.querySelector('.result-status').innerText = _this2.customMessage;
	      targetHtmlContainer += '<li class="result"  role="listitem" tabindex="1" >' + item.title + '</li>';
	    });
	    this.target.innerHTML = targetHtmlContainer;
	  }
	  /*
	  * populates the selected matching values
	  */
	  ;

	  _proto._populateSelect = function _populateSelect() {
	    var _this3 = this;

	    var filteredSearchData = this.suggestedData;

	    if (typeof this.suggestedData === 'string') {
	      this._getData(this.suggestedData).then(function (filteredSearchData) {
	        if (_this3.filter === 'true') {
	          filteredSearchData = _this3._filterData(filteredSearchData);
	        }

	        _this3._fetchData(filteredSearchData);
	      });
	    } else {
	      if (this.filter === 'true') {
	        filteredSearchData = this._filterData(filteredSearchData);
	      }

	      this._fetchData(filteredSearchData);
	    }
	  }
	  /*
	   * after entering the data,populating the value through populateSelect function
	   * @param {object} e - present event
	   */
	  ;

	  _proto._validate = function _validate(e) {
	    if (this.searchInput.value === '') {
	      var _target = this.target;
	      _target.innerHTML = '';

	      _target.classList.remove('active');

	      this.searchInput.setAttribute('aria-expanded', 'false');
	      this.container.querySelector('.result-status').innerText = '';
	    } else {
	      this._populateSelect(e);
	    }
	  }
	  /*
	   * fetch the suggested data from drop down to the autocomplete
	   * @param {object} e - present event
	   */
	  ;

	  _proto._selectResult = function _selectResult(e) {
	    var _target = this.target;

	    _target.classList.remove('selected');

	    _target.classList.add('selected');

	    this.searchInput.value = e.target.innerHTML;
	    this.searchInput.focus();

	    _target.classList.remove('active');

	    this.searchInput.setAttribute('aria-expanded', 'false');
	    e.stopPropagation();
	  }
	  /*
	   * adds the user selected dropdown value to input.
	   * @param {object} e - present event
	   * @returns {Boolean} - returns boolean state
	   */
	  ;

	  _proto._selectResultKey = function _selectResultKey(e) {
	    if (this.target.classList.contains('active')) {
	      var _target = this.target;

	      if (e.keyCode === Util$1.keyCodes.ENTER) {
	        var selected = _target.querySelector('.selected');

	        if (selected) {
	          this.searchInput.value = selected.innerHTML;

	          _target.classList.remove('active');

	          this.searchInput.setAttribute('aria-expanded', 'false');
	          e.preventDefault();
	        }
	      }

	      if (e.keyCode === Util$1.keyCodes.ARROW_UP) {
	        var _selected = _target.querySelector('.selected');

	        if (_target.querySelector('li.selected')) {
	          var prevSibling = null;
	          prevSibling = _selected.previousElementSibling;

	          if (prevSibling) {
	            _target.querySelectorAll('li').forEach(function (item) {
	              item.classList.remove('selected');
	            });

	            prevSibling.classList.add('selected');
	            prevSibling.focus();
	          }

	          if (_selected === _target.querySelector('li:first-of-type')) {
	            var firstLiElement = _target.querySelector('li:first-of-type');

	            firstLiElement.classList.remove('selected');
	          }
	        }
	      }

	      if (e.keyCode === Util$1.keyCodes.ARROW_DOWN) {
	        if (_target.querySelector('li') && !_target.querySelector('li.selected')) {
	          var _firstLiElement = _target.querySelector('li:first-of-type');

	          _firstLiElement.classList.add('selected');

	          _firstLiElement.focus();

	          _firstLiElement.setAttribute('aria-selected', 'true');
	        } else {
	          var nextSibling = null;

	          if (_target.querySelector('li')) {
	            var _selected2 = _target.querySelector('.selected');

	            nextSibling = _selected2.nextElementSibling;

	            if (nextSibling) {
	              _target.querySelectorAll('li').forEach(function (item) {
	                item.classList.remove('selected');
	              });

	              nextSibling.classList.add('selected');
	              nextSibling.setAttribute('aria-selected', 'true');
	              nextSibling.focus();
	            }
	          }
	        }
	      }

	      if (e.keyCode === Util$1.keyCodes.ESC || e.keyCode === Util$1.keyCodes.TAB) {
	        this.searchInput.focus();

	        _target.classList.remove('active');

	        this.searchInput.setAttribute('aria-expanded', 'false');
	      }
	    }
	  };

	  return AutoComplete;
	}();

	(function () {
	  Util$1.initializeComponent('[data-mount="autocomplete"]', function (node) {
	    new AutoComplete({
	      target: node
	    });
	  });
	})();

	var controlElements = []; // YIQ Threshold for color changes

	var yiqContrastedThreshold = 128;
	var Event$h = {
	  ON_CHANGE: 'onChange',
	  ON_REMOVE: 'onRemove',
	  CHANGE: 'change'
	};
	var Selector$h = {
	  COLOR_PICKER_DOT: '.color-picker-dot'
	};
	var Attributes = {
	  DATA_CONTROLS: 'data-controls',
	  IMAGE: 'data-color-picker-image',
	  ID: 'id',
	  SRC: 'src'
	};
	var ClassName$g = {
	  COLOR_LIGHT: 'color-picker-dot-light'
	};
	/**
	 * Perform the calulations to figure out color of elements
	 */

	function _initializeColor() {
	  var id = this.target.getAttribute(Attributes.ID);
	  var label = this.target.parentNode.querySelector("label[for=\"" + id + "\"]");
	  var backgroundColor = label.querySelector(Selector$h.COLOR_PICKER_DOT).style.backgroundColor;
	  var rgbObject = Util$1.getRGB(backgroundColor);
	  var darkColor = {
	    r: 0,
	    g: 0,
	    b: 0
	  };
	  var darkYiq = Util$1.getYiq(darkColor);
	  var bgYiq = Util$1.getYiq(rgbObject);

	  if (Math.floor(Math.abs(bgYiq - darkYiq) > yiqContrastedThreshold)) {
	    label.classList.add(ClassName$g.COLOR_LIGHT);
	  }
	}
	/**
	 * Switches the image based on the color selected
	 * @param {node} node an html element to perform the action on
	 */


	function _initializeClickHandler() {
	  var _this = this;

	  // Iterate through our controls, adding an event listener to change the image
	  this.eventHandler = this.target.addEventListener(Event$h.CHANGE, function (e) {
	    return _this._controlListener(e, _this.containerTarget);
	  });
	}

	var ColorPickerControl = /*#__PURE__*/function () {
	  function ColorPickerControl(opts) {
	    this.target = opts.controlTarget;
	    this.containerTarget = opts.containerTarget;

	    _initializeColor.bind(this)();

	    _initializeClickHandler.bind(this)();

	    controlElements.push(this);
	  }
	  /**
	   * Event handler for change events
	   * @param {evemt} e Event
	   * @param {string} imageContainer a reference to the image container
	   */


	  var _proto = ColorPickerControl.prototype;

	  _proto._controlListener = function _controlListener(e, imageContainer) {
	    if (imageContainer) {
	      var nodeName = imageContainer.nodeName.toLowerCase();
	      var imageUrl = e.target.getAttribute(Attributes.IMAGE);
	      var event = new CustomEvent(Event$h.ON_CHANGE, {
	        element: imageContainer.getAttribute(Attributes.ID),
	        imageUrl: imageUrl
	      });

	      if (imageUrl) {
	        // Figure out whether its an image element or not
	        if (nodeName === 'img') {
	          imageContainer.setAttribute(Attributes.SRC, imageUrl);
	        } else {
	          imageContainer.style.backgroundImage = "url(" + imageUrl + ")";
	        }

	        imageContainer.dispatchEvent(event);
	      }
	    }
	  }
	  /**
	   * Get an array of color picker radio instances.
	   * @returns {Object[]} color picker radio instances.
	   */
	  ;

	  ColorPickerControl.getInstances = function getInstances() {
	    return controlElements;
	  }
	  /**
	  * Remove the imageSwitcher event handlers.
	  */
	  ;

	  _proto.remove = function remove() {
	    if (this.eventHandler) {
	      this.eventHandler.removeEventListener(Event$h.CHANGE, this._controlListener);
	      this.target.dispatchEvent(Event$h.ON_REMOVE);
	    }
	  };

	  return ColorPickerControl;
	}();

	var Selector$i = {
	  CONTROL: 'input',
	  CONTAINER: '[data-mount="color-picker"]',
	  CHECKED: ':checked'
	};
	var Attributes$1 = {
	  DATA_CONTROLS: 'data-controls',
	  IMAGE: 'data-color-picker-image'
	};
	var colorPickers = [];

	function _initializeImageSrc() {
	  // Find all the fieldsets that have a target
	  var currentFieldSet = this.target;
	  var nodeName = this.containerTarget.nodeName.toLowerCase();
	  var defaultElement = currentFieldSet.querySelector(Selector$i.CHECKED); // Set the default selected image

	  if (defaultElement) {
	    var imageUrl = defaultElement.getAttribute(Attributes$1.IMAGE);

	    if (imageUrl) {
	      if (nodeName === 'img') {
	        this.containerTarget.setAttribute('src', imageUrl);
	      } else {
	        this.containerTarget.style.backgroundImage = "url(" + imageUrl + ")";
	      }
	    }
	  }
	}

	var ColorPicker = /*#__PURE__*/function () {
	  function ColorPicker(opts) {
	    var _this = this;

	    this.target = opts.target;
	    var controlElement = this.target.getAttribute(Attributes$1.DATA_CONTROLS);

	    if (controlElement) {
	      this.containerTarget = document.querySelector("#" + controlElement);

	      _initializeImageSrc.bind(this)();
	    }

	    this.controls = [];
	    var controls = this.target.querySelectorAll(Selector$i.CONTROL); // Iterate through our controls, adding an event listener to change the image

	    controls.forEach(function (control) {
	      return _this.controls.push(new ColorPickerControl(Object.assign({
	        controlTarget: control,
	        containerTarget: _this.containerTarget
	      }, opts)));
	    });
	    colorPickers.push(this);
	  }
	  /**
	   * Get an array of color picker radio instances.
	   * @returns {Object[]} color picker radio instances.
	   */


	  ColorPicker.getInstances = function getInstances() {
	    return colorPickers;
	  }
	  /**
	  * Remove the imageSwitcher event handlers.
	  */
	  ;

	  ColorPicker.remove = function remove() {
	    // Call remove on each of the child elements
	    colorPickers.forEach(function (colorPicker) {
	      return colorPicker.controls.forEach(function (element) {
	        element.remove();
	      });
	    });
	  };

	  return ColorPicker;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$i.CONTAINER, function (node) {
	    return new ColorPicker({
	      target: node
	    });
	  });
	})();

	var Selector$j = {
	  DATA_TOGGLE: '[data-toggle="contentSwap"]'
	};
	var Event$i = {
	  ON_SWAP: 'onSwap',
	  ON_HIDE: 'onHide',
	  ON_SHOW: 'onShow',
	  ON_UPDATE: 'onUpdate',
	  ON_REMOVE: 'onRemove'
	};
	var contentSwapInstances = [];

	function _getTargetList() {
	  // Reads selector from data-target attribute
	  var selector = Util$1.getSelectorFromElement(this.swapTrigger);
	  return [].slice.call(document.querySelectorAll(selector));
	}

	var ContentSwap = /*#__PURE__*/function () {
	  function ContentSwap(opts) {
	    this.swapTrigger = opts.swapTrigger;
	    this.targetList = _getTargetList.bind(this)(); // Add event handlers

	    this.events = [{
	      el: this.swapTrigger,
	      type: 'click',
	      handler: this.swapContent.bind(this)
	    }];
	    Util$1.addEvents(this.events); // Create custom events.

	    this[Event$i.ON_SWAP] = new CustomEvent(Event$i.ON_SWAP, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$i.ON_HIDE] = new CustomEvent(Event$i.ON_HIDE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$i.ON_SHOW] = new CustomEvent(Event$i.ON_SHOW, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$i.ON_UPDATE] = new CustomEvent(Event$i.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$i.ON_REMOVE] = new CustomEvent(Event$i.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    }); // push to instances list

	    contentSwapInstances.push(this);
	  }

	  var _proto = ContentSwap.prototype;

	  _proto.update = function update() {
	    this.targetList = _getTargetList.bind(this)();
	    this.swapTrigger.dispatchEvent(this[Event$i.ON_UPDATE]);
	  };

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events);
	    var index = contentSwapInstances.indexOf(this);
	    contentSwapInstances.splice(index, 1);
	    this.swapTrigger.dispatchEvent(this[Event$i.ON_REMOVE]);
	  };

	  _proto.hide = function hide(element) {
	    element.setAttribute('hidden', 'true');
	    element.dispatchEvent(this[Event$i.ON_HIDE]);
	  };

	  _proto.show = function show(element) {
	    element.removeAttribute('hidden');
	    element.dispatchEvent(this[Event$i.ON_SHOW]);
	  };

	  _proto.swapContent = function swapContent() {
	    var _this = this;

	    this.swapTrigger.dispatchEvent(this[Event$i.ON_SWAP]);
	    this.targetList.forEach(function (element) {
	      if (element.hasAttribute('hidden')) {
	        // unhides the hidden
	        _this.show(element);
	      } else {
	        // hides the unhidden
	        _this.hide(element);
	      }
	    });
	  };

	  ContentSwap.getInstances = function getInstances() {
	    return contentSwapInstances;
	  };

	  return ContentSwap;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$j.DATA_TOGGLE, function (node) {
	    new ContentSwap({
	      swapTrigger: node
	    });
	  });
	})();

	var Selector$k = {
	  ITEM: '[data-sticky]',
	  ITEM_STICK_ATTRIBUTE: 'data-sticky',
	  ITEM_STUCK_CLASS: 'stuck'
	};
	var Attribute$2 = {
	  INITIALIZED: 'data-sticky-initialized'
	};
	var Direction$2 = {
	  TOP: 'top',
	  BOTTOM: 'bottom'
	};
	var stickies = [];
	/**
	 * Private functions.
	 */

	/**
	 * Handle intersection observer
	 * @param {Boolean} stuck - Resolution on whether the sticky should be stuck or not
	 */

	function _onStickyChange(stuck) {
	  this.sticky.classList.toggle(Selector$k.ITEM_STUCK_CLASS, stuck);
	}
	/**
	 * Class representing a Sticky Element.
	 */


	var Sticky = /*#__PURE__*/function () {
	  /**
	   * Create the Sticky Element
	   * @param {Object} opts - The Sticky Element options.
	   * @param {Node} opts.el - The Sticky Element DOM node.
	   * @param {boolean} opts.enableObserver - enable the observer on initialization (defaults to true)
	   */
	  function Sticky(opts) {
	    var _this = this;

	    this.sticky = opts.el;
	    this.stickyDir = opts.dir ? opts.dir : this.sticky.getAttribute(Selector$k.ITEM_STICK_ATTRIBUTE); // Set the anchor direction

	    switch (this.stickyDir) {
	      case Direction$2.TOP:
	        this.sticky.style.top = '0';
	        break;

	      case Direction$2.BOTTOM:
	        this.sticky.style.bottom = '0';
	        break;
	    }

	    this.sentinel = document.createElement('div');
	    this.enableObserver = typeof opts.enableObserver === 'undefined' ? true : opts.enableObserver;
	    this.sticky.setAttribute(Attribute$2.INITIALIZED, 'true');
	    this.observer = new IntersectionObserver(function (entries) {
	      // fire onStickyChange if not in viewport
	      if (_this.enableObserver) {
	        var isStuck = _this.doesSentinelExceedBoundary() && !entries[0].isIntersecting;

	        _onStickyChange.bind(_this)(isStuck);
	      }
	    });
	    this.sticky.insertAdjacentElement('beforebegin', this.sentinel);
	    this.observer.observe(this.sentinel);

	    if (this.doesSentinelExceedBoundary() && this.enableObserver) {
	      this.sticky.classList.add(Selector$k.ITEM_STUCK_CLASS);
	    }

	    stickies.push(this);
	  }
	  /**
	   * Check if the sentinel Exceeds the boundary
	   * @returns {boolean} State of sentinel boundary
	   */


	  var _proto = Sticky.prototype;

	  _proto.doesSentinelExceedBoundary = function doesSentinelExceedBoundary() {
	    var sentinelRect = this.sentinel.getBoundingClientRect();
	    var sentinelTop = sentinelRect.top;
	    var sentinelBottom = sentinelRect.bottom;

	    if (this.stickyDir === Direction$2.TOP && sentinelTop < 0) {
	      return true;
	    }

	    if (this.stickyDir === Direction$2.BOTTOM && sentinelBottom > window.innerHeight) {
	      return true;
	    }

	    return false;
	  }
	  /**
	   * Set the status of the observer (to enable or disable the observer)
	   * @param {boolean} status The status to set
	   */
	  ;

	  _proto.setObserverStatus = function setObserverStatus(status) {
	    if (this.doesSentinelExceedBoundary()) {
	      this.sticky.classList.toggle(Selector$k.ITEM_STUCK_CLASS, status);
	    }

	    this.enableObserver = status;
	  }
	  /**
	   * Remove the sticky.
	   */
	  ;

	  _proto.remove = function remove() {
	    // remove the attribute from the element
	    this.sticky.removeAttribute(Attribute$2.INITIALIZED); // remove this sticky reference from array of instances

	    var index = stickies.indexOf(this);
	    stickies.splice(index, 1);
	  }
	  /**
	   * Get an array of sticky instances.
	   * @returns {Object[]} Array of sticky instances.
	   */
	  ;

	  Sticky.getInstances = function getInstances() {
	    return stickies;
	  };

	  return Sticky;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$k.ITEM, function (node) {
	    new Sticky({
	      el: node
	    });
	  });
	})();

	var Selector$l = {
	  DATA_MOUNT: '[data-mount="character-count"]'
	};
	var Event$j = {
	  ON_UPDATE: 'onUpdate',
	  ON_REMOVE: 'onRemove'
	};
	var characterCountInstances = [];
	var UPDATE_RATE_LIMIT = 400; // rate limit in ms for screen reader announcement

	/**
	 * Gets the target form element to monitor
	 * @returns {Node} The target element
	 */

	function _getTarget() {
	  // Reads selector from data-target attribute
	  var selector = Util$1.getSelectorFromElement(this.statusMessage); // There should only be one element targeted, gets the first match

	  return document.querySelector(selector);
	}
	/**
	 * Updates the textContent of a node with the most up to date character count status message
	 * @param {Node} node The node to update the textContent of
	 */


	function _updateStatusMessageText(node) {
	  var msgTemplate = this.isMaxInputReached() ? this.maxMessageTemplate : this.statusMessageTemplate;
	  var inputLength = this.getUserInputLength();
	  node.textContent = Util$1.interpolateString(msgTemplate, {
	    remaining: this.inputMaxLength - inputLength,
	    entered: inputLength,
	    max: this.inputMaxLength
	  });
	}
	/**
	 * Updates the visual status message only, immediately
	 */


	function _updateVisualStatusMessage() {
	  _updateStatusMessageText.bind(this)(this.statusMessageVisual);
	}
	/**
	 * Updates the screen reader status message only, immediately
	 */


	function _updateScreenReaderStatusMessage() {
	  _updateStatusMessageText.bind(this)(this.statusMessageSR);
	}
	/**
	 * Computes whether key typed is printable
	 * @param {KeyboardEvent} keyboardEventKey
	 * @returns {Boolean} Whether the key entered is printable
	 */


	function _isPrintable(keyboardEventKey) {
	  return /^.$/.test(keyboardEventKey);
	}
	/**
	 * Causes the screen reader status message to narrate
	 */


	function _narrateStatusMessage() {
	  var _this = this;

	  this.statusMessageSR.textContent = '';
	  setTimeout(function () {
	    _updateScreenReaderStatusMessage.bind(_this)();
	  }, 200);
	}
	/**
	 * Narrates the screen reader status message if the given KeyboardEvent represents a printable character
	 * @param {KeyboardEvent} keyboardEvent
	 */


	function _narrateIfMaxInputAndPrintableKey(keyboardEvent) {
	  if (this.isMaxInputReached() && _isPrintable(keyboardEvent.key)) {
	    _narrateStatusMessage.bind(this)();
	  }
	}

	var CharacterCount = /*#__PURE__*/function () {
	  /**
	   * Creates a CharacterCount object
	   * @param {Object} opts The CharacterCount options
	   * @param {Node} opts.statusMessage The node that wraps the status message elements and stores configuration information
	   */
	  function CharacterCount(opts) {
	    var _this2 = this;

	    this.statusMessage = opts.statusMessage;
	    this.statusMessageSR = this.statusMessage.querySelector('.sr-only');
	    this.statusMessageVisual = this.statusMessage.querySelector(':not(.sr-only)');
	    this.target = _getTarget.bind(this)();
	    this.inputMaxLength = Number(this.target.getAttribute('maxLength'));
	    this.statusMessageTemplate = this.statusMessage.getAttribute('data-status-msg-template');
	    this.maxMessageTemplate = this.statusMessage.getAttribute('data-max-msg-template');
	    this.debouncedSRUpdate = debounce(UPDATE_RATE_LIMIT, function () {
	      _updateScreenReaderStatusMessage.bind(_this2)();
	    });
	    this.srLowCharWarnLvl = this.statusMessage.getAttribute('data-sr-low-char-warning-lvl');
	    this.userHasBeenWarned = false;
	    this.ariaLiveWasReset = false; // Add event handlers

	    this.events = [{
	      el: this.target,
	      type: 'input',
	      handler: this.updateStatusMessage.bind(this)
	    }, {
	      el: this.target,
	      type: 'keydown',
	      handler: _narrateIfMaxInputAndPrintableKey.bind(this)
	    }, {
	      el: this.target,
	      type: 'focus',
	      handler: _narrateStatusMessage.bind(this)
	    }];
	    Util$1.addEvents(this.events); // Create custom events.

	    this[Event$j.ON_UPDATE] = new CustomEvent(Event$j.ON_UPDATE, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$j.ON_REMOVE] = new CustomEvent(Event$j.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    }); // Initialize visual message

	    _updateVisualStatusMessage.bind(this)(); // push to instances list


	    characterCountInstances.push(this);
	  }
	  /**
	   * Get the length of the current value of the monitored form element
	   * @returns {Number} The length of the value
	   */


	  var _proto = CharacterCount.prototype;

	  _proto.getUserInputLength = function getUserInputLength() {
	    return this.target.value.length;
	  }
	  /**
	   * Determine whether the max input length has been reached
	   * @returns {Boolean} Whether the max input length has been reached
	   */
	  ;

	  _proto.isMaxInputReached = function isMaxInputReached() {
	    return this.getUserInputLength() === this.inputMaxLength;
	  }
	  /**
	   * Determine whether the low character warning level has been met
	   * @returns {Boolean} Whether the low character warning level has been met
	   */
	  ;

	  _proto.isInputAtOrBelowLowCharWarnLvl = function isInputAtOrBelowLowCharWarnLvl() {
	    return this.inputMaxLength - this.getUserInputLength() <= this.srLowCharWarnLvl;
	  }
	  /**
	   * Updates both status messages. The visual one immediatey, the screen reader in a debounced manner.
	   */
	  ;

	  _proto.updateStatusMessage = function updateStatusMessage() {
	    this.debouncedSRUpdate();

	    _updateVisualStatusMessage.bind(this)();

	    if (!this.isMaxInputReached() && this.userHasBeenWarned && !this.ariaLiveWasReset) {
	      this.statusMessageSR.setAttribute('aria-live', 'polite');
	    }

	    if (this.isMaxInputReached() || !this.userHasBeenWarned && this.isInputAtOrBelowLowCharWarnLvl()) {
	      this.debouncedSRUpdate.cancel();
	      this.statusMessageSR.setAttribute('aria-live', 'assertive');

	      _updateScreenReaderStatusMessage.bind(this)();
	    }
	  }
	  /**
	   * Updates the object by re-reading all configuration options stored in the DOM
	   */
	  ;

	  _proto.update = function update() {
	    var _this3 = this;

	    this.target = _getTarget.bind(this)();
	    this.inputMaxLength = Number(this.target.getAttribute('maxLength'));
	    this.statusMessageTemplate = this.statusMessage.getAttribute('data-status-msg-template');
	    this.maxMessageTemplate = this.statusMessage.getAttribute('data-max-msg-template');
	    this.debouncedSRUpdate = debounce(UPDATE_RATE_LIMIT, function () {
	      _updateScreenReaderStatusMessage.bind(_this3)();
	    });
	    this.srLowCharWarnLvl = this.statusMessage.getAttribute('data-sr-low-char-warning-lvl');
	    this.userHasBeenWarned = false;
	    this.ariaLiveWasReset = false;
	    this.statusMessage.dispatchEvent(this[Event$j.ON_UPDATE]);
	  }
	  /**
	   * Removes the CharacterCount instance
	   */
	  ;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events);
	    var index = characterCountInstances.indexOf(this);
	    characterCountInstances.splice(index, 1);
	    this.statusMessage.dispatchEvent(this[Event$j.ON_REMOVE]);
	  }
	  /**
	   * Gets the array of CharacterCount instances
	   * @returns {Object[]} Array of CharacterCount instances
	   */
	  ;

	  CharacterCount.getInstances = function getInstances() {
	    return characterCountInstances;
	  };

	  return CharacterCount;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$l.DATA_MOUNT, function (node) {
	    new CharacterCount({
	      statusMessage: node
	    });
	  });
	})();

	var backToTopInstances = [];
	var Selector$m = {
	  BACK_TO_TOP: '[data-mount="back-to-top"]'
	};
	var Event$k = {
	  SCROLL: 'scroll',
	  RESIZE: 'resize',
	  ON_STATIC: 'onStatic',
	  ON_STICKY: 'onSticky',
	  ON_REMOVE: 'onRemove'
	};
	var Attributes$2 = {
	  TABINDEX: 'tabindex'
	};
	/**
	 * Switch the back to top element between static and sticky
	 */

	function _scrollListener() {
	  // measure window height once because it asking for window dimensions will trigger a reflow
	  var winHeight = window.innerHeight;
	  var scrollY = window.scrollY || window.pageYOffset;

	  if (scrollY > winHeight * 2) {
	    this.stickyElement.setObserverStatus(true);
	  } else {
	    this.stickyElement.setObserverStatus(false);
	  }
	}

	var BackToTop = /*#__PURE__*/function () {
	  function BackToTop(opts) {
	    this.el = opts.el;
	    this.setTabindex(); // Create custom events

	    this[Event$k.ON_STATIC] = new CustomEvent(Event$k.ON_STATIC, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$k.ON_STICKY] = new CustomEvent(Event$k.ON_STICKY, {
	      bubbles: true,
	      cancelable: true
	    });
	    this[Event$k.ON_REMOVE] = new CustomEvent(Event$k.ON_REMOVE, {
	      bubbles: true,
	      cancelable: true
	    });
	    backToTopInstances.push(this);
	    this.stickyElement = new Sticky({
	      el: this.el,
	      dir: Direction$2.BOTTOM
	    }); // Do the initial firing of the listener to set the state

	    _scrollListener.call(this); // attach event listeners


	    this.events = [{
	      el: document,
	      type: Event$k.SCROLL,
	      handler: throttle(200, _scrollListener.bind(this)),
	      options: {
	        passive: true
	      }
	    }];
	    Util$1.addEvents(this.events);
	  }
	  /**
	   * Check if the element needs a tabindex and set it
	   */


	  var _proto = BackToTop.prototype;

	  _proto.setTabindex = function setTabindex() {
	    var link = this.el.querySelector('a');
	    var href = link.getAttribute('href');
	    var targetElement = document.querySelector(href);
	    var isElementFound = document.querySelector(href) !== null;

	    if (isElementFound) {
	      // Only do something if the element is not tabbable
	      if (!Util$1.isElementTabbable(targetElement)) {
	        var tabindex = targetElement.getAttribute(Attributes$2.TABINDEX); // If we don't have a tabindex

	        if (tabindex === null) {
	          // Set the tabindex of the element to -1
	          targetElement.setAttribute(Attributes$2.TABINDEX, '-1');
	        }
	      }
	    }
	  }
	  /**
	   * Remove the event listener from the back to top element
	   */
	  ;

	  _proto.remove = function remove() {
	    Util$1.removeEvents(this.events);
	    this.stickyElement.remove();
	    this.el.dispatchEvent(this[Event$k.ON_REMOVE]); // remove this carousel reference from array of instances

	    var index = backToTopInstances.indexOf(this);
	    backToTopInstances.splice(index, 1);
	  }
	  /**
	   * Get a back to top instances.
	   * @returns {Object} A back to top instance
	   */
	  ;

	  BackToTop.getInstances = function getInstances() {
	    return backToTopInstances;
	  };

	  return BackToTop;
	}();

	(function () {
	  Util$1.initializeComponent(Selector$m.BACK_TO_TOP, function (node) {
	    var backToTop = new BackToTop({
	      el: node
	    });
	    return backToTop;
	  });
	})();

	(function () {
	  console.log('Moray');
	})();

	exports.Alert = Alert;
	exports.AutoComplete = AutoComplete;
	exports.BackToTop = BackToTop;
	exports.Button = Button;
	exports.Carousel = Carousel;
	exports.CharacterCount = CharacterCount;
	exports.ClickGroup = ClickGroup;
	exports.Collapse = Collapse;
	exports.CollapseControls = CollapseControls;
	exports.ColorPicker = ColorPicker;
	exports.ContentSwap = ContentSwap;
	exports.Debug = Debug;
	exports.Dropdown = Dropdown;
	exports.FormValidation = FormValidation;
	exports.Modal = Modal;
	exports.Popover = Popover$1;
	exports.Scrollspy = ScrollSpy;
	exports.ShowMoreShowLess = ShowMoreShowLess;
	exports.Sticky = Sticky;
	exports.Tab = Tab;
	exports.TabSlider = TabSlider;
	exports.Toast = Toast;
	exports.Tooltip = Tooltip$1;
	exports.Util = Util$1;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=bundle.js.map
