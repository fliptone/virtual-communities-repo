using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace VirtualCommunities
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddAuthentication(options =>
            //{
            //    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = MicrosoftAccountDefaults.AuthenticationScheme;
            //})
            //.AddCookie().AddMicrosoftAccount(microsoftOptions =>
            //{
            //    microsoftOptions.ClientId = "4a822f44-e8b7-4c0c-8af7-5e5bab327f34";
            //    microsoftOptions.ClientSecret = "~i_nZO6OJwj~V7Mp3O0HEz44rhj10ML_eq";
            //});

            services.AddRazorPages().AddRazorRuntimeCompilation().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Index", "/");
                options.Conventions.AddPageRoute("/Index", "/{locale}/store/workshops-training-and-events");
                options.Conventions.AddPageRoute("/Api", "/{locale}/store/workshops-training-and-events/api");
                options.Conventions.AddPageRoute("/Events", "/{locale}/store/workshops-training-and-events/all");
                options.Conventions.AddPageRoute("/Spotlight", "/{locale}/store/workshops-training-and-events/digital-passport");
                options.Conventions.AddPageRoute("/Spotlight", "/{locale}/store/workshops-training-and-events/professionals");
                options.Conventions.AddPageRoute("/Spotlight", "/{locale}/store/workshops-training-and-events/surface-duo");
                options.Conventions.AddPageRoute("/Shelby", "/{locale}/store/workshops-training-and-events/shelbycounty-schools");
                options.Conventions.AddPageRoute("/EventDetail", "/{locale}/store/workshops-training-and-events/{id}");
                options.Conventions.AddPageRoute("/Sweepstakes", "/{locale}/store/workshops-training-and-events/sweepstakes-rules");
                options.Conventions.AddPageRoute("/CodeCheck", "/{locale}/store/workshops-training-and-events/code-check");
            });

            services.AddMemoryCache();
            services.AddMvc();
            services.AddApplicationInsightsTelemetry(Configuration["APPINSIGHTS_INSTRUMENTATIONKEY"]);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //app.UseAuthentication();
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });

            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
            };
            app.UseCookiePolicy(cookiePolicyOptions);

        }
    }
}
